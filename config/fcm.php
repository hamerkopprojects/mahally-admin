<?php

return [
    'driver' => env('FCM_PROTOCOL', 'http'),
    'log_enabled' => false,

    'http' => [
        'server_key' => env('FCM_SERVER_KEY', 'AAAA1lVyd8A:APA91bHWQhT2kfYSen29ZVUI-hThzCobD_uzCvqUWtdaK9mWK7omQXHkQ6WWWLcH73LxxT_rtuwhw5xFMTOs9BgGZrPeL6UPUQqA8gZg7UoCiQpZ3uWoCkFefHnNkd5n-cut0X6sREAX'),
        'sender_id' => env('FCM_SENDER_ID', '920556566464'),
        'server_send_url' => 'https://fcm.googleapis.com/fcm/send',
        'server_group_url' => 'https://android.googleapis.com/gcm/notification',
        'timeout' => 30.0, // in second
    ],
];
