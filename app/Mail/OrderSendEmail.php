<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class OrderSendEmail extends Mailable {

    use Queueable,
        SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $name;
    public $order;
    public $ord_status;

    public function __construct($name, $subject, $order, $ord_status) {
        $this->name = $name;
        $this->order = $order;
        $this->subject = $subject;
        $this->ord_status = $ord_status;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build() {
        return $this->subject($this->subject)
                        // ->markdown('mail.order_send')
                        ->markdown('mail.order')
                        ->with('name', $this->name)
                        ->with('order', $this->order)
                        ->with('order_status', $this->ord_status);
    }

}
