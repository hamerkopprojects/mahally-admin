<?php

namespace App\Exports;


use App\Models\Order;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Events\BeforeExport;
use Maatwebsite\Excel\Events\AfterSheet;

class SettlementReportExport implements FromCollection,WithHeadings, WithMapping, ShouldAutoSize, WithEvents
{
    use Exportable;
    /**
    * @return \Illuminate\Support\Collection
    */
    public $orders;
 function __construct($orders) {
        $this->orders = $orders;
 }

    public function collection()
    {
        return collect($this->orders);
    }

    public function headings(): array {
        return [
           "Order ID",
           "Customer",
           "Sale Amount",
           "Commission",
           "Restaurant Revenue",
        ];
    }
    public function map($orders): array {
        return [
            $orders['ord_id'],
            $orders['cust_name'],
            $orders['grant_total'],
            $orders['commission'],
            $orders['revenue'],

        ];
    }

    public function registerEvents(): array
        {
        $styleArray = [
        'font' => [
        'family' => 'Calibri',
        'bold' => true,
        'size' => 12,
        ],
        'borders' => [
        'outline' => [
            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
            'color' => ['argb' => 'FFFF'],
        ],
       ],
     ];
        return [
            AfterSheet::class    => function(AfterSheet $event) use ($styleArray)
            {
             $event->getSheet()->getDelegate()->getStyle('A1:E1')->applyFromArray($styleArray);
            },
      ];
       }
}
