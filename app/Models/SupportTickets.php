<?php

namespace App\Models;

use App\Models\Customers;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SupportTickets extends Model
{
    use  SoftDeletes;
    /**
     * guarded variable
     *
     * @var array
     */
    protected $guarded = [];
    /**
     * $table variable
     *
     * @var string
     */

    protected $table = "support_ticket";

    public function lang()
    {
        return $this->hasMany(SupportTicketsLang::class, 'support_id');
    }
    public function customer()
    {
        return $this->belongsTo(Customers::class, 'submitted_by');
    }
    // public function owner()
    // {
    //     return $this->morphTo(__FUNCTION__, 'app_type', 'submitted_by');
    // }
}
