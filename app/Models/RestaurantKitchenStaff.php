<?php

namespace App\Models;

use Laravel\Passport\HasApiTokens;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;


class RestaurantKitchenStaff extends Authenticatable
{
    use  SoftDeletes, HasApiTokens, Notifiable;
    /**
     * guarded variable
     *
     * @var array
     */
    protected $guarded = [];
    /**
     * $table variable
     *
     * @var string
     */
    protected $hidden = [
        'password'
    ];

    protected $table = "kitchen_staff";


    public function roles()
    {
        return $this->hasMany(RestaurantStaffRoles::class, 'id', 'role_id');
    }
}
