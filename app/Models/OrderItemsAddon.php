<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OrderItemsAddon extends Model
{
    use  SoftDeletes;
    /**
     * guarded variable
     *
     * @var array
     */
    protected $guarded = [];
    /**
     * $table variable
     *
     * @var string
     */
    protected  $table = "order_addons";

    public function resturant_addons()
    {
        return $this->belongsTo(RestaurantAddons::class, 'addon_id');
    }
    public function addons()
    {
        return $this->belongsToMany(AutoSuggest::class, 'addon_id');
    }
}
