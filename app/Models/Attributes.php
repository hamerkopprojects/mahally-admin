<?php

namespace App\Models;

use App\Models\AttributesLang;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Attributes extends Model
{
    use  SoftDeletes;
    /**
     * guarded variable
     *
     * @var array
     */
    protected $guarded = [];
    /**
     * $table variable
     *
     * @var string
     */

    protected $table = "attributes";

    public function lang()
    {
        return $this->hasMany(AttributesLang::class, 'attribute_id');
    }
    public function attr_value()
    {
        return $this->hasOne(AttributesValue::class, 'attribute_id');
    }
}
