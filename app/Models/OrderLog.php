<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OrderLog extends Model
{
    use SoftDeletes;

    protected $table = 'order_log';

    protected $guarded = [];

    public function user()
    {
        return $this->morphTo(__FUNCTION__, 'done_by', 'submitted_by_id'); 
    }
}
