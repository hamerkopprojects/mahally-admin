<?php

namespace App\Models;

use App\Models\MenuItems;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class FreeMeals extends Model
{
    use  SoftDeletes;
    /**
     * guarded variable
     *
     * @var array
     */
    protected $guarded = [];
    /**
     * $table variable
     *
     * @var string
     */

    protected $table = "free_meal_loyalty";

    public function lang()
    {
        return $this->hasMany(FreeMealsLang::class, 'free_meal_id');
    }
    public function item()
    {
        return $this->belongsTo(MenuItems::class,'menu_item_id');
    }

    public function retaurant()
    {
        return $this->belongsTo(Restaurant::class,'restuarants_id');
    }
}
