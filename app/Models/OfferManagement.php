<?php

namespace App\Models;

use App\Models\Restaurant;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OfferManagement extends Model
{
    use  SoftDeletes;
    /**
     * guarded variable
     *
     * @var array
     */
    protected $guarded = [];
    /**
     * $table variable
     *
     * @var string
     */

    protected $table = "offer_management";

    public function lang()
    {
        return $this->hasMany(OfferManagementLang::class, 'offer_id');
    }
    public function restaurent()
    {
        return $this->belongsTo(Restaurant::class, 'restuarants_id');

    }
    public function items()
    {
        return $this->belongsTo(MenuItems::class, 'menu_item_id');
        
    }
}
