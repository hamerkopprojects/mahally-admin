<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Order extends Model
{
    use  SoftDeletes;
    /**
     * guarded variable
     *
     * @var array
     */
    protected $guarded = [];
    /**
     * $table variable
     *
     * @var string
     */
    protected  $table = "orders";

    public function order_item()
    {
        return $this->hasMany(OrderItems::class, 'order_id');
    }
    public function order_addons()
    {
        return $this->belongsToMany(RestaurantAddons::class, 'order_addons', 'order_id', 'addon_id');
    }
    public function customer()
    {
        return $this->hasMany(Customers::class, 'id', 'customer_id');
    }
    public function menu_item()
    {
        return $this->belongsToMany(MenuItems::class, 'order_items', 'order_id', 'item_id');
    }
    public function restaurant()
    {
        return $this->belongsTo(Restaurant::class, 'restaurant_id');
    }
    public function order_restaurant()
    {
        return $this->belongsTo(Restaurant::class, 'restaurant_id')->where('deleted_at', NULL);
    }
}
