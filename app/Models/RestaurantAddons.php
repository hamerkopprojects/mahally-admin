<?php

namespace App\Models;

use App\Models\AutoSuggest;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RestaurantAddons extends Model
{
    use  SoftDeletes;
    /**
     * guarded variable
     *
     * @var array
     */
    protected $guarded = [];
    /**
     * $table variable
     *
     * @var string
     */

    protected $table = "restaurant_addons";

    public function lang()
    {
        return $this->hasMany(RestaurantAddonsLang::class, 'addon_id');
    }
    // public function images()
    // {
    //     return $this->belongsTo(AutoSuggest::class, 'autosuggest_id');
    // }
}
