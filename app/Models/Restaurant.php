<?php

namespace App\Models;

use App\Models\Reviews;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Restaurant extends Model
{
    use  SoftDeletes;
    /**
     * guarded variable
     *
     * @var array
     */
    protected $guarded = [];
    /**
     * $table variable
     *
     * @var string
     */

    protected $table = "restuarants";

    // protected $dates = [
    //     'opening_time' => 'datetime:HH:mm',
    //     'closing_time' => 'datetime:HH:mm',
    // ];

    protected $casts = [
        'opening_time' => 'date:hh:mm',
        'closing_time' => 'date:hh:mm'
    ];
    public function lang()
    {
        return $this->hasMany(RestaurantLang::class, 'restuarants_id');
    }
    public function restaurant_type()
    {
        return $this->hasMany(RestaurantsRestaurantType::class, 'restuarants_id');
    }
    public function contact()
    {
        return $this->hasMany(RestaurantsContactInfo::class, 'restuarants_id');
    }
    public function city()
    {
        return $this->hasMany(AutoSuggestLang::class, 'autosuggest_id', 'city_id');
    }
    public function restaurant_images()
    {
        return $this->hasMany(RestaurantsImages::class, 'restuarants_id');
    }
    public function documents()
    {
        return $this->hasMany(RestaurantsDocuments::class, 'restuarants_id');
    }
    public function facilities()
    {
        return $this->hasMany(RestaurantsFacilities::class, 'restuarants_id');
    }
    public function terms()
    {
        return $this->hasMany(RestaurantsTerms::class, 'restuarants_id');
    }

    public function type()
    {
        return $this->belongsToMany(AutoSuggest::class, 'restuarants_restuarant_type', 'restuarants_id', 'restuarant_type_id');
    }
    public function restaurent_terms()
    {
        return $this->belongsToMany(AutoSuggest::class, 'restuarants_terms', 'restuarants_id', 'terms_id');
    }
    public function restaurent_facilities()
    {
        return $this->belongsToMany(AutoSuggest::class, 'restuarants_facilities', 'restuarants_id', 'facilities_id');
    }
    public function offers()
    {
        return $this->hasMany(OfferManagement::class, 'restuarants_id');
    }
    public function menucategory()
    {
        return $this->hasmany(MenuCategory::class, 'restuarants_id');
    }
    public function menuitems()
    {
        return $this->hasmany(MenuItems::class, 'restuarants_id');
    }
    public function rating()
    {
        return $this->hasOne(Reviews::class, 'restaurant_id')
            ->groupBy('restaurant_id')
            ->selectRaw('ROUND(AVG(rating),1) as value, COUNT(DISTINCT customer_id) as count, restaurant_id')
            ->withDefault(['value' => 0, 'count' => 0]);
    }
    public function commission_category()
    {
        return $this->belongsTo(CommissionCategory::class, 'commission_category_id');
    }
}
