<?php

namespace App\Models;

use App\Models\RestaurantAddons;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OrderItems extends Model
{
    use  SoftDeletes;
    /**
     * guarded variable
     *
     * @var array
     */
    protected $guarded = [];
    /**
     * $table variable
     *
     * @var string
     */
    protected  $table = "order_items";

    public function menu_items()
    {
        return $this->belongsTo(MenuItems::class, 'item_id');
    }
    public function order()
    {
        return $this->belongsTo(Order::class, 'order_id');
    }
    public function order_addon()
    {
        return $this->belongsTo(OrderItemsAddon::class, 'order_item_id');
    }
    public function order_item_addon()
    {
        return $this->hasMany(OrderItemsAddon::class, 'order_item_id');
    }
    public function addon()
    {
        return $this->hasMany(OrderItemsAddon::class, 'order_item_id');
    }
    public function order_addons()
    {
        return $this->belongsTo(OrderItemsAddon::class, 'order_item_id');
    }
}
