<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MenuCategory extends Model
{
    use  SoftDeletes;
    /**
     * guarded variable
     *
     * @var array
     */
    protected $guarded = [];
    /**
     * $table variable
     *
     * @var string
     */

    protected $table = "menu_category";

    public function lang()
    {
        return $this->hasMany(MenuCategoryLang::class, 'menu_cat_id');
    }
    public function menu_items()
    {
        return $this->hasMany(MenuItems::class, 'menu_cate_id');
    }
    public function available_time()
    {
        return $this->hasMany(MenuCategoryAvailableTime::class, 'menu_cat_id');
    }
}
