<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RestaurantAttributesValues extends Model
{
    use  SoftDeletes;
    /**
     * guarded variable
     *
     * @var array
     */
    protected $guarded = [];
    /**
     * $table variable
     *
     * @var string
     */

    protected $table = "restaurant_attribute_values";

    protected $dates = ['deleted_at'];

    public function lang()
    {
        return $this->hasMany(RestaurantAttributesValuesLang::class, 'rest_attribute_value_id');
    }
}
