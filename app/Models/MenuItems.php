<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MenuItems extends Model
{
    use  SoftDeletes;
    /**
     * guarded variable
     *
     * @var array
     */
    protected $guarded = [];
    /**
     * $table variable
     *
     * @var string
     */

    protected $table = "menu_items";

    public function lang()
    {
        return $this->hasMany(MenuItemsLang::class, 'menu_item_id');
    }
    public function available_time()
    {
        return $this->hasMany(MenuItemAvailableTime::class, 'menu_item_id');
    }
    public function ingredients()
    {
        return $this->hasMany(MenuItemIngredients::class, 'menu_item_id');
    }
    public function addons()
    {
        return $this->hasMany(MenuItemAddons::class, 'menu_item_id');
    }
    public function photos()
    {
        return $this->hasMany(MenuItemPhotos::class, 'menu_item_id');
    }

    public function timing()
    {
        return $this->belongsToMany(Timing::class, 'menu_items_available_time', 'menu_item_id', 'time_id');
    }
    public function item_ingredients()
    {
        return $this->belongsToMany(RestaurantIngredients::class, 'menu_items_ingredients', 'menu_item_id', 'ingredient_id');
    }
    public function item_addons()
    {
        return $this->belongsToMany(RestaurantAddons::class, 'menu_items_addons', 'menu_item_id', 'addon_id');
    }
    public function item_attributes()
    {
        return $this->belongsToMany(RestaurantAttributes::class, 'menu_items_attributes', 'menu_item_id', 'attribute_id');
    }
    public function item_attributes_val()
    {
        return $this->belongsToMany(RestaurantAttributesValues::class, 'menu_items_attributes', 'menu_item_id', 'attribute_value_id');
    }
    public function menu_category()
    {
        return $this->belongsTo(MenuCategory::class, 'menu_cate_id');
    }
    public function order_addons()
    {
        return $this->belongsTo(OrderItemsAddon::class, 'order_item_id');
    }
}
