<?php

namespace App\Models;

use App\Models\AutoSuggest;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RestaurantIngredients extends Model
{
    use  SoftDeletes;
    /**
     * guarded variable
     *
     * @var array
     */
    protected $guarded = [];
    /**
     * $table variable
     *
     * @var string
     */

    protected $table = "restaurant_ingredients";

    public function lang()
    {
        return $this->hasMany(RestaurantIngredientsLang::class, 'ingredient_id');
    }
    // public function images()
    // {
    //     return $this->belongsTo(AutoSuggest::class, 'autosuggest_id');
    // }
}
