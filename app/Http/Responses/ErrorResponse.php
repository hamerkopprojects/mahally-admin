<?php

namespace App\Http\Responses;

use Illuminate\Contracts\Support\Responsable;

class ErrorResponse implements Responsable
{
    protected $msg;

    protected $status;

    public function __construct($msg, $status = 400)
    {
        $this->msg = $msg;
        $this->status = $status;
    }

    /**
     * Create an HTTP response that represents the object.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function toResponse($request)
    {
        return response()->json([
            'success' => false,
            'error' => [
                'msg' => $this->msg,
            ]
        ], $this->status);
    }
    
}
