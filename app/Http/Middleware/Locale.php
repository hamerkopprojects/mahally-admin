<?php

namespace App\Http\Middleware;

use Closure;
use App;
use Config;
use Session;

class Locale
{

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if (\Session::has('locale')) {
            \App::setlocale(\Session::get('locale'));
        } else {
            \App::setLocale('en');
            Session::put('locale', 'en');
        }
        return $next($request);
    }
}
