<?php

namespace App\Http\Controllers\Restaurant;

use App\Models\Timing;
use App\Models\MenuItems;
use App\Models\MenuCategory;
use Illuminate\Http\Request;
use App\Models\MenuItemsLang;
use App\Models\MenuItemAddons;
use App\Models\MenuItemPhotos;
use App\Models\RestaurantAddons;
use App\Models\MenuItemAttributes;
use Illuminate\Support\Facades\DB;
use App\Models\MenuItemIngredients;
use App\Http\Controllers\Controller;
use App\Models\MenuItemRelatedItems;
use App\Models\RestaurantAttributes;
use Illuminate\Support\Facades\Auth;
use App\Models\MenuItemAvailableTime;
use App\Models\RestaurantIngredients;
use App\Models\MenuCategoryAvailableTime;
use Illuminate\Support\Facades\Validator;
use App\Models\RestaurantAttributesValues;

class MenuItemController extends Controller
{
    public function get(Request $request)
    {
        $rest_id = Auth::user()->restuarants_id;
        $menuItems = MenuItems::with('lang')->where('restuarants_id', $rest_id)->orderBy('order', 'asc')->paginate(20);

        $menuCategory = MenuCategory::select('menu_category.id', 'menu_category_i18n.name')->leftJoin('menu_category_i18n', 'menu_category_i18n.menu_cat_id', 'menu_category.id')->where('menu_category.restuarants_id', $rest_id)->where('menu_category_i18n.language', 'en')->get();
        $menuSortItems = MenuItems::with('lang')->where('restuarants_id', $rest_id)->orderBy('order', 'asc')->get();
        return view('restaurant.menu_items.index', compact('menuItems', 'rest_id', 'menuSortItems', 'menuCategory'));
    }
    public function create(Request $req)
    {
        $rest_id = Auth::user()->restuarants_id;
        $_menu_id = $req->id;
        $row_data = array();
        $menu_abl_in = array();
        $exist_addons = array();
        $exist_ingredients = array();
        $available_in = Timing::where('restuarants_id', $rest_id)->get();
        $menu_category  = MenuCategory::with('lang')->where('restuarants_id', $rest_id)->get();
        $ingredients = RestaurantIngredients::with('lang')->where('restuarants_id', $rest_id)->get();
        $addons = RestaurantAddons::with('lang')->where('restuarants_id', $rest_id)->get();



        if ($_menu_id) {
            $row_data = MenuItems::with('lang')->where('id', $_menu_id)->first();
            $available_time = MenuItemAvailableTime::select('time_id')->where('menu_item_id', $_menu_id)->get()->toArray();
            $menu_ingredients = MenuItemIngredients::select('ingredient_id')->where('menu_item_id', $_menu_id)->get()->toArray();
            $menu_addons = MenuItemAddons::select('addon_id')->where('menu_item_id', $_menu_id)->get()->toArray();
            foreach ($available_time as $data) {
                $menu_abl_in[] = $data['time_id'];
            }
            foreach ($menu_ingredients as $data) {
                $exist_ingredients[] = $data['ingredient_id'];
            }
            foreach ($menu_addons as $data) {
                $exist_addons[] = $data['addon_id'];
            }
        }

        $data = [
            'available_in' => $available_in,
            'menu_category' => $menu_category,
            'ingredients' => $ingredients,
            'row_data' => $row_data,
            'addons' => $addons,
            'menu_abl_in' => $menu_abl_in,
            'menu_ingredients' => $exist_ingredients,
            'menu_addons' => $exist_addons,
        ];
        return view('restaurant.menu_items.create', $data);
    }
    public function sort_item(Request $req)
    {
        $rest_id = Auth::user()->restuarants_id;

        $menuCategory = MenuCategory::select('menu_category.id', 'menu_category_i18n.name')->leftJoin('menu_category_i18n', 'menu_category_i18n.menu_cat_id', 'menu_category.id')->where('menu_category.restuarants_id', $rest_id)->where('menu_category_i18n.language', 'en')->get();
        $menuSortItems = MenuItems::with('lang')->where('restuarants_id', $rest_id)->orderBy('order', 'asc')->get();
        return view('restaurant.menu_items.sort_item')->with(compact('menuCategory', 'menuSortItems'))->render();
    }
    public function get_tabs(Request $req)
    {
        $menu_id = $req->menu_id;
        $rest_id = Auth::user()->restuarants_id;
        if ($req->activeTab == 'BASIC INFO') {
            $_menu_id = $req->menu_id;
            $row_data = array();
            $menu_abl_in = array();
            $exist_addons = array();
            $exist_ingredients = array();
            $available_in = Timing::where('restuarants_id', $rest_id)->get();
            $menu_category  = MenuCategory::with('lang')->where('restuarants_id', $rest_id)->get();
            $ingredients = RestaurantIngredients::with('lang')->where('restuarants_id', $rest_id)->get();
            $addons = RestaurantAddons::with('lang')->where('restuarants_id', $rest_id)->get();

            if ($_menu_id) {
                $row_data = MenuItems::with('lang')->where('id', $_menu_id)->first();
                $available_time = MenuItemAvailableTime::select('time_id')->where('menu_item_id', $_menu_id)->get()->toArray();
                $menu_ingredients = MenuItemIngredients::select('ingredient_id')->where('menu_item_id', $_menu_id)->get()->toArray();
                $menu_addons = MenuItemAddons::select('addon_id')->where('menu_item_id', $_menu_id)->get()->toArray();
                foreach ($available_time as $data) {
                    $menu_abl_in[] = $data['time_id'];
                }
                foreach ($menu_ingredients as $data) {
                    $exist_ingredients[] = $data['ingredient_id'];
                }
                foreach ($menu_addons as $data) {
                    $exist_addons[] = $data['addon_id'];
                }
            }

            $data = [
                'available_in' => $available_in,
                'menu_category' => $menu_category,
                'ingredients' => $ingredients,
                'row_data' => $row_data,
                'addons' => $addons,
                'menu_abl_in' => $menu_abl_in,
                'menu_ingredients' => $exist_ingredients,
                'menu_addons' => $exist_addons,
            ];
            return view('restaurant.menu_items.basic_info', $data);
        } elseif ($req->activeTab == 'PHOTOS') {

            $coverData = MenuItems::select('cover_photo')->where(['id' => $menu_id])->first();
            $cover_image = $coverData['cover_photo'];
            $photos = MenuItemPhotos::where(['menu_item_id' => $menu_id])->get();

            return view('restaurant.menu_items.photos')->with(compact('photos', 'menu_id', 'cover_image'))->render();
        } elseif ($req->activeTab == 'ATTRIBUTES') {
            $attributes = RestaurantAttributes::with('lang')->where('restuarants_id', $rest_id)->get();
            $MenuAttr = MenuItemAttributes::where(['menu_item_id' => $menu_id])->get();
            return view('restaurant.menu_items.attributes')->with(compact('attributes', 'menu_id', 'MenuAttr'))->render();
        } elseif ($req->activeTab == 'RELATED ITEMS') {
            $menu_items = MenuItems::with('lang')->where('restuarants_id', $rest_id)->where('id', '!=', $menu_id)->get();

            $related_items = MenuItemRelatedItems::select('related_item_id')->where('menu_item_id', $menu_id)->get()->toArray();

            $exist_items = array();

            foreach ($related_items as $data) {
                $exist_items[] = $data['related_item_id'];
            }

            return view('restaurant.menu_items.related_items')->with(compact('menu_items', 'menu_id', 'exist_items'))->render();
        }
    }
    public function basic_info(Request $req)
    {
        if ($req->menu_id) {
            $unique = ',' . $req->menu_id;
        } else {
            $unique = ',NULL';
        }
        $flag = 0;
        $rest_id = Auth::user()->restuarants_id;
        $photos = array();
        $rules = [
            'name_en' => 'required',
            'name_ar' => 'required',
            'menu_cate_id' => 'required',
            'calory' => 'required',
            'price' => 'required',
            // 'discount_price' => 'required',
            'description_ar' => 'required',
            'description_en' => 'required',
        ];
        $messages = [
            'name_en.required' => 'Item Name(EN) is required.',
            'name_ar.required' => 'Item Name(AR) is required.',
            'menu_cate_id.required' => 'Category is required.',
            'calory.required' => 'Calory is required.',
            'price.required' => 'Price is required.',
            // 'discount_price.required' => 'Discount price is required.',
            'description_en.required' => 'Description (EN) is required.',
            'description_ar.required' => 'Description (AR) is required.',
        ];


        $validator = Validator::make($req->all(), $rules, $messages);
        if (!$validator->passes()) {
            return response()->json(['status' => 0, 'message' => $validator->errors()->first()]);
        } else {

            if (empty($req->menu_id)) {
                $menu = DB::transaction(function () use ($req, $rest_id) {
                    $menu = MenuItems::create([
                        'restuarants_id' => $rest_id,
                        'menu_cate_id' => $req->menu_cate_id,
                        'calory' => $req->calory,
                        'price' => $req->price,
                        'discount_price' => $req->discount_price,
                        'status' => 'deactive',
                    ]);
                    $menu->lang()->createMany([
                        [
                            'name' => $req->name_en,
                            'description' => $req->description_en,
                            'language' => 'en',
                        ],
                        [
                            'name' => $req->name_ar,
                            'description' => $req->description_ar,
                            'language' => 'ar',
                        ],
                    ]);
                    return $menu;
                });
                $msg = 'Basic info added successfully';
            } else {
                $menu = DB::transaction(function () use ($req) {
                    $menu = MenuItems::where('id', $req->menu_id)
                        ->update([
                            'menu_cate_id' => $req->menu_cate_id,
                            'calory' => $req->calory,
                            'price' => $req->price,
                            'discount_price' => $req->discount_price,
                        ]);
                    MenuItemsLang::where('language', 'en')
                        ->where('menu_item_id', $req->menu_id)
                        ->update([
                            'name' => $req->name_en,
                            'description' => $req->description_en,
                        ]);
                    MenuItemsLang::where('language', 'ar')
                        ->where('menu_item_id', $req->menu_id)
                        ->update([
                            'name' => $req->name_ar,
                            'description' => $req->description_ar,
                        ]);

                    return $menu;
                });

                $msg = 'Basic info updated successfully';
                $flag = 1;
            }
            if ($menu) {
                $menu_id = !empty($menu->id) ? $menu->id : $req->menu_id;
                MenuItemAvailableTime::where(['menu_item_id' => $menu_id])->delete();
                if ($req->available_time) {
                    foreach ($req->available_time as $time) {
                        $availableTime = MenuItemAvailableTime::create([
                            'menu_item_id' => $menu_id,
                            'time_id' => $time,
                        ]);
                    }
                }
                MenuItemIngredients::where(['menu_item_id' => $menu_id])->delete();
                if ($req->ingredients) {
                    foreach ($req->ingredients as $ingredient) {
                        $availableTime = MenuItemIngredients::create([
                            'menu_item_id' => $menu_id,
                            'ingredient_id' => $ingredient,
                        ]);
                    }
                }
                MenuItemAddons::where(['menu_item_id' => $menu_id])->delete();
                if ($req->addons) {
                    foreach ($req->addons as $addon) {
                        $availableTime = MenuItemAddons::create([
                            'menu_item_id' => $menu_id,
                            'addon_id' => $addon,
                        ]);
                    }
                }
            }
            $cover_image = '';
            $photos = array();
            if ($flag) {
                $coverData = MenuItems::select('cover_photo')->where(['id' => $menu_id])->first();
                $cover_image = $coverData->cover_photo;
                $photos = MenuItemPhotos::where(['menu_item_id' => $menu_id])->get();
            }
            $html = view('restaurant.menu_items.photos')->with(compact('menu_id', 'photos', 'cover_image'))->render();
            return response()->json(['status' => 1, 'message' => $msg, 'result' => $html]);
        }
    }
    public function item_images(Request $req)
    {
        $menu_id = $req->menu_id;
        if ($req->upload_type == 'single') {
            $rules = [
                'photo' => 'required|image|mimes:png,jpg,jpeg'
            ];
            $messages = [
                'photo.max' => 'The image must be less than 2Mb in size',
                'photo.mimes' => "The image must be of the format jpeg or png",
            ];
        } else {
            $rules = [
                'photos' => ['required', 'array'],
                'photos.*' => 'image|mimes:png,jpg,jpeg',
            ];
            $messages = [
                'photos.max' => 'The image must be less than 2Mb in size',
                'photos.mimes' => "The image must be of the format jpeg or png",
            ];
        }

        $validator = Validator::make($req->all(), $rules, $messages);
        if (!$validator->passes()) {
            return response()->json(['status' => 0, 'message' => $validator->errors()->first()]);
        } else {
            if ($req->upload_type == 'single') {
                $file = request()->file('photo');
                $path = $file->store("restaurant/menu_items", ['disk' => 'public_uploads']);
                MenuItems::where('id', $menu_id)
                    ->update([
                        "cover_photo" => $path
                    ]);
                $full_path = url('uploads/' . $path);
                return response()->json(['status' => 1, 'message' => 'Cover image added successfully', 'image_path' => $full_path, 'menu_id' => $menu_id]);
            } else {
                $productImages = $req->file('photos') ? $req->file('photos') : [];

                foreach ($productImages as $image) {
                    $saved = MenuItemPhotos::create([
                        'menu_item_id' => $menu_id,
                        'image_path' => $image->store("restaurant/menu_items/images", ['disk' => 'public_uploads']),
                    ]);
                }
                $item_images = MenuItemPhotos::where(['id' => $saved['id']])->first();
                $full_path = url('uploads/' . $item_images->image_path);
                return response()->json(['status' => 1, 'message' => 'Menu item image added successfully', 'image_path' => $full_path, 'image_id' => $saved['id']]);
            }
        }
    }
    public function detete_img(Request $request)
    {
        if ($request->type == 'cover') {
            $menu = MenuItems::where('id', $request->id)->first();
            MenuItems::where('id', $request->id)
                ->update([
                    "cover_photo" => Null
                ]);
            if (!empty($menu)) {
                $file_path = public_path() . '/uploads/' . $menu["cover_photo"];
                if (is_file($file_path)) {
                    unlink($file_path);
                }
            }
            $msg = 'Cover image deleted successfully';
        } else {
            $menu_img = MenuItemPhotos::find($request->id);
            if (!empty($menu_img)) {
                $file_path = public_path("uploads/{$menu_img["path"]}");
                $menu_img->delete();
                if (is_file($file_path)) {
                    unlink($file_path);
                }
            }
            $msg = 'Menu item image deleted successfully';
        }
        $loader_image = asset('assets/images/loader.gif');
        return response()->json(['status' => 1, 'message' => $msg, 'id' => $request->id, 'type' => $request->type, 'loader_image' => $loader_image]);
    }
    public function photos(Request $req)
    {
        $menu_id = $req->menu_id;
        $flag = 0;
        $rest_id = Auth::user()->restuarants_id;
        $menu_data = MenuItems::where(['id' => $menu_id])->first();
        if (!empty($menu_data)) {
            $document_status = MenuItemPhotos::where(['menu_item_id' => $req->menu_id])->get();
            if (count($document_status) > 0) {
                $flag = 1;
            }
            $attributes = RestaurantAttributes::with('lang')->where('restuarants_id', $rest_id)->get();
            $MenuAttr = MenuItemAttributes::where(['menu_item_id' => $menu_id])->get();
            $html = view('restaurant.menu_items.attributes')->with(compact('attributes', 'menu_id', 'MenuAttr'))->render();
            if ($flag == 1) {
                $msg = "Photos updated successfully";
            } else {
                $msg = "Photos added successfully";
            }
            return response()->json(['status' => 1, 'message' => $msg, 'result' => $html]);
        } else {
            return response()->json(['status' => 0, 'message' => 'Sorry something went wrong.']);
        }
    }
    public function getAttributeValue(Request $req)
    {
        $rest_id = Auth::user()->restuarants_id;
        $attributes = RestaurantAttributes::with('lang')->where('restuarants_id', $rest_id)->get();
        $attribute_value = RestaurantAttributesValues::with('lang')->where('rest_attribute_id', $req->attr_id)->get();
        return [
            'attributes' => $attributes,
            'attribute_value' => $attribute_value,
        ];
    }
    public function attributes(Request $req)
    {
        $menu_id = $req->menu_id;
        $flag = 0;
        $rest_id = Auth::user()->restuarants_id;
        $menu_data = MenuItems::where(['id' => $menu_id])->first();

        if (!empty($menu_data)) {
            $document_status = MenuItemAttributes::where(['menu_item_id' => $req->menu_id])->get();
            if (count($document_status) > 0) {
                MenuItemAttributes::where(['menu_item_id' => $req->menu_id])->delete();
                $flag = 1;
            }

            for ($i = 0; $i < count($req->attr); $i++) {
                if ($req->attr[$i]) {
                    $attributes = MenuItemAttributes::create([
                        'menu_item_id' => $menu_id,
                        'attribute_id' => $req->attr[$i],
                        'attribute_value_id' => $req->attribute_values[$i],
                    ]);
                }
            }

            $menu_items = MenuItems::with('lang')->where('restuarants_id', $rest_id)->where('id', '!=', $menu_id)->get();
            $related_items = MenuItemRelatedItems::select('related_item_id')->where('menu_item_id', $menu_id)->get()->toArray();

            $exist_items = array();

            foreach ($related_items as $data) {
                $exist_items[] = $data['related_item_id'];
            }
            $html = view('restaurant.menu_items.related_items')->with(compact('menu_items', 'menu_id', 'exist_items'))->render();
            if ($flag == 1) {
                $msg = "Attributes updated successfully";
            } else {
                $msg = "Attributes added successfully";
            }
            return response()->json(['status' => 1, 'message' => $msg, 'result' => $html]);
        } else {
            return response()->json(['status' => 0, 'message' => 'Sorry something went wrong.']);
        }
    }
    public function related_items(Request $req)
    {
        $menu_id = $req->menu_id;
        $flag = 0;
        $rest_id = Auth::user()->restuarants_id;
        $menu_data = MenuItems::where(['id' => $menu_id])->first();
        if (!empty($menu_data)) {
            $document_status = MenuItemRelatedItems::where(['menu_item_id' => $req->menu_id])->get();
            if (count($document_status) > 0) {
                MenuItemRelatedItems::where(['menu_item_id' => $req->menu_id])->delete();
                $flag = 1;
            }
            if ($req->related_items) {
                foreach ($req->related_items as $item) {
                    $attributes = MenuItemRelatedItems::create([
                        'menu_item_id' => $menu_id,
                        'related_item_id' => $item,
                    ]);
                }
            }

            if ($flag == 1) {
                $msg = "Related items updated successfully";
            } else {
                $msg = "Related items added successfully";
            }
            return response()->json(['status' => 1, 'message' => $msg]);
        } else {
            return response()->json(['status' => 0, 'message' => 'Sorry something went wrong.']);
        }
    }
    public function statusUpdate(Request $request)
    {
        $status = $request->status === 'Deactivate' ? 'deactive' : 'active';

        if ($status == 'active') {
            $photos = MenuItemPhotos::select('id')->where('menu_item_id', $request->id)->first();
            $attri = MenuItemAttributes::select('id')->where('menu_item_id', $request->id)->first();
            // $related_item = MenuItemRelatedItems::select('id')->where('menu_item_id', $request->id)->first();


            if (!empty($photos) && !empty($attri)) {
                MenuItems::where('id', $request->id)
                    ->update([
                        'status' => $status
                    ]);
                $msg = 'Status updated successfully';
                $stat_val = 2;
            } else {
                $stat_val = 2;
                $msg = 'Please complete all menu item forms';
            }
        } else {
            MenuItems::where('id', $request->id)
                ->update([
                    'status' => $status
                ]);
            $msg = 'Status updated successfully';
            $stat_val = 1;
        }

        return response()->json(['status' => $stat_val, 'message' => $msg]);


        // MenuItems::where('id', $request->id)
        //     ->update([
        //         'status' => $status
        //     ]);
        // return response()->json(['status' => 1, 'message' => "Status updated successfully"]);
    }

    public function destroy(Request $request)
    {
        if ($request->id) {
            $items = MenuItems::find($request->id);
            if (!empty($items)) {
                $items->lang()->delete();
                $items->delete();
                return response()->json(['status' => 1, 'message' => 'Menu item deleted successfully']);
            }
        } else {
            return response()->json(['status' => 0, 'message' => 'Sorry something went wrong.']);
        }
    }
    public function sortable(Request $request)
    {
        $posts = MenuItems::all();

        foreach ($posts as $post) {
            foreach ($request->order as $order) {
                if ($order['id'] == $post->id) {
                    $post->update(['order' => $order['position']]);
                }
            }
        }

        return response()->json(['status' => 1, 'message' => 'Menu items sorted successfully']);
    }
}
