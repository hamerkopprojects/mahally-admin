<?php

namespace App\Http\Controllers\Restaurant;

use App\Models\Order;
use App\Models\Customers;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;

class OrderController extends Controller
{
    protected $order;

    const SINGLE = 1;

    public function __construct(Order $order)
    {
        $this->order = $order;
    }

    public function activeOrders(Request $req)
    {
        $rest_id = Auth::user()->restuarants_id;
        $cust_id = $req->customer ?? '';
        $search = $req->search;
        $stat = $req->status;
        $date = $req->order_date;

        $customer = Customers::RightJoin('orders', 'orders.customer_id', 'customer.id')->select('customer.id', 'customer.name','customer.phone_number')->groupBy('orders.customer_id')->get();
        $status = array("1", "2", "3", "4");

        $query = $this->order::with('customer')->where('restaurant_id', $rest_id)->whereIn('order_status', $status);

        if ($req->search) {
            $query->where('order_id', 'like', "%" . $req->search . "%");
        }
        if ($req->order_date) {
            $date_new = date('Y-m-d', strtotime($req->order_date));
            $query->whereDate('created_at', $date_new);
        }
        if ($req->customer) {
            $query->where('customer_id', $req->customer);
        }
        if ($req->status) {
            $query->where('order_status', $req->status);
        }

        $orders = $query->orderBy('created_at', 'desc')->paginate(20);
        $order_status = Config::get('constants.frontdisk_order_status');
        return view('restaurant.orders.active_orders', compact('orders', 'order_status', 'search', 'customer','cust_id','date'));
    }
    public function deliveredOrders(Request $req)
    {
        $rest_id = Auth::user()->restuarants_id;
        $cust_id = $req->customer ?? '';
        $search = $req->search;
        $stat = $req->status;
        $selected_region = $req->region;
        $date = $req->order_date;
        $customer = Customers::RightJoin('orders', 'orders.customer_id', 'customer.id')->select('customer.id', 'customer.name','customer.phone_number')->groupBy('orders.customer_id')->get();
        $query = $this->order::with('customer')->where('restaurant_id', $rest_id)->where('order_status', "5");

        if ($req->search) {
            $query->where('order_id', 'like', "%" . $req->search . "%");
        }
        if ($req->order_date) {
            $date_new = Carbon::parse($req->order_date)->format('Y-m-d');
            $query->whereDate('created_at', $date_new);
        }
        if ($req->customer) {
            $query->where('customer_id', $req->customer);
        }


        $orders = $query->orderBy('created_at', 'desc')->paginate(20);
        $order_status = Config::get('constants.frontdisk_order_status');
        return view('restaurant.orders.delivered_orders', compact('orders', 'order_status','cust_id','search','date','customer'));
    }
}
