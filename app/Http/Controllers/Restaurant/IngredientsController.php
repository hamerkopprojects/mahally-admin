<?php

namespace App\Http\Controllers\Restaurant;

use App\Models\AutoSuggest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Models\RestaurantIngredients;
use App\Models\RestaurantIngredientsLang;
use Illuminate\Support\Facades\Validator;

class IngredientsController extends Controller
{
    public function index(Request $request)
    {
        $search = $request->ingredient_select;
        $search_field = $request->search_field ?? '';
        $rest_id = Auth::user()->restuarants_id;

        if ($search_field) {
            $data = RestaurantIngredientsLang::select('ingredient_id')->where('name', 'like', "%" . $search_field . "%")->get()->toArray();
            $da = [];
            foreach ($data as  $d) {
                $da[] = $d['ingredient_id'];
            }
            $ingredients = RestaurantIngredients::with('lang')->whereIn('id', $da)->where('restuarants_id', $rest_id)->orderBy('created_at', 'desc')->paginate(20);
        } else {
            $ingredients = RestaurantIngredients::with('lang')->where('restuarants_id', $rest_id)->orderBy('created_at', 'desc')->paginate(20);
        }

        $ingredientMainList = AutoSuggest::leftJoin('autosuggest_i18n', 'autosuggest.id', 'autosuggest_i18n.autosuggest_id')->select('autosuggest.id', 'autosuggest_i18n.name')->where('autosuggest.type', 'ingredients')->where('autosuggest.status', 'active')->where('autosuggest_i18n.language', 'en')->where('autosuggest.deleted_at', NULL)->orderBy('autosuggest_i18n.name', 'asc')->get();

        $ingredientExist = RestaurantIngredients::select('autosuggest_id')->where('restuarants_id', $rest_id)->where('type', 'From Main List')->get()->toArray();
        $ingredient_data = array();
        foreach ($ingredientExist as  $addon_data) {
            $ingredient_data[] = $addon_data['autosuggest_id'];
        }
        return view('restaurant.ingredient.index', compact('ingredients', 'search_field', 'ingredientMainList', 'ingredient_data'));
    }
    public function store(Request $request)
    {
        $rules = [
            'title_en' => 'required',
            'title_ar' => 'required',
            'upload_image' => 'image|mimes:png,jpg,jpeg|max:2048',
        ];
        $messages = [
            'title_en.required' => 'This filed is required.',
            'title_ar.required' => 'This filed is required.',
            'upload_image.max' => 'The image must be less than 1MB in size',
            'upload_image.mimes' => "The image must be of the format jpeg or png",
        ];
        $rest_id = Auth::user()->restuarants_id;
        $validator = Validator::make($request->all(), $rules, $messages);
        if (!$validator->passes()) {
            return response()->json(['status' => 0, 'message' => $validator->errors()->first()]);
        } else {
            if (!empty($request->file('upload_image'))) {

                $file = $request->file('upload_image');
                $image_name = $file->getClientOriginalName() . '.' . $file->getClientOriginalExtension();
                $path_en = $file->move(public_path() . '/uploads/restaurant/ingredients/', $image_name);
            } else {
                $image_name =  NULL;
            }

            $ingredient = DB::transaction(function () use ($request, $image_name, $rest_id) {
                $ingredient = RestaurantIngredients::create([
                    'status' => 'active',
                    'type' => 'By Restaurant',
                    'image_path' => $image_name,
                    'restuarants_id' => $rest_id,
                ]);
                $ingredient->lang()->createMany([
                    [
                        'name' => $request->title_en,
                        'language' => 'en',
                    ],
                    [
                        'name' => $request->title_ar,
                        'language' => 'ar',
                    ],
                ]);
                return $ingredient;
            });
            $msg = "Ingredient added successfully";
            if ($ingredient) {
                return response()->json(['status' => 1, 'message' => $msg]);
            } else {
                return response()->json(['status' => 0, 'message' => 'Sorry something went wrong.']);
            }
        }
    }
    public function storeMainList(Request $request)
    {
        $rest_id = Auth::user()->restuarants_id;
        $addonsExist = RestaurantIngredients::where('restuarants_id', $rest_id)->where('type', 'From Main List')->delete();
        if ($request->ingredients) {
            foreach ($request->ingredients as $key => $item) {
                $ingredientsMainList = AutoSuggest::with('lang')->where('id', $item)->first();
                $ingredentsNew = DB::transaction(function () use ($request, $ingredientsMainList, $key, $item, $rest_id) {
                    $ingredentsNew = RestaurantIngredients::create([
                        'status' => 'deactive',
                        'type' => 'From Main List',
                        'restuarants_id' =>  $rest_id,
                        'autosuggest_id' => $item,
                        'image_path' => $ingredientsMainList->image_path,
                    ]);
                    $ingredentsNew->lang()->createMany([
                        [
                            'name' => $ingredientsMainList->lang[0]->name,
                            'language' => 'en',
                        ],
                        [
                            'name' => $ingredientsMainList->lang[1]->name,
                            'language' => 'ar',
                        ],
                    ]);
                    return $ingredentsNew;
                });
            }
            $msg = "Ingredients added successfully";
            if ($ingredentsNew) {
                return response()->json(['status' => 1, 'message' => $msg]);
            } else {
                return response()->json(['status' => 0, 'message' => 'Sorry something went wrong.']);
            }
        } else {
            return response()->json(['status' => 1, 'message' => 'Ingredients updated successfully']);
        }
    }
    public function edit($id)
    {
        $data = RestaurantIngredients::with('lang')
            ->where('id', $id)
            ->first();
        return [
            'page' => $data,

        ];
    }
    public function update(Request $request)
    {
        $rules = [
            'title_en' => 'required',
            'title_ar' => 'required',
            'upload_image' => 'image|mimes:png,jpg,jpeg|max:2048',
        ];
        $messages = [
            'title_en.required' => 'This filed is required.',
            'title_ar.required' => 'This filed is required.',
            'upload_image.max' => 'The image must be less than 1MB in size',
            'upload_image.mimes' => "The image must be of the format jpeg or png",
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if (!$validator->passes()) {
            return response()->json(['status' => 0, 'message' => $validator->errors()->first()]);
        } else {
            if (!empty($request->file('upload_image'))) {

                $file = $request->file('upload_image');
                $image_name = $file->getClientOriginalName() . '.' . $file->getClientOriginalExtension();
                $path_en = $file->move(public_path() . '/uploads/restaurant/ingredients/', $image_name);
            } else {
                $image_name =  NULL;
            }
            $ingredient = RestaurantIngredients::where('id', $request->id_pg)
                ->update([
                    'image_path' => $image_name,
                ]);

            $ingredient = RestaurantIngredientsLang::where('ingredient_id', $request->id_pg)
                ->where('language', 'en')
                ->update(
                    [
                        'name' => $request->title_en,
                    ]
                );
            $ingredient = RestaurantIngredientsLang::where('ingredient_id', $request->id_pg)
                ->where('language', 'ar')
                ->update(
                    [
                        'name' => $request->title_ar,
                    ]
                );

            $msg = "Ingredient updated successfully";
            if ($ingredient) {
                return response()->json(['status' => 1, 'message' => $msg]);
            } else {
                return response()->json(['status' => 0, 'message' => 'Sorry something went wrong.']);
            }
        }
    }
    public function statusUpdate(Request $request)
    {
        $status = $request->status === 'Deactivate' ? 'deactive' : 'active';

        RestaurantIngredients::where('id', $request->id)
            ->update([
                'status' => $status
            ]);
        return response()->json(['status' => 1, 'message' => "Status updated successfully"]);
    }

    public function destroy(Request $request)
    {
        if ($request->id) {
            $ingredient = RestaurantIngredients::find($request->id);
            if (!empty($ingredient)) {
                $ingredient->lang()->delete();
                $ingredient->delete();
                return response()->json(['status' => 1, 'message' => 'Ingredient deleted successfully']);
            }
        } else {
            return response()->json(['status' => 0, 'message' => 'Sorry something went wrong.']);
        }
    }
}
