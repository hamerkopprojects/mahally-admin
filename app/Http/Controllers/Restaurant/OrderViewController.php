<?php

namespace App\Http\Controllers\Restaurant;

use App\Models\Order;
use App\Models\Reviews;
use Illuminate\Http\Request;
use App\Models\ReviewSegments;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;
use PDF;

class OrderViewController extends Controller
{
    public function details(Request $req)
    {
        $order_id = $req->id;
        $order_status = Config::get('constants.frontdisk_order_status');
        $change_order_status = Config::get('constants.change_order_status');

        $order = Order::with('order_item')->with(['order_addons' => function ($q) {
            $q->withPivot(['order_id', 'addon_id', 'item_count', 'sub_total']);
            $q->with('lang');
        }])->with('customer')->with(['restaurant' => function ($q) {
            $q->with('lang');
        }])->with(['menu_item' => function ($q) {
            $q->with('lang');
        }])->where('id', $order_id)
            ->first();

        return view('restaurant.orders.views.order_view', compact('order', 'order_status', 'change_order_status'));
    }
    public function delivered_order_details(Request $req)
    {
        $order_id = $req->id;
        $order_status = Config::get('constants.frontdisk_order_status');

        $order = Order::with('order_item')->with(['order_addons' => function ($q) {
            $q->withPivot(['order_id', 'addon_id', 'item_count', 'sub_total']);
            $q->with('lang');
        }])->with('customer')->with(['restaurant' => function ($q) {
            $q->with('lang');
        }])->with(['menu_item' => function ($q) {
            $q->with('lang');
        }])->where('id', $order_id)
            ->first();

        return view('restaurant.orders.views.delivered_order_view', compact('order', 'order_status'));
    }
    public function getViewTabs(Request $req)
    {
        $order_id = $req->id;
        $order = Order::where('id', $order_id);
        if ($req->activeTab == 'ORDER INFO') {
            $order_status = Config::get('constants.frontdisk_order_status');
            $order = Order::with('order_item')
                ->with('order_addons')
                ->with('customer')
                ->with(['menu_item' => function ($q) {
                    $q->withPivot(['order_id', 'item_id', 'item_price', 'item_count', 'grant_total', 'sub_total', 'tax_total']);
                }])
                ->where('id', $order_id)
                ->first();
            return view('restaurant.orders.views.basic_details', compact('order', 'order_status'));
        } else {
            $reviews = Reviews::with('customer')->where('order_id', $order_id)->first();
            $reviews = Reviews::with('customer')->where('order_id', $order_id)->first();
            if ($reviews) {
                $review_segments = ReviewSegments::with(['segment' => function ($q) {
                    $q->with('lang:id,autosuggest_id,language,name');
                }])
                    ->where('review_id', $reviews->id)->get();
            }
            return view('restaurant.orders.views.reviews_view', compact('reviews', 'review_segments'));
        }
    }
    public function reviewStatusUpdate(Request $req)
    {
        $status = $req->status;

        Reviews::where('id', $req->review_id)
            ->update([
                'approval_status' => $status
            ]);
        if ($status === 'approve') {
            $msg = "Review Approved";
        } else {
            $msg = "Review Rejected";
        }
        return response()->json(['status' => 1, 'message' => $msg]);
    }
    public function orderStatusUpdate(Request $req)
    {
        $status = $req->status;
        $order = Order::where('id', $req->id)
            ->update([
                'order_status' => $status
            ]);
        if ($order) {
            $msg = "Status Changed";
        } else {
            $msg = "Status can not be changed";
        }
        return response()->json(['status' => 1, 'message' => $msg, 'id' => $req->id]);
    }
    public function orderInvoice(Request $req)
    {
        $order_id = $req->id;
        $order_status = Config::get('constants.frontdisk_order_status');
        $order = Order::with('customer')->with(['order_item' => function ($q) {
            $q->with(['menu_items' => function ($q) {
                $q->with('lang');
            }])->with(['order_addons' => function ($q) {
                $q->with(['resturant_addons' => function ($q) {
                    $q->with('lang');
                }]);
            }]);
        }])
            ->where('id', $order_id)
            ->first();
        $pdf = PDF::loadView('restaurant.orders.invoice', compact('order', 'order_status'));
        return $pdf->stream();
    }
}
