<?php

namespace App\Http\Controllers\Restaurant;

use App\Models\MenuItems;
use Illuminate\Http\Request;
use App\Models\OfferManagement;
use Illuminate\Support\Facades\DB;
use App\Models\OfferManagementLang;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class OfferManagementController extends Controller
{
    public function index(Request $request)
    {
        $rest_id = Auth::user()->restuarants_id;
        $offer = OfferManagement::with('lang')->where('restuarants_id', $rest_id)->orderBy('created_at', 'desc')->paginate(20);
        $menuItemList = MenuItems::with('lang')->where('restuarants_id', $rest_id)->get();
        return view('restaurant.offer_management.index', compact('offer', 'menuItemList', 'rest_id'));
    }
    public function store(Request $request)
    {
        $rules = [
            'title_en' => 'required',
            'title_ar' => 'required',
            'discount_value' => 'required|numeric',
            'menu_item_id' => 'required',
            'expire_in' => 'required',
            'upload_image_en' => 'image|mimes:png,jpg,jpeg|max:2048',
        ];
        $messages = [
            'title_en.required' => 'Item Title (EN) is required.',
            'title_ar.required' => 'Item Title (AR) is required.',
            'discount_value.required' => 'Discount value is required.',
            'menu_item_id.required' => 'Item is required.',
            'expire_in.required' => 'Expire In is required.',
            'upload_image_en.max' => 'The image must be less than 1MB in size',
            'upload_image_en.mimes' => "The image must be of the format jpeg or png",
        ];
        $rest_id = Auth::user()->restuarants_id;
        $validator = Validator::make($request->all(), $rules, $messages);
        if (!$validator->passes()) {
            return response()->json(['status' => 0, 'message' => $validator->errors()->first()]);
        } else {
            if (!empty($request->file('upload_image_en'))) {

                $file = $request->file('upload_image_en');
                $image_name_en = $file->getClientOriginalName() . '.' . $file->getClientOriginalExtension();
                $path_en = $file->move(public_path() . '/uploads/restaurant/offer_management/en/', $image_name_en);
            } else {
                $image_name_en =  NULL;
            }
            if (!empty($request->file('upload_image_ar'))) {
                $file = $request->file('upload_image_ar');
                $image_name_ar = $file->getClientOriginalName() . '.' . $file->getClientOriginalExtension();
                $path_en = $file->move(public_path() . '/uploads/restaurant/offer_management/ar/', $image_name_ar);
            } else {
                $image_name_ar =  NULL;
            }

            $offer = DB::transaction(function () use ($request, $image_name_en, $rest_id, $image_name_ar) {
                $offer = OfferManagement::create([
                    'restuarants_id' => $rest_id,
                    'status' => 'deactive',
                    'menu_item_id' => $request->menu_item_id,
                    'expire_in' => date('Y-m-d H:i:s', strtotime($request->expire_in)),
                    'discount_type' => $request->discount_type,
                    'discount_value' => $request->discount_value,
                    'coupon_code' => $request->coupon_code,
                ]);
                $offer->lang()->createMany([
                    [
                        'name' => $request->title_en,
                        'image_path' => $image_name_en,
                        'language' => 'en',
                    ],
                    [
                        'name' => $request->title_ar,
                        'image_path' => $image_name_ar,
                        'language' => 'ar',
                    ],
                ]);
                return $offer;
            });
            $msg = "Offer added successfully";
            if ($offer) {
                return response()->json(['status' => 1, 'message' => $msg]);
            } else {
                return response()->json(['status' => 0, 'message' => 'Sorry something went wrong.']);
            }
        }
    }
    public function edit($id)
    {
        $offer = OfferManagement::with('lang')
            ->where('id', $id)
            ->first();
        return [
            'offer' => $offer,

        ];
    }
    public function update(Request $request)
    {
        $rules = [
            'title_en' => 'required',
            'title_ar' => 'required',
            'discount_value' => 'required|numeric',
            'menu_item_id' => 'required',
            'expire_in' => 'required',
            'upload_image_en' => 'image|mimes:png,jpg,jpeg|max:2048',
        ];
        $messages = [
            'title_en.required' => 'Item Title (EN) is required.',
            'title_ar.required' => 'Item Title (AR) is required.',
            'discount_value.required' => 'Discount value is required.',
            'menu_item_id.required' => 'Item is required.',
            'expire_in.required' => 'Expire In is required.',
            'upload_image_en.max' => 'The image must be less than 1MB in size',
            'upload_image_en.mimes' => "The image must be of the format jpeg or png",
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if (!$validator->passes()) {
            return response()->json(['status' => 0, 'message' => $validator->errors()->first()]);
        } else {
            $offerData = OfferManagement::with('lang')->where('id', $request->id_pg)->first();
            if (!empty($request->file('upload_image_en'))) {

                $file = $request->file('upload_image_en');
                $image_name_en = $file->getClientOriginalName() . '.' . $file->getClientOriginalExtension();
                $path_en = $file->move(public_path() . '/uploads/restaurant/offer_management/en/', $image_name_en);
            } else {
                if ($request->delete_image_en == '1') {
                    $image_name_en = ' ';
                } else {
                    $image_name_en = $offerData->lang[0]->image_path;
                }
            }
            if (!empty($request->file('upload_image_ar'))) {
                $file = $request->file('upload_image_ar');
                $image_name_ar = $file->getClientOriginalName() . '.' . $file->getClientOriginalExtension();
                $path_en = $file->move(public_path() . '/uploads/restaurant/offer_management/ar/', $image_name_ar);
            } else {
                if ($request->delete_image_ar == '1') {
                    $image_name_ar = ' ';
                } else {
                    $image_name_ar = $offerData->lang[1]->image_path;
                }
            }
            $offer = OfferManagement::where('id', $request->id_pg)
                ->update([
                    'menu_item_id' => $request->menu_item_id,
                    'expire_in' => date('Y-m-d H:i:s', strtotime($request->expire_in)),
                    'discount_type' => $request->discount_type,
                    'discount_value' => $request->discount_value,
                    'coupon_code' => $request->coupon_code,
                ]);

            $offer = OfferManagementLang::where('offer_id', $request->id_pg)
                ->where('language', 'en')
                ->update(
                    [
                        'name' => $request->title_en,
                        'image_path' => $image_name_en,
                    ]
                );
            $offer = OfferManagementLang::where('offer_id', $request->id_pg)
                ->where('language', 'ar')
                ->update(
                    [
                        'name' => $request->title_ar,
                        'image_path' => $image_name_ar,
                    ]
                );

            $msg = "Offer updated successfully";
            if ($offer) {
                return response()->json(['status' => 1, 'message' => $msg]);
            } else {
                return response()->json(['status' => 0, 'message' => 'Sorry something went wrong.']);
            }
        }
    }
    public function statusUpdate(Request $request)
    {
        $status = $request->status === 'Deactivate' ? 'deactive' : 'active';

        OfferManagement::where('id', $request->id)
            ->update([
                'status' => $status
            ]);
        return response()->json(['status' => 1, 'message' => "Status updated successfully"]);
    }

    public function destroy(Request $request)
    {
        if ($request->id) {
            $offer = OfferManagement::find($request->id);
            if (!empty($offer)) {
                $offer->lang()->delete();
                $offer->delete();
                return response()->json(['status' => 1, 'message' => 'Offer deleted successfully']);
            }
        } else {
            return response()->json(['status' => 0, 'message' => 'Sorry something went wrong.']);
        }
    }
}
