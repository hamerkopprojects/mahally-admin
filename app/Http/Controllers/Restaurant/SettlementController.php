<?php

namespace App\Http\Controllers\Restaurant;
use Illuminate\Support\Carbon;
use App\Models\Restaurant;
use App\Models\Order;
use App\Models\CommissionCategory;
use App\Models\PaymentSettlement;
use App\Models\SettlementHistory;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use DB;
use Config;

class SettlementController extends Controller
{
     public function index(Request $request)
    {
         $rest_id = Auth::user()->restuarants_id;
         
         $restaurant = Restaurant::with('lang')->with('contact')->where('id', $rest_id)->first();
         $c_perecent = CommissionCategory::where(['id' => $restaurant->commission_category_id])->pluck('value')->first();

         $total_order_online = Order::select(DB::raw('sum(grand_total) as total'), DB::raw('count(id) as online_count'))
            ->where('order_type', 'dine')
            ->where('restaurant_id', $rest_id)
            ->whereIn('order_status', [5])
            ->where('settled', 'yes')
            ->first();

         $total_order_at_counter = Order::select(DB::raw('sum(grand_total) as total'), DB::raw('count(id) as online_count'))
            ->where('order_type', 'takeaway')
            ->where('restaurant_id', $rest_id)
            ->whereIn('order_status', [5])
            ->where('settled', 'yes')
            ->first();

         $total_orders=Order::where('restaurant_id', $rest_id)
            ->whereIn('order_status', [5])
            ->where('settled', 'yes')->count();

         $total_sale = $total_order_online->total + $total_order_at_counter->total;

         $commmission = $total_sale * $c_perecent / 100;
         $total_revenue = $total_sale - $commmission;
         
         $payment_settlement = PaymentSettlement::where('restaurant_id', $rest_id)->orderBy('id', 'desc')->paginate(10);
         $transaction_type = (Config::get('constants.transaction_type'));
         
         return view('restaurant.payment_settlement.index', compact('restaurant', 'payment_settlement', 'transaction_type',
                 'c_perecent', 'total_order_online', 'total_order_at_counter', 'total_order_at_counter',
                 'total_sale', 'total_orders', 'commmission', 'total_revenue'));
     }
     
    public function filter(Request $request)
    {
        $start = Carbon::parse($request->start_date)->format('Y-m-d');
        $end = Carbon::parse($request->end_date)->format('Y-m-d');

        $rest_id = Auth::user()->restuarants_id;
         
         $restaurant = Restaurant::with('lang')->with('contact')->where('id', $rest_id)->first();
         $c_perecent = CommissionCategory::where(['id' => $restaurant->commission_category_id])->pluck('value')->first();

         $total_order_online = Order::select(DB::raw('sum(grand_total) as total'), DB::raw('count(id) as online_count'))
            ->where('order_type', 'dine')
            ->where('restaurant_id', $rest_id)
            ->whereIn('order_status', [5])
            ->where('settled', 'yes')
            ->whereDate('created_at', '>=', $start)
            ->whereDate('created_at', '<=', $end)
            ->first();

         $total_order_at_counter = Order::select(DB::raw('sum(grand_total) as total'), DB::raw('count(id) as online_count'))
            ->where('order_type', 'takeaway')
            ->where('restaurant_id', $rest_id)
            ->whereIn('order_status', [5])
            ->where('settled', 'yes')
            ->whereDate('created_at', '>=', $start)
            ->whereDate('created_at', '<=', $end)
            ->first();

         $total_orders=Order::where('restaurant_id', $rest_id)
            ->whereIn('order_status', [5])
            ->whereDate('created_at', '>=', $start)
            ->whereDate('created_at', '<=', $end)
            ->where('settled', 'yes')->count();

         $total_sale = $total_order_online->total + $total_order_at_counter->total;

         $commmission = $total_sale * $c_perecent / 100;
         $total_revenue = $total_sale - $commmission;
         
         $payment_settlement = PaymentSettlement::where('restaurant_id', $rest_id)
         ->whereDate('settlement_date', '>=', $start)
         ->whereDate('settlement_date', '<=', $end)->orderBy('id', 'desc')->paginate(10);
         
         $transaction_type = (Config::get('constants.transaction_type'));
         
         return view('restaurant.payment_settlement.details', compact('restaurant', 'c_perecent', 
                 'total_order_online', 'total_order_at_counter', 'total_order_at_counter',
                 'total_sale', 'total_orders', 'commmission', 'total_revenue', 'payment_settlement', 'transaction_type'));
     }
    
}
