<?php

namespace App\Http\Controllers\Restaurant;

use Illuminate\Http\Request;
use App\Mail\sendWelcomeEmail;
use App\Mail\SetPasswordEmail;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Models\RestaurantStaffRoles;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use App\Models\RestaurantKitchenStaff;
use Illuminate\Support\Facades\Validator;

class StaffController extends Controller
{
    protected const ZERO = 0;
    public function get(Request $req)
    {
        $search = $req->search ?? '';
        $rest_id = Auth::user()->restuarants_id;
        $role = RestaurantStaffRoles::get();
        if ($search) {
            $query = RestaurantKitchenStaff::query();
            if ($search) {
                $query->where(function ($sub) use ($search) {
                    $sub->where('name', 'like', "%" . $search . "%");
                    $sub->orWhere('email', 'like', "%" . $search . "%");
                    $sub->orWhere('phone', 'like', "%" . $search . "%");
                    $sub->orWhere('user_id', 'like', "%" . $search . "%");
                });
            }
            $staff = $query->where('restuarants_id', '=', $rest_id)->paginate(20);
            return view('restaurant.staff.index', compact('staff', 'search', 'role'));
        } else {
            $staff = RestaurantKitchenStaff::with('roles')->where('restuarants_id', '=', $rest_id)->paginate(20);
            return view('restaurant.staff.index', compact('staff', 'search', 'role'));
        }
    }
    public function store(Request $req)
    {
        $rules = [
            'name' => 'required',
            'email' => 'required|unique:kitchen_staff,email',
            'phone' => 'required|unique:kitchen_staff,phone',
            'role' => 'required',
        ];
        $messages = [
            'name.required' => 'Name is required.',
            'email.required' => 'Email is required.',
            'email.unique' => 'Staff with same email already exists',
            'phone.required' => 'Phone number is required.',
            'phone.unique' => 'Staff with same phone number already exists',
            'role.required' => 'Role is required.',
        ];
        $rest_id = Auth::user()->restuarants_id;
        $validator = Validator::make($req->all(), $rules, $messages);
        if (!$validator->passes()) {

            return response()->json(['status' => 0, 'message' => $validator->errors()->first()]);
        } else {

            $user = $this->generateUserId();
            // $password = mt_rand(100000, 999999);
            $password = '123456';
            $userData = RestaurantKitchenStaff::create([
                'name' => $req->name,
                'email' => $req->email,
                'phone' => (int)$req->phone,
                'user_id' => $user,
                'restuarants_id' => $rest_id,
                'status' => 'active',
                'password' => Hash::make($password),
                'role_id' => $req->role,
            ]);


            $msg = "Staff added successfully";
            if ($userData) {
                return response()->json(['status' => 1, 'message' => $msg]);
                $user = RestaurantKitchenStaff::where('id', $userData->id)->first();
                $data = [
                    'password' => $password,
                    'user' => $user
                ];
                Mail::to($user->email)->send(new SetPasswordEmail($data));
            } else {
                return response()->json(['status' => 0, 'message' => 'Sorry something went wrong.']);
            }
        }
    }
    public function edit($id)
    {
        $users = RestaurantKitchenStaff::where('id', $id)->first();
        return [
            'user' => $users
        ];
    }
    public function generateUserId()
    {
        $lastUser = RestaurantKitchenStaff::select('user_id')
            ->orderBy('id', 'desc')
            ->first();

        $lastId = 0;

        if ($lastUser) {
            $lastId = (int) substr($lastUser->user_id, 9);
        }
        $lastId++;
        $userId = 'MAH-0000-' . str_pad($lastId, 5, '0', STR_PAD_LEFT);
        return $userId;
    }

    public function update(Request $req)
    {
        $rules = [
            'name' => 'required',
            'email' => 'required|unique:kitchen_staff,email,' . $req->user_unique,
            'phone' => 'required|unique:kitchen_staff,phone,' . $req->user_unique,
            'role' => 'required',
        ];
        $messages = [
            'name.required' => 'Name is required.',
            'email.required' => 'Email is required.',
            'email.unique' => 'Staff with same email already exists',
            'phone.required' => 'Phone number is required.',
            'phone.unique' => 'Staff with same phone number already exists',
            'role.required' => 'Role is required.',
        ];
        $validator = Validator::make($req->all(), $rules, $messages);
        if (!$validator->passes()) {

            return response()->json(['status' => 0, 'message' => $validator->errors()->first()]);
        } else {
            $userData = RestaurantKitchenStaff::where('id', $req->user_unique)
                ->update([
                    'name' => $req->name,
                    'email' => $req->email,
                    'phone' => (int)$req->phone,
                    'role_id' => $req->role,
                ]);
            $msg = "Staff details updated successfully";
            return response()->json(['status' => 1, 'message' => $msg]);
        }
    }

    public function statusUpdate(Request $req)
    {
        if ($req->status == 'Deactivate') {
            $status = 'deactive';
        } else {
            $status = 'active';
        }

        $statusUpdate = RestaurantKitchenStaff::where('id', $req->id)
            ->update([
                'status' => $status
            ]);
        return response()->json(['status' => 1, 'message' => "Status updated successfully"]);
    }

    public function destroy(Request $req)
    {
        if ($req->id) {
            $user = RestaurantKitchenStaff::find($req->id);
            if (!empty($user)) {
                $user->delete();
                return response()->json(['status' => 1, 'message' => 'Staff deleted successfully']);
            }
        } else {
            return response()->json(['status' => 0, 'message' => 'Sorry something went wrong.']);
        }
    }

    public function welcomeSend(Request $req)
    {

        $user = RestaurantKitchenStaff::where('id', $req->id)->first();
        $password = mt_rand(100000, 999999);
        $data = [
            'password' => Hash::make($password),
            'user' => $user
        ];
        $email = Mail::to($user->email)->send(new sendWelcomeEmail($data));
        if ($email) {
            return response()->json(['status' => 1, 'message' => 'Welcome mail send successfully']);
        } else {
            return response()->json(['status' => 0, 'message' => 'Sorry something went wrong.']);
        }
    }
}
