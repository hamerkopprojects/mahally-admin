<?php

namespace App\Http\Controllers\Api\Kitchen;

use App\Models\Order;
use App\Models\OrderLog;
use App\Models\Customers;
use App\Models\CookingTime;
use Illuminate\Http\Request;
use App\Traits\CanSaveNotification;
use App\Traits\CanSendNotification;
use App\Http\Controllers\Controller;
use App\Http\Responses\ErrorResponse;
use Illuminate\Support\Facades\Config;
use App\Http\Responses\SuccessWithData;

class KitchenOrderController extends Controller
{
    use CanSaveNotification, CanSendNotification;
    protected $order;

    public function __construct(Order $order)
    {
        $this->order = $order;
    }
    public function ActiveOrders()
    {
        $order = Order::with(['order_item' => function ($q) {
            $q->with(['menu_items' => function ($q) {
                $q->with('lang:id,menu_item_id,name,description,language')
                    ->select('id', 'restuarants_id', 'menu_cate_id', 'calory', 'price', 'discount_price');
            }])->with(['order_item_addon' => function ($q) {
                $q->with(['resturant_addons' => function ($q) {
                    $q->with('lang');
                }]);
            }])
                ->select('id', 'order_id', 'item_id', 'item_price', 'item_count', 'grant_total', 'sub_total', 'tax_total');
        }])->with(['customer' => function ($q) {
            $q->select('id', 'name', 'email', 'phone_number');
        }])->where('order_status', '2')
            ->OrWhere('order_status', '3')
            ->orderBy('created_at', 'desc')
            ->where('restaurant_id', auth()->user()->restuarants_id)
            ->get();

        if (!$order) {
            return new ErrorResponse(trans('kitchen-messages.orders.no_orders'));
        }
        return new SuccessWithData([
            'orders' => $order,
            'msg' => trans('kitchen-messages.orders.success_orders'),
        ]);
    }
    public function ChangeOrderStatus(Request $request)
    {
        $order = $this->order::where('id', $request->id)
            ->update([
                'order_status' => $request->order_status,
            ]);
        $ord_status = (Config::get('constants.frontdisk_order_status'));

        if (!$order) {
            return new ErrorResponse(trans('kitchen-messages.orders.error_change_status'));
        } else {
            OrderLog::create([
                'order_id' => $request->id,
                'log' => auth()->user()->name . " (Kitchen Staff) marked order as " . $ord_status[$request->order_status] ?? '',
                'user_id' => auth()->user()->id,
                'done_by' => 'kitchen',
                'created_at' => now(),
                'updated_at' => now(),
            ]);
        }
        if ($request->order_status == 4) {
            $this->notifyUser($request->id, $request->order_status);
        }
        return new SuccessWithData([
            'orders' => $order,
            'msg' => trans('kitchen-messages.orders.success_change_status'),
        ]);
    }
    public function cookingTime()
    {
        $cooking_time = CookingTime::all();
        return new SuccessWithData([
            'cooking_time' => $cooking_time
        ]);
    }
    public function ReadyForPickupOrders()
    {
        $order = Order::with(['order_item' => function ($q) {
            $q->with(['menu_items' => function ($q) {
                $q->with('lang:id,menu_item_id,name,description,language')
                    ->select('id', 'restuarants_id', 'menu_cate_id', 'calory', 'price', 'discount_price');
            }])->with(['order_item_addon' => function ($q) {
                $q->with(['resturant_addons' => function ($q) {
                    $q->with('lang');
                }]);
            }])
                ->select('id', 'order_id', 'item_id', 'item_price', 'item_count', 'grant_total', 'sub_total', 'tax_total');
        }])->with(['customer' => function ($q) {
            $q->select('id', 'name', 'email', 'phone_number');
        }])->where('order_status', '4')
            ->orderBy('created_at', 'desc')
            ->where('restaurant_id', auth()->user()->restuarants_id)
            ->get();

        if (!$order) {
            return new ErrorResponse(trans('kitchen-messages.orders.no_orders'));
        }
        return new SuccessWithData([
            'orders' => $order,
            'msg' => trans('kitchen-messages.orders.success_orders'),
        ]);
    }
    public function Delivered()
    {
        $order = Order::with(['order_item' => function ($q) {
            $q->with(['menu_items' => function ($q) {
                $q->with('lang:id,menu_item_id,name,description,language')
                    ->select('id', 'restuarants_id', 'menu_cate_id', 'calory', 'price', 'discount_price');
            }])->with(['order_item_addon' => function ($q) {
                $q->with(['resturant_addons' => function ($q) {
                    $q->with('lang');
                }]);
            }])
                ->select('id', 'order_id', 'item_id', 'item_price', 'item_count', 'grant_total', 'sub_total', 'tax_total');
        }])->with(['customer' => function ($q) {
            $q->select('id', 'name', 'email', 'phone_number');
        }])->where('order_status', '5')
            ->orderBy('created_at', 'desc')
            ->where('restaurant_id', auth()->user()->restuarants_id)
            ->get();

        if (!$order) {
            return new ErrorResponse(trans('kitchen-messages.orders.no_orders'));
        }
        return new SuccessWithData([
            'orders' => $order,
            'msg' => trans('kitchen-messages.orders.success_orders'),
        ]);
    }
    public function OrderStatus()
    {
        $stat = (Config::get('constants.kitchen_order_status'));
        $ord_status = trans("kitchen-messages.order_status.{$stat}");

        return new SuccessWithData([
            'order_status' => $ord_status,
        ]);
    }
    public function ActiveOrderIdList(Request $request)
    {
        $order_id = $this->order::select('order_id')
            ->where('order_id', 'like', "%" . $request->key . "%")
            ->where('order_status', '2')
            ->OrWhere('order_status', '3')
            ->orderBy('created_at', 'desc')
            ->where('restaurant_id', auth()->user()->restuarants_id)->get();
        if (!$order_id) {
            return new ErrorResponse(trans('kitchen-messages.orders.no_orders'));
        }
        return new SuccessWithData([
            'order_id' => $order_id,
        ]);
    }
    public function ReadyForPickupOrderIdList(Request $request)
    {
        $order_id = $this->order::select('order_id')
            ->where('order_id', 'like', "%" . $request->key . "%")
            ->where('order_status', '4')
            ->orderBy('created_at', 'desc')
            ->where('restaurant_id', auth()->user()->restuarants_id)->get();
        if (!$order_id) {
            return new ErrorResponse(trans('kitchen-messages.orders.no_orders'));
        }
        return new SuccessWithData([
            'order_id' => $order_id,
        ]);
    }
    public function DeliveredOrderIdList(Request $request)
    {
        $order_id = $this->order::select('order_id')
            ->where('order_id', 'like', "%" . $request->key . "%")
            ->where('order_status', '5')
            ->orderBy('created_at', 'desc')
            ->where('restaurant_id', auth()->user()->restuarants_id)->get();
        if (!$order_id) {
            return new ErrorResponse(trans('kitchen-messages.orders.no_orders'));
        }
        return new SuccessWithData([
            'order_id' => $order_id,
        ]);
    }
    public function SearchOrdersList(Request $request)
    {
        $order = Order::with(['order_item' => function ($q) {
            $q->with(['menu_items' => function ($q) {
                $q->with('lang:id,menu_item_id,name,description,language')
                    ->select('id', 'restuarants_id', 'menu_cate_id', 'calory', 'price', 'discount_price');
            }])->with(['order_item_addon' => function ($q) {
                $q->with(['resturant_addons' => function ($q) {
                    $q->with('lang');
                }]);
            }])
                ->select('id', 'order_id', 'item_id', 'item_price', 'item_count', 'grant_total', 'sub_total', 'tax_total');
        }])->with(['customer' => function ($q) {
            $q->select('id', 'name', 'email', 'phone_number');
        }])->where('order_id', $request->order_id)
            ->orderBy('created_at', 'desc')
            ->where('restaurant_id', auth()->user()->restuarants_id)
            ->get();

        if (!$order) {
            return new ErrorResponse(trans('frontdisk-messages.orders.no_orders'));
        }
        return new SuccessWithData([
            'orders' => $order,
            'msg' => trans('frontdisk-messages.orders.success_orders'),
        ]);
    }
    public function UpdateCookingTIme(Request $request)
    {
        $order = $this->order::where('id', $request->id)
            ->update([
                'cooking_time' => $request->cooking_time,
            ]);
        if (!$order) {
            return new ErrorResponse(trans('kitchen-messages.orders.error_update_cooking_time'));
        } else {
            OrderLog::create([
                'order_id' => $request->id,
                'log' => auth()->user()->name . " (Kitchen Staff) updating cooking time",
                'user_id' => auth()->user()->id,
                'done_by' => 'kitchen',
                'created_at' => now(),
                'updated_at' => now(),
            ]);
        }
        return new SuccessWithData([
            'msg' => trans('kitchen-messages.orders.success_update_cooking_time'),
        ]);
    }
    public function orderView(Request $request)
    {
        $order = Order::with(['order_item' => function ($q) {
            $q->with(['menu_items' => function ($q) {
                $q->with('lang:id,menu_item_id,name,description,language')
                    ->select('id', 'restuarants_id', 'menu_cate_id', 'calory', 'price', 'discount_price');
            }])->with(['order_item_addon' => function ($q) {
                $q->with(['resturant_addons' => function ($q) {
                    $q->with('lang');
                }]);
            }])
                ->select('id', 'order_id', 'item_id', 'item_price', 'item_count', 'grant_total', 'sub_total', 'tax_total');
        }])->with(['customer' => function ($q) {
            $q->select('id', 'name', 'email', 'phone_number');
        }])->where('order_id', $request->order_id)
            ->first();

        if (!$order) {
            return new ErrorResponse(trans('frontdisk-messages.orders.no_orders'));
        }
        return new SuccessWithData([
            'orders' => $order,
            'msg' => trans('frontdisk-messages.orders.success_orders'),
        ]);
    }
    protected function notifyUser($order_id, $or_status)
    {
        $order = Order::find($order_id);
        $owner = $order->customer;
        $language = $owner[0]->language;

        $title = trans("userapp-messages.order.title.ready_delivery", [
            'name' => $owner[0]->name
        ], $language);

        $body = [
            'en' => $this->getContent($order, 'ready_delivery', 'en'),
            'ar' => $this->getContent($order, 'ready_delivery', 'ar'),
        ];

        $content = [
            'title' => $title,
            'body' => $body[$language],
            'type' => 'order',
            'order_id' => $order->id,
            'order_uniq' => $order->order_id,
            'time' => now()->format('Y-m-d H:i:s')
        ];
        $val = $this->saveNotification(
            $body,
            'admin',
            'userapp',
            [
                'type' => 'order',
                'order_id' => $order->id,

            ],
            $owner[0]->id
        );
        if ($owner[0]->need_notification == 1 &&  $owner[0]->fcm_token) {
            $this->sendNotification($owner[0]->fcm_token, $title, $content, 0);
            $badge = Customers::where('id', $owner[0]->id)->update([
                'badge_seen' => 'true'
            ]);
        }
    }
    protected function getContent($order, $status, $lang)
    {
        return  trans(
            "userapp-messages.order.content.ready_delivery",
            [

                'order_id' => $order->order_id
            ],
            $lang
        );
    }
}
