<?php

namespace App\Http\Controllers\Api\Frontoffice;

use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use App\Http\Responses\ErrorResponse;
use App\Models\RestaurantKitchenStaff;
use App\Http\Responses\SuccessWithData;
use App\Http\Responses\SuccessResponseMessage;
use App\Http\Requests\Api\Kitchen\ResetPasswordRequest;

class FrontDiskLoginController extends Controller
{
    protected $staff;

    public function __construct(RestaurantKitchenStaff $staff)
    {
        $this->staff = $staff;
    }
    public function login(Request $request)
    {
        $user = $this->staff
            ->where('email', $request->email)
            // ->where('password', $request->password)
            ->where('status', 'active')
            ->where('role_id', '2')
            ->select('id','restuarants_id','name','user_id','email','phone','status','password')
            ->first();
            if (!$user) {
                return new ErrorResponse(trans('frontdisk-messages.login.user_nt_exist'));
            } else{
                if (!Hash::check($request->password,$user->password)) {
                    return response()->json(['success'=>false, 'message' => trans('kitchen-messages.login.pass_incorrect')]);
                 }
                 else {
                    $token = $user->createToken('Api Token', ['frontdesk'])->accessToken;
                        return new SuccessWithData([
                            'user' => $user,
                            'access_token' => $token,
                        ]);
                 }
                }
        
    }
    public function forgotPassword(Request $request)
    {
        $staff = $this->staff
            ->where('email', $request->email)
            ->where('status', 'active')
            ->where('role_id', '2')
            ->first();
        if (!$staff) {
            return new ErrorResponse(trans('frontdisk-messages.login.invalid_email'));
        } else {
            // $veri_code = mt_rand(100000, 999999);
            $veri_code = '123456';
            $updateData = RestaurantKitchenStaff::where('id', $staff->id)
                ->update([
                    'verification_code' => $veri_code,
                    'email_verified_at' => Carbon::now()
                ]);
            if ($updateData) {
                // Mail::to($staff->email)->send(new verificationCodeEmail($staff));
                return new SuccessResponseMessage(trans('frontdisk-messages.login.send_varification_msg'));
            }
        }
    }
    public function verifyCode(Request $request)
    {
        $staff = $this->staff
            ->where('email', $request->email)
            ->where('verification_code', $request->verification_code)
            ->where('status', 'active')
            ->where('role_id', '2')
            ->first();
        if (!$staff) {
            return new ErrorResponse(trans('frontdisk-messages.login.invalid_verify_msg'));
        }
        return new SuccessWithData([
            'user' => $staff,
            'msg' => trans('frontdisk-messages.login.success_verify_msg'),
        ]);
    }
    public function resetPassword(ResetPasswordRequest $request)
    {
        RestaurantKitchenStaff::where('id', $request->id)
            ->update([
                'password' => Hash::make($request->password),
            ]);
        return new SuccessResponseMessage(trans('frontdisk-messages.login.reset_pwd_success'));
    }
    public function logout()
    {
        $user = auth()->user();
        $user->token()->revoke();
        return new SuccessResponseMessage(trans('frontdisk-messages.login.logout'));
    }
}
