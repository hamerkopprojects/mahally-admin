<?php

namespace App\Http\Controllers\Api\Userapp;

use Illuminate\Http\Request;
use App\Models\SupportTickets;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Http\Responses\SuccessWithData;
use App\Http\Responses\SuccessResponseMessage;
use App\Http\Requests\Api\Userapp\SupportRequest;
use App\Http\Requests\Api\Userapp\SupportChatRequest;

class SupportRequestController extends Controller
{
    protected $support;

    public function __construct(SupportTickets $support)
    {
        $this->support = $support;
    }

    public function index(SupportRequest $request)
    {
        $support = DB::transaction(function() use ($request) {
            $support = $this->support->create([
                'subject' => $request->subject,
                'message' => $request->message,
                'app_type' => 'userapp',
                'submitted_by' => auth()->id(),
            ]);

            $support->lang()->create([
                'message' => $request->message,
                'user_type' => 'userapp',
            ]);

            return $support;
        });

       
        return new SuccessResponseMessage(trans('userapp-messages.support.success_msg.support_success'));
    }
    public function supportChat($id)
    {
        $request = $this->support
            ->where('id', $id)
            ->where('submitted_by', auth()->id())
            ->firstOrFail();
            
        $chatHistory = $request->lang()
            ->whereIn('user_type', ['userapp', 'admin'])
            ->select('id', 'message', 'user_type')
            ->orderBy('created_at', 'asc')
            ->get();

        return new SuccessWithData($chatHistory);
    }
    public function sendChat(SupportChatRequest $req)
    {
        $supportRequest = $this->support
            ->where('app_type', 'userapp')
            ->where('submitted_by', auth()->id())
            ->where('id', $req->request_id)
            ->firstOrFail();

        DB::transaction(function () use ($req,$supportRequest ) {
            $supportRequest->lang()->create([
                'message' => $req->message,
                'user_type' => 'userapp',
            ]);

            $supportRequest->touch();
        });

        return new SuccessResponseMessage('New message sent successfully');

    }
}
