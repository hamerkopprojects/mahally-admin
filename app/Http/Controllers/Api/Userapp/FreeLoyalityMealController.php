<?php

namespace App\Http\Controllers\Api\Userapp;

use Carbon\Carbon;
use App\Models\FreeMeals;
use App\Models\OrderItems;
use App\Models\Restaurant;
use Illuminate\Http\Request;
use App\Models\OfferLoyality;
use App\Traits\FormatLanguage;
use App\Models\ProductLoyality;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Http\Responses\SuccessWithData;

class FreeLoyalityMealController extends Controller
{
   use FormatLanguage ;
    public function getMeal(Request $req)
    {
        $lat = $req->latitude ?? 0;
        $long = $req->longitude ?? 0;
        // $rest_id = '';
        // if($req->latitude && $req->longitude)
        // {
        //     $rest_id = Restaurant::where(['latitude'=> $req->latitude,'longitude'=>$req->longitude])
        //                     ->pluck('id')
        //                     ->toArray();
        // }
        $meals = FreeMeals::with('lang')
                ->with(['retaurant'=>function($q) use($lat, $long){
                    $q->with('lang:id,restuarants_id,name,description,language')
                    ->select('id','location','latitude','longitude')
                    ->selectRaw('ROUND( 6371 * acos( cos( radians(?) ) *
                cos( radians(latitude ) )
                * cos( radians(longitude ) - radians(?)
                ) + sin( radians(?) ) *
                sin( radians(latitude ) ) )
                ,2) AS distance', 
                [$lat, $long, $lat]
                 )
                ;
                }])
                ->with(['item'=>function($q){
                    $q->with('lang');
                }])
                ->whereDate('expire_in','>=',Carbon::now())
                ->where('status','active')
                ->get()
                ->sortBy('retaurant.distance');
        // $language = auth()->user()->language ?? 'en';
        $meals->each(function($meal) use($req){
            $meal->distance = $meal->retaurant->distance ?? 0 ;
          $val_loya =
          ProductLoyality::where([
              'customer_id' =>$req->cust_id,
              'restaurant_id'=>$meal->restuarants_id,
              'product_id'=>$meal->menu_item_id
          ])
          ->first();
          $already_done = $val_loya ? $val_loya->product_count : 0 ;
          $laoyality_offer = OfferLoyality::where([
            'product_id'=>$meal->menu_item_id,
            'offer_id'=>$meal->id,
            'customer_id' =>$req->cust_id,
        ])->first();
        //    OrderItems::join('orders','orders.id','=','order_items.order_id')
        //     ->where('orders.customer_id',$req->cust_id)
        //     ->where('orders.restaurant_id',$meal->restuarants_id)
        //     ->where('order_items.item_id',$meal->menu_item_id)
        //     ->count();
            
            // $meal->order_alrady_done = ($already_done <= $meal->order ) ? $already_done : "completed ";
            $offer = (int)($already_done / $meal->order) ;
           
            if($laoyality_offer){
                $taken =  $laoyality_offer->loyality_taken ?? 0 ;
                $laoyality_offer->update([
                    'loyality_count_available'=>  $offer - $taken,
                ]);
            }else{
                DB::table('offer_loyality')->insert([
                    'product_id'=>$meal->menu_item_id,
                    'offer_id'=>$meal->id,
                    'customer_id' =>$req->cust_id,
                    'loyality_count_available'=> $offer ?? 0
                ]);
            }
            
            $remain = $already_done % $meal->order ;
            // $meal->order - $already_done ;
            $meal->remain = $remain ;
            $meal->already_done = $already_done;
            $alreadt_take_loyality = $laoyality_offer ? $laoyality_offer->loyality_taken : 0 ;
            $already_available = $laoyality_offer ? $laoyality_offer->loyality_count_available : 0 ;
            $meal->no_offer_available =  $already_available ;
            $meal->remaining_orders = ($remain > 0  &&  $remain <= $meal->order )? trans('userapp-messages.restaurant.loyality_meal',[
                'remaining'=>$meal->order - $remain
            ]) : null ;

            $meal->time_status = $this->restaurantStatus($meal->restuarants_id);
        });
        $meals->map(function ($data){
            // dd($data->item);
            if($data->item)
            $data->item->cover_photo = $data->item  ? asset('uploads/' . $data->item->cover_photo) : null;
        });
        $this->keyByNested($meals, 'lang', 'language');
        
        return new SuccessWithData($meals);
    }

    protected function restaurantStatus($rest_id)
    {
        $details = Restaurant::where('id',$rest_id)->first();
        $time_open =optional($details->opening_time)->format('H:i:s');
        // dd($time_open);
        $current_time = Carbon::now()->timezone('Asia/Riyadh')->format('H:i:s');
                $time_close =optional($details->closing_time)->format('H:i:s');
                // createFromTimeString($details->closing_time);
                // Carbon::parse($details->closing_time);
                // ->format('H:i:s') ;
                $details->closing_time = $time_close;
                $details->opening_time =$time_open ;
                // dd($time_close);
                if($current_time >= $time_open && $current_time < $time_close)
                {
                    $diff_min = Carbon::parse($details->closing_time)->diffInMinutes(Carbon::now()) ;
                    if( $diff_min <= 30 ){

                        $time_stat = 'Closing in '.$diff_min .' minutes';
                    }else{
                        $time_stat = 'OPEN';
                    }
                   
                }else{
                   $time_stat = 'CLOSED';
                }

                return $time_stat ;
    }
}
