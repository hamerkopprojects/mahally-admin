<?php

namespace App\Http\Controllers\Api\Userapp;

use Carbon\Carbon;
use App\Models\MenuItems;
use Illuminate\Http\Request;
use App\Traits\FormatLanguage;
use App\Models\AttributesValue;
use App\Models\MenuItemAttributes;
use App\Http\Controllers\Controller;
use App\Http\Responses\SuccessWithData;
use App\Models\AutoSuggest;
use App\Models\RestaurantAttributesValues;
use Illuminate\Support\Facades\DB;

class ItemDetailsPageController extends Controller
{
    use FormatLanguage ;

    protected $items;

    public function __construct(MenuItems $items)
    {
        $this->items = $items;
    }

    public function getItems($id)
    {
        $details = $this->items->with('lang')
                ->with('photos:id,menu_item_id,image_path')
                ->with('timing')
                ->with(['item_ingredients'=>function($q){
                        $q->with('lang:id,name,language,ingredient_id')
                        ->withPivot(['menu_item_id', 'ingredient_id','deleted_at'])
                        ->where('menu_items_ingredients.deleted_at', null);
                        // ->where('status','active');
                }])
                ->with(['item_addons'=>function($q){
                    $q->with('lang:id,name,language,addon_id')
                   ->withPivot(['menu_item_id', 'addon_id','deleted_at'])
                   ->where('menu_items_addons.deleted_at', null);
                        // ->where('status','active');
                }])
                ->with(['item_attributes'=>function($q){
                    $q->withPivot(['menu_item_id', 'attribute_id','attribute_value_id','deleted_at'])
                    ->with('lang:id,name,language,rest_attribute_id')
                    ->where('menu_items_attributes.deleted_at', null);

                    // ->where('status','active');
                }])
                // ->with(['item_attributes_val'=>function($q){
                //     $q->withPivot(['menu_item_id', 'attribute_id','attribute_value_id'])
                //     ->with('lang:id,name,language,rest_attribute_value_id');
                // }])
                ->where('id',$id)
                ->first();
                $details->cover_photo = $details->cover_photo ? asset('uploads/'.$details->cover_photo) : null ;
                $details->photos && $details->photos->each(function($photo){
                    $photo->image_path  =  $photo->image_path  ? asset('uploads/'.$photo->image_path) : null ;
                });
             $details->item_ingredients->each(function ($item_ingre){
                 if($item_ingre->type == 'By Restaurant'){
                    
                    $item_ingre->image_path =  $item_ingre->image_path ? asset('/uploads/restaurant/ingredients/'.$item_ingre->image_path) : null ;
                 }else{
                    //  dd($item_ingre);
                   $auto_sugg =  AutoSuggest::where('id',$item_ingre->autosuggest_id)->where('deleted_at',null)->first();
                    $item_ingre->image_path =  $auto_sugg ? asset('/uploads/autosuggest/'.$auto_sugg->image_path) : null ;
                 }
               
            });
            $details->item_addons->each(function ($item_ingre){
                if($item_ingre->type == 'By Restaurant')
                {
                    $item_ingre->image_path  = $item_ingre->image_path ? asset('/uploads/restaurant/addons/'.$item_ingre->image_path) : null;

                }else{
                    $auto_sugg =  AutoSuggest::where('id',$item_ingre->autosuggest_id)->where('deleted_at',null)->first();
                    $item_ingre->image_path  = $auto_sugg ? asset('/uploads/autosuggest/'.$auto_sugg->image_path) : null;
                }
               
              
           });
           $details->item_attributes->each(function ($attrribute) use($id){ 
             $val =  RestaurantAttributesValues::with('lang:id,name,language,rest_attribute_value_id')
                    ->where('id',$attrribute->pivot->attribute_value_id)
                    ->first();
            $atr_val =  $attrribute->attribute_val = $val ? $val : null ;
            return $atr_val ;
       });
       
        $this->keyBy($details, 'lang', 'language');
        $this->keyByNested($details->item_addons, 'lang', 'language');
        $this->keyByNested($details->item_attributes, 'lang', 'language');
        $this->keyByNested($details->item_ingredients, 'lang', 'language');
        // $this->keyByNested($details->item_attributes_val, 'lang', 'language');
        $rela_item_ids = DB::table('menu_items_related_item')
                            ->where('menu_item_id',$id)
                            ->where('deleted_at',null)
                            ->pluck('related_item_id')
                            ->toArray();
        $relatedItems =MenuItems::with('lang')
        ->with(['timing' =>function($q){
            $q->where('menu_items_available_time.deleted_at','=',null);
        }])
        ->with(['item_ingredients'=>function($q){
                $q->with('lang');
                // ->where('status','active');
        }])
        ->with(['item_addons'=>function($q){
            $q->with('lang')
            ;
                // ->where('status','active');
        }])
        ->with(['item_attributes'=>function($q){
            $q->with('lang');
            // ->where('status','active');
        }])
        // ->where('menu_cate_id', $details->menu_cate_id)
        ->whereIn('menu_items.id',$rela_item_ids)
        ->get();
        $this->keyByNested($relatedItems, 'lang', 'language');

        $relatedItems->each(function($details){
            $details->timing->map(function($time){
                $time->end_time = $time->end_time ?  Carbon::parse($time->end_time)->format('h:i A') : null ;
                $time->start_time = $time->start_time ?  Carbon::parse($time->start_time)->format('h:i A') : null ;
            });
            $details->cover_photo= $details->cover_photo ? asset('uploads/'.$details->cover_photo) : null ;
            $this->keyByNested($details->item_addons, 'lang', 'language');
        $this->keyByNested($details->item_attributes, 'lang', 'language');
        $this->keyByNested($details->item_ingredients, 'lang', 'language');
        });
         
        return new SuccessWithData([
            'details'=>$details,
            'related'=> $relatedItems
        ]);

    }
    
}
