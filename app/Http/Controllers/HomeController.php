<?php

namespace App\Http\Controllers;

use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
         User::where('id', Auth::user()->id)
                ->update([
                    'ip_address' => request()->ip(),
                    'last_login_date' => Carbon::now(),
                ]);

        return redirect()->route('dashboard.get');
    }
}
