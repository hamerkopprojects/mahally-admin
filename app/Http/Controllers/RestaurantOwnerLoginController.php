<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class RestaurantOwnerLoginController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function login()
    {
        return view('restaurantowner.auth.login');
    }

    public function restaurantLogin(Request $request)
    {
        if (Auth::guard('restaurant')->attempt(['email' => $request->email, 'password' => $request->password])) {

            return redirect()->intended('/dashboard');
        } else {
            return redirect()->back()->withErrors(['email' => 'Email/Password Incorrect!']);
        }
    }
    public function logout()
    {
        Auth::logout();
        Session::flash('success', 'Logout successfully.');
        return redirect()->route('owner.login');
    }
}
