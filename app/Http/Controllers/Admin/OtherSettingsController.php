<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\OtherSettings;
use App\Http\Controllers\Controller;

class OtherSettingsController extends Controller
{
    public function get()
    {
        $other = OtherSettings::first();

        return view('admin.settings.other.index', compact('other'));
    }

    public function store(Request $request)
    {
        $other = OtherSettings::first();
        $data = [
            'currency_en' => $request->currency_en,
            'currency_ar' => $request->currency_ar,
            'earning_amount' => $request->earning_amount,
        ];
        if (!empty($other)) {
            $setting_save = OtherSettings::where('id', $other->id)->update(
                $data
            );
            return redirect()->route('other.get')->with('success', 'Other Settings Updated Successfully');
        } else {
            $setting_save = OtherSettings::Create(
                $data
            );
            return redirect()->route('other.get')->with('success', 'Other setting Added Successfully');
        }
    }
}
