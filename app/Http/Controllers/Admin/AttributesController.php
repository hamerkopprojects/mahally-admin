<?php

namespace App\Http\Controllers\Admin;

use App\Models\Attributes;
use Illuminate\Http\Request;
use App\Models\AttributesLang;
use App\Models\AttributesValue;
use Illuminate\Support\Facades\DB;
use App\Models\AttributesValueLang;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class AttributesController extends Controller
{
    public function get(Request $request)
    {
        $search = $request->faq_select;
        $search_field = $request->search_field ?? '';

        $attributeLang = AttributesLang::where('language', 'en')->get();

        if ($search_field) {
            $data = AttributesLang::select('attribute_id')->where('name', 'like', "%" . $search_field . "%")->get()->toArray();
            $da = [];
            foreach ($data as  $d) {
                $da[] = $d['attribute_id'];
            }
            $attribute = Attributes::with('lang')->with('attr_value')->whereIn('id', $da)->paginate(20);
        } else {
            $attribute = Attributes::with('lang')->with('attr_value')->paginate(20);
        }

        return view('admin.settings.attributes.index', compact('attribute', 'attributeLang', 'search_field'));
    }
    public function create(Request $req)
    {
        $_attribute_id = $req->id;
        $row_data = $lang_array = array();
        if ($_attribute_id != '') {
            $row_data = Attributes::with('lang')->where('id', $req->id)->first();
            $variant = AttributesValue::leftJoin('attributes_value_i18n', 'attributes_value_i18n.attribute_value_id', '=', 'attributes_value.id')
                ->select('attributes_value_i18n.id', 'attributes_value_i18n.name', 'attributes_value_i18n.attribute_value_id as attr_id')
                ->where('attributes_value.attribute_id', '=', $row_data['id'])
                ->where('attributes_value_i18n.language', '=', 'en')
                ->where('attributes_value_i18n.deleted_at', NULL)
                ->where('attributes_value.deleted_at', NULL)
                ->get(['id', 'attr_id', 'name']);
            foreach ($variant as $val) {
                $var_ar = AttributesValueLang::where(['en_matching_id' => $val['id'], 'attribute_value_id' => $val['attr_id'], 'language' => 'ar', 'deleted_at' => NULL])->first();
                $variant_array['en_name'] = $val['name'];
                $variant_array['en_variant_id'] = $val['id'];
                $variant_array['ar_name'] = $var_ar['name'];
                $variant_array['ar_variant_id'] = $var_ar['id'];
                $lang_array[] = $variant_array;
            }
        }

        $data = [
            'row_data' => $row_data,
            'variant_data' => $lang_array,
        ];

        return view('admin.settings.attributes.create', $data);
    }

    public function save(Request $req)
    {
        $rules = [
            'name_en' => 'required',
            'name_ar' => 'required',
        ];
        $messages = [
            'name_en.required' => 'Name(EN) is required.',
            'name_ar.required' => 'Name(AR) is required.',
        ];
        $validator = Validator::make($req->all(), $rules, $messages);
        if (!$validator->passes()) {
            return response()->json(['status' => 0, 'message' => $validator->errors()->first()]);
        } else {
            $variant_array = ([
                [
                    'name' => $req->att_value_en,
                    'language' => 'en',
                ],
                [
                    'name' => $req->att_value_ar,
                    'language' => 'ar',
                ],
            ]);
            if (empty($req->_attribute_id)) {
                $attribute = DB::transaction(function () use ($req) {
                    $attribute = Attributes::create([
                        'status' => 'active',
                        'is_mandatory' => $req->is_mandatory,
                    ]);
                    $attribute->lang()->createMany([
                        [
                            'name' => $req->name_en,
                            'language' => 'en',
                        ],
                        [
                            'name' => $req->name_ar,
                            'language' => 'ar',
                        ],
                    ]);

                    return $attribute;
                });


                $variant = DB::transaction(function () use ($req, $attribute) {
                    $variant = AttributesValue::create([
                        'attribute_id' => $attribute->id,
                    ]);

                    foreach ($req->att_value_en as $key => $var_value) {
                        $variant_lang = AttributesValueLang::create([
                            'attribute_value_id' => $variant->id,
                            'name' => $var_value,
                            'language' => 'en',
                        ]);
                        AttributesValueLang::create([
                            'attribute_value_id' => $variant->id,
                            'en_matching_id' => $variant_lang->id,
                            'name' => $req->att_value_ar[$key],
                            'language' => 'ar',
                        ]);
                    }
                    return $variant;
                });
                $msg = "Attribute added successfully";
            } else {
                $get_attribute = Attributes::where('id', $req->_attribute_id)->first();

                $attribute = DB::transaction(function () use ($req) {

                    // $get_variant = AttributesValue::where('attribute_id', $req->_attribute_id)->first();
                    // if (!empty($get_variant)) {
                    //     $get_variant->lang()->delete();
                    // }

                    $attribute = Attributes::where('id', $req->_attribute_id)
                        ->update([
                            'is_mandatory' => $req->is_mandatory,
                        ]);

                    AttributesLang::where('language', 'en')
                        ->where('attribute_id', $req->_attribute_id)
                        ->update([
                            'name' => $req->name_en,
                        ]);
                    AttributesLang::where('language', 'ar')
                        ->where('attribute_id', $req->_attribute_id)
                        ->update([
                            'name' => $req->name_ar,
                        ]);

                    return $attribute;
                });
                $variant = DB::transaction(function () use ($req) {
                    $attr = AttributesValue::where('attribute_id', $req->_attribute_id)->first();
                    if (!empty($attr)) {
                        AttributesValueLang::where('attribute_value_id', $attr->id)->delete();
                        AttributesValue::where('id', $attr->id)->delete();
                    }

                    $variant = AttributesValue::create([
                        'attribute_id' => $req->_attribute_id,
                    ]);

                    foreach ($req->att_value_en as $key => $var_value) {
                        $variant_lang = AttributesValueLang::create([
                            'attribute_value_id' => $variant->id,
                            'name' => $var_value,
                            'language' => 'en',
                        ]);
                        AttributesValueLang::create([
                            'attribute_value_id' => $variant->id,
                            'en_matching_id' => $variant_lang->id,
                            'name' => $req->att_value_ar[$key],
                            'language' => 'ar',
                        ]);
                    }
                    return $variant;

                    // foreach ($req->att_value_en as $key => $var_value) {
                    //     if ($req->att_value_en_id[$key] == '') {
                    //         $variant_lang1 = AttributesValueLang::create([
                    //             'attribute_value_id' => $req->_attribute_id,
                    //             'name' => $var_value,
                    //             'language' => 'en',
                    //         ]);
                    //         $variant_lang2 = AttributesValueLang::create([
                    //             'attribute_value_id' => $req->_attribute_id,
                    //             'en_matching_id' => $variant_lang1->id,
                    //             'name' => $req->att_value_ar[$key],
                    //             'language' => 'ar',
                    //         ]);
                    //         $var_ids[] = $variant_lang1->id;
                    //         $var_ids[] = $variant_lang2->id;
                    //     } else {
                    //         AttributesValueLang::where('id', $req->att_value_en_id[$key])
                    //             ->update([
                    //                 'name' => $var_value,
                    //                 'language' => 'en',
                    //             ]);
                    //         AttributesValueLang::where('id', $req->att_value_ar_id[$key])
                    //             ->update([
                    //                 'name' => $req->att_value_ar[$key],
                    //                 'language' => 'ar',
                    //             ]);
                    //         $var_ids[] = $req->att_value_en_id[$key];
                    //         $var_ids[] = $req->att_value_ar_id[$key];
                    //     }
                    // }
                    // if (!empty($var_ids)) {
                    //     AttributesValueLang::whereNotIn('id', $var_ids)->where(['attribute_value_id' => $req->_attribute_id])->delete();
                    // }
                });

                $msg = "Attribute updated successfully";
            }
            if ($attribute) {
                return response()->json(['status' => 1, 'message' => $msg]);
            } else {
                return response()->json(['status' => 0, 'message' => 'Sorry something went wrong.']);
            }
        }
    }


    public function activate(Request $req)
    {
        $att = Attributes::where('id', $req->id)->first();
        if ($att) {
            if ($att->status == 'deactive') {
                Attributes::where('id', $req->id)
                    ->update([
                        'status' => 'active'
                    ]);
            } else {
                Attributes::where('id', $req->id)
                    ->update([
                        'status' => 'deactive'
                    ]);
            }
            return response()->json(['status' => 1, 'message' => 'Status updated successfully']);
        } else {
            return response()->json(['status' => 0, 'message' => 'Sorry something went wrong.']);
        }
    }



    public function destroy(Request $request)
    {
        $attr = Attributes::find($request->id);
        $attr->lang()->delete();
        $attr->delete();
        return response()->json(['status' => 1, 'message' => 'Attribute deleted successfully']);
    }
    public function search(Request $req)
    {
        $lang = AttributesLang::where('name', 'like', "%" . $req->search . "%")->where('deleted_at', NULL)->get();
        $response = array();
        foreach ($lang as $lan) {
            $response[] = array("value" => $lan->faq_id, "label" => $lan->name);
        }

        return response()->json($response);
    }
}
