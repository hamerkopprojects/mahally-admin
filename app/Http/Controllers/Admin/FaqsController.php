<?php

namespace App\Http\Controllers\Admin;

use App\Models\Faqs;
use App\Models\FaqsLang;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class FaqsController extends Controller
{
    public function get(Request $request)
    {
        $search = $request->faq_select;
        $search_field = $request->search_field ?? '';

        $faqLang = FaqsLang::where('language', 'en')->get();

        if ($search_field) {
            $data = FaqsLang::select('faq_id')->where('question', 'like', "%" . $search_field . "%")
                ->orwhere('answer', 'like', "%" . $search_field . "%")->get()->toArray();
            $da = [];
            foreach ($data as  $d) {
                $da[] = $d['faq_id'];
            }
            $faq = Faqs::with('lang')->whereIn('id', $da)->paginate(20);
        } else {
            $faq = Faqs::with('lang')->paginate(20);
        }
        return view('admin.cms.faqs.index', compact('faq', 'faqLang', 'search_field'));
    }

    public function store(Request $request)
    {
        $faq = DB::transaction(function () use ($request) {
            $faq = Faqs::create([
                'status' => 'active',
            ]);
            $faq->lang()->createMany([
                [
                    'question' => $request->title_en,
                    'answer' => $request->content_en,
                    'language' => 'en',
                ],
                [
                    'question' => $request->title_ar,
                    'answer' => $request->content_ar,
                    'language' => 'ar',
                ],
            ]);
            return $faq;
        });
        if ($faq) {
            return [
                "msg" => "success"
            ];
        } else {
            return [
                "msg" => "error"
            ];
        }
    }
    public function statusUpdate(Request $request)
    {

        $status = $request->status === 'Deactivate' ? 'deactive' : 'active';
        Faqs::where('id', $request->id)
            ->update([
                'status' => $status
            ]);
        return response()->json(['status' => 1, 'message' => "Status updated successfully"]);
    }
    public function edit($id)
    {
        $faq = Faqs::with('lang')
            ->where('id', $id)
            ->first();
        return [
            'page' => $faq,
        ];
    }
    public function update(Request $request)
    {
        $faq = DB::transaction(function () use ($request) {

            FaqsLang::where('faq_id', $request->id)
                ->where('language', 'en')
                ->update(
                    [
                        'question' => $request->title_en,
                        'answer' => $request->content_en,

                    ]
                );
            FaqsLang::where('faq_id', $request->id)
                ->where('language', 'ar')
                ->update(
                    [
                        'question' => $request->title_ar,
                        'answer' => $request->content_ar,

                    ]
                );
        });

        return [
            'msg' => "success"
        ];
    }

    public function destroy(Request $request)
    {
        $faq = Faqs::find($request->id);
        $faq->lang()->delete();
        $faq->delete();
        return response()->json(['status' => 1, 'message' => 'FAQ deleted successfully']);
    }
    public function search(Request $req)
    {
        $lang = FaqsLang::where('question', 'like', "%" . $req->search . "%")->where('deleted_at', NULL)->get();
        $response = array();
        foreach ($lang as $lan) {
            $response[] = array("value" => $lan->faq_id, "label" => $lan->question);
        }

        return response()->json($response);
    }
}
