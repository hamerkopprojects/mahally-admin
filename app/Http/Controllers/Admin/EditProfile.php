<?php

namespace App\Http\Controllers\Admin;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class EditProfile extends Controller
{
    public function editUser()
    {
        $user = Auth::user();
        return view('user.edit_user', compact('user'));
    }

    public function updateName(Request $request)
    {
        $rules = [
            'auth_name' => 'required',
            'auth_email' => 'required|unique:users,email,'.Auth::user()->id,
            'phone' => 'required|unique:users,phone,'.Auth::user()->id,
        ];
        $messages = [
            'auth_name.required' => 'Name required',
            'auth_email.required' => 'Email is required.',
            'auth_email.unique' => 'User with same email already exists',
            'phone.required' => 'Phone number is required.',
            'phone.unique' => 'User with same phone number already exists',
        ];
        $validator = Validator::make($request->all(), $rules, $messages);
        if (!$validator->passes()) {
            return response()->json(['status' => 0, 'message' => $validator->errors()->first()]);
        } else {
            User::where('id', Auth::user()->id)
                ->update([
                    'name' => $request->auth_name,
                    'email' => $request->auth_email,
                    'phone' => (int)$request->phone,
                ]);
            return response()->json(['status' => 1, 'message' => 'Profile updated successfully']);
        }
    }

    public function updatePassword(Request $request)
    {
        $rules = [
            'password' => 'min:6|required_with:confirm_password|same:confirm_password',
            'confirm_password' => 'min:6'
        ];
        $messages = [
            'password.required' => 'Password required',
            'confirm_password.required' => 'Confirm password required'
        ];
        $validator = Validator::make($request->all(), $rules, $messages);
        if (!$validator->passes()) {
            return response()->json(['status' => 0, 'message' => $validator->errors()->first()]);
        } else {
            User::where('id', Auth::user()->id)
                ->update([
                    'password' => Hash::make($request->password)
                ]);
            return response()->json(['status' => 1, 'message' => 'Password updated successfully']);
        }
    }
}
