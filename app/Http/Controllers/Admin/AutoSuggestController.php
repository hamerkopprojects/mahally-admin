<?php

namespace App\Http\Controllers\Admin;

use App\Models\AutoSuggest;
use Illuminate\Http\Request;
use App\Models\AutoSuggestLang;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Carbon;
class AutoSuggestController extends Controller
{
    protected const RESTAURANT_TYPE = 'restaurant_type';

    protected const CITY = 'city';

    protected const FACILITY = 'facility';

    protected const TERMS = 'terms';

    protected const ADDONS = 'addons';

    protected const INGREDIENTS = 'ingredients';

    protected const RATING_SEGMENTS = 'rating_segments';

    public $arrayData;

    public function index(Request $request)
    {
        $search = $request->autosuggest_select;
        $search_field = $request->search_field ?? '';

        if ($request->title == static::RESTAURANT_TYPE) {
            $this->arrayData = [
                'title' => 'Restaurant Type',
                'mainHeading' => 'RESTAURANT TYPE',
                'keyValue' =>  static::RESTAURANT_TYPE,
                'is_search' => false,
                'is_slno' => false,
                'is_status' => false,
                'is_delete' => true,
                'is_picture' => true,
                'image_heading' => 'Add Icon',
            ];
        } else if ($request->title == static::CITY) {
            $this->arrayData = [
                'title' => 'City',
                'mainHeading' => 'CITY',
                'keyValue' =>  static::CITY,
                'is_search' => true,
                'is_slno' => true,
                'is_status' => false,
                'is_delete' => true,
                'is_picture' => false,
                'image_heading' => 'Add Icon',
            ];
        } else if ($request->title == static::FACILITY) {
            $this->arrayData = [
                'title' => 'Facility',
                'mainHeading' => 'FACILITY',
                'keyValue' =>  static::FACILITY,
                'is_search' => false,
                'is_slno' => false,
                'is_status' => false,
                'is_delete' => true,
                'is_picture' => true,
                'image_heading' => 'Add Icon',
            ];
        } else if ($request->title == static::TERMS) {
            $this->arrayData = [
                'title' => 'Terms',
                'mainHeading' => 'TERMS',
                'keyValue' =>  static::TERMS,
                'is_search' => false,
                'is_slno' => false,
                'is_status' => false,
                'is_delete' => true,
                'is_picture' => true,
                'image_heading' => 'Add Icon',
            ];
        } else if ($request->title == static::ADDONS) {
            $this->arrayData = [
                'title' => 'Addons',
                'mainHeading' => 'ADDONS',
                'keyValue' =>  static::ADDONS,
                'is_search' => false,
                'is_slno' => false,
                'is_status' => true,
                'is_delete' => true,
                'is_picture' => true,
                'image_heading' => 'Add Image',
            ];
        } else if ($request->title == static::INGREDIENTS) {
            $this->arrayData = [
                'title' => 'Ingredients',
                'mainHeading' => 'INGREDIENTS',
                'keyValue' =>  static::INGREDIENTS,
                'is_search' => false,
                'is_slno' => false,
                'is_status' => true,
                'is_delete' => true,
                'is_picture' => true,
                'image_heading' => 'Add Image',
            ];
        } else if ($request->title == static::RATING_SEGMENTS) {
            $this->arrayData = [
                'title' => 'Rating Segments',
                'mainHeading' => 'RATING SEGMENTS',
                'keyValue' =>  static::RATING_SEGMENTS,
                'is_search' => false,
                'is_slno' => false,
                'is_status' => false,
                'is_delete' => false,
                'is_picture' => false,
            ];
        } else {
            $this->arrayData = [
                'title' => '',
                'mainHeading' => '',
                'keyValue' =>  $request->title ?? '',
                'is_search' => false,
                'is_slno' => true,
                'is_status' => false,
                'is_delete' => false,
                'is_picture' => false,
            ];
        }

        if ($search_field) {
            $data = AutoSuggestLang::select('autosuggest_id')->where('name', 'like', "%" . $search_field . "%")->get()->toArray();
            $da = [];
            foreach ($data as  $d) {
                $da[] = $d['autosuggest_id'];
            }
            $autosuggestData = AutoSuggest::with('lang')->where('type', $this->arrayData['keyValue'])->whereIn('id', $da)->orderBy('created_at', 'desc')->paginate(20);
        } else {
            $autosuggestData = AutoSuggest::with('lang')->where('type', $this->arrayData['keyValue'])->orderBy('created_at', 'desc')->paginate(20);
        }

        $arrayData = $this->arrayData;
        return view('admin.autosuggest.index', compact('arrayData', 'autosuggestData', 'search_field'));
    }

    public function store(Request $request)
    {
        $rules = [
            'title_en' => 'required',
            'title_ar' => 'required',
        ];
        $messages = [
            'title_en.required' => 'This filed is required.',
            'title_ar.required' => 'This filed is required.',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if (!$validator->passes()) {
            return response()->json(['status' => 0, 'message' => $validator->errors()->first()]);
        } else {
            $autosuggest = DB::transaction(function () use ($request) {
                $autosuggest = AutoSuggest::create([
                    'status' => 'deactive',
                    'type' => $request->type,
                    'created_by' => Auth::user()->id,
                ]);
                $autosuggest->lang()->createMany([
                    [
                        'name' => $request->title_en,
                        'language' => 'en',
                    ],
                    [
                        'name' => $request->title_ar,
                        'language' => 'ar',
                    ],
                ]);
                return $autosuggest;
            });
            $msg = "Added successfully";
            if ($autosuggest) {
                return response()->json(['status' => 1, 'message' => $msg]);
            } else {
                return response()->json(['status' => 0, 'message' => 'Sorry something went wrong.']);
            }
        }
    }
    public function edit($id)
    {
        $autosuggestData = AutoSuggest::with('lang')
            ->where('id', $id)
            ->first();
        return [
            'page' => $autosuggestData,

        ];
    }

    public function update(Request $request)
    {
        $rules = [
            'title_en' => 'required',
            'title_ar' => 'required',
            'upload_image' => 'image|mimes:png,jpg,jpeg|max:2048',
        ];
        $messages = [
            'title_en.required' => 'This filed is required.',
            'title_ar.required' => 'This filed is required.',
            'upload_image.max' => 'The image must be less than 2mb in size',
            'upload_image.mimes' => "The image must be of the format jpeg or png",
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if (!$validator->passes()) {
            return response()->json(['status' => 0, 'message' => $validator->errors()->first()]);
        } else {
            AutoSuggest::where('id', $request->id_pg)
                ->update([
                    'type' => $request->type,
            ]);
            $autosuggest = AutoSuggestLang::where('autosuggest_id', $request->id_pg)
                ->where('language', 'en')
                ->update(
                    [
                        'name' => $request->title_en,
                    ]
                );
            $autosuggest = AutoSuggestLang::where('autosuggest_id', $request->id_pg)
                ->where('language', 'ar')
                ->update(
                    [
                        'name' => $request->title_ar,
                    ]
                );

            $msg = "Updated successfully";
            if ($autosuggest) {
                return response()->json(['status' => 1, 'message' => $msg]);
            } else {
                return response()->json(['status' => 0, 'message' => 'Sorry something went wrong.']);
            }
        }
    }

    public function statusUpdate(Request $request)
    {

        $status = $request->status === 'active' ? 'deactive' : 'active';

        AutoSuggest::where('id', $request->id)
            ->update([
                'status' => $status
            ]);
        return response()->json(['status' => 1, 'message' => "Status updated successfully"]);
    }

    public function destroy(Request $request)
    {
        $autosuggest = AutoSuggest::find($request->id);
        $autosuggest->lang()->delete();
        $autosuggest->delete();
        return [
            'msg' => 'success'
        ];
    }
    public function search(Request $req)
    {
        $lang = AutoSuggestLang::where('name', 'like', "%" . $req->search . "%")->where('deleted_at', NULL)->get();
        $response = array();
        foreach ($lang as $lan) {
            $response[] = array("value" => $lan->autosuggest_id, "label" => $lan->name);
        }

        return response()->json($response);
    }
    public function upload_image(Request $request) {
        $rules = [
            'photo' => 'required|image|mimes:png,jpg,jpeg|max:10240'
        ];
        $messages = [
            'photo.max' => 'The image must be less than 2Mb in size',
            'photo.mimes' => "The image must be of the format jpeg or png",
        ];
        $validator = Validator::make($request->all(), $rules, $messages);
        if (!$validator->passes()) {
            return response()->json(['status' => 0, 'message' => $validator->errors()->first()]);
        } else {
            $file = request()->file('photo');
            $path = $file->store("autosuggest", ['disk' => 'public_uploads']);
            if(!empty($request->id)){
            AutoSuggest::where('id', $request->id)
                    ->update([
                    "image_path" => $path
            ]);
            }else{
              $autosuggest = AutoSuggest::create([
               "image_path" => $path,
               'created_by' => Auth::user()->id,
               'created_at' => Carbon::now(),
                            ]);
              $autosuggest->lang()->createMany([
                    [
                        'name' => '',
                        'language' => 'en',
                    ],
                    [
                        'name' => '',
                        'language' => 'ar',
                    ],
                ]);
            }
            $auto_id = $request->id ? $request->id : $autosuggest->id;
            $full_path = url('uploads/' . $path);
            return response()->json(['status' => 1, 'message' => 'File uploaded successfully', 'path' => $full_path, 'id' => $auto_id, 'type' => $request->type]);
        }
    }
    public function detete_img(Request $request) {
        $auto = AutoSuggest::where('id', $request->id)->first();
        AutoSuggest::where('id', $request->id)
                ->update([
                    'image_path' => ''
        ]);
        if (!empty($auto)) {
            $file_path = public_path() . '/uploads/' . $auto->image_path;
            if (is_file($file_path)) {
                    unlink($file_path);
            }
        }
        return response()->json(['status' => 1, 'message' => 'File deleted successfully', 'id' => $request->id]);
    }
}
