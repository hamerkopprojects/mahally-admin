<?php

namespace App\Http\Controllers\Admin;

use App\Models\Membership;
use App\Models\Restaurant;
use App\Models\AutoSuggest;
use Illuminate\Http\Request;
use App\Mail\sendWelcomeEmail;
use App\Models\MembershipLang;
use App\Models\RestaurantLang;
use App\Models\RestaurantsTerms;
use App\Models\RestaurantsImages;
use App\Models\CommissionCategory;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Models\Order;
use App\Models\RestaurantsDocuments;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use App\Models\RestaurantsFacilities;
use App\Models\RestaurantsContactInfo;
use App\Models\RestaurantsRestaurantType;
use Illuminate\Support\Facades\Validator;
use SebastianBergmann\GlobalState\Restorer;
use Illuminate\Support\Facades\File;
use SimpleSoftwareIO\QrCode\Facades\QrCode;
use PDF;

class RestaurantManagementController extends Controller
{
    public function get(Request $request)
    {
        $search = $request->rest_select;
        $search_field = $request->search_field ?? '';
        $city_id = $request->select_city ?? '';
        if ($search_field || $city_id) {
            $query = Restaurant::with('lang')->with('contact');
            if ($search_field) {
                $data = Restaurant::select('restuarants_id')
                    ->leftJoin('restuarants_i18n', 'restuarants_i18n.restuarants_id', 'restuarants.id')->where('name', 'like', "%" . $search_field . "%")->orWhere('unique_id', 'like', "%" . $search_field . "%")->get()->toArray();
                $da = [];
                foreach ($data as  $d) {
                    $da[] = $d['restuarants_id'];
                }
                $query->whereIn('id', $da);
            }
            if ($city_id) {
                $query->where('city_id', $city_id);
            }
            $restaurant = $query->paginate(20);
        } else {
            $restaurant = Restaurant::with('lang')->with('contact')->with('city')->paginate(20);
        }
        $city = AutoSuggest::select('autosuggest.id', 'autosuggest_i18n.name')
            ->leftJoin('autosuggest_i18n', 'autosuggest_i18n.autosuggest_id', 'autosuggest.id')->where('autosuggest_i18n.language', '=', 'en')->where('autosuggest.type', '=', 'city')->get();

        return view('admin.restaurant.index', compact('restaurant', 'search_field', 'city', 'city_id'));
    }
    public function create(Request $req)
    {
        $_rest_id = $req->id;
        $row_data = array();
        $rt_tp_ids = array();
        $test = 'test';
        $restaurant_type = AutoSuggest::with('lang')->where('type', '=', 'restaurant_type')->get();
        $city = AutoSuggest::select('autosuggest.id', 'autosuggest_i18n.name')
            ->leftJoin('autosuggest_i18n', 'autosuggest_i18n.autosuggest_id', 'autosuggest.id')->where('autosuggest_i18n.language', '=', 'en')->where('autosuggest.type', '=', 'city')->get();
        $membership = Membership::select('membership_i18n.membership_id', 'membership_i18n.membership_name')
            ->leftJoin('membership_i18n', 'membership_i18n.membership_id', 'membership.id')->where('membership_i18n.language', '=', 'en')->where('membership.status', '=', 'active')->get();
        $commission = CommissionCategory::all();
        if ($_rest_id) {
            $row_data = Restaurant::with('lang')->where('id', $_rest_id)->first();
            $rt_tp = RestaurantsRestaurantType::select('restuarant_type_id')->where('restuarants_id', $_rest_id)->get();
            foreach ($rt_tp as $type_id) {
                $rt_tp_ids[] = $type_id->restuarant_type_id;
            }
        }

        $data = [
            'restaurant_type' => $restaurant_type,
            'rt_tp_ids' => $rt_tp_ids,
            'membership_data' => $membership,
            'row_data' => $row_data,
            'city_data' => $city,
            'commission_data' => $commission,
        ];
        return view('admin.restaurant.create', $data);
    }
    public function get_tabs(Request $req)
    {
        $rest_id = $req->rest_id;
        if ($req->activeTab == 'BASIC INFO') {
            $_rest_id = $req->rest_id;
            $row_data = array();
            $rt_tp_ids = array();
            $restaurant_type = AutoSuggest::with('lang')->where('type', '=', 'restaurant_type')->get();
            $city = AutoSuggest::select('autosuggest.id', 'autosuggest_i18n.name')
                ->leftJoin('autosuggest_i18n', 'autosuggest_i18n.autosuggest_id', 'autosuggest.id')->where('autosuggest_i18n.language', '=', 'en')->where('autosuggest.type', '=', 'city')->get();
            $membership = Membership::select('membership_i18n.membership_id', 'membership_i18n.membership_name')
                ->leftJoin('membership_i18n', 'membership_i18n.membership_id', 'membership.id')->where('membership_i18n.language', '=', 'en')->where('membership.status', '=', 'active')->get();
            $commission = CommissionCategory::all();
            if ($_rest_id) {
                $row_data = Restaurant::with('lang')->where('id', $_rest_id)->first();
                $rt_tp = RestaurantsRestaurantType::select('restuarant_type_id')->where('restuarants_id', $_rest_id)->get();
                foreach ($rt_tp as $type_id) {
                    $rt_tp_ids[] = $type_id->restuarant_type_id;
                }
            }

            $data = [
                'restaurant_type' => $restaurant_type,
                'rt_tp_ids' => $rt_tp_ids,
                'membership_data' => $membership,
                'row_data' => $row_data,
                'city_data' => $city,
                'commission_data' => $commission
            ];
            return view('admin.restaurant.basic_info', $data);
        } elseif ($req->activeTab == 'CONTACT INFO') {
            $contact_details = RestaurantsContactInfo::where('restuarants_id', '=', $rest_id)->get();
            return view('admin.restaurant.contact_info')->with(compact('rest_id', 'contact_details'))->render();
        } elseif ($req->activeTab == 'DOCUMENTS & PHOTOS') {
            $rest_data = Restaurant::where(['id' => $rest_id])->first();
            $cover_image = $rest_data->cover_photo ? $rest_data->cover_photo : '';
            $logo = $rest_data->logo ? $rest_data->logo : '';

            $rest_images = RestaurantsImages::where(['restuarants_id' => $rest_id])->get();
            $document_data = RestaurantsDocuments::where(['restuarants_id' => $rest_id])->get();
            if (count($document_data) == 0) {
                $documents = array();
            } else {
                $documents = $document_data;
            }
            return view('admin.restaurant.documents_photos')->with(compact('rest_id', 'cover_image', 'logo', 'rest_images', 'documents'))->render();
        } elseif ($req->activeTab == 'FACILITIES & TERMS') {

            $facilities = AutoSuggest::with('lang')->where('type', '=', 'facility')->get();
            $terms = AutoSuggest::with('lang')->where('type', '=', 'terms')->get();

            $exist_facilities_data = RestaurantsFacilities::select('facilities_id')->where(['restuarants_id' => $rest_id])->get();
            $exist_tearms_data = RestaurantsTerms::select('terms_id')->where(['restuarants_id' => $rest_id])->get();
            $exist_facilities = array();
            $exist_tearms = array();

            foreach ($exist_facilities_data as $data) {
                $exist_facilities[] = $data->facilities_id;
            }
            foreach ($exist_tearms_data as $data) {
                $exist_tearms[] = $data->terms_id;
            }

            return view('admin.restaurant.facilities_terms')->with(compact('facilities', 'terms', 'exist_facilities', 'exist_tearms', 'rest_id'))->render();
        }
    }
    public function basic_info(Request $req)
    {
        if ($req->rest_id) {
            $unique = ',' . $req->rest_id;
        } else {
            $unique = ',NULL';
        }
        $flag = 0;
        $contact_details = array();
        $rules = [
            'name_en' => 'required',
            'name_ar' => 'required',
            'membership_id' => 'required',
            'city_id' => 'required',
            'commission_category_id' => 'required',
            'opening_time' => 'required',
            'closing_time' => 'required',
            'description_ar' => 'required',
            'description_en' => 'required',
        ];
        $messages = [
            'name_en.required' => 'Restaurant Name(EN) is required.',
            'name_ar.required' => 'Restaurant Name(AR) is required.',
            'membership_id.required' => 'Membership is required.',
            'city_id.required' => 'City is required.',
            'commission_category_id.required' => 'Commission category is required.',
            'opening_time.required' => 'Opening time is required.',
            'closing_time.required' => 'Closing time is required.',
            'description_en.required' => 'Description (EN) is required.',
            'description_ar.required' => 'Description (AR) is required.',
        ];


        $validator = Validator::make($req->all(), $rules, $messages);
        if (!$validator->passes()) {
            return response()->json(['status' => 0, 'message' => $validator->errors()->first()]);
        } else {

            if (empty($req->rest_id)) {
                $restaurant = DB::transaction(function () use ($req) {
                    $restaurant = Restaurant::create([
                        'city_id' => $req->city_id,
                        'commission_category_id' => $req->commission_category_id,
                        'membership_id' => $req->membership_id,
                        'opening_time' => date('Y-m-d H:i:s', strtotime($req->opening_time)),
                        'closing_time' => date('Y-m-d H:i:s', strtotime($req->closing_time)),
                        'vat_includes' => $req->vat_includes ? $req->vat_includes : 'no',
                        'vat_percentage' => $req->vat_percentage,
                        'vat_reg_number' => $req->vat_reg_number,
                        'vat_percentage' => $req->vat_percentage,
                        'location' => $req->location,
                        'latitude' => $req->latitude,
                        'longitude' => $req->longitude,
                        'unique_id' => $this->generateRestId(),
                        'status' => 'deactive',
                    ]);
                    $restaurant->lang()->createMany([
                        [
                            'name' => $req->name_en,
                            'description' => $req->description_en,
                            'language' => 'en',
                        ],
                        [
                            'name' => $req->name_ar,
                            'description' => $req->description_ar,
                            'language' => 'ar',
                        ],
                    ]);
                    return $restaurant;
                });
                $msg = 'Basic info added successfully';
            } else {
                $restaurant = DB::transaction(function () use ($req) {
                    $restaurant = Restaurant::where('id', $req->rest_id)
                        ->update([
                            'city_id' => $req->city_id,
                            'commission_category_id' => $req->commission_category_id,
                            'membership_id' => $req->membership_id,
                            'opening_time' => date('Y-m-d H:i:s', strtotime($req->opening_time)),
                            'closing_time' => date('Y-m-d H:i:s', strtotime($req->closing_time)),
                            'vat_includes' => $req->vat_includes ? $req->vat_includes : 'no',
                            'vat_percentage' => $req->vat_percentage,
                            'vat_reg_number' => $req->vat_reg_number,
                            'vat_percentage' => $req->vat_percentage,
                            'location' => $req->location,
                            'latitude' => $req->latitude,
                            'longitude' => $req->longitude,
                        ]);
                    RestaurantLang::where('language', 'en')
                        ->where('restuarants_id', $req->rest_id)
                        ->update([
                            'name' => $req->name_en,
                            'description' => $req->description_en,
                        ]);
                    RestaurantLang::where('language', 'ar')
                        ->where('restuarants_id', $req->rest_id)
                        ->update([
                            'name' => $req->name_en,
                            'description' => $req->description_en,
                        ]);

                    return $restaurant;
                });

                $msg = 'Basic info updated successfully';
                $flag = 1;
            }
            if ($restaurant) {
                $rest_id = !empty($restaurant->id) ? $restaurant->id : $req->rest_id;
                if ($req->restaurant_type) {
                    RestaurantsRestaurantType::where(['restuarants_id' => $rest_id])->delete();
                    foreach ($req->restaurant_type as $type) {
                        $restaurantType = RestaurantsRestaurantType::create([
                            'restuarants_id' => $rest_id,
                            'restuarant_type_id' => $type,
                        ]);
                    }
                }
            }
            if ($flag) {
                $contact_details = RestaurantsContactInfo::where(['restuarants_id' => $rest_id])->get();
            }
            $html = view('admin.restaurant.contact_info')->with(compact('rest_id', 'contact_details'))->render();
            return response()->json(['status' => 1, 'message' => $msg, 'result' => $html]);
        }
    }
    public function generateRestId()
    {
        $lastUser = Restaurant::select('unique_id')
            ->orderBy('id', 'desc')
            ->first();
        $lastId = 0;

        if ($lastUser) {
            $lastId = (int) substr($lastUser->unique_id, 8);
        }
        $lastId++;

        return 'RES-000-' . str_pad($lastId, 5, '0', STR_PAD_LEFT);
    }
    public function contact_info(Request $req)
    {
        $rest_id = $req->rest_id;
        $flag = 0;
        $contact_info_ids = array();
        $rest_data = Restaurant::where(['id' => $rest_id])->first();

        if (!empty($rest_data)) {
            $contact_info_status = RestaurantsContactInfo::where(['restuarants_id' => $req->rest_id])->get();
            if (count($contact_info_status) > 0) {
                $flag = 1;
            }
            foreach ($req->contact_person as $key => $contact_value) {
                if (!empty($contact_value) && !empty($req->designation[$key]) && !empty($req->phone[$key]) && !empty($req->email[$key])) {
                    $contact_ids = $req->contact_id ? $req->contact_id : array();
                    $contact_info = RestaurantsContactInfo::whereIn('id', $contact_ids)->where(['restuarants_id' => $req->rest_id])->get();
                    if (count($contact_info) > 0 && !empty($contact_ids) && !empty($contact_info[$key]->id)) {
                        RestaurantsContactInfo::where(['id' => $contact_info[$key]->id, 'restuarants_id' => $req->rest_id])
                            ->update([
                                'contact_person' => $req->contact_person[$key],
                                'designation' => $req->designation[$key],
                                'country_code' => $req->country_code[$key],
                                'phone' => $req->phone[$key],
                                'email' => $req->email[$key],
                                'password' => '123456',
                            ]);
                        $contact_info_ids[] = $contact_info[$key]->id;
                    } else {
                        $contact_info_data = RestaurantsContactInfo::create([
                            'restuarants_id' => $req->rest_id,
                            'contact_person' => $req->contact_person[$key],
                            'designation' => $req->designation[$key],
                            'country_code' => $req->country_code[$key],
                            'phone' => $req->phone[$key],
                            'email' => $req->email[$key],
                            'password' => Hash::make('123456'),
                        ]);
                        $contact_info_ids[] = $contact_info_data->id;
                    }
                }
            }
            if (!empty($contact_info_ids)) {
                RestaurantsContactInfo::whereNotIn('id', $contact_info_ids)->where(['restuarants_id' => $req->rest_id])->delete();
            }

            $cover_image = $rest_data->cover_photo ? $rest_data->cover_photo : '';
            $logo = $rest_data->logo ? $rest_data->logo : '';

            $rest_images = RestaurantsImages::where(['restuarants_id' => $rest_id])->get();
            $document_data = RestaurantsDocuments::where(['restuarants_id' => $rest_id])->get();

            if (count($document_data) == 0) {
                $documents = array();
            } else {
                $documents = $document_data;
            }
            $html = view('admin.restaurant.documents_photos')->with(compact('rest_id', 'cover_image', 'logo', 'rest_images', 'documents'))->render();

            if ($flag == 1) {
                $msg = "Contact info updated successfully";
            } else {
                $msg = "Contact info added successfully";
            }
            return response()->json(['status' => 1, 'message' => $msg, 'result' => $html]);
        } else {
            return response()->json(['status' => 0, 'message' => 'Sorry something went wrong.']);
        }
    }
    public function documents_photos(Request $req)
    {
        $rest_id = $req->rest_id;
        $flag = 0;

        $rest_data = Restaurant::where(['id' => $rest_id])->first();
        if (!empty($rest_data)) {
            $document_status = RestaurantsDocuments::where(['restuarants_id' => $req->rest_id])->get();
            if (count($document_status) > 0) {
                $flag = 1;
            }
            foreach ($req->license_title as $key => $doc_value) {
                if (!empty($doc_value) && !empty($req->expire_in[$key])) {
                    $doc_ids = $req->doc_ids ? $req->doc_ids : array();
                    $documents = RestaurantsDocuments::whereIn('id', $doc_ids)->where(['restuarants_id' => $req->rest_id])->get();
                    if (count($documents) > 0 && !empty($doc_ids) && !empty($documents[$key]->id)) {
                        RestaurantsDocuments::where(['id' => $documents[$key]->id, 'restuarants_id' => $req->rest_id])
                            ->update([
                                'license_title' => $req->license_title[$key],
                                'cr_copy' => $req->image_path[$key],
                                'expire_at' => date('Y-m-d H:i:s', strtotime($req->expire_in[$key])),
                            ]);
                        $document_ids[] = $documents[$key]->id;
                    } else {
                        $doc_data = RestaurantsDocuments::create([
                            'restuarants_id' => $req->rest_id,
                            'license_title' => $req->license_title[$key],
                            'cr_copy' => $req->image_path[$key],
                            'expire_at' => date('Y-m-d H:i:s', strtotime($req->expire_in[$key])),
                        ]);
                        $document_ids[] = $doc_data->id;
                    }
                }
            }
            if (!empty($document_ids)) {
                RestaurantsDocuments::whereNotIn('id', $document_ids)->where(['restuarants_id' => $req->rest_id])->delete();
            }

            $facilities = AutoSuggest::with('lang')->where('type', '=', 'facility')->get();
            $terms = AutoSuggest::with('lang')->where('type', '=', 'terms')->get();

            $exist_facilities_data = RestaurantsFacilities::select('facilities_id')->where(['restuarants_id' => $rest_id])->get();
            $exist_tearms_data = RestaurantsTerms::select('terms_id')->where(['restuarants_id' => $rest_id])->get();
            $exist_facilities = array();
            $exist_tearms = array();

            foreach ($exist_facilities_data as $data) {
                $exist_facilities[] = $data->facilities_id;
            }
            foreach ($exist_tearms_data as $data) {
                $exist_tearms[] = $data->terms_id;
            }

            $html = view('admin.restaurant.facilities_terms')->with(compact('facilities', 'terms', 'exist_facilities', 'exist_tearms', 'rest_id'))->render();

            if ($flag == 1) {
                $msg = "Documents and photos updated successfully";
            } else {
                $msg = "Documents and photos added successfully";
            }
            return response()->json(['status' => 1, 'message' => $msg, 'result' => $html]);
        } else {
            return response()->json(['status' => 0, 'message' => 'Sorry something went wrong.']);
        }
    }
    public function rest_images(Request $req)
    {
        $rest_id = $req->rest_id;
        if ($req->upload_type == 'single') {
            $rules = [
                'photo' => 'required|image|mimes:png,jpg,jpeg'
            ];
            $messages = [
                'photo.max' => 'The image must be less than 2Mb in size',
                'photo.mimes' => "The image must be of the format jpeg or png",
            ];
        } else if ($req->upload_type == 'logo') {
            $rules = [
                'photo' => 'required|image|mimes:png,jpg,jpeg'
            ];
            $messages = [
                'photo.max' => 'The image must be less than 2Mb in size',
                'photo.mimes' => "The image must be of the format jpeg or png",
            ];
        } else if ($req->upload_type == 'cr') {
            $rules = [
                'cr_photo' => 'required|image|mimes:png,jpg,jpeg'
            ];
            $messages = [
                'cr_photo.max' => 'The image must be less than 2Mb in size',
                'cr_photo.mimes' => "The image must be of the format jpeg or png",
            ];
        } else {
            $rules = [
                'photos' => ['required', 'array'],
                'photos.*' => 'image|mimes:png,jpg,jpeg',
            ];
            $messages = [
                'photos.max' => 'The image must be less than 2Mb in size',
                'photos.mimes' => "The image must be of the format jpeg or png",
            ];
        }

        $validator = Validator::make($req->all(), $rules, $messages);
        if (!$validator->passes()) {
            return response()->json(['status' => 0, 'message' => $validator->errors()->first()]);
        } else {
            if ($req->upload_type == 'single') {
                $file = request()->file('photo');
                $path = $file->store("restaurant/cover", ['disk' => 'public_uploads']);
                Restaurant::where('id', $rest_id)
                    ->update([
                        "cover_photo" => $path
                    ]);
                $full_path = url('uploads/' . $path);
                return response()->json(['status' => 1, 'message' => 'Cover image added successfully', 'image_path' => $full_path, 'rest_id' => $rest_id]);
            } else if ($req->upload_type == 'logo') {
                $file = request()->file('photo');
                $path = $file->store("restaurant/logo", ['disk' => 'public_uploads']);
                Restaurant::where('id', $rest_id)
                    ->update([
                        "logo" => $path
                    ]);
                $full_path = url('uploads/' . $path);
                return response()->json(['status' => 1, 'message' => 'Logo added successfully', 'image_path' => $full_path, 'rest_id' => $rest_id]);
            } else if ($req->upload_type == 'cr') {
                $div_id = $req->div_id;
                $file = request()->file('cr_photo');
                $path = $file->store("restaurant/cr_copy", ['disk' => 'public_uploads']);
                // Restaurant::where('id', $rest_id)
                //     ->update([
                //         "logo" => $path
                //     ]);
                $full_path = url('uploads/' . $path);
                return response()->json(['status' => 1, 'message' => 'CR Copy added successfully', 'image_path' => $full_path, 'rest_id' => $rest_id, 'div_id' => $div_id]);
            } else {
                $productImages = $req->file('photos') ? $req->file('photos') : [];

                foreach ($productImages as $image) {
                    $saved = RestaurantsImages::create([
                        'restuarants_id' => $rest_id,
                        'image_path' => $image->store("restaurant/restaurant_images", ['disk' => 'public_uploads']),
                    ]);
                }
                $rest_images = RestaurantsImages::where(['id' => $saved['id']])->first();
                $full_path = url('uploads/' . $rest_images->image_path);
                return response()->json(['status' => 1, 'message' => 'Product image added successfully', 'image_path' => $full_path, 'image_id' => $saved['id']]);
            }
        }
    }
    public function facilities_terms(Request $req)
    {
        $rest_id = $req->rest_id;
        $city_id = $req->select_city ?? '';

        RestaurantsFacilities::where(['restuarants_id' => $rest_id])->delete();
        RestaurantsTerms::where(['restuarants_id' => $rest_id])->delete();

        if ($req->facilities) {
            foreach ($req->facilities as $key => $data) {
                $facility = RestaurantsFacilities::create([
                    'restuarants_id' => $rest_id,
                    'facilities_id' => $data,
                ]);
            }
        }

        if ($req->terms) {
            foreach ($req->terms as  $data) {
                $facility = RestaurantsTerms::create([
                    'restuarants_id' => $rest_id,
                    'terms_id' => $data,
                ]);
            }
        }
        $search_field = '';
        $restaurant = Restaurant::with('lang')->with('contact')->with('city')->paginate(20);
        $city = AutoSuggest::select('autosuggest.id', 'autosuggest_i18n.name')
            ->leftJoin('autosuggest_i18n', 'autosuggest_i18n.autosuggest_id', 'autosuggest.id')->where('autosuggest_i18n.language', '=', 'en')->where('autosuggest.type', '=', 'city')->get();
        $city_id = '';
        $html = view('admin.restaurant.index')->with(compact('restaurant', 'search_field', 'city', 'city_id'))->render();
        $msg = "Terms and facilities added successfully";
        return response()->json(['status' => 1, 'message' => $msg, 'result' => $html]);
    }
    public function detete_img(Request $request)
    {
        if ($request->type == 'cover') {
            $restaurant = Restaurant::where('id', $request->id)->first();
            Restaurant::where('id', $request->id)
                ->update([
                    "cover_photo" => Null
                ]);
            if (!empty($restaurant)) {
                $file_path = public_path() . '/uploads/' . $restaurant["cover_photo"];
                if (is_file($file_path)) {
                    unlink($file_path);
                }
            }
            $msg = 'Cover image deleted successfully';
        } else if ($request->type == 'logo') {
            $restaurant = Restaurant::where('id', $request->id)->first();
            Restaurant::where('id', $request->id)
                ->update([
                    "logo" => Null
                ]);
            if (!empty($restaurant)) {
                $file_path = public_path() . '/uploads/' . $restaurant["logo"];
                if (is_file($file_path)) {
                    unlink($file_path);
                }
            }
            $msg = 'Logo deleted successfully';
        } else if ($request->type == 'cr_copy') {
            $restaurant = RestaurantsDocuments::where('id', $request->id)->first();
            RestaurantsDocuments::where('id', $request->id)
                ->update([
                    "cr_copy" => Null
                ]);
            if (!empty($restaurant)) {
                // $file_path = public_path() . '/uploads/' . $restaurant["logo"];
                $file_path = $restaurant["cr_copy"];
                if (is_file($file_path)) {
                    unlink($file_path);
                }
            }
            $msg = 'CR Copy deleted successfully';
        } else {
            $rest_img = RestaurantsImages::find($request->id);
            if (!empty($rest_img)) {
                $file_path = public_path("uploads/{$rest_img["path"]}");
                $rest_img->delete();
                if (is_file($file_path)) {
                    unlink($file_path);
                }
            }
            $msg = 'Restaurant image deleted successfully';
        }
        $loader_image = asset('assets/images/loader.gif');
        return response()->json(['status' => 1, 'message' => $msg, 'id' => $request->id, 'type' => $request->type, 'loader_image' => $loader_image]);
    }
    public function statusUpdate(Request $request)
    {
        $status = $request->status === 'Deactivate' ? 'deactive' : 'active';

        if ($status == 'active') {
            $contact_info = RestaurantsContactInfo::select('id')->where('restuarants_id', $request->id)->first();
            $documents_photos = RestaurantsDocuments::select('id')->where('restuarants_id', $request->id)->first();
            $terms_facilities = RestaurantsFacilities::select('id')->where('restuarants_id', $request->id)->first();

            if (!empty($contact_info) && !empty($documents_photos) && !empty($terms_facilities)) {
                Restaurant::where('id', $request->id)
                    ->update([
                        'status' => $status
                    ]);
                $status_val = 1;
                $msg = 'Status updated successfully';
            } else {
                $status_val = 2;
                $msg = 'Please complete all restaurant forms';
            }
        } else {
            Restaurant::where('id', $request->id)
                ->update([
                    'status' => $status
                ]);
            $status_val = 1;
            $msg = 'Status updated successfully';
        }

        return response()->json(['status' => $status_val, 'message' => $msg]);
    }
    // public function edit($id)
    // {
    //     $promocode = Promocodes::where('id', $id)->first();
    //     return [
    //         'page' => $promocode,
    //     ];
    // }


    public function destroy(Request $request)
    {
        $testurant = Restaurant::find($request->id);
        if (!empty($testurant)) {
            $status = [1,2,3,4];
            $order = Order::select('id')->where('restaurant_id', $request->id)->whereIn('order_status', $status)->exists();
        }
        if ($order == false){
        $testurant->lang()->delete();
        $testurant->restaurant_type()->delete();
        $testurant->contact()->delete();
        $testurant->city()->delete();
        $testurant->restaurant_images()->delete();
        $testurant->documents()->delete();
        $testurant->facilities()->delete();
        $testurant->terms()->delete();
        $testurant->delete();
        return response()->json(['status' => 1, 'message' => 'Restaurant deleted successfully']);
        }
        return response()->json(['status' => 0, 'message' => 'You cannot delete,have related records']);
    }
    public function autoComplete(Request $req)
    {
        $lang = RestaurantLang::where('name', 'like', "%" . $req->search . "%")->get();
        $response = array();
        foreach ($lang as $lan) {
            $response[] = array("value" => $lan->restaurant_id, "label" => $lan->name);
        }

        return response()->json($response);
    }
    public function welcomeSend(Request $req)
    {
        $user = RestaurantsContactInfo::where('restuarants_id', $req->id)->first();
        // $password = mt_rand(100000, 999999);
        $password = '123456';
        $data = [
            'password' => $password,
            'user' => $user
        ];
        DB::transaction(function () use ($req, $password) {
            RestaurantsContactInfo::where('restuarants_id', $req->id)->update([
                'password' => Hash::make($password)
            ]);
        });

        // return response()->json(['status' => 1, 'message' => 'Welcome mail send successfully']);
        $email = Mail::to($user->email)->send(new sendWelcomeEmail($data));
        if ($email) {
            return response()->json(['status' => 1, 'message' => 'Welcome mail send successfully']);
        } else {
            return response()->json(['status' => 0, 'message' => 'Sorry something went wrong.']);
        }
    }

    public function qrScan($rest_id)
    {
        $target_dir_path = "public/uploads/qrcodes/restaurant";
        $target_orgin_dir_path = "/uploads/qrcodes/restaurant";
        if (!File::isDirectory($target_dir_path)) {
            File::makeDirectory($target_dir_path, 0777, true, true);
        }
        $invEncode = str_pad($rest_id, 5, '0', STR_PAD_LEFT);

        $createfilepath = $target_dir_path . '/qr_' . $invEncode;
        $filepath = $target_orgin_dir_path . '/qr_' . $invEncode;
        
        $data = Restaurant::with('lang')->with('contact')->where('id', $rest_id)->first();
        $pdf = PDF::loadView('admin.restaurant.qr-code', compact('invEncode','data'));
        return $pdf->stream();
    }
}
