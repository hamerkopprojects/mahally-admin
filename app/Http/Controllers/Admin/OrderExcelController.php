<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\AdminActiveOrderExport;
use App\Exports\AdminDeliveredOrderExport;

class OrderExcelController extends Controller
{
    public function downloadExcelActive()
    {
        return Excel::download(new AdminActiveOrderExport, 'Orders.xlsx');
    }
    public function downloadExcelDelivered()
    {
        return Excel::download(new AdminDeliveredOrderExport, 'Orders.xlsx');
    }
}
