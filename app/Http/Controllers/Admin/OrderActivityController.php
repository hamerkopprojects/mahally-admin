<?php

namespace App\Http\Controllers\Admin;

use App\User;
use App\Models\Customers;
use Carbon\Carbon;
use App\Models\OrderLog;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class OrderActivityController extends Controller {

    public function index(Request $req) {
        $customer_id = $req->customer_id;
        $user_id = $req->user_id;
        $date = $req->date;
        $query = OrderLog::query();
        if ($req->date) {
            $date = Carbon::parse($req->date)->format('Y-m-d');
            $query->whereDate('created_at', $date);
        }
        if ($req->user_id) {
            $query->where('user_id', $user_id);
        }
        if ($req->customer_id) {
            $query->where('customer_id', $customer_id);
        }
        $activity = $query->orderBy('id', 'desc')->paginate(20);
        $customer = Customers::where('status', 'active')->get();
        
        $users = User::where('status', 'active')->get();
        return view('admin.logs.order_activity', compact('activity', 'customer', 'customer_id', 'users', 'user_id', 'date'));
    }
    

}
