<?php

namespace App\Http\Controllers\Admin;

use App\Models\Order;
use App\Models\Customers;
use App\Models\OrderItems;
use App\Models\Restaurant;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;
use DB;

class DashBoardController extends Controller
{
    public function get()
    {
        $total_sale =  Order::whereIn('orders.order_status', [5])->sum('orders.grand_total');
        $total_customer = Customers::leftJoin('orders', 'customer.id', 'orders.customer_id')->where('customer.status', 'active')->groupBy('orders.customer_id')->count();
        $total_customers =  $total_customer;

        $total_orders = Order::whereIn('order_status', [1, 2, 3, 4, 5])->where('deleted_at', NULL)->count();
        $sales_count = 'SAR ' . number_format($total_sale, 2, ".", "");


        $total_restaurants = Restaurant::where('status', 'active')->where('deleted_at', NULL)->count();
        $order_status = Config::get('constants.frontdisk_order_status');
        $status = array("2", "3", "4");
        $recent_order =  DB::table('orders')->select('orders.id', 'orders.grand_total', 'orders.order_id', 'orders.order_status', 'restuarants_i18n.name as rest_name', 'customer.name as customer_name', 'orders.created_at')
            ->leftJoin('customer', 'orders.customer_id', 'customer.id')
            ->leftJoin('restuarants', 'orders.restaurant_id', 'restuarants.id')
            ->leftJoin('restuarants_i18n', 'restuarants.id', 'restuarants_i18n.restuarants_id')
            ->where('restuarants_i18n.language', 'en')
            ->where('restuarants.deleted_at', NULL)
            ->whereIn('orders.order_status', $status)->orderBy('orders.created_at', 'desc')->limit(5)->get();

        $resturants =  Restaurant::with('lang')->where('status', 'active')->where('deleted_at', NULL)->get();


        $top_selling_restaurant = Order::with(['restaurant' => function ($p) {
            $p->with('lang:restuarants_id,language,name');
        }])
            ->groupBy('restaurant_id')
            ->selectRaw('order_id,restaurant_id, sum(grand_total) as grand_total')->orderBy('grand_total', 'desc')->limit(5)
            ->get();


        $top_selling_item = OrderItems::with(['menu_items' => function ($q) {
            $q->with('lang:menu_item_id,name');
        }])
            ->with(['order' => function ($q) {
                $q->with(['restaurant' => function ($p) {
                    $p->with('lang:restuarants_id,language,name');
                }]);
            }])
            ->groupBy('item_id')
            ->selectRaw('order_id,item_id, sum(item_count) as item_count')->orderBy('item_count', 'desc')->limit(5)
            ->get();
        $top_customer_using_app = Order::with('customer')
            ->groupBy('customer_id')
            ->selectRaw('order_id,customer_id, sum(grand_total) as grand_total,count(id) as total_order')->orderBy('grand_total', 'desc')->limit(5)
            ->get();

        $top_order_cancelling_restaurant = Order::with(['restaurant' => function ($p) {
            $p->with('lang:restuarants_id,language,name')
                ->with('commission_category');
        }])
            ->groupBy('restaurant_id')
            ->where('order_status', 5)
            ->selectRaw('order_id,restaurant_id, sum(grand_total) as grand_total,count(id) as total_order')
            ->get();


        return view('admin.dashboard.index', compact('recent_order', 'order_status', 'total_customers', 'sales_count', 'total_orders', 'total_restaurants', 'resturants', 'top_selling_item', 'top_selling_restaurant', 'top_customer_using_app', 'top_order_cancelling_restaurant'));
    }
    public function getCount(Request $req)
    {
        $start = Carbon::parse($req->start_date)->format('Y-m-d');
        $end = Carbon::parse($req->end_date)->format('Y-m-d');

        $total_sale =  Order::whereIn('orders.order_status', [5])->whereBetween('created_at', [$start, $end])
            ->sum('orders.grand_total');
        $total_customers = Customers::RightJoin('orders', 'orders.customer_id', 'customer.id')->where('customer.status', 'active')->whereBetween('orders.created_at', [$start, $end])->groupBy('customer.id')->count();

        $total_orders = Order::whereIn('order_status', [1, 2, 3, 4, 5])->whereBetween('created_at', [$start, $end])->count();

        $total_restaurants = Restaurant::where('status', 'active')->whereBetween('created_at', [$start, $end])->where('deleted_at', NULL)->count();
        $sales_count = 'SAR ' . number_format($total_sale, 2, ".", "");
        return [
            'sales_count' => $sales_count,
            'total_customers' => $total_customers,
            'total_orders' => $total_orders,
            'total_restaurants' => $total_restaurants,
        ];
    }
    public function getCountWithRestaurant(Request $req)
    {
        $rest_id = $req->rest_id;

        $query = Order::whereIn('orders.order_status', [5]);
        if ($rest_id !== "0") {
            $query = $query->where('restaurant_id', $rest_id);
        }
        $total_sale = $query->sum('orders.grand_total');

        $query1 =  Order::whereIn('order_status', [1, 2, 3, 4, 5]);
        if ($rest_id !== "0") {
            $query1 = $query1->where('restaurant_id', $rest_id);
        }
        $total_orders = $query1->count();
        $query2 = Restaurant::where('status', 'active');
        if ($rest_id !== "0") {
            $query2 = $query2->where('id', $rest_id);
        }
        $total_restaurants = $query2->where('deleted_at', NULL)->count();
        $query3 = Customers::leftJoin('orders', 'customer.id', 'orders.customer_id')->where('customer.status', 'active');
        if ($rest_id !== "0") {
            $query3 = $query3->where('orders.restaurant_id', $rest_id);
        }
        $total_customers = $query3->groupBy('orders.customer_id')->count();

        $sales_count = 'SAR ' . number_format($total_sale, 2, ".", "");
        return [
            'sales_count' => $sales_count,
            'total_customers' => $total_customers,
            'total_orders' => $total_orders,
            'total_restaurants' => $total_restaurants,
        ];
    }
    public function getRecentorders(Request $req)
    {
        $rest_id = $req->rest_id;

        $status = array("2", "3", "4");
        $order_status = Config::get('constants.frontdisk_order_status');

        $query =  DB::table('orders')->select('orders.id', 'orders.grand_total', 'orders.order_id', 'orders.order_status', 'restuarants_i18n.name as rest_name', 'customer.name as customer_name', 'orders.created_at')
            ->leftJoin('customer', 'orders.customer_id', 'customer.id')
            ->leftJoin('restuarants', 'orders.restaurant_id', 'restuarants.id')
            ->leftJoin('restuarants_i18n', 'restuarants.id', 'restuarants_i18n.restuarants_id');
        if ($rest_id !== "0") {
            $query = $query->where('restaurant_id', $rest_id);
        }
        $recent_order = $query->where('restuarants_i18n.language', 'en')
            ->where('restuarants.deleted_at', NULL)
            ->whereIn('orders.order_status', $status)->orderBy('orders.created_at', 'desc')->limit(5)->get();



        return view('admin.dashboard.recent_order_filter_table', compact('recent_order', 'order_status'));
    }
    public function getTopSellingItems(Request $req)
    {
        $rest_id = $req->rest_id;
        $top_selling_item = OrderItems::with(['menu_items' => function ($q) {
            $q->with('lang:menu_item_id,name');
        }])
            ->with(['order' => function ($q) {
                $q->with(['restaurant' => function ($p) {
                    $p->with('lang:restuarants_id,language,name');
                }]);
            }])->leftJoin('orders', 'orders.id', 'order_items.order_id')

            ->where('orders.restaurant_id', $rest_id)
            ->groupBy('order_items.item_id')
            ->selectRaw('order_items.order_id,order_items.item_id, sum(order_items.item_count) as item_count')->orderBy('order_items.item_count', 'desc')->limit(5)
            ->get();

        return view('admin.dashboard.top_selling_item_filter_table', compact('top_selling_item'));
    }
}
