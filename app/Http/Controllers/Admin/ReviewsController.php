<?php

namespace App\Http\Controllers\Admin;

use App\Models\Reviews;
use App\Models\Customers;
use App\Models\Restaurant;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ReviewsController extends Controller
{
    public function index(Request $request)
    {
        $search = $request->customer;
        $search_field = $request->search_field ?? '';
        $customer = Customers::RightJoin('reviews', 'reviews.customer_id', 'customer.id')->select('customer.id', 'customer.name', 'customer.phone_number')->groupBy('reviews.customer_id')->get();
        $restaurant = Restaurant::with('lang')->where('status', 'active')->where('deleted_at', NULL)->get();

        $query = Reviews::with('customer')->with(['restaurant' => function ($q) {
            $q->with('lang');
        }]);
        if ($request->customer) {
            $query->where('customer_id', $request->customer);
        }
        if ($request->restaurant_id) {
            $query->where('restaurant_id', $request->restaurant_id);
        }
        $reviews = $query->orderBy('created_at', 'desc')->paginate(20);
        return view('admin.reviews.index', compact('reviews', 'search_field', 'customer', 'restaurant'));
    }
}
