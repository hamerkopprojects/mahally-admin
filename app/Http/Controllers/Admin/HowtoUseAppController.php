<?php

namespace App\Http\Controllers\Admin;

use App\Models\HowtoUse;
use App\Models\HowtoUseLang;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

class HowtoUseAppController extends Controller
{
    public function get()
    {
        $pages = HowtoUse::with('lang')
            ->paginate(15);
        return view('admin.cms.useapp.index', compact('pages'));
    }
    public function edit($id)
    {
        $imgEn = '';
        $imgAr = '';
        $lang = HowtoUseLang::where('how_to_id', $id)->get();
        if ($lang[0]->screen_shot) {
            $imgEn = asset(Storage::url($lang[0]->screen_shot));
        }
        if ($lang[1]->screen_shot) {
            $imgAr = asset(Storage::url($lang[1]->screen_shot));
        }



        $use_data = HowtoUse::with('lang')
            ->where('id', $id)
            ->first();
        return [
            'page' => $use_data,
            'imgEn' => $imgEn,
            'imgAr' => $imgAr
        ];
    }
    public function removeFile(Request $request)
    {
        if ($request->type === 'screenEn') {

            HowtoUseLang::where('how_to_id', $request->id)
                ->where('language', 'en')
                ->update([
                    'screen_shot' => NULL,
                ]);
        } else {

            HowtoUseLang::where('how_to_id', $request->id)
                ->where('language', 'ar')
                ->update([
                    'screen_shot' => NULL,
                ]);
        }
        return [
            'msg' => 'success'
        ];
    }
    public function uploadFile($id, Request $request)
    {
        $path = $request->file('photo')->store('public/screenshot');
        if ($request->type === 'screenEn') {

            HowtoUseLang::where('how_to_id', $id)
                ->where('language', 'en')
                ->update([
                    "screen_shot" => $path,
                ]);
        } else {
            HowtoUseLang::where('how_to_id', $id)
                ->where('language', 'ar')
                ->update([
                    "screen_shot" => $path,
                ]);
        }

        return ['image' => asset(Storage::url($path))];
    }
    public function update(Request $request)
    {
        $region = DB::transaction(function () use ($request) {

            HowtoUseLang::where('language', 'en')
                ->where('how_to_id', $request->id)
                ->update([
                    'title' => $request->title_en,
                    'content' => $request->content_en
                ]);
            HowtoUseLang::where('language', 'ar')
                ->where('how_to_id', $request->id)
                ->update([
                    'title' => $request->title_ar,
                    'content' => $request->content_ar
                ]);
        });
        return [
            'msg' => 'Success'
        ];
    }

    public function statusUpdate(Request $request)
    {

        $status = $request->status == 'Deactivate' ? 'deactive' : 'active';
        HowtoUse::where('id', $request->id)
            ->update([
                'status' => $status
            ]);
        return response()->json(['status' => 1, 'message' => 'Status updated successfully']);
    }
}
