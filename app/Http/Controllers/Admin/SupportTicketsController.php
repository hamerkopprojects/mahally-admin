<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\SupportTickets;
use Illuminate\Support\Carbon;
use App\Models\SupportTicketsLang;
use App\Http\Controllers\Controller;
use App\Http\Responses\SuccessWithData;
use App\Models\Customers;
use App\Traits\CanSaveNotification;
use App\Traits\CanSendNotification;

class SupportTicketsController extends Controller
{
    use CanSaveNotification ,CanSendNotification ;

    public function get()
    {
        $support = SupportTickets::with('lang')->with('customer')->orderBy('created_at', 'desc')->paginate(20);

        return view('admin.cms.support_tickets.index', compact('support'));
    }
    public function destroy(Request $request)
    {
        $support = SupportTickets::find($request->id);
        $support->lang()->delete();
        $support->delete();
        return response()->json(['status' => 1, 'message' => 'Support Tickets deleted successfully']);
    }
    public function getChat($id)
    {
        $chat = SupportTicketsLang::where('support_id', $id)
            ->orderBy('created_at')
            ->get();
        $details = SupportTickets::with('customer')
            ->where('id', $id)
            ->first();
        $now = now();

        $chat->map(function ($msg) use ($now) {
            $msg->message = nl2br($msg->message);
            $msg->time = Carbon::parse($msg->created_at)->diffForHumans($now);
            return $msg;
        });

        return new SuccessWithData([
            'chat' => $chat,
            'details' => $details
        ]);
    }
    public function send(Request $request, $id)
    {

        SupportTicketsLang::create([
            'support_id' => $id,
            'message' => $request->message,
            'user_type' => 'admin',
        ]);
        $support = SupportTickets::find($id);
        if ($support->app_type == 'userapp') {
            $this->notifyUser($id);
        }
        //  else if ($support->app_type == 'website') {
            // $this->sendNotificationMail($id);
        // }
        return response()->json(['status' => 1, 'message' => 'Message send successfully']);
    }
    
    protected  function notifyUser($requestId)
    {
       
        $request_data= SupportTickets::find($requestId);
        
        $owner = $request_data->customer;
        $language = $owner->language;
        $name =  $owner->name ?? '';
        
        $title = trans(
            'userapp-messages.support.title.replied', 
            ['user' => $name],
            $language
        );

        $content = [
            'en' => trans(
                'userapp-messages.support.content.replied', 
                ['subject' => $request_data->subject],
                'en'
            ),
            'ar' => trans(
                'userapp-messages.support.content.replied', 
                ['subject' => $request_data->subject],
                'ar'
            ),
        ];
       $data= $this->saveNotification(
            $content,
            'admin',
            $request_data->app_type,
            [
                'type' => 'support',
                'request_id' => $requestId,
            ],
            $owner->id
        );
        // $badge = $this->countNotification($owner->id,'user');
        if($owner->fcm_token && $owner->need_notification == 1)
        {
            $this->sendNotification(
                $owner->fcm_token,
                $title,
                [
                    'title' => $title,
                    'body' => $content[$language],
                    'type' => 'support',
                    'request_id' => $requestId,
                    'time' => now()->format('Y-m-d H:i:s'),
                    'notification_id'=>$data->id ?? ''
                ],
                0
            );

            $badge = Customers::where('id',$owner->id)->update([
                'badge_seen'=>'true'
                ]);
        }
      
    }
}
