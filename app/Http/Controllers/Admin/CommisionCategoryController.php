<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\CommissionCategory;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class CommisionCategoryController extends Controller
{
    public function get(Request $request)
    {
        $commision = CommissionCategory::paginate(20);
        return view('admin.settings.commission.index', compact('commision'));
    }

    public function store(Request $request)
    {
        $commission = DB::transaction(function () use ($request) {
            $commission = CommissionCategory::create([
                'name' => $request->name,
                'value' => $request->value,
            ]);

            return $commission;
        });
        if ($commission) {
            return [
                "msg" => "success"
            ];
        } else {
            return [
                "msg" => "error"
            ];
        }
    }
    public function edit($id)
    {
        $commission = CommissionCategory::where('id', $id)
            ->first();
        return [
            'commission' => $commission,
        ];
    }
    public function update(Request $request)
    {
        $faq = DB::transaction(function () use ($request) {

            CommissionCategory::where('id', $request->id)
                ->update(
                    [
                        'name' => $request->name,
                        'value' => $request->value,
                    ]
                );
        });

        return [
            'msg' => "success"
        ];
    }

    public function destroy(Request $request)
    {
        $commission = CommissionCategory::find($request->id);
        $commission->delete();
        return response()->json(['status' => 1, 'message' => 'Commission category deleted successfully']);
    }
}
