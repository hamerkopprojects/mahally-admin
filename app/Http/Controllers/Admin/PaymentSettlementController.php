<?php

namespace App\Http\Controllers\Admin;
use Illuminate\Support\Carbon;
use App\Models\Restaurant;
use App\Models\Order;
use App\Models\CommissionCategory;
use App\Models\PaymentSettlement;
use App\Models\SettlementHistory;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Config;
use Excel;
use App\Exports\SettlementReportExport;

class PaymentSettlementController extends Controller
{
     public function index(Request $request)
    {

         $restaurant =Restaurant::with('lang')->with('contact')->where('id', $request->id)->first();
         $c_perecent = CommissionCategory::where(['id' => $restaurant->commission_category_id])->pluck('value')->first();

         $total_order_online = Order::select(DB::raw('sum(grand_total) as total'), DB::raw('count(id) as online_count'))
            ->where('order_type', 'dine')
            ->where('restaurant_id', $request->id)
            ->whereIn('order_status', [5])
            ->where('settled', 'no')
            ->first();

         $total_order_at_counter = Order::select(DB::raw('sum(grand_total) as total'), DB::raw('count(id) as online_count'))
            ->where('order_type', 'takeaway')
            ->where('restaurant_id', $request->id)
            ->whereIn('order_status', [5])
            ->where('settled', 'no')
            ->first();

         $del_order = Order::with('customer')->with('order_item')
            ->where('restaurant_id', $request->id)
            ->whereIn('order_status', [5])
            ->where('settled', 'no')
            ->paginate(10);

         $total_orders=Order::where('restaurant_id', $request->id)
            ->whereIn('order_status', [5])
            ->where('settled', 'no')->count();

         $total_sale = $total_order_online->total + $total_order_at_counter->total;

         $commmission = $total_sale * $c_perecent / 100;
         $total_revenue = $total_sale - $commmission;
         $from = 'list';
         return view('admin.payment_settlement.index', compact('restaurant', 'c_perecent', 'total_order_online', 'total_order_at_counter', 'total_order_at_counter',
                 'total_sale', 'total_orders', 'commmission', 'total_revenue', 'del_order', 'from'));
     }
     
    public function details(Request $request)
    {
        $start = Carbon::parse($request->start_date)->format('Y-m-d');
        $end = Carbon::parse($request->end_date)->format('Y-m-d');

         $restaurant =Restaurant::with('lang')->with('contact')->where('id', $request->id)->first();
         $c_perecent = CommissionCategory::where(['id' => $restaurant->commission_category_id])->pluck('value')->first();

         $total_order_online = Order::select(DB::raw('sum(grand_total) as total'), DB::raw('count(id) as online_count'))
            ->where('order_type', 'dine')
            ->where('restaurant_id', $request->id)
            ->whereIn('order_status', [5])
            ->where('settled', 'no')
            ->whereDate('created_at', '>=', $start)
            ->whereDate('created_at', '<=', $end)
            ->first();

         $total_order_at_counter = Order::select(DB::raw('sum(grand_total) as total'), DB::raw('count(id) as online_count'))
            ->where('order_type', 'takeaway')
            ->where('restaurant_id', $request->id)
            ->whereIn('order_status', [5])
            ->where('settled', 'no')
            ->whereDate('created_at', '>=', $start)
            ->whereDate('created_at', '<=', $end)
            ->first();

         $del_order = Order::with('customer')->with('order_item')
            ->where('restaurant_id', $request->id)
            ->whereIn('order_status', [5])
            ->where('settled', 'no')
            ->whereDate('created_at', '>=', $start)
            ->whereDate('created_at', '<=', $end)
            ->paginate(10);

         $total_orders=Order::where('restaurant_id', $request->id)
            ->whereIn('order_status', [5])
            ->whereDate('created_at', '>=', $start)
            ->whereDate('created_at', '<=', $end)
            ->where('settled', 'no')->count();

         $total_sale = $total_order_online->total + $total_order_at_counter->total;

         $commmission = $total_sale * $c_perecent / 100;
         $total_revenue = $total_sale - $commmission;
         $from = 'filter';
         return view('admin.payment_settlement.details', compact('restaurant', 'c_perecent', 'total_order_online', 'total_order_at_counter', 'total_order_at_counter',
                 'total_sale', 'total_orders', 'commmission', 'total_revenue', 'del_order', 'from'));
     }
    public function create(Request $request)
    {
        $order_ids = $request->order_ids;
        $c_percent = $request->c_percent;
        $restaurant_id = $request->restaurant_id;
        $transaction_type = (Config::get('constants.transaction_type'));

        $total_orders=Order::select(DB::raw('sum(grand_total) as total'))->whereIn('id', $order_ids)
            ->whereIn('order_status', [5])->first();
        $total_sale = $total_orders->total;
        $commmission = $total_sale * $c_percent / 100;
        $settled_amount = $total_sale - $commmission;
        $order_ids = implode(' , ', $request->order_ids);
        return view('admin.payment_settlement.create', compact('order_ids', 'transaction_type', 'settled_amount', 'restaurant_id'));

    }
   public function save(Request $request)
    {
       $settle_ment=PaymentSettlement::create([
            'restaurant_id' => $request->restaurant_id,
            'settlement_date' => Carbon::parse($request->settlement_date)->format('Y-m-d H:i:s'),
            'settlement_amount' => $request->settlement_amount,
            'type' => $request->trans_type,
        ]);
        $order_ids = $request->order_ids;
        $order_id_arr = explode(' , ', $order_ids);
        foreach($order_id_arr as $order_id_val ){
            
            SettlementHistory::create([
                'settlement_id' => $settle_ment->id,
                'order_id' => $order_id_val,
            ]);    
            
            Order::where('id', $order_id_val)
                    ->update([
                        'settled' => 'yes',
                    ]);
        }
       return response()->json(['status' => 1, 'message' => 'Payment settled successfully']);
    }
    
     public function history(Request $request)
    {
         $restaurant =Restaurant::with('lang')->with('contact')->where('id', $request->id)->first();
         $payment_settlement = PaymentSettlement::where('restaurant_id', $request->id)->orderBy('id', 'desc')->paginate(10);
         $transaction_type = (Config::get('constants.transaction_type'));
         return view('admin.payment_settlement.history', compact('restaurant', 'payment_settlement', 'transaction_type'));
     }
     public function history_details(Request $request)
    {
         $pay_settle =PaymentSettlement::where('id', $request->id)->first(); 
         $order_ids =SettlementHistory::select('order_id')->where('settlement_id', $request->id)->get()->toArray();
          
          $order = Order::with('customer')->with('order_item')
            ->where('restaurant_id', $pay_settle->restaurant_id)
            ->whereIn('order_status', [5])
            ->where('settled', 'yes')
            ->whereIn('id', $order_ids)
            ->paginate(50);
          
         $restaurant =Restaurant::where('id', $pay_settle->restaurant_id)->first();
         $c_perecent = CommissionCategory::where(['id' => $restaurant->commission_category_id])->pluck('value')->first();
         return view('admin.payment_settlement.view_history', compact('order', 'c_perecent')); 
     }
     
     public function downloadExcel(Request $request, $id=null) {
        $pay_settle =PaymentSettlement::where('id', $request->id)->first();
        
        $order_ids =SettlementHistory::select('order_id')->where('settlement_id', $request->id)->get()->toArray();
        $order = Order::with('customer')->with('order_item')
            ->where('restaurant_id', $pay_settle->restaurant_id)
            ->whereIn('order_status', [5])
            ->where('settled', 'yes')
            ->whereIn('id', $order_ids)
            ->get();
          
         $restaurant =Restaurant::where('id', $pay_settle->restaurant_id)->first();
         $c_perecent = CommissionCategory::where(['id' => $restaurant->commission_category_id])->pluck('value')->first();
         
         $ord_arr = [];
         foreach($order as $row_data){
            $commission = $row_data->grand_total * $c_perecent / 100;
            $final_amount = $row_data->grand_total - $commission;
            $ord_arr[]=array(
                'ord_id' => $row_data->order_id, 
                'cust_name' => $row_data->customer[0]->name, 
                'grant_total' => $row_data->grand_total,
                'commission' => $commission,
                'revenue' => $final_amount);
         }
        
        return Excel::download( new SettlementReportExport($ord_arr), 'report.xls');
    }
}
