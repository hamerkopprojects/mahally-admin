<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Storage;
use App\User;
use App\Models\UserRoles;
use Validator;
use Config;
use DB;

class LogController extends Controller {

    //list customer
    public function index(Request $req) {
        $role = UserRoles::get();
        $search = $req->search;
        $query = User::query()->with('roles');
        if ($search) {
            $from = date("Y-m-d 00:00:00", strtotime($search));
            $to = date("Y-m-d 23:59:59", strtotime($search));

//            echo $from. ' ......... '.$to;exit;
            $query->whereBetween('last_login_date', [$from, $to]);
        }
        $user = $query->paginate(10);
        $data = [
            'user' => $user,
            'search' => $search,
        ];

        return view('admin.logs.login_activity', $data);
    }

}
