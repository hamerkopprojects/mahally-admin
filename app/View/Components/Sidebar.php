<?php

namespace App\View\Components;

use Illuminate\View\Component;
use Illuminate\Support\Facades\Auth;

class Sidebar extends Component
{

    public $menuArray;
    public $adminLogoArray;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct()
    {
        if (Auth::guard('restaurant')->check()) {
            $this->adminLogoArray = array(
                'uri' => route('restaurant.dashboard.get'),
                'img' => 'assets/images/sidelogo.png'
            );
        } else {
            $this->adminLogoArray = array(
                'uri' => route('dashboard.get'),
                'img' => 'assets/images/sidelogo.png'
            );
        }


        if (Auth::guard('restaurant')->check()) {
            $this->menuArray = [
                'dashboard' => [
                    'position' => 1,
                    'level' => 1,
                    'name' => 'Dashboard',
                    'icon' => 'ti-dashboard',
                    'uri' => route('restaurant.dashboard.get'),
                ],
                'orders' => [
                    'position' => 2,
                    'level' => 1,
                    'name' => 'Orders',
                    'icon' => 'ti-id-badge',
                    'uri' => route('active_orders'),
                ],
                'table_management' => [
                    'position' => 2,
                    'level' => 1,
                    'name' => 'Table Management',
                    'icon' => 'ti-id-badge',
                    'uri' => route('table_management.get'),
                ],
                'menu_category' => [
                    'position' => 2,
                    'level' => 1,
                    'name' => 'Menu Category',
                    'icon' => 'ti-id-badge',
                    'uri' => route('menu_categories.get'),
                ],
                'menu_items' => [
                    'position' => 2,
                    'level' => 1,
                    'name' => 'Menu Items',
                    'icon' => 'ti-id-badge',
                    'uri' => route('menu_items.get'),
                ],
                'offer_management' => [
                    'position' => 2,
                    'level' => 1,
                    'name' => 'Offer Management',
                    'icon' => 'ti-id-badge',
                    'uri' => route('offer_management'),
                ],
                'free_meal' => [
                    'position' => 2,
                    'level' => 1,
                    'name' => 'Free Meal Loyalty Program',
                    'icon' => 'ti-id-badge',
                    'uri' => route('free_meal_loyalty'),
                ],
                'payment_settlement' => [
                    'position' => 2,
                    'level' => 1,
                    'name' => 'Payment Settlement',
                    'icon' => 'ti-money',
                    'uri' => route('payment_settlement'),
                ],
                'reviews' => [
                    'position' => 2,
                    'level' => 1,
                    'name' => 'Reviews',
                    'icon' => 'ti-id-badge',
                    'uri' => route('reviews'),
                ],
                'edit_details' => [
                    'position' => 2,
                    'level' => 1,
                    'name' => 'Edit Details',
                    'icon' => 'ti-id-badge',
                    'uri' => route('create_restaurant_details'),
                ],
                'staff' => [
                    'position' => 2,
                    'level' => 1,
                    'name' => 'Staffs',
                    'icon' => 'ti-id-badge',
                    'uri' => route('staff.get'),
                ],
                'timing' => [
                    'position' => 2,
                    'level' => 1,
                    'name' => 'Timing',
                    'icon' => 'ti-id-badge',
                    'uri' => route('timing.get'),
                ],
                'addons' => [
                    'position' => 2,
                    'level' => 1,
                    'name' => 'Addons',
                    'icon' => 'ti-id-badge',
                    'uri' => route('addons'),
                ],
                'ingredients' => [
                    'position' => 2,
                    'level' => 1,
                    'name' => 'Ingredients',
                    'icon' => 'ti-id-badge',
                    'uri' => route('ingredients'),
                ],
                'attributes' => [
                    'position' => 2,
                    'level' => 1,
                    'name' => 'Attributes',
                    'icon' => 'ti-id-badge',
                    'uri' => route('resturant_attributes.get'),
                ],
            ];
        } else {
            if (Auth::user()->role_id == '1') {
                $this->menuArray = [
                    'dashboard' => [
                        'position' => 1,
                        'level' => 1,
                        'name' => 'Dashboard',
                        'icon' => 'ti-dashboard',
                        'uri' => route('dashboard.get'),
                    ],
                    'orders' => [
                        'position' => 2,
                        'level' => 1,
                        'name' => 'Orders',
                        'icon' => 'ti-shopping-cart',
                        'uri' => route('admin.active_orders'),
                    ],
                    'restaurant' => [
                        'position' => 2,
                        'level' => 1,
                        'name' => 'Restaurants',
                        'icon' => 'ti-id-badge',
                        'uri' => route('restaurants.get'),
                    ],
                    'customers' => [
                        'position' => 2,
                        'level' => 1,
                        'name' => 'Customers',
                        'icon' => 'ti-user',
                        'uri' => route('customers.get'),
                    ],
                    'reviews' => [
                        'position' => 2,
                        'level' => 1,
                        'name' => 'Reviews',
                        'icon' => 'ti-id-badge',
                        'uri' => route('admin.reviews'),
                    ],
                    'promocode' => [
                        'position' => 2,
                        'level' => 1,
                        'name' => 'Promo codes',
                        'icon' => 'ti-shortcode',
                        'uri' => route('promocode.get'),
                    ],
                    'users' => [
                        'position' => 2,
                        'level' => 1,
                        'name' => 'Users',
                        'icon' => 'ti-user',
                        'uri' => route('user-list.get'),
                    ],
                    'reports' => [
                        'position' => 8,
                        'level' => 1,
                        'name' => 'Reports',
                        'icon' => 'ti-bar-chart-alt',
                        'uri' => route('reports'),
                    ],
                    'notifications' => [
                        'position' => 7,
                        'level' => 2,
                        'name' => 'Notifications',
                        'icon' => 'ti-bell',
                        'submenus' => [
                            [
                                'name' => 'Admin Notifications',
                                'icon' => '',
                                'uri' => route('admin-notification.get'),
                            ],
                            // [
                            //     'name' => 'Other Notification',
                            //     'icon' => '',
                            //     'uri' => '#',
                            // ],
                        ]
                    ],
                    'cms' => [
                        'position' => 3,
                        'level' => 2,
                        'name' => 'CMS',
                        'icon' => 'ti-book',
                        'submenus' => [
                            [
                                'name' => 'FAQs',
                                'icon' => '',
                                'uri' => route('faq.get'),
                            ],
                            [
                                'name' => 'Pages',
                                'icon' => '',
                                'uri' => route('pages.get'),
                            ],
                            [
                                'name' => 'How To Use App',
                                'icon' => '',
                                'uri' => route('use.get'),
                            ],
                            [
                                'name' => 'Support Tickets',
                                'icon' => '',
                                'uri' => route('support-tickets.get'),
                            ],
                        ]
                    ],
                    'logs' => [
                        'position' => 11,
                        'level' => 2,
                        'name' => 'Logs',
                        'icon' => 'ti-timer',
                        'submenus' => [
                            [
                                'name' => 'Login activity',
                                'icon' => '',
                                'uri' => route('logs'),
                            ],
                            [
                                'name' => 'Order activity',
                                'icon' => '',
                                'uri' => route('order-activity'),
                            ]
                        ],
                    ],
                    'autosuggest' => [
                        'position' => 4,
                        'level' => 2,
                        'name' => 'Settings',
                        'icon' => 'ti-anchor',
                        'submenus' => [
                            [
                                'name' => 'Restaurant Types',
                                'icon' => '',
                                'uri' => route('autosuggest', ['title' => 'restaurant_type']),
                            ],
                            [
                                'name' => 'Cities',
                                'icon' => '',
                                'uri' => route('autosuggest', ['title' => 'city']),
                            ],
                            [
                                'name' => 'Facilities',
                                'icon' => '',
                                'uri' => route('autosuggest', ['title' => 'facility']),
                            ],
                            [
                                'name' => 'Terms',
                                'icon' => '',
                                'uri' => route('autosuggest', ['title' => 'terms']),
                            ],
                            [
                                'name' => 'Addons',
                                'icon' => '',
                                'uri' => route('autosuggest', ['title' => 'addons']),
                            ],
                            [
                                'name' => 'Ingredients',
                                'icon' => '',
                                'uri' => route('autosuggest', ['title' => 'ingredients']),
                            ],
                            [
                                'name' => 'Attributes',
                                'icon' => '',
                                'uri' => route('attribute.get'),
                            ],
                            [
                                'name' => 'Rating Segments',
                                'icon' => '',
                                'uri' => route('autosuggest', ['title' => 'rating_segments']),
                            ],
                            [
                                'name' => 'Commission Categories',
                                'icon' => '',
                                'uri' => route('commission.get'),
                            ],
                            [
                                'name' => 'Cancellation Reasons',
                                'icon' => '',
                                'uri' => route('cancel.get'),
                            ],
                            [
                                'name' => 'Memberships',
                                'icon' => '',
                                'uri' => route('membership.get'),
                            ],
                            [
                                'name' => 'Other Settings',
                                'icon' => '',
                                'uri' => route('other.get')
                            ],


                        ],
                    ],
                ];
            } else {
                $this->menuArray = [
                    'dashboard' => [
                        'position' => 1,
                        'level' => 1,
                        'name' => 'Dashboard',
                        'icon' => 'ti-dashboard',
                        'uri' => route('restaurant-owner-dashboard.get'),
                    ],
                ];
            }
        }
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return view('components.sidebar');
    }
}
