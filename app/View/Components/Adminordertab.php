<?php

namespace App\View\Components;


use Illuminate\View\Component;

class Adminordertab extends Component
{
    public $activeTab;

    public $tabs = [
        'active_order' => 'ACTIVE ORDERS',
        'delivered_order' => 'DELIVERED ORDERS',
    ];

    public $url = [
        'active_order' => "active_orders",
        'delivered_order' => "delivered_order",
    ];



    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($activeTab)
    {

        $this->activeTab = $activeTab;
    }



    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return view('components.adminordertab');
    }
}
