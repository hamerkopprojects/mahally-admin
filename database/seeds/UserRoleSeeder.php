<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserRoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('user_roles')->insert([
            [
                'name' => 'Admin ',
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'name' => 'Sales Agent ',
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'name' => 'Restaurant Owner',
                'created_at' => now(),
                'updated_at' => now(),
            ],
        ]);
    }
}
