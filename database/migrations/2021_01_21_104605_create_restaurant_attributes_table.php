<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRestaurantAttributesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('restaurant_attributes', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->smallIncrements('id');
            $table->unsignedBigInteger('restuarants_id');
            $table->unsignedSmallInteger('attribute_id')->nullable();
            $table->enum('status', ['active', 'deactive']);
            $table->enum('is_mandatory', ['Y', 'N'])->default('N');
            $table->enum('type', ['From Main List', 'By Restaurant'])->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('restuarants_id')
                ->references('id')
                ->on('restuarants')
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->foreign('attribute_id')
                ->references('id')
                ->on('attributes')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('restaurant_attributes');
    }
}
