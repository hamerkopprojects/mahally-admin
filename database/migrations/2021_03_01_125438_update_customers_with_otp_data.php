<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateCustomersWithOtpData extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('customer', function (Blueprint $table) {
            $table->text('name')->nullable()->change();
        });

        Schema::table('customer', function (Blueprint $table) {
            $table->enum('language',['en','ar'])->default('en')->after('status');
            $table->boolean('need_notification')->default(1)->after('status');
            $table->dateTime('otp_generated_at')->nullable()->after('status');
            $table->string('otp', 6)->nullable()->after('status');
            $table->string('fcm_token', 1000)
                ->nullable()
                ->after('status');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('customer', function (Blueprint $table) {
            
        });
    }
}
