<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRestaurantIngredientsI18nTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('restaurant_ingredients_i18n', function (Blueprint $table) {
            $table->id();
            $table->engine = 'InnoDB';
            $table->unsignedBigInteger('ingredient_id');
            $table->text('name');
            $table->enum('language', ['en', 'ar']);
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('ingredient_id')
                ->references('id')
                ->on('restaurant_ingredients')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('restaurant_ingredients_i18n');
    }
}
