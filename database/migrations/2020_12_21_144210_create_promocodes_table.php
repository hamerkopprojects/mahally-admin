<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePromocodesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('promocodes', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->id();
            $table->string('promocode');
            $table->enum('type', ['Amount', 'Percentage'])->nullable();
            $table->string('amount')->nullable();
            $table->datetime('expire_in')->nullable();
            $table->text('max_usage', 20)->nullable();
            $table->text('max_usage_per_user', 20)->nullable();
            $table->enum('status', ['active', 'deactive']);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('promocodes');
    }
}
