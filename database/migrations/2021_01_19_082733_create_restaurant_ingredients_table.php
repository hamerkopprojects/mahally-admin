<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRestaurantIngredientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('restaurant_ingredients', function (Blueprint $table) {
            $table->id();
            $table->engine = 'InnoDB';
            $table->unsignedBigInteger('restuarants_id');
            $table->unsignedBigInteger('autosuggest_id')->nullable();
            $table->enum('status', ['active', 'deactive'])->nullable();
            $table->enum('type', ['From Main List', 'By Restaurant'])->nullable();
            $table->text('image_path')->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('restuarants_id')
                ->references('id')
                ->on('restuarants')
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->foreign('autosuggest_id')
                ->references('id')
                ->on('autosuggest')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('restaurant_ingredients');
    }
}
