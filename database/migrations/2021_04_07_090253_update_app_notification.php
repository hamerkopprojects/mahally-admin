<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateAppNotification extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('app_notifications', function (Blueprint $table) {
            $table->dropColumn('from_type');
            $table->dropColumn('to_type');
        });

        Schema::table('app_notifications', function (Blueprint $table) {
           $table->enum('from_type',['userapp','admin']);
           $table->enum('to_type',['userapp','admin']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
