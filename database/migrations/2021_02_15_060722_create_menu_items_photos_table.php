<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMenuItemsPhotosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('menu_items_photos', function (Blueprint $table) {
            $table->smallIncrements('id');
            $table->engine = 'InnoDB';
            $table->unsignedSmallInteger('menu_item_id');
            $table->string('image_path');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('menu_item_id')
                ->references('id')
                ->on('menu_items')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('menu_items_photos');
    }
}
