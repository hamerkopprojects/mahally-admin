<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRestuarantsContactinfoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('restuarants_contactinfo', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->id();
            $table->unsignedBigInteger('restuarants_id');
            $table->text('contact_person', 200)->nullable();
            $table->text('designation', 200)->nullable();
            $table->text('phone', 20)->nullable();
            $table->text('email', 20)->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('restuarants_id')
                ->references('id')
                ->on('restuarants')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('restuarants_contactinfo');
    }
}
