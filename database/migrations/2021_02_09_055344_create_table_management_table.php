<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableManagementTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('table_management', function (Blueprint $table) {
            $table->smallIncrements('id');
            $table->engine = 'InnoDB';
            $table->unsignedBigInteger('restuarants_id');
            $table->text('table_number');
            $table->text('table_color');
            $table->enum('status', ['active', 'deactive'])->default('deactive');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('restuarants_id')
                ->references('id')
                ->on('restuarants')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('table_management');
    }
}
