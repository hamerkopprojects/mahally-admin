<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\Userapp\FaqController;
use App\Http\Controllers\Api\Userapp\LoginController;
use App\Http\Controllers\Api\Userapp\OfferController;
use App\Http\Controllers\Api\Userapp\OrderController;
use App\Http\Controllers\Api\Userapp\AppDataController;
use App\Http\Controllers\Api\Userapp\CheckoutController;
use App\Http\Controllers\Api\Userapp\LanguageController;
use App\Http\Controllers\Api\Userapp\EditProfileController;
use App\Http\Controllers\Api\Userapp\NotificationController;
use App\Http\Controllers\Api\Userapp\RestaurentListController;
use App\Http\Controllers\Api\Userapp\SupportRequestController;
use App\Http\Controllers\Api\Userapp\ItemDetailsPageController;
use App\Http\Controllers\Api\Userapp\FreeLoyalityMealController;
use App\Http\Controllers\Api\Userapp\RestaurentDetailsController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::post('login', [LoginController::class, 'UserLogin']);
Route::post('verify', [LoginController::class, 'otpVerify']);
Route::post('resend', [LoginController::class, 'resend']);
Route::get('terms-and-conditions', [AppDataController::class, 'terms']);
Route::get('aboutas', [AppDataController::class, 'aboutas']);
Route::get('useapp', [AppDataController::class, 'getUseAppData']);
Route::post('vat-data', [CheckoutController::class, 'vatDetails']);



Route::get('faq', [FaqController::class, 'get']);
Route::post('offers', [OfferController::class, 'listOffers']);
Route::post('restaurent', [RestaurentListController::class, 'get']);
Route::get('privacy-policies', [AppDataController::class, 'privacy']);
Route::post('restaurent/{id}', [RestaurentDetailsController::class, 'details']);
Route::post('item/{id}', [ItemDetailsPageController::class, 'getItems']);
Route::get('restaurant-type', [RestaurentListController::class, 'restaurantType']);
Route::post('nearby/restaurant', [RestaurentListController::class, 'nearBy']);
Route::post('loyality-meal', [FreeLoyalityMealController::class, 'getMeal']);

Route::get('/rating', [OfferController::class, 'listRating']);
Route::get('/reviews/{restaurant}', [OfferController::class, 'getReviews']);
Route::get('/order-review/{order}', [OfferController::class, 'getOrderReview']);
Route::post('/promocode', [OfferController::class, 'promocodeCheck']);


Route::group(['middleware' => ['auth:userapp' ,'scope:customer']], function () {
    Route::post('user-logout', [LoginController::class, 'logout']);
    Route::get('profile', [EditProfileController::class, 'editProfile']);
    Route::post('profile/update', [EditProfileController::class, 'updateProfile']);
    Route::post('lang', [LanguageController::class, 'update']);
    Route::post('checkout', [CheckoutController::class, 'checkOut']);
    Route::get('order/list', [OrderController::class, 'orderList']);
    Route::get('order/details/{id}', [OrderController::class, 'orderDetails']);
    Route::post('order/active-orders', [OrderController::class, 'activeOrderList']);
    
    Route::post('support', [SupportRequestController::class, 'index']);
    Route::get('support/{id}', [SupportRequestController::class, 'supportChat']);
    Route::post('support/chat', [SupportRequestController::class, 'sendChat']);

    // notification 
    Route::get('/notification/list', [NotificationController::class, 'get']);
    Route::get('/notification/read/{id}', [NotificationController::class, 'markAsRead']);
    Route::put('notifications', [NotificationController::class, 'toggle']);
    Route::get('/check/need-notification', [NotificationController::class, 'notificationCheck']);
    Route::get('notification/count', [NotificationController::class, 'unreadCount']);
    Route::get('notification/badge', [NotificationController::class, 'badgeUpdate']);
    //  Rating 
    Route::post('rate/order', [OfferController::class, 'ratingOrder']);


});
