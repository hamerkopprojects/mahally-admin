<?php

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Session;
use App\Http\Controllers\Admin\EditProfile;
use App\Http\Controllers\Admin\FaqsController;
use App\Http\Controllers\Admin\PagesController;
use App\Http\Controllers\Admin\CustomersController;
use App\Http\Controllers\Admin\DashBoardController;
use App\Http\Controllers\Restaurant\ChangePassword;
use App\Http\Controllers\Admin\AttributesController;
use App\Http\Controllers\Admin\MembershipController;
use App\Http\Controllers\Admin\PromocodesController;
use App\Http\Controllers\Restaurant\OrderController;
use App\Http\Controllers\Restaurant\StaffController;
use App\Http\Controllers\UserVerificationController;
use App\Http\Controllers\Admin\AutoSuggestController;
use App\Http\Controllers\Admin\HowtoUseAppController;
use App\Http\Controllers\Restaurant\AddonsController;
use App\Http\Controllers\Restaurant\TimingController;
use App\Http\Controllers\Restaurant\ReviewsController;
use App\Http\Controllers\Admin\OrderActivityController;
use App\Http\Controllers\Admin\OtherSettingsController;
use App\Http\Controllers\Restaurant\MenuItemController;
use App\Http\Controllers\Admin\OwnerDashboardController;
use App\Http\Controllers\Admin\SupportTicketsController;
use App\Http\Controllers\Admin\UserManagementController;
use App\Http\Controllers\Restaurant\FreeMealsController;
use App\Http\Controllers\Restaurant\OrderViewController;
use App\Http\Controllers\RestaurantOwnerLoginController;
use App\Http\Controllers\Restaurant\OrderExcelController;
use App\Http\Controllers\Restaurant\SettlementController;
use App\Http\Controllers\Restaurant\IngredientsController;
use App\Http\Controllers\Admin\AdminNotificationController;
use App\Http\Controllers\Admin\CommisionCategoryController;
use App\Http\Controllers\Admin\PaymentSettlementController;
use App\Http\Controllers\Restaurant\MenuCategoryController;
use App\Http\Controllers\Admin\CancellationReasonController;
use App\Http\Controllers\Restaurant\RestaurantHomeController;
use App\Http\Controllers\Admin\RestaurantManagementController;
use App\Http\Controllers\Restaurant\OfferManagementController;
use App\Http\Controllers\Restaurant\RestaurantLoginController;
use App\Http\Controllers\Restaurant\TableManagementController;
// use App\Http\Controllers\Restaurant\SettlementController;
use App\Http\Controllers\Restaurant\RestaurantDetailsController;
use App\Http\Controllers\Restaurant\RestaurantDashboardController;
use App\Http\Controllers\Restaurant\RestaurantAttributesController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Route::get('/salesagent/login', function () {
    return view('auth.sales_login');
});
Route::get('/owner/login', function () {
    return view('auth.owner_login');
});

Auth::routes();
Route::get('setlocale/{locale}', function ($locale) {
    if (in_array($locale, \Config::get('app.locales'))) {
        session(['locale' => $locale]);
    }
    return redirect()->back();
});

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['prefix' => 'admin', 'middleware' => 'auth:web'], function () {
    Route::get("/dashboard", [DashBoardController::class, 'get'])->name('dashboard.get');
    Route::post('/dashboard/getCount', [DashBoardController::class, 'getCount'])->name('dashboard.getCount');
    Route::post('/dashboard/getCountWithRestaurant', [DashBoardController::class, 'getCountWithRestaurant'])->name('dashboard.getCountWithRestaurant');
    Route::post('/dashboard/getRecentorders', [DashBoardController::class, 'getRecentorders'])->name('dashboard.getRecentorders');
    Route::post('/dashboard/getTopSellingItems', [DashBoardController::class, 'getTopSellingItems'])->name('dashboard.getTopSellingItems');

    Route::get("/restaurant_owner_dashboard", [OwnerDashboardController::class, 'get'])->name('restaurant-owner-dashboard.get');

    Route::get("/edit-user", [EditProfile::class, 'editUser'])->name('user.edit');
    Route::post("/name-rest", [EditProfile::class, 'updateName'])->name('name.reset');
    Route::post("/password-change", [EditProfile::class, 'updatePassword'])->name('password.new');

    Route::get("/users", [UserManagementController::class, 'get'])->name('user-list.get');
    Route::post("/user/store", [UserManagementController::class, 'store'])->name('user-list.store');
    Route::get("/user-list/edit/{id}", [UserManagementController::class, 'edit'])->name('user-list.edit');
    Route::post("/user-list/update", [UserManagementController::class, 'update'])->name('user-list.update');
    Route::post("/user-list/status/update", [UserManagementController::class, 'statusUpdate'])->name('user-list.status.update');
    Route::post("/user-list/delete", [UserManagementController::class, 'destroy'])->name('user-list.delete');
    Route::post("/user-list/sendPassword", [UserManagementController::class, 'passwordSend'])->name('user-list.setPassword');
    Route::get("/user-list/link_resturant/{id}", [UserManagementController::class, 'link_resturant'])->name('user-list.link_resturant');
    Route::post("/user-list/add_resturants", [UserManagementController::class, 'add_resturants'])->name('user-list.add_resturants');

    Route::get("/faqs", [FaqsController::class, 'get'])->name('faq.get');
    Route::get("/faq/edit/{id}", [FaqsController::class, 'edit'])->name('faq.edit');
    Route::post("/faqs/update", [FaqsController::class, 'update'])->name('faq.update');
    Route::post("/faqs/store", [FaqsController::class, 'store'])->name('faq.store');
    Route::post("/faqs/status-update", [FaqsController::class, 'statusUpdate'])->name('faq.status.update');
    Route::post("/faqs/delete", [FaqsController::class, 'destroy'])->name('faq.delete');
    Route::post("/faqs/search", [FaqsController::class, 'search'])->name('faq.search');

    Route::get("/pages", [PagesController::class, 'get'])->name('pages.get');
    Route::get("/pages/edit/{id}", [PagesController::class, 'edit'])->name('pages.edit');
    Route::post("/pages/update", [PagesController::class, 'update'])->name('pages.update');

    Route::get("/how-to-use-app", [HowtoUseAppController::class, 'get'])->name('use.get');
    Route::get("/howto-use/edit/{id}", [HowtoUseAppController::class, 'edit'])->name('use.edit');
    Route::post("/howto-use/update", [HowtoUseAppController::class, 'update'])->name('use.update');
    Route::post("/howto-use/file/delete", [HowtoUseAppController::class, 'removeFile'])->name('use.image.delete');
    Route::post("/howto-use/file/delete", [HowtoUseAppController::class, 'removeFile'])->name('use.image.delete');
    Route::post("/howto-use/upload/file/{id}", [HowtoUseAppController::class, 'uploadFile'])->name('use.image.upload');
    Route::post("/howto-use/update-status", [HowtoUseAppController::class, 'statusUpdate'])->name('userapp.status-change');

    // Auto Suggest
    Route::get("/autosuggest/{title}", [AutoSuggestController::class, 'index'])->name('autosuggest');
    Route::post("/autosuggest/store", [AutoSuggestController::class, 'store'])->name('autosuggest.store');
    Route::get("/autosuggest/edit/{id}", [AutoSuggestController::class, 'edit'])->name('autosuggest.edit');
    Route::post("/autosuggest/update", [AutoSuggestController::class, 'update'])->name('autosuggest.update');
    Route::post("/autosuggest/status/update", [AutoSuggestController::class, 'statusUpdate'])->name('autosuggest.status.update');
    Route::post("/autosuggest/delete", [AutoSuggestController::class, 'destroy'])->name('autosuggest.delete');
    Route::post("/autosuggest/search", [AutoSuggestController::class, 'search'])->name('autosuggest.search');
    Route::get("/autosuggest/restaurant_type", [AutoSuggestController::class, 'restaurant_type'])->name('autosuggest.restaurant_type');
    
    Route::post('/autosuggest/upload_image', [AutoSuggestController::class, 'upload_image'])->name('admin.upload_image');
    Route::post('/autosuggest/detete_img', [AutoSuggestController::class, 'detete_img'])->name('admin.detete_img');


    Route::get("/other-settings", [OtherSettingsController::class, 'get'])->name('other.get');
    Route::post("/other-settings/store", [OtherSettingsController::class, 'store'])->name('other.store');

    Route::get("/cancel-reasons", [CancellationReasonController::class, 'get'])->name('cancel.get');
    Route::post("/cancel-reasons/store", [CancellationReasonController::class, 'store'])->name('cancel.store');
    Route::get("/cancel-reasons/edit/{id}", [CancellationReasonController::class, 'edit'])->name('cancel.edit');
    Route::post("/cancel-reasons/update", [CancellationReasonController::class, 'update'])->name('cancel.update');
    Route::post("/cancel-reasons/delete", [CancellationReasonController::class, 'destroy'])->name('cancel.delete');
    Route::post("/cancel-reasons/auto-complete", [CancellationReasonController::class, 'autoComplete'])->name('cancel.auto');

    Route::get("/commission", [CommisionCategoryController::class, 'get'])->name('commission.get');
    Route::get("/commission/edit/{id}", [CommisionCategoryController::class, 'edit'])->name('commission.edit');
    Route::post("/commission/update", [CommisionCategoryController::class, 'update'])->name('commission.update');
    Route::post("/commission/store", [CommisionCategoryController::class, 'store'])->name('commission.store');
    Route::post("/commission/delete", [CommisionCategoryController::class, 'destroy'])->name('commission.delete');

    Route::get("/membership", [MembershipController::class, 'get'])->name('membership.get');
    Route::get("/membership/edit/{id}", [MembershipController::class, 'edit'])->name('membership.edit');
    Route::post("/membership/update", [MembershipController::class, 'update'])->name('membership.update');
    Route::post("/membership/store", [MembershipController::class, 'store'])->name('membership.store');
    Route::post("/membership/status/update", [MembershipController::class, 'statusUpdate'])->name('membership.status.update');
    Route::post("/membership/delete", [MembershipController::class, 'destroy'])->name('membership.delete');

    Route::get("/attribute", [AttributesController::class, 'get'])->name('attribute.get');
    Route::get("/attribute/edit/{id}", [AttributesController::class, 'edit'])->name('attribute.edit');
    Route::post("/attribute/update", [AttributesController::class, 'update'])->name('attribute.update');
    Route::post("/attribute/store", [AttributesController::class, 'store'])->name('attribute.store');
    Route::post("/attribute/status-update", [AttributesController::class, 'statusUpdate'])->name('attribute.status.update');
    Route::post("/attribute/delete", [AttributesController::class, 'destroy'])->name('attribute.delete');
    Route::post("/attribute/search", [AttributesController::class, 'search'])->name('attribute.search');
    Route::get("/attribute/create", [AttributesController::class, 'create'])->name('create_attribute');
    Route::post('/attribute/save', [AttributesController::class, 'save'])->name('save_attribute');
    Route::post('/attribute/activate', [AttributesController::class, 'activate'])->name('activate_attribute');
    // Notification 
    Route::get("/admin-notifications", [AdminNotificationController::class, 'get'])->name('admin-notification.get');
    Route::post("/admin-notifications/store", [AdminNotificationController::class, 'store'])->name('admin-notification.store');
    Route::post("/admin-notifications/delete", [AdminNotificationController::class, 'delete'])->name('admin-notification.delete');

    Route::get("/promocode", [PromocodesController::class, 'get'])->name('promocode.get');
    Route::get("/promocode/edit/{id}", [PromocodesController::class, 'edit'])->name('promocode.edit');
    Route::post("/promocode/update", [PromocodesController::class, 'update'])->name('promocode.update');
    Route::post("/promocode/store", [PromocodesController::class, 'store'])->name('promocode.store');
    Route::post("/promocode/status-update", [PromocodesController::class, 'statusUpdate'])->name('promocode.status.update');
    Route::post("/promocode/delete", [PromocodesController::class, 'destroy'])->name('promocode.delete');
    Route::post("/promocode/search", [PromocodesController::class, 'search'])->name('promocode.search');

    Route::get("/restaurants", [RestaurantManagementController::class, 'get'])->name('restaurants.get');
    Route::get('/restaurants/create', [RestaurantManagementController::class, 'create'])->name('create_restaurant');
    Route::get('/restaurants/tabs', [RestaurantManagementController::class, 'get_tabs'])->name('rest_tabs');
    Route::get("/restaurants/edit/{id}", [RestaurantManagementController::class, 'edit'])->name('restaurants.edit');
    Route::post("/restaurants/update", [RestaurantManagementController::class, 'update'])->name('restaurants.update');
    Route::post("/restaurants/store", [RestaurantManagementController::class, 'store'])->name('restaurants.store');
    Route::post("/restaurants/status-update", [RestaurantManagementController::class, 'statusUpdate'])->name('restaurants.status.update');
    Route::post("/restaurants/delete", [RestaurantManagementController::class, 'destroy'])->name('restaurants.delete');
    Route::post("/restaurants/welcomeSend", [RestaurantManagementController::class, 'welcomeSend'])->name('restaurants.sentWelcome');

    Route::get("restaurant-qr/{id}", [RestaurantManagementController::class, 'qrScan'])->name('admin.restaurant-qrcode');

    Route::post('/restaurants/basic_info', [RestaurantManagementController::class, 'basic_info'])->name('save_basic_info');
    Route::get('/restaurants/tabs', [RestaurantManagementController::class, 'get_tabs'])->name('rest_tabs');
    Route::post('/restaurants/contact_info', [RestaurantManagementController::class, 'contact_info'])->name('save_contact_info');
    Route::post('/restaurants/documents_photos', [RestaurantManagementController::class, 'documents_photos'])->name('save_documents_photos');
    Route::post('/restaurants/rest_images', [RestaurantManagementController::class, 'rest_images'])->name('upload_rest_images');
    Route::post('/restaurants/detete_img', [RestaurantManagementController::class, 'detete_img'])->name('detete_rest_img');
    Route::post('/restaurants/facilities_terms', [RestaurantManagementController::class, 'facilities_terms'])->name('save_facility_terms');

    Route::post("/restaurants/auto-complete", [RestaurantManagementController::class, 'autoComplete'])->name('restaurants.auto');

    Route::get("/restaurants/payment_settlement/{id}", [PaymentSettlementController::class, 'index'])->name('restaurants.payment_settlement');
    Route::get('/restaurants/create_settlement', [PaymentSettlementController::class, 'create'])->name('restaurants.payment');
    Route::post('/restaurants/save_settlement', [PaymentSettlementController::class, 'save'])->name('restaurants.save_settlement');
    Route::get('/restaurants/settlement_filter', [PaymentSettlementController::class, 'details'])->name('restaurants.settlement_details');
    Route::get("/restaurants/settlement_history/{id}", [PaymentSettlementController::class, 'history'])->name('settlement.history');
    Route::get("/restaurants/view_settlement", [PaymentSettlementController::class, 'history_details'])->name('settlement.history_details');
    Route::get("/settlement/download-excel/{id}", [PaymentSettlementController::class, 'downloadExcel'])->name('settlement.excel-download');



    Route::get("/customers", [CustomersController::class, 'get'])->name('customers.get');
    Route::post("/customers/status-update", [CustomersController::class, 'statusUpdate'])->name('customers.status.update');
    Route::post("/customers/delete", [CustomersController::class, 'destroy'])->name('customers.delete');
    Route::post("/customers/search", [CustomersController::class, 'search'])->name('customers.search');
    Route::get('/customers/details/{id}', [CustomersController::class, 'details'])->name('view_customer');
    Route::get('/customers/viewtabs', [CustomersController::class, 'get_viewtabs'])->name('customerview_tabs');

    //Orders
    Route::get('/active_orders', 'Admin\OrderController@activeOrders')->name('admin.active_orders');
    Route::get('/delivered_order', 'Admin\OrderController@deliveredOrders')->name('admin.delivered_order');
    Route::get('/admin/order/details/{id}', 'Admin\OrderViewController@details')->name('admin.view_order');
    Route::get('/admin/order/delivered_order_details/{id}', 'Admin\OrderViewController@delivered_order_details')->name('admin.view_delivered_order');
    Route::get('/admin/order/orderview_tabs', 'Admin\OrderViewController@getViewTabs')->name('admin.orderview_tabs');
    Route::post('/admin/review_status_update', 'Admin\OrderViewController@reviewStatusUpdate')->name('admin.review_status_update');
    Route::post('/admin/order_status_update', 'Admin\OrderViewController@orderStatusUpdate')->name('admin.order_status_update');
    Route::get('/admin/order_invoice/{id}', 'Admin\OrderViewController@orderInvoice')->name('admin.order_invoice');

    //Reviews
    Route::get("/admin/reviews", 'Admin\ReviewsController@index')->name('admin.reviews');

    //Excell download
    Route::get("/download-excel/active", 'Admin\OrderExcelController@downloadExcelActive')->name('admin.order.excel-download.active');
    Route::get("/download-excel/delivered", 'Admin\OrderExcelController@downloadExcelDelivered')->name('admin.order.excel-download.delivered');


    // Reports
    Route::get('/reports', 'Admin\ReportsController@index')->name('reports');
    Route::post('/reports/salestab', 'Admin\ReportsController@salestab')->name('sales.reports');

    Route::get('/reports/saleslinechart', 'Admin\ReportsController@linechart')->name('sales.linechart');
    Route::post('/reports/linecharttab', 'Admin\ReportsController@linecharttab')->name('sales.linecharttab');

    Route::get('/reports/hourly', 'Admin\ReportsController@hourlychart')->name('sales.hourly');
    Route::post('/reports/hourlytab', 'Admin\ReportsController@hourlytab')->name('sales.hourlytab');


    // Logs
    Route::get('/logs', 'Admin\LogController@index')->name('logs');
    // order activity
    Route::get("/order-activity", [OrderActivityController::class, 'index'])->name('order-activity');

    // Support Tickets

    Route::get("/support-tickets", [SupportTicketsController::class, 'get'])->name('support-tickets.get');
    Route::get("/support-request/chat/{id}", [SupportTicketsController::class, 'getChat'])->name('support.chat.get');
    Route::POST("/support-request/chat/{id}", [SupportTicketsController::class, 'send'])->name('support.chat.send');
    Route::get("/user/list-chat/{type_app}", [SupportTicketsController::class, 'usersList'])->name('support.list_user');
});

Route::post("token/verify", [UserVerificationController::class, 'resetUserPassword'])->name('user.verify');
Route::post("verify", [UserVerificationController::class, 'verify'])->name('token.verify');
Route::post("user/password/change", [UserVerificationController::class, 'passwordUpdating'])->name('user.reset');
Route::get("user/logout", [UserVerificationController::class, 'logout'])->name('user.logout');


//Restaurant Login 

Route::get('restaurant/login', [RestaurantLoginController::class, 'login'])->name('restaurant.login');
Route::post('restaurant/login', [RestaurantLoginController::class, 'restaurantLogin'])->name('restaurant.restaurantLogin');

Route::group(['middleware' => 'auth:restaurant'], function () {

    Route::get("/edit-restaurant-password", [ChangePassword::class, 'changePassword'])->name('resturant.password.new');
    Route::post("/password-change", [ChangePassword::class, 'updatePassword'])->name('resturant.password.change');

    Route::get('restaurant/logout', [RestaurantLoginController::class, 'logout'])->name('restaurant.logout');

    Route::get('/restaurant', [RestaurantHomeController::class, 'index'])->name('restaurant');
    // Route::post('/restaurant/dashboard_getCount', [RestaurantHomeController::class, 'getCount'])->name('getCount');

    Route::get("/dashboard", [RestaurantDashboardController::class, 'get'])->name('restaurant.dashboard.get');
    Route::post('/restaurant/dashboard_getCount', [RestaurantDashboardController::class, 'getCount'])->name('getCount');

    Route::get("/staffs", [StaffController::class, 'get'])->name('staff.get');
    Route::post("/staffs/store", [StaffController::class, 'store'])->name('staffs.store');
    Route::get("/staffs/edit/{id}", [StaffController::class, 'edit'])->name('staffs.edit');
    Route::post("/staffs/update", [StaffController::class, 'update'])->name('staffs.update');
    Route::post("/staffs/status/update", [StaffController::class, 'statusUpdate'])->name('staffs.status.update');
    Route::post("/staffs/delete", [StaffController::class, 'destroy'])->name('staffs.delete');
    Route::post("/staffs/welcomeSend", [StaffController::class, 'welcomeSend'])->name('staffs.sentWelcome');

    Route::get("/addons", [AddonsController::class, 'index'])->name('addons');
    Route::post("/addons/store", [AddonsController::class, 'store'])->name('addons.store');
    Route::get("/addons/edit/{id}", [AddonsController::class, 'edit'])->name('addons.edit');
    Route::post("/addons/update", [AddonsController::class, 'update'])->name('addons.update');
    Route::post("/addons/status/update", [AddonsController::class, 'statusUpdate'])->name('addons.status.update');
    Route::post("/addons/delete", [AddonsController::class, 'destroy'])->name('addons.delete');
    Route::post("/addons/welcomeSend", [AddonsController::class, 'welcomeSend'])->name('addons.sentWelcome');
    Route::post("/addons/search", [AddonsController::class, 'search'])->name('addons.search');
    Route::post("/addons/storeMainList", [AddonsController::class, 'storeMainList'])->name('addons.storeMainList');

    Route::get("/ingredients", [IngredientsController::class, 'index'])->name('ingredients');
    Route::post("/ingredients/store", [IngredientsController::class, 'store'])->name('ingredients.store');
    Route::get("/ingredients/edit/{id}", [IngredientsController::class, 'edit'])->name('ingredients.edit');
    Route::post("/ingredients/update", [IngredientsController::class, 'update'])->name('ingredients.update');
    Route::post("/ingredients/status/update", [IngredientsController::class, 'statusUpdate'])->name('ingredients.status.update');
    Route::post("/ingredients/delete", [IngredientsController::class, 'destroy'])->name('ingredients.delete');
    Route::post("/ingredients/welcomeSend", [IngredientsController::class, 'welcomeSend'])->name('ingredients.sentWelcome');
    Route::post("/ingredients/search", [IngredientsController::class, 'search'])->name('ingredients.search');
    Route::post("/ingredients/storeMainList", [IngredientsController::class, 'storeMainList'])->name('ingredients.storeMainList');

    Route::get("/resturant_attributes", [RestaurantAttributesController::class, 'get'])->name('resturant_attributes.get');
    Route::get("/resturant_attributes/edit/{id}", [RestaurantAttributesController::class, 'edit'])->name('resturant_attributes.edit');
    Route::post("/resturant_attributes/update", [RestaurantAttributesController::class, 'update'])->name('resturant_attributes.update');
    Route::post("/resturant_attributes/store", [RestaurantAttributesController::class, 'store'])->name('resturant_attributes.store');
    Route::post("/resturant_attributes/status-update", [RestaurantAttributesController::class, 'statusUpdate'])->name('resturant_attributes.status.update');
    Route::post("/resturant_attributes/delete", [RestaurantAttributesController::class, 'destroy'])->name('resturant_attributes.delete');
    Route::post("/resturant_attributes/search", [RestaurantAttributesController::class, 'search'])->name('resturant_attributes.search');
    Route::get("/resturant_attributes/create", [RestaurantAttributesController::class, 'create'])->name('create_resturant_attributes');
    Route::post('/resturant_attributes/save', [RestaurantAttributesController::class, 'save'])->name('save_resturant_attributes');
    Route::post('/resturant_attributes/activate', [RestaurantAttributesController::class, 'activate'])->name('activate_resturant_attributes');
    Route::post("/resturant_attributes/storeMainList", [RestaurantAttributesController::class, 'storeMainList'])->name('resturant_attributes.storeMainList');

    Route::get("/restaurant_details", [RestaurantDetailsController::class, 'get'])->name('restaurant_details.get');
    Route::get('/restaurant_details/create', [RestaurantDetailsController::class, 'create'])->name('create_restaurant_details');
    Route::get('/restaurant_details/tabs', [RestaurantDetailsController::class, 'get_tabs'])->name('rest_details_tabs');
    Route::get("/restaurant_details/edit/{id}", [RestaurantDetailsController::class, 'edit'])->name('restaurant_details.edit');
    Route::post("/restaurant_details/update", [RestaurantDetailsController::class, 'update'])->name('restaurant_details.update');
    Route::post("/restaurant_details/store", [RestaurantDetailsController::class, 'store'])->name('restaurant_details.store');
    Route::post("/restaurant_details/status-update", [RestaurantDetailsController::class, 'statusUpdate'])->name('restaurant_details.status.update');
    Route::post("/restaurant_details/delete", [RestaurantDetailsController::class, 'destroy'])->name('restaurant_details.delete');

    Route::post('/restaurant_details/basic_info', [RestaurantDetailsController::class, 'basic_info'])->name('save_basic_info_details');
    Route::get('/restaurant_details/tabs', [RestaurantDetailsController::class, 'get_tabs'])->name('rest_details_tabs');
    Route::post('/restaurant_details/contact_info', [RestaurantDetailsController::class, 'contact_info'])->name('save_contact_info_details');
    Route::post('/restaurant_details/documents_photos', [RestaurantDetailsController::class, 'documents_photos'])->name('save_documents_photos_details');
    Route::post('/restaurant_details/rest_images', [RestaurantDetailsController::class, 'rest_images'])->name('upload_rest__details_images');
    Route::post('/restaurant_details/detete_img', [RestaurantDetailsController::class, 'detete_img'])->name('detete_rest_details_img');
    Route::post('/restaurant_details/facilities_terms', [RestaurantDetailsController::class, 'facilities_terms'])->name('save_facility_terms_details');
    Route::post("/restaurant_details/auto-complete", [RestaurantDetailsController::class, 'autoComplete'])->name('restaurant_details.auto');

    Route::get("/table_management", [TableManagementController::class, 'get'])->name('table_management.get');
    Route::post("/table_management/store", [TableManagementController::class, 'store'])->name('table_management.store');
    Route::get("/table_management/edit/{id}", [TableManagementController::class, 'edit'])->name('table_management.edit');
    Route::post("/table_management/update", [TableManagementController::class, 'update'])->name('table_management.update');
    Route::post("/table_management/status/update", [TableManagementController::class, 'statusUpdate'])->name('table_management.status.update');
    Route::post("/table_management/delete", [TableManagementController::class, 'destroy'])->name('table_management.delete');
    Route::post("/table_management/search", [TableManagementController::class, 'search'])->name('table_management.search');
    Route::get('/table-qr/{id}', [TableManagementController::class, 'TableCodeGeneration'])->name('table-qrcode');
    Route::get("restaurant-qr/{id}", [RestaurantManagementController::class, 'qrScan'])->name('restaurant-qrcode');

    //Timing
    Route::get("/timing", [TimingController::class, 'get'])->name('timing.get');
    Route::post("/timing/store", [TimingController::class, 'store'])->name('timing.store');
    Route::get("/timing/edit/{id}", [TimingController::class, 'edit'])->name('timing.edit');
    Route::post("/timing/update", [TimingController::class, 'update'])->name('timing.update');
    Route::post("/timing/status/update", [TimingController::class, 'statusUpdate'])->name('timing.status.update');
    Route::post("/timing/delete", [TimingController::class, 'destroy'])->name('timing.delete');
    Route::post("/timing/search", [TimingController::class, 'search'])->name('timing.search');

    //Menu Category
    Route::get("/menu_categories", [MenuCategoryController::class, 'get'])->name('menu_categories.get');
    Route::post("/menu_categories/store", [MenuCategoryController::class, 'store'])->name('menu_categories.store');
    Route::get("/menu_categories/edit/{id}", [MenuCategoryController::class, 'edit'])->name('menu_categories.edit');
    Route::post("/menu_categories/update", [MenuCategoryController::class, 'update'])->name('menu_categories.update');
    Route::post("/menu_categories/status/update", [MenuCategoryController::class, 'statusUpdate'])->name('menu_categories.status.update');
    Route::post("/menu_categories/delete", [MenuCategoryController::class, 'destroy'])->name('menu_categories.delete');
    Route::post("/menu_categories/sortable", [MenuCategoryController::class, 'sortable'])->name('menu_categories.sortable');

    //Menu Items

    Route::get("/menu_items", [MenuItemController::class, 'get'])->name('menu_items.get');
    Route::get('/menu_items/create', [MenuItemController::class, 'create'])->name('create_menu_item');
    Route::get('/menu_items/tabs', [MenuItemController::class, 'get_tabs'])->name('item_tabs');
    Route::post("/menu_items/status-update", [MenuItemController::class, 'statusUpdate'])->name('menu_items.status.update');
    Route::post("/menu_items/delete", [MenuItemController::class, 'destroy'])->name('menu_items.delete');

    Route::post('/menu_items/basic_info', [MenuItemController::class, 'basic_info'])->name('save_menu_basic_info');
    Route::post('/menu_items/photos', [MenuItemController::class, 'photos'])->name('save_photos');
    Route::post('/menu_items/item_images', [MenuItemController::class, 'item_images'])->name('upload_item_images');
    Route::post('/menu_items/detete_img', [MenuItemController::class, 'detete_img'])->name('detete_item_img');
    Route::post('/menu_items/attributes', [MenuItemController::class, 'attributes'])->name('save_attributes');
    Route::post('/menu_items/related_items', [MenuItemController::class, 'related_items'])->name('save_related_items');
    Route::get('/menu_items/attribute_value', [MenuItemController::class, 'getAttributeValue'])->name('getAttributeValue');
    Route::post("/menu_items/sortable", [MenuItemController::class, 'sortable'])->name('menu_items.sortable');
    Route::get('/menu_items/sort_item', [MenuItemController::class, 'sort_item'])->name('sort_item');

    //Offer Management

    Route::get("/offer_management", [OfferManagementController::class, 'index'])->name('offer_management');
    Route::post("/offer_management/store", [OfferManagementController::class, 'store'])->name('offer_management.store');
    Route::get("/offer_management/edit/{id}", [OfferManagementController::class, 'edit'])->name('offer_management.edit');
    Route::post("/offer_management/update", [OfferManagementController::class, 'update'])->name('offer_management.update');
    Route::post("/offer_management/status/update", [OfferManagementController::class, 'statusUpdate'])->name('offer_management.status.update');
    Route::post("/offer_management/delete", [OfferManagementController::class, 'destroy'])->name('offer_management.delete');

    //Free Meal Loyalty Program

    Route::get("/free_meal_loyalty", [FreeMealsController::class, 'index'])->name('free_meal_loyalty');
    Route::post("/free_meal_loyalty/store", [FreeMealsController::class, 'store'])->name('free_meal_loyalty.store');
    Route::get("/free_meal_loyalty/edit/{id}", [FreeMealsController::class, 'edit'])->name('free_meal_loyalty.edit');
    Route::post("/free_meal_loyalty/update", [FreeMealsController::class, 'update'])->name('free_meal_loyalty.update');
    Route::post("/free_meal_loyalty/status/update", [FreeMealsController::class, 'statusUpdate'])->name('free_meal_loyalty.status.update');
    Route::post("/free_meal_loyalty/delete", [FreeMealsController::class, 'destroy'])->name('free_meal_loyalty.delete');

    //Orders
    Route::get("/active_orders", [OrderController::class, 'activeOrders'])->name('active_orders');
    Route::get("/delivered_order", [OrderController::class, 'deliveredOrders'])->name('delivered_order');
    Route::get('/order/details/{id}', [OrderViewController::class, 'details'])->name('view_order');
    Route::get('/order/delivered_order_details/{id}', [OrderViewController::class, 'delivered_order_details'])->name('view_delivered_order');
    Route::get('/orderview_tabs', [OrderViewController::class, 'getViewTabs'])->name('orderview_tabs');
    Route::post('/review_status_update', [OrderViewController::class, 'reviewStatusUpdate'])->name('review_status_update');
    Route::post('/order_status_update', [OrderViewController::class, 'orderStatusUpdate'])->name('order_status_update');
    Route::get('/order_invoice/{id}', [OrderViewController::class, 'orderInvoice'])->name('order_invoice');

    //Reviews
    Route::get("/reviews", [ReviewsController::class, 'index'])->name('reviews');

    //Excell download
    Route::get("/download-excel/active", [OrderExcelController::class, 'downloadExcelActive'])->name('order.excel-download.active');
    Route::get("/download-excel/delivered", [OrderExcelController::class, 'downloadExcelDelivered'])->name('order.excel-download.delivered');


    Route::get("/payment_settlement", [SettlementController::class, 'index'])->name('payment_settlement');
    Route::get("/payment_settlement/filter", [SettlementController::class, 'filter'])->name('payment_settlement.filter');
});

// Route::get('restaurant/login', [RestaurantOwnerLoginController::class, 'login'])->name('restaurant.login');
