<?php

return [
    'login'=>[
        'send_succes' =>'Otp send successfully',
        'user_nt_exist'=>'User does not exist',
        'otp_wrong'=>'Wrong otp',
        'resend'=>'Otp resend successfully',
        'logout'=>'Logout successfully',
        'activate' =>'Your account is disable. please Contact mahally support team'
    ],
    'profile'=>[
        'update_succ'=>'Profile updated successfully',
        'loyality_meal'=>':already_done / :total ORDER :remaining MORE TIME TO GET THE FREE LOYALITY MEAL'
    ],
    'restaurant'=>[
        'loyality_meal'=>'ORDER :remaining MORE TIME TO GET THE FREE LOYALITY MEAL'
    ],
    'support'=>[
        'success_msg'=>[
            'support_success'=>'Thank you for submitting the support request. Our team will get back to you!',
            
        ],
        'title'=>[
           'replied'  => 'Hi :user'
        ],
        'content' =>[
            'replied' =>'Opertaion team replied on your query :subject'
        ]
        ],
        'notification'=>[
            'enabled' => 'Notification setting enabled successfully',
            'disabled' => 'Notification setting disabled successfully',
        ],
        'order'=>[
            'title'=>[
                'confirmed' => 'Congratulations :name',
                'ready_delivery'=>'Congratulations :name',
                'delivered'=>'Congratulations :name'
            ],
            'content'=>[
                'confirmed' => 'Your order :order_id confirmed',
                'ready_delivery'=>'Your order :order_id is ready for delivery',
                'delivered'=>'Your order :order_id  is delivered successfully'
            ],
        ],
        'order_status'=>[
            'Order Pending'=>'Order Pending',
            'Order Accepted' => "Order Accepted",
            'In Kitchen'=> "In Kitchen",
            'Ready for pickup'=> "Ready for pickup",
            'Delivered'=> "Delivered",
            'Cancel order' => "Cancel order",
        ],
        "promocode"=>[
            'user_exceed'=>'Maximum usage exceeded for you',
            'in_valid'=>'Invalid promocode',
            'apply'=>'Promocode applied Succesfully',
            'max_usage'=>'Maximum usage exceeded',
            
        ],
        'review' =>[
            'add_succes'=>'Review added successfully',
            'canot_review'=>'Cannot review, order is not complete'
        ],


];