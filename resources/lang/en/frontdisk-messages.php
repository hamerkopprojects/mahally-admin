<?php

return [
    'login' => [
        'user_nt_exist' => 'User does not exist',
        'invalid_email' => 'Invalid email Id',
        'send_varification_msg' => 'We have sent varification code to your mail entered',
        'invalid_verify_msg' => 'Invalid verification code',
        'success_verify_msg' => 'Email verified successfully',
        'logout' => 'Logout successfully',
        'reset_pwd_success' => 'Reset password successfully',
    ],
    'orders' => [
        'no_orders' => 'No orders available',
        'success_orders' => 'Orders available',
        'error_change_status' => 'Can not be change',
        'success_change_status' => 'Status changed successfully',
    ],
    'order_status' => [
        'Order pending' => "Order pending",
        'Order confirmed' => "Order confirmed",
        'In Kitchen'=> "In Kitchen",
        'Ready for pickup' => "Ready for pickup",
        'Delivered' => "Delivered",
        'Cancel order' => "Cancel order",
    ]

];
