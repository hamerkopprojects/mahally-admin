<?php

return [
    'login' => [
        'user_nt_exist' => 'User does not exist',
        'invalid_email' => 'Invalid email Id',
        'send_varification_msg' => 'We have sent varification code to your mail entered',
        'invalid_verify_msg' => 'Invalid verification code',
        'success_verify_msg' => 'Email verified successfully',
        'logout' => 'Logout successfully',
        'reset_pwd_success' => 'Reset password successfully',
        'pass_incorrect'=>'Login Fail, pls check your email/password'
    ],
    'order_status' =>[
        "Order confirmed" => 'Order confirmed',
        "In Kitchen" => "In Kitchen",
        "Ready for pickup" => "Ready for pickup",
    ]


];
