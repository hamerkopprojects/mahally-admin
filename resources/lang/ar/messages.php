<?php

return [
    'admin' => [
        'my_account' => 'حسابي',
        'edit_profile' => 'تعديل الملف الشخصي',
        'logout' => 'تسجيل خروج',
        'dashboard' => 'لوحة القيادة',
        'change_password' => 'Change Password'
    ],
    'search' => 'Search',
    'reset' => 'Reset',
    'form' => [
        'add' => 'ADD',
        'cancel' => 'Cancel',
        'save' => 'Save',
        'save_and_continue' => 'Save & Continue',
        'back_to_listing' => 'Back To Listing',
        'back' => 'Back',
        'done' => 'Done',
    ],
    'kitchen_staff' => [
        'add_kitchen_staff' => 'Add New Staff',
        'staffs' => 'Staffs',
        'table_header' => [
            'sl_no' => 'S.No.',
            'user_id' => 'User ID',
            'name' => 'Name',
            'email' => 'Email',
            'phone' => 'Phone',
            'role' => 'Role',
        ],
        'form' => [
            'name' => 'Name',
            'email' => 'Email',
            'phone' => 'Phone',
            'role' => 'Role',
        ],
        'placeholder' => [
            'entr_name' => 'Enter name',
            'entr_email' => 'Enter email address',
            'entr_phone' => 'Enter phone number',
        ],
        'validation' => [
            'user_name' => 'Enter required',
            'email' => 'Email required',
            'email1' => 'Enter valid email',
            'phone' => 'Phone number required',
            'phone1' => 'Please enter valid phone number',
            'role' => 'Role required',
        ],
    ],
    'addons' => [
        'addons' => 'Addons',
        'add_addon' => 'Add New Addon',
        'add_addon_from_main_list' => 'Add From Main List',
        'table_header' => [
            'ad_name_en' => 'Addons Name (EN)',
            'ad_name_ar' => 'Addons Name (AR)',
            'price' => 'Price',
            'from' => 'From',
        ],
        'form' => [
            'ad_name_en' => 'Addons Name (EN)',
            'ad_name_ar' => 'Addons Name (AR)',
            'price' => 'Price',
            'img' => 'Image',
        ],
        'placeholder' => [
            'ad_name_en' => 'Enter addons name (EN)',
            'ad_name_ar' => 'Enter addons name (AR)',
            'price' => 'Enter price',
        ],
        'validation' => [
            'ad_name_en' => 'Addon name (EN) required)',
            'ad_name_ar' => 'Addon name (AR) required',
            'price' => 'Price required',
            'price1' => 'Accept only numeric value',
        ],
    ],
    'ingredient' => [
        'ingredient' => 'INGREDIENTS',
        'add_ingredient' => 'Add New Ingredient',
        'add_ingredient_from_main_list' => 'Add From Main List',
        'table_header' => [
            'ingr_name_en' => 'Ingredients Name (EN)',
            'ingr_name_ar' => 'Ingredients Name (AR)',
            'from' => 'From',
        ],
        'form' => [
            'ingr_name_en' => 'Ingredients Name (EN)',
            'ingr_name_ar' => 'Ingredients Name (AR)',
            'img' => 'Image',
        ],
        'placeholder' => [
            'ingr_name_en' => 'Enter ingredient name (EN)',
            'ingr_name_ar' => 'Enter ingredient name (AR)',
        ],
        'validation' => [
            'title_en' => 'Ingredient name (EN) required',
            'title_ar' => 'Ingredient name (AR) required',
        ],
    ],
    'attributes' => [
        'attributes' => 'ATTRIBUTES',
        'add_attribute' => 'Add New Attribute',
        'add_attributes_from_main_list' => 'Add From Main List',
        'attr' => 'Attribute',
        'table_header' => [
            'sl_no' => 'S.No.',
            'attr_name_en' => 'Attribute Name (EN)',
            'attr_name_ar' => 'Attribute Name (AR)',
            'from' => 'From',
        ],
        'form' => [
            'attr_name_en' => 'Attribute Name (EN)',
            'attr_name_ar' => 'Attribute Name (AR)',
            'attr_val_en' => 'Attribute value (EN)',
            'attr_val_ar' => 'Attribute value (AR)',
            'is_mandatory' => 'Is this attribute is mandatory?',
        ],
        'placeholder' => [
            'attr_name_en' => 'Enter attribute name (EN)',
            'attr_name_ar' => 'Enter attribute name (AR)',
            'attr_val_en' => 'Enter attribute value (EN)',
            'attr_val_ar' => 'Enter attribute value (AR)',
        ],
        'validation' => [
            'name_en' => 'Attribute name (EN) is required.',
            'name_ar' => 'Attribute name (AR) is required.',
        ]
    ],
    'table_management' => [
        'table_management' => 'TABLE MANAGEMENT',
        'add_table_management' => 'Add New Table',
        'search_placeholder' => 'Search by Table Number',
        'table_header' => [
            'sl_no' => 'S.No.',
            'table_number' => 'Table Number',
            'table_color' => 'Table Color',
        ],
        'form' => [
            'table_number' => 'Table Number',
            'table_color' => 'Table Color',
            'table_number_placeholder' => 'Enter table number',
            'add_table' => 'Add Table',
            'edit_table' => 'Edit Table',
        ],
        'validation' => [
            'table_number_required' => 'Table number required',
            'table_color_required' => 'Table color required',
        ]
    ],
    'timing' => [
        'timing' => 'TIMING',
        'add_timing' => 'Add New Time',
        'table_header' => [
            'sl_no' => 'S.No.',
            'timing' => 'Timing.',
        ],
        'form' => [
            'start_time' => 'Start Time ',
            'end_time' => 'End Time ',
        ],
        'validation' => [
            'start_time' => 'Start time required',
            'end_time' => 'End time required',
        ]
    ],
    'menu_category' => [
        'menu_category' => 'MENU CATEGORY',
        'add_menu_category' => 'Add New Menu Category',
        'sort_item' => 'Sort Items',
        'table_header' => [
            'sl_no' => 'S.No.',
            'menu_cate_en' => 'Menu Category (EN)',
            'menu_cate_ar' => 'Menu Category (AR)',
            'available_in' => 'Available In',
            'calory' => 'Calory',
        ],
        'form' => [
            'menu_cate_en' => 'Menu Category (EN)',
            'menu_cate_ar' => 'Menu Category (AR)',
            'calories' => 'Calories',
            'available_in' => 'Available In',
            'image' => 'Image',
        ],
        'placeholder' => [
            'etr_menu_cate_en' => 'Enter menu category (EN)',
            'etr_menu_cate_ar' => 'Enter menu category (AR)',
            'calories' => 'Enter calories',
            'sel_available_in' => 'Select Available Time',
        ],
        'validation' => [
            'title_en' => 'Menu Category (EN) required',
            'title_ar' => 'Menu Category (AR) required',
            'calories' => 'Calory required',
            'available_time' => 'Available In  required.',
        ]
    ],
    'menu_items' => [
        'menu_items' => 'MENU ITEMS',
        'add_menu_item' => 'Add New Item',
        'sort_item' => 'Sort Items',
        'tab' => [
            'menu_item' => 'Menu Items',
        ],
        'table_header' => [
            'sl_no' => 'S.No.',
            'menu_item_en' => 'Menu Items (EN)',
            'menu_item_ar' => 'Menu Items (AR)',
            'menu_cate' => 'Menu Category',
            'price' => 'Price',
            'dis_price' => 'Discounted Price',
        ],
        'form' => [
            'basic_info' => [
                'menu_item_en' => 'Menu Items (EN)',
                'menu_item_ar' => 'Menu Items (AR)',
                'item_category' => 'Item Category',
                'calory' => 'Calory',
                'available_in' => 'Available In',
                'price' => 'Price',
                'dis_price' => 'Discount Price',
                'ingredients' => 'Ingredients',
                'addons' => 'Addons',
                'description_en' => 'Description (EN)',
                'description_ar' => 'Description (AR)',
            ],
            'attributes' => [
                'attri' => 'Attibutes',
                'attribute_val' => 'Attibute Values',
                'sel_attribute' => 'Select attibute',
            ],
            'photos' => [
                'photos' => 'Photos',
                'add_cover_photo' => 'Add cover photo',
                'sel_cover_photo' => 'Select cover photo:',
                'rest_images' => 'Restaurant Images',
                'photos' => 'Photos',
                'crop_img' => 'Crop Image',
                'crop_and_save' => 'Crop & Save',
                'close' => 'Close',

            ],
            'related_item' => [
                'related_item' => 'Related Items',
            ],
            'sort_items' => [
                'menu_category' => 'Menu Category',
                'sel_category' => 'Select Category',
                'sl_no' => 'S.No.',
                'menu_item_en' => 'Menu Items (EN)',
                'menu_item_ar' => 'Menu Items (AR)',
                'menu_cate' => 'Menu Category',
            ],
            'save' => 'Save',
            'save_and_continue' => 'Save & Continue',
            'back_to_listing' => 'Back To Listing',
            'back' => 'Back',

        ],
        'validation' => []
    ],
    'offer_management' => [
        'offer_management' => 'OFFER MANAGEMENT',
        'add_offer_management' => 'Add New Offer',
        'table_header' => [
            'offer_title_en' => 'Offer Title (EN)',
            'item' => 'Item',
            'dis_type' => 'Discount Type',
            'dis_value' => 'Discount Value',
            'offer_expire_in' => 'Offer Expire In',
            'coupon_code' => 'Coupon Code',
        ],
        'form' => [
            'offer_title_en' => 'Offer Title (EN)',
            'offer_title_ar' => 'Offer Title (AR)',
            'sel_item' => 'Select Item',
            'expire_in' => 'Expires In',
            'dis_type' => 'Discount Type',
            'dis_value' => 'Discount Value',
            'img' => 'Image',
        ],
        'placeholder' => [
            'etr_offer_title_en' => 'Enter Offer Title (EN)',
            'etr_offer_title_ar' => 'Enter Offer Title (AR)',
            'sel_date' => 'Select date',
            'etr_value' => 'Enter value',
        ],
        'validation' => [
            'title_en' => 'Offer Item (EN) required',
            'title_ar' => 'Offer Item (AR) required',
            'menu_item_id' => 'Item required',
            'expire_in' => 'Expire date required',
            'discount_value' => 'Price required',
            'discount_value1' => 'Accept only numeric value',
            'img_en' => 'Image (EN) required',
            'img_ar' => 'Image (AR) required',
        ]
    ],
    'free_meals' => [
        'free_meals' => 'FREE MEAL LOYALTY PROGRAM',
        'add_free_meals' => 'Add New Free Meal Loyalty',
        'table_header' => [
            'sl_no' => 'S.No',
            'title_en' => 'Title (EN)',
            'title_ar' => 'Title (AR)',
            'item' => 'Item',
            'no_order' => 'No. Of Orders',
            'expire_in' => 'Expire In',
        ],
        'form' => [
            'title_en' => 'Title (EN)',
            'title_ar' => 'Title (AR)',
            'sel_item' => 'Select Item',
            'no_order' => 'No. Of Orders',
            'expire_in' => 'Expire In',
            'description_en' => 'Description (EN)',
            'description_ar' => 'Description (AR)',
        ],
        'placeholder' => [
            'title_en' => 'Enter Title (EN)',
            'title_ar' => 'Enter Title (AR)',
            'no_order' => 'Enter number of orders',
            'expire_in' => 'Select date',
            'description_en' => 'Enter description (EN)',
            'description_ar' => 'Enter description (AR)',
        ],
        'validation' => [
            'title_en' => 'Title (EN) required',
            'title_ar' => 'Title (AR) required',
            'menu_item_id' => 'Item required',
            'expire_in' => 'Expire date required',
            'order' => 'Price required',
            'order1' => 'Accept only numeric value',
        ]
    ],
    'edit_details' => [
        'rest_profile' => 'RESTAURANT PROFILE',
        'form' => [
            'rest_name_en' => 'Restaurant Name (EN)',
            'rest_name_ar' => 'Restaurant Name (AR)',
            'rest_type' => 'Restaurant Type',
            'membership' => 'Membership',
            'sel_membership' => 'Select Membership',
            'comm_category' => 'Commission Category',
            'sel_comm_category' => 'Select commission category',
            'city' => 'City',
            'sel_city' => 'Select city',
            'open_time' => 'Opening Time',
            'close_time' => 'Closing Time',
            'vat_includes' => 'VAT Includes',
            'vat_per' => 'VAT Percentage',
            'vat_reg_no' => 'VAT Register Number',
            'location' => 'Location',
            'description_en' => 'Description (EN)',
            'description_ar' => 'Description (AR)',
            'contact_person' => 'Contact Person',
            'designation' => 'Designation',
            'phone' => 'Phone',
            'email' => 'Email',
            'restaurant' => 'Restaurant',
            'documents' => 'Documents',
            'lic_title' => 'License Title',
            'exp_in' => 'Expires In',
            'cr_copy' => 'CR Copy',
            'photos' => 'Photos',
            'add_cov_photo' => 'Add cover photo',
            'sel_cov_photo' => 'Select cover photo:',
            'add_logo' => 'Add logo',
            'sel_logo' => 'Select logo:',
            'rest_img' => 'Restaurant Images',
            'facilities' => 'Facilities',
            'terms' => 'Terms',
        ]
    ],
    'orders' => [
        'order' => 'ORDERS',
        'active_order' => 'ACTIVE ORDERS',
        'completed_order' => 'COMPLETED ORDERS',
        'dwld_reports' => 'Download report as XLS',
        'placeholder' => [
            'by_order_id' => 'By Order ID',
            'by_customer' => 'By Customer',
            'by_date' => 'By Date',
            'status' => 'Status',
        ],
        'form' => [
            'sl_no' => 'S.No',
            'order_id' => 'Order ID',
            'customer' => 'Customer',
            'order_date' => 'Order Date',
            'amount' => 'Amount',
            'status' => 'Status',
            'payment_type' => 'Payment Type',
        ],
        'order_info' => [
            'order_details' => 'ORDER DETAILS',
            'print' => 'Print',
            'order_info' => 'ORDER INFO',
            'order_id' => 'Order ID',
            'order_date' => 'Order Date',
            'payment_type' => 'Payment Type',
            'customer' => 'Customer',
            'status' => 'Status',
            'item' => 'Item',
            'quantity' => 'Quantity',
            'price' => 'Price',
            'subtotal' => 'Subtotal',
            'discount' => 'Discount',
            'total_amt' => 'TOTAL AMOUNT',
            'inclusive_vat' => 'Inclusive of VAT',
        ],
        'reviews' => [
            'reviews' => 'REVIEWS',
            'approve' => 'APPROVE',
            'reject' => 'REJECT',
            'service_quality' => 'Service quality',
            'on_time_delivery' => 'On time delivery',
            'food_quality' => 'Food quality',
        ],

    ],
    'dashboard' => [
        'restaurant_dashboard' => 'RESTAURANT DASHBOARD',
        'summary' => 'SUMMARY',
        'select_date' => 'SELECT DATE',
        'my_account' => 'MY ACCOUNT',
        'total_sale' => 'TOTAL SALE',
        'total_orders' => 'TOATL ORDERS',
        'total_customers' => 'TOTAL CUSTOMERS',
        'change_password' => 'CHANGE PASSWORD',
        'logout' => 'LOGOUT',
        'recent_orders' => 'REENT ORDERS',
        'sno' => 'SNO',
        'order_id' => 'ORDER_ID',
        'customer' => 'CUSTOMER',
        'order_date' => 'ORDER DATE',
        'amount' => 'AMOUNT',
        'status' => 'STATUS',
        'top_selling_items' => 'TOP SELLING ITEMS',
        'item_name' => 'ITEM NAME',
        'restaurant_name' => 'RESTAURANT NAME',
        'sale_count' => 'SALE COUNT',
        'password' => 'password',
        'confirm_password' => 'CONFIRM PASSWORD',
        'enter_new_password' => 'ENTER NEW PASSWORD',
        'enter_confirm_password' => 'ENTER CONFORM PASSSWORD',
        'submit' => 'SUBMIT',
        'cancel' => 'CANCEL',
    ],
    'payment_settlement' => [
        'payment_settlement' => 'PAYMENT SETTLEMENT',
        'by_date' => 'By Date',
        'rest_name' => 'Restaurant Name',
        'phone' => 'Phone',
        'rest_code' => 'Restaurant Code',
        'email' => 'Email',
        'comm_percent' => 'Commission Percentage',
        'total_number_of_order' => 'Total number of orders',
        'tot_ord_counter' => 'Total number of order(At Counter)',
        'tot_ord_online' => 'Total number of order(Online)',
        'total_sale' => 'Total Sale',
        'commision_of_mahally' => 'Commission for Mahally',
        'membership_due_amount' => 'Membership due amount',
        'settlement_amount' => 'Settlement Amount',
        'sno' => 'S.No.',
        'trans_id' => 'Trans ID',
        'settlement_date' => 'Settlement Date',
        'settlement_amt' => 'Settlement Amount',
        'settlement_type' => 'Settlement Type',
        'no_records_found' => 'No records found!',
        'search' => 'Search',
        'reset' => 'Reset',
    ],
    'review' => [
        'reviews' => 'REVIEWS',
        'reviewed_by' => 'REVIEWED BY',
        'search_by_user' => 'SEARCH BY USER',
        'reset' => 'reset',
        'search' => 'search',
        'reviewed_by' => 'REVIEWED BY',
        'rating' => 'RATING',
        'order_details' => 'ORDER DETAILS',
        'order_info' => 'ORDER INFO',
        'approve' => 'APPROVE',
        'reject' => 'REJECT',
        'print' => 'PRINT',
        'order_info' => 'ORDER INFO',
        'order_id' => 'ORDER ID',
        'order_date' => 'ORDER DATE',
        'restaurant' => 'RESTAURANT',
        'search_by_user' => 'SEARCH BY USER',
        'customer' => 'CUSTOMER',
        'status' => 'STATUS',
        'payment_type' => 'PAYMENT TYPE',
        'item' => 'ITEM',
        'quantity' => 'QUANTITY',
        'price' => 'PRICE',
        'subtotal' => 'SUBTOTAL',
        'discount' => 'DISCOUNT',
        'total_amount' => 'TOTAL AMOUNT',
        'inclusive_of_vat' => 'INCLUSIVE OF VAT',
    ]
];
