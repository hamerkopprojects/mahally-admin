# Token verification

---

Verification Code

### Details

| Method | Uri                     | Authorization |
| :----- | :---------------------- | :------------ |
| POST   | `api/front-desk-verify` | No            |

### Request Params

```json
{
    "email": "admin@mahally.com5",
    "verification_code": "123456"
}
```

### Response

```json
{
    "success": true,
    "data": {
        "user": {
            "id": 10,
            "restuarants_id": 1,
            "name": "test3",
            "user_id": "MAH-0000-00007",
            "email": "test@test3.com",
            "phone": "7896857419",
            "status": "active",
            "created_at": "2021-03-02T08:02:05.000000Z",
            "updated_at": "2021-03-02T13:19:10.000000Z",
            "deleted_at": null,
            "role_id": null,
            "email_verified_at": "2021-03-02 09:20:30",
            "password": "1234567",
            "verification_code": "123456"
        },
        "msg": "Email verified successfully"
    }
}
```
