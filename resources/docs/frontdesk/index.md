-   ## Get Started
    -   [Overview](/{{route}}/{{version}}/overview)
-   ## Login

    -   [Login](/{{route}}/{{version}}/auth/login)
    -   [Forgot Password](/{{route}}/{{version}}/auth/forgotpassword)
    -   [Verifivation Code](/{{route}}/{{version}}/auth/verify)
    -   [Reset Password](/{{route}}/{{version}}/auth/resetpassword)
    -   [Logout](/{{route}}/{{version}}/auth/logout)

    ## Order

    -   [Order](/{{route}}/{{version}}/order/orders)
    -   [Order Status](/{{route}}/{{version}}/order/changeorderstatus)
    -   [Search](/{{route}}/{{version}}/order/orderid)
    -   [Search Item Listing](/{{route}}/{{version}}/order/search)
