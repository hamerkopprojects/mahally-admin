# Search Id

---

Listing order id for searching

### Details

| Method | Uri                           | Authorization |
| :----- | :---------------------------- | :------------ |
| POST   | `api/frontdesk/order-id-list` | Yes           |

### Request Params

```json
{
    "key": "MAH03",
    "order_status": "1 or 2 or 3 or 4 or 5"
}
```

### Status Value

| param | value            |
| :---- | :--------------- |
| 1     | Pending Orders   |
| 2     | Confirmed Orders |
| 3     | In Kitchen       |
| 4     | Ready For Pickup |
| 5     | Delivered        |

### Response

```json
{
    "success": true,
    "msg": "Orders Id list available"
}
```
