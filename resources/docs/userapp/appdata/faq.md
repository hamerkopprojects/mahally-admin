# FAQ

---

FAQ details

### Details

| Method | Uri                    | Authorization |
| :----- | :--------------------- | :------------ |
| GET    | `api/user-app/faq` | No            |



### Response

```json
{
    "success": true,
    "data": [
        {
            "id": 1,
            "lang": {
                "en": {
                    "id": 1,
                    "faq_id": 1,
                    "question": "English",
                    "answer": "<p>testing</p>",
                    "language": "en"
                },
                "ar": {
                    "id": 2,
                    "faq_id": 1,
                    "question": "Arabic",
                    "answer": "<p>testing</p>",
                    "language": "ar"
                }
            }
        }
    ]
}
```
