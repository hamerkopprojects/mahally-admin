# Switch language

---


### Details

| Method | Uri                  | Authorization |
| :----- | :------------------- | :------------ |
| POST   | `api/user-app/lang` | Yes           |

### Request Params

```json
{
    "lang": "ar"
}
```

### Response

```json
{
    "success": true,
    "msg": "Language updated successfully"
}
```