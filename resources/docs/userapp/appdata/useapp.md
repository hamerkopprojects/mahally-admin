# How to use app

---

How to use app

### Details

| Method | Uri              | Authorization |
| :----- | :--------------- | :------------ |
| GET    | `api/user-app/useapp` | No            |



### Response

```json
{
"success": true,
"data": [
    {
        "id": 1,
        "available_for": "user",
        "status": "active",
        "created_at": null,
        "updated_at": "2020-12-22T19:26:38.000000Z",
        "deleted_at": null,
        "lang": {
            "en": {
                "id": 1,
                "how_to_id": 1,
                "title": "cheese",
                "content": "<p>How to use en 11</p>",
                "language": "en"
            },
            "ar": {
            "id": 2,
            "how_to_id": 1,
            "title": "cheese",
            "content": "<p>How to use ar 1</p>",
            "language": "ar"
            }
        }
    },
    ]
}
```
