# Support Request

---
We are here to help

### Details

| Method | Uri   | Authorization |
| : |   :-   |  :  |
| POST | `api/user-app/support` | Yes |

### Request Params

```json
{
   "message":"support request user app",
    "subject":"new testt"
}
```
### Response

```json
{
    "success": true,
    "msg": "Thank you for submitting the support request. Our team will get back to you!"
}
```
