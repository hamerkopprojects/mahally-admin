# Support chat send

---
We are here to help

### Details

| Method | Uri   | Authorization |
| : |   :-   |  :  |
| POST | `api/user-app/support/chat` | Yes |

### Request Params

```json
{
   "request_id":1,
    "message":"Second chat"
}
```

### Response

```json
{
    "success": true,
    "msg": "New message sent successfully"
}
```
