# Support Chat

---
We are here to help

### Details

| Method | Uri   | Authorization |
| : |   :-   |  :  |
| GET| `api/user-app/support/{request_id}` | Yes |


### Response

```json
{
    "success": true,
    "data": [
        {
            "id": 3,
            "message": "support request user app",
            "user_type": "userapp"
        }
    ]
}
```
