# Offers 
---
List offers
### Details

| Method | Uri                    | Authorization |
| :----- | :--------------------- | :------------ |
| POST   | `api/user-app/offers` | No            |

### Request
```json

{
    "latitude":"8.5685708",
    "longitude":"76.87313329999999"
}
```
### Response
```json
{
    "success": true,
    "data": [
        {
            "id": 1,
            "restuarants_id": 4,
            "menu_item_id": 21,
            "expire_in": "2021-04-07",
            "discount_type": "Percentage",
            "restaurent": {
                "id": 4,
                "unique_id": "RES-000-00001",
                "city_id": 51,
                "commission_category_id": 2,
                "membership_id": 2,
                "lang": [
                    {
                        "id": 7,
                        "restuarants_id": 4,
                        "name": "Cafe Arabesque",
                        "language": "en"
                    },{
                        "id": 8,
                        "restuarants_id": 4,
                        "name": "Cafe Arabesque",
                        "language": "ar"
                    }]
            },
            "items": {
                "order": 0,
                "lang": [
                    {
                        "id": 41,
                        "menu_item_id": 21,
                        "language": "en"
                    },
                    {
                        "id": 42,
                        "menu_item_id": 21,
                        "language": "ar"
                    }]},
            "lang": {"en": {
                    "id": 1,
                    "offer_id": 1,
                    "name": "20%",
                    "deleted_at": null
                },"ar": {
                    "id": 2,
                    "offer_id": 1,
                    "name": "النوع وتدافعت عليه لصنع",
                    "deleted_at": null
                }}}]
}
```
