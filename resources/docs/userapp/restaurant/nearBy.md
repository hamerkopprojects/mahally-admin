# Restaurant near by
---
### Details

| Method | Uri                  | Authorization |
| :----- | :------------------- | :------------ |
| POST   | `api/user-app/nearby/restaurant` | No            |

### Request Params

```json
{
    "latitude":22.3452,
    "longitude":44.3256,
    "category":[],
    "popularity":
}
```

### Response

```json
{
    "success": true,
    "data": [
        {
            "id": 4,
            "logo": "http://mahally.test/uploads/restaurant/logo/X9ThFpdAqnFecfMiTXdyNlSXUnwUGHnXgYfY2auq.png",
            "cover_photo": "http://mahally.test/uploads/restaurant/cover/RHUs69SQKQnI62Qsbk6LsxAFBjQNHqVIUqGOPcnb.png",
            "location": "Riyadh Saudi Arabia",
            "latitude": "24.7136",
            "longitude": "46.6753",
            "closing_time": "0505:0303",
            "opening_time": "0808:0303",
            "distance": 0,
            "time_status": "CLOSE",
        }
        .....
    ]
}
```