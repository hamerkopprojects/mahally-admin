# Item Details
---
### Details

| Method | Uri                  | Authorization |
| :----- | :------------------- | :------------ |
| GET   | `api/user-app/item/{id}` | No            |

### Response

```json
{
    "success": true,
    "data": {
        "details": {
            "id": 21,
            "restuarants_id": 4,
            "menu_cate_id": 3,
            "calory": "20",
            "price": 50,
            "discount_price": 25,
            "cover_photo": "http://mahally.test/uploads/restaurant/menu_items/8wUdDUbd6RlPu0I80uOz7Ro9esnlzFI31yM96F3G.png",
            "status": "active",
            "created_at": "2021-03-02T12:19:10.000000Z",
            "updated_at": "2021-03-02T12:30:41.000000Z",
            "deleted_at": null,
            "order": 0,
            "lang": [
                {
                    "id": 41,
                    "menu_item_id": 21,
                    "name": "Desert",
                    "description": "food tastey",
                    "language": "en",
                    "created_at": "2021-03-02T12:19:10.000000Z",
                    "updated_at": "2021-03-02T12:19:10.000000Z",
                    "deleted_at": null
                },
                {
                    "id": 42,
                    "menu_item_id": 21,
                    "name": "Harry Osborn (User)",
                    "description": "ccbacsnb",
                    "language": "ar",
                    "created_at": "2021-03-02T12:19:10.000000Z",
                    "updated_at": "2021-03-02T12:19:10.000000Z",
                    "deleted_at": null
                }
            ],
            "photos": [
                {
                    "id": 2,
                    "menu_item_id": 21,
                    "image_path": "http://mahally.test/uploads/restaurant/menu_items/images/iJ0G5B6z0bHGkOPh1gy4nRvdmisfG8wVmlqC51Ij.png"
                }
            ],
            ....
        }
    }
}
```