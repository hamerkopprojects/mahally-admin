# Need Notifications

---

Need notification check

### Details

| Method | Uri                 | Authorization |
| :----- | :------------------ | :------------ |
| GET    | `api/user-app/check/need-notification` | Yes           |


### Response

```json
{
    "success": true,
    "data": {
        "id": 5,
        "name": "Reshma",
        "need_notification": 1
    }
}
```
