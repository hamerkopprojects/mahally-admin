# Update  Driver

---

### Details

| Method | Uri                  | Authorization |
| :----- | :------------------- | :------------ |
| POST   | `api/user-app/profile/update` | YES         |


### Request Params

Form Data

| param | value                          |
| :---- | :----------------------------- |
| photo | image                          |
| name  | name                           |
|email  |  email                         |
|ph_in_order|enable                      |
|ph_in_review|enable                    |


### Response

```json
{
    "success": true,
    "msg": "Profile updated succesfully"
}
```
