# Free loyality meals

---

### Details

| Method | Uri                  | Authorization |
| :----- | :------------------- | :------------ |
| GET   | `user-app/loyality-meal` | YES         |



### Response

```json
{
    "success": true,
    "data": [
        {
            "id": 1,
            "restuarants_id": 4,
            "menu_item_id": 21,
            "order": "5",
            "expire_in": "2021-04-09",
            "status": "active",
            "created_at": "2021-03-02T12:40:19.000000Z",
            "updated_at": "2021-03-02T12:40:32.000000Z",
            "deleted_at": null,
            "remaining_orders": "1 / 5 ORDER 4 MORE TIME TO GET THE FREE LOYALITY MEAL-ar",
            "lang": [
                {
                    "id": 1,
                    "free_meal_id": 1,
                    "name": "salon",
                    "description": "gfhvhb",
                    "language": "en",
                    "created_at": "2021-03-02T12:40:19.000000Z",
                    "updated_at": "2021-03-02T12:40:19.000000Z",
                    "deleted_at": null
                },
                {
                    "id": 2,
                    "free_meal_id": 1,
                    "name": "النوع وتدافعت عليه لصنع",
                    "description": "frdgdgfcgtf",
                    "language": "ar",
                    "created_at": "2021-03-02T12:40:19.000000Z",
                    "updated_at": "2021-03-02T12:40:19.000000Z",
                    "deleted_at": null
                }
            ]
        }
    ]
}
```
