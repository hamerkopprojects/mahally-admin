
# Resend otp

---

 Resend OTP

### Details

| Method | Uri                  | Authorization |
| :----- | :------------------- | :------------ |
| POST   | `api/user-app/resend` | No            |

### Request Params

```json
{
    "phone": "9995256535"
}
```

### Response

```json
{
    "success": true,
    "msg": "Otp resend successfully"
}
```
