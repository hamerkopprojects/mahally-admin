
# Rating Segments


### Details

| Method | Uri                    | Authorization |
| :----- | :--------------------- | :------------ |
| GET    | `api/user-app/rating` | No|
### Response
```json
{
    "success": true,
    "data": [
        {
            "id": 28,
            "type": "rating_segments",
            "lang": {
                "en": {
                    "id": 55,
                    "language": "en",
                    "autosuggest_id": 28,
                    "name": "Quality"
                },
                "ar": {
                    "id": 56,
                    "language": "ar",
                    "autosuggest_id": 28,
                    "name": "Quality-ar"
                }
            }
        },
    ]
       
}
```
