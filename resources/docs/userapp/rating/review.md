
# Get review


### Details

| Method | Uri                    | Authorization |
| :----- | :--------------------- | :------------ |
| GET    | `api/user-app/reviews/{restaurant_id}` | No|
### Response
```json
{
    "success": true,
    "data": {
        "5": [
            {
                "id": 3,
                "restaurant_id": 17,
                "order_id": 2,
                "customer_id": 5,
                "rating": 3,
                "comment": "bad",
                "created_at": "2021-04-17T03:57:37.000000Z",
                "updated_at": "2021-04-17T05:30:49.000000Z",
                "deleted_at": null,
                "approval_status": "Approve",
                "reviewsegment": [
                    {
                        "id": 7,
                        "review_id": 3,
                        "segment_id": 28,
                        "rating": 4,
                        "created_at": "2021-04-17T05:30:49.000000Z",
                        "updated_at": "2021-04-17T05:30:49.000000Z",
                        "deleted_at": null,
                        "segment": {
                            "id": 28,
                            "lang": [
                                {
                                    "id": 55,
                                    "autosuggest_id": 28,
                                    "name": "Quality",
                                    "language": "en"
                                },
                                {
                                    "id": 56,
                                    "autosuggest_id": 28,
                                    "name": "Quality-ar",
                                    "language": "ar"
                                }
                            ]
                        }
                    },
                ],
                "customerdata": {
                    "id": 5,
                    "name": "Reshma"
                }
            }
        ]
    }
}
```
