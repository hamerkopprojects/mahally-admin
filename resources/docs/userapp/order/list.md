
# Order  List
Order list

### Details

| Method | Uri                    | Authorization |
| :----- | :--------------------- | :------------ |
| GET    | `api/user-app/order/list` | Yes|
### Response
```json
{
    "success": true,
    "data": {
        "current_page": 1,
        "data": [
            {
                "id": 1,
                "order_id": "MAH-ORD-00001",
                "customer_id": 5,
                "restaurant_id": 17,
                "order_type": "dine",
                "item_count": 2,
                "cooking_time": null,
                "created_at": "2021-03-26T14:59:03.000000Z",
                "updated_at": "2021-03-26T14:59:03.000000Z",
                "deleted_at": null,
                "order_date": "26 Mar 2021",
                "restaurant": {
                    "id": 17,
                    "unique_id": "RES-000-00012",
                    "city_id": 51,
                    "commission_category_id": 2,
                    "latitude": "8.5685708",
                    "longitude": "76.87313329999999",
                    "lang": [
                        {
                            "name": "Cafe Arabesque",
                            "id": 33,
                            "restuarants_id": 17
                        },
                        {
                            "name": "Cafe Arabesque",
                            "id": 34,
                            "restuarants_id": 17
                        }
                    ]
                }
            }
        ],
        "first_page_url": "http://mahally.test/api/user-app/order/list?page=1",
        "from": 1,
        "last_page": 1,
        "last_page_url": "http://mahally.test/api/user-app/order/list?page=1",
        "next_page_url": null,
        "path": "http://mahally.test/api/user-app/order/list",
        "per_page": 10,
        "prev_page_url": null,
        "to": 1,
        "total": 1
    }
}
```
