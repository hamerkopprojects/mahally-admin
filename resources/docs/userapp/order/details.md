# Order  details 
---
Orderdetails
### Details
| Method | Uri                    | Authorization |
| :----- | :--------------------- | :------------ |
| GET    | `api/user-app/order/details/{id}` | Yes|
### Response
```json
{
    "success": true,
    "data": {
        "id": 1,
        "order_id": "MAH-ORD-00001",
        "customer_id": 5,
        "order_item": [
            {
                "id": 1,
                "order_id": 1,
                "item_id": 21,
                "item_price": "100",
                "menu_items": {
                    "id": 21,
                    "restuarants_id": 17,
                    "menu_cate_id": 1,
                    "calory": "50",
                    "order": 1,
                    "lang": [
                        {
                            "id": 41,
                            "menu_item_id": 21,
                            "name": "Biriyani",
                            "description": "Tastey Fresh Biriyani",
                            "language": "en",
                        }
                    ]
                },
                "addon": [
                    {
                        "id": 1,
                        "order_id": 1,
                        "order_item_id": 1,
                        "resturant_addons": {
                            "id": 107,
                            "lang": [
                                {
                                    "id": 213,
                                    "addon_id": 107,
                                    "name": "cheese buttor ice cream",
                                    "language": "en"
                                }
                            ]
                        }
                    },
                    
                ]
            }
        ]
    }
}
```
