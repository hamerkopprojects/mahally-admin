# Forgot Password

---

Forgot Password

### Details

| Method | Uri                   | Authorization |
| :----- | :-------------------- | :------------ |
| POST   | `api/forgot-password` | No            |

### Request Params

```json
{
    "email": "test@test3.com"
}
```

### Response

```json
{
    "success": true,
    "msg": "We have sent varification code to your mail entered"
}
```
