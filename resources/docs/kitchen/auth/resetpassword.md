# Forgot Password

---

Reset Password

### Details

| Method | Uri                  | Authorization |
| :----- | :------------------- | :------------ |
| POST   | `api/reset-password` | No            |

### Request Params

```json
{
    "password": "1234567",
    "c_password": "1234567",
    "id": "10"
}
```

### Response

```json
{
    "success": true,
    "msg": "Reset password successfully"
}
```
