-   ## Get Started
    -   [Overview](/{{route}}/{{version}}/overview)
-   ## Login

    -   [Login](/{{route}}/{{version}}/auth/login)
    -   [Forgot Password](/{{route}}/{{version}}/auth/forgotpassword)
    -   [Verifivation Code](/{{route}}/{{version}}/auth/verify)
    -   [Reset Password](/{{route}}/{{version}}/auth/resetpassword)
    -   [Logout](/{{route}}/{{version}}/auth/logout)

    ## Order

    -   [Active Orders](/{{route}}/{{version}}/order/activeorder)
    -   [Ready For Pickup Orders](/{{route}}/{{version}}/order/readyforpickup)
    -   [Delivered Orders](/{{route}}/{{version}}/order/delivered)
    -   [Order Status](/{{route}}/{{version}}/order/changeorderstatus)
    -   [Cooking Time](/{{route}}/{{version}}/order/cookingtime)
    -   [Active Order Id](/{{route}}/{{version}}/order/activeorderid)
    -   [Order Id](/{{route}}/{{version}}/order/pickuporderid)
    -   [Delivered OrderId](/{{route}}/{{version}}/order/delivered)
    -   [Search Item Listing](/{{route}}/{{version}}/order/search)
    -   [Update Cooking Time](/{{route}}/{{version}}/order/updatecookingtime)
