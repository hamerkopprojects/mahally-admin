# Order

---

Change order status

### Details

| Method | Uri                         | Authorization |
| :----- | :-------------------------- | :------------ |
| POST   | `api/kitchen/change-status` | Yes           |

### Request Params

```json
{
    "order_status": "2 or 3 or 4",
    "id": "1"
}
```

### Status Value

| param | value            |
| :---- | :--------------- |
| 2     | Confirmed Orders |
| 3     | In Kitchen       |
| 4     | Ready For Pickup |

### Response

```json
{
    "success": true,
    "msg": "Status changed successfully"
}
```
