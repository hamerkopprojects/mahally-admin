# Search Order

---

Search order item listing

### Details

| Method | Uri                             | Authorization |
| :----- | :------------------------------ | :------------ |
| POST   | `api/kitchen/search-order-list` | Yes           |

### Request Params

```json
{
    "order_id": "MAH0396"
}
```

### Response

```json
{
    "success": true,
    "msg": "Orders list available"
}
```
