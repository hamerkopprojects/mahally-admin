# Update Cooking Time

---

Updating cooking time from kitchen

### Details

| Method | Uri                       | Authorization |
| :----- | :------------------------ | :------------ |
| POST   | `api/update_cooking_time` | Yes           |

### Request Params

```json
{
    "id": "1",
    "cooking_time": "10 mins"
}
```

### Response

```json
{
    "success": true,
    "msg": "Cooking time updated successfully"
}
```
