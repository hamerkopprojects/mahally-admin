# Search Id for active order

---

Listing active order id for searching

### Details

| Method | Uri                        | Authorization |
| :----- | :------------------------- | :------------ |
| POST   | `api/active-order-id-list` | Yes           |

### Request Params

```json
{
    "key": "MAH03"
}
```

### Response

```json
{
    "success": true,
    "msg": "Orders Id list available"
}
```
