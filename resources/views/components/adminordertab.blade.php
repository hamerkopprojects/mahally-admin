<ul class="nav nav-pills" role="tablist">
    @foreach ($tabs as $key => $tab)
    <li class="nav-item" >
        <a class="nav-link @if($key == $activeTab) active @endif" 
            href="{{ $url[$key] }}" role="tab">
            {{ $tab }} 
        </a>

    </li>
    @endforeach
</ul>

<style>
.nav-pills .nav-link.active, .show>.nav-pills .nav-link {
    color: #fff;
    background-color: #111;

</style>