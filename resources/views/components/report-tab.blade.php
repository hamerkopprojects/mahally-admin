<ul class="nav nav-tabs" role="tablist">
    @foreach($reportArray as $tab )
    <li class="nav-item">
        <a class="nav-link @if(request()->url() == $tab['url']) active @endif" href="{{$tab['url']}}" data-id="" role="tab">
            <span class="hidden-sm-up"><i class="{{ $tab['icon'] }}"></i></span><span class="hidden-xs-down">&nbsp;&nbsp;{{$tab['tabs']}}</span></a>
    </li>
    @endforeach
</ul>
