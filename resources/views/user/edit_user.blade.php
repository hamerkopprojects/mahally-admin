@extends('layouts.master')
@section('content-title')
EDIT PROFILE
@endsection
@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <form id="frm_edit_profile" action="javascript:;" method="POST">
                    <div class="row">
                        <div class="col-md-6">
                            <label>Your Name <span class="text-danger">*</span></label>
                            <input class="form-control" name="auth_name" autocomplete="off" value="{{ $user->name ?? ''}}">
                        </div>
                        <div class="col-md-6">
                            <label>Email Address <span class="text-danger">*</span></label>
                            <input type="text" class="form-control" id="auth_email" name="auth_email" value="{{ $user->email ?? ''}}">
                        </div>
                        <div class="col-md-6">
                            <label>Phone <span class="text-danger">*</span></label>
                            <input type="text" class="form-control" id="phone" name="phone" value="{{ $user->phone ?? ''}}" autocomplete="off">
                        </div>
                        <div class="col-md-12">
                            <button type="submit" class="btn btn-info waves-effect waves-light">Save</button>
                            <button type="button" class="btn btn-default waves-effect" onclick="window.location.reload();">Cancel</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-lg-8 p-r-0 title-margin-right">
        <div class="page-header">
            <div class="page-title">
                <h1>CHANGE PASSWORD</h1>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <form id="frm_change_password" action="javascript:;" method="POST">
                    <div class="row">
                        <div class="col-md-6">
                            <label>Password <span class="text-danger">*</span></label>
                            <input type="password" class="form-control" name="password" id="password" autocomplete="off" placeholder="enter your new password">
                        <span class="input-group-text" toggle="#password">
                                            <i id="eye" class="fa fa-eye-slash toggle-password" style="color:black;"></i>
                                        </span>
                        </div>
                        <div class="col-md-6">
                            <label>Confirm password <span class="text-danger">*</span></label>
                            <input type="password" placeholder="Enter the confirm password" class="form-control" id="confirm_password" name="confirm_password">
                            <span class="input-group-text" toggle="#confirm_password">
                                            <i id="eye" class="fa fa-eye-slash toggle-confirm-password" style="color:black;"></i>
                                        </span>
                        </div>
                        <div class="col-lg-12">
                            <button type="submit" class="btn btn-info waves-effect waves-light save-categorys">
                                Save
                            </button>
                            <button type="button" class="btn btn-default waves-effect" onclick="window.location.reload();">Cancel</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
@push('css')
<style>
    #eye {
    float: right;
    margin-right: 14px;
    margin-top: -25px;
    position: relative;
    z-index: 2;
}
</style>
@endpush
@push('scripts')
<script>
    $("#frm_edit_profile").validate({
        normalizer: function (value) {
            return $.trim(value);
        },
        rules: {
            auth_name: {
                required: true,
            },
            auth_email: {
                required: true,
                email: true,
            },
            phone: {
                required: true,
                number: true,
                minlength: 6, // will count space
                maxlength: 12
            },
        },
        messages: {
            auth_name: {
                required: 'Name is required.'
            },
            auth_email: {
                required: 'Email is required.',
                email: 'Invalid email',
            },
            phone: {
                required: 'Phone number is required.',
                number: 'Invalid phone number.'
            },
        },
        submitHandler: function (form) {
            $.ajax({
                type: "POST",
                url: "{{route('name.reset')}}",
                data: $('#frm_edit_profile').serialize(),
                dataType: "json",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function (data) {
                    if (data.status == 1) {
                        Toast.fire({
                            icon: 'success',
                            title: data.message
                        });
                        window.setTimeout(function () {
                            window.location.href = '{{route("user.edit")}}';
                        }, 1000);
                    } else {
                        Toast.fire({
                            icon: 'error',
                            title: data.message
                        });
                    }
                }
            });
            return false;
        }
    });
    $("#frm_change_password").validate({
        normalizer: function (value) {
            return $.trim(value);
        },
        rules: {
            password: {
                required: true,
                minlength: 6, 
            },
            confirm_password: {
                required: true,
                minlength: 6, 
                equalTo: '#password'
            }
        },
        messages: {
            password: {
                required: 'Password is required.',
            },
            confirm_password: {
                required: 'Confirm password is required.',
                equalTo: 'Password and confirm password not matching.'
            }
        },
        submitHandler: function (form) {
            $.ajax({
                type: "POST",
                url: "{{route('password.new')}}",
                data: $('#frm_change_password').serialize(),
                dataType: "json",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function (data) {
                    if (data.status == 1) {
                        Toast.fire({
                            icon: 'success',
                            title: data.message
                        });
                        window.setTimeout(function () {
                            window.location.href = '{{route("user.edit")}}';
                        }, 1000);
                    } else {
                        Toast.fire({
                            icon: 'error',
                            title: data.message
                        });
                    }
                }
            });
            return false;
        }
    });
    $(".toggle-password").click(function () {

        $(this).toggleClass("fa fa-eye fa fa-eye-slash");
        var input = $($(this).attr("toggle"));
        if ($('#password').attr('type') === 'password') {
            $('#password').attr("type", "text");
        } else {
            $('#password').attr("type", "password");
        }
    });
    $(".toggle-confirm-password").click(function () {

        $(this).toggleClass("fa fa-eye fa fa-eye-slash");
        if ($('#confirm_password').attr('type') === 'password') {
            $('#confirm_password').attr("type", "text");
        } else {
            $('#confirm_password').attr("type", "password");
        }
    });
</script>
@endpush