{{-- @component('mail::message')
## Dear ,


@component('mail::panel')

<h3> The verification code is : {{$token}}</h3>
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent --}}
<html>

<head>
    <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="format-detection" content="date=no" />
    <meta name="format-detection" content="address=no" />
    <meta name="format-detection" content="telephone=no" />
    <meta name="x-apple-disable-message-reformatting" />
    <link href="https://fonts.googleapis.com/css?family=Muli:400,400i,700,700i" rel="stylesheet" />
    <title>Order</title>
</head>

<body class="body"
    style="padding:0 !important; margin:0 !important; display:block !important; min-width:100% !important; width:100% !important;  -webkit-text-size-adjust:none; text-align: center;">
    <table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#fff" align="center"
        style=" background-image:url({{asset('images/Bg.png')}}); background-repeat: no-repeat;">
        <tr>
            <td align="center" valign="top">
                <table width="650" border="0" cellspacing="0" cellpadding="0" class="mobile-shell">
                    <tr>
                        <td class="td container"
                            style="width:650px; min-width:650px; font-size:0pt; line-height:0pt; margin:0; font-weight:normal; padding:55px 0px;">
                            <repeater>
                                <!-- Intro -->
                                <layout label='Intro'>
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td style="padding-bottom: 10px;">
                                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td class="tbrr p30-15"
                                                            style="padding: 40px 30px; border-radius:26px 26px 0px 0px;">
                                                            <table width="100%" border="0" cellspacing="0"
                                                                cellpadding="0">
                                                                <tr>
                                                                    <td class="img m-center"
                                                                        style="font-size:0pt; line-height:30pt;padding-bottom:30px; text-align:center;">
                                                                        <img src="{{asset('images/logo.png')}}"
                                                                            width="129" height="203" editable="true"
                                                                            border="0" alt=""
                                                                            style="    border-radius: 0px;"></td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="h1 pb25"
                                                                        style="color:#000; font-family:'Muli', Arial,sans-serif; font-size:30px; line-height:30px; text-align:center; padding-bottom:5px;">
                                                                        <multiline>
                                                                            Hello
                                                                    </td>
                                                                </tr>
                    
                                                                <tr>
                                                                    <td class="text-center pb25" style="color:#00000; font-family:'Muli', Arial,sans-serif; font-size:16px; line-height:20px; margin-bottom:10px; text-align:center; padding-bottom:10px;">
                                                                        <multiline>Here is your code for resetting your password</b></multiline>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="text-center pb25" style="color:#00000; font-family:'Muli', Arial,sans-serif; font-size:16px;  text-align:center; padding-bottom:10px; padding-top:15px; line-height:20px; margin-bottom:5px; ">
                                                                        <multiline> <b><span style="color: #383a50; font-size: 18px; border: 2px solid #9E9E9E; padding: 5px 18px 5px 19px;"> {{$token}}</span></b></multiline>
                                                                </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="text-center pb25" style="color:#00000; font-family:'Muli', Arial,sans-serif; font-size:16px; line-height:20px; margin-bottom:10px; text-align:center; padding-bottom:10px;">
                                                                        <multiline>If you did not initiate this request, you can safely ignore this mail</multiline>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <table width="100%" cellpadding="10" cellspacing="0" border="0"
                                                    style="background-color:#fff;color: #404041; font-family:'Muli', Arial,sans-serif;  padding-bottom:10px; border-radius: 20px">
                                                    <tr>
                                                        <td class="dark-blue-button2 text-button"
                                                            style=" color:#939393; font-family:'Muli', Arial,sans-serif; font-size:14px; line-height:18px; padding:35px 30px; text-align:center;  font-weight:bold;">
                                                            <multiline><a href="#" target="_blank" class="link"
                                                                    style="color:#939393; text-decoration:none;"><span
                                                                        class="link"
                                                                        style="color:#939393; text-decoration:none;">Call
                                                                        us at <b>1-800-672-4399</b> or reply to this
                                                                        email</span></a></multiline>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <table width="95%" cellspacing="10" border="0"
                                                                style="font-size: 13px;color: #404041; " align="center">
                                                                <tr>
                                                                    <td width="25%"
                                                                        style="text-align:center;vertical-align:top">
                                                                        <div style="height:42px; padding-bottom:20px;">
                                                                            <img src="{{asset('images/24-hours.png')}}"
                                                                                width="60px">
                                                                        </div>
                                                                    </td>

                                                                    <td width="25%"
                                                                        style="text-align:center;vertical-align:top">
                                                                        <div style="height:42px; padding-bottom:20px;">
                                                                            <img src="{{asset('images/img_458847.png')}}"
                                                                                width="60px">
                                                                        </div>
                                                                    </td>
                                                                    <td width="25%"
                                                                        style="text-align:center;vertical-align:top">
                                                                        <div style="height:42px; padding-bottom:20px;">
                                                                            <img src="{{asset('images/easy-return.png')}}"
                                                                                width="65px">
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td width="25%"
                                                                        style="text-align:center;vertical-align:top">
                                                                        <div style="line-height:22px;text-align:center ;   
                                                                font-weight: 700;">
                                                                            Customer Service
                                                                        </div>
                                                                    </td>

                                                                    <td width="25%"
                                                                        style="text-align:center;vertical-align:top">
                                                                        <div style="line-height:22px;text-align:center;    
                                                                font-weight: 700;">
                                                                            Satisfaction guaranteed
                                                                        </div>
                                                                    </td>
                                                                    <td width="25%"
                                                                        style="text-align:center;vertical-align:top">
                                                                        <div style="line-height:22px;text-align:center;    
                                                                font-weight: 700;">
                                                                            Quality Service
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="3"
                                                                        style="border-top: solid 1px #5d5d5d;border-bottom: solid 1px #5d5d5d; padding: 25px; text-align: center;">
                                                                        <a href="www.mahallyapp.com"
                                                                            style="color: #5e2e66; font-size: 20px; text-decoration: none">www.mahallyapp.com</a>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="3" class="text-footer1 pb10"
                                                                        style="color:#9b9b9b; font-family:'Muli', Arial,sans-serif; font-size:16px; line-height:20px; text-align:center; padding-bottom:10px;">
                                                                        <multiline>Copyright ©2021 All rights reserved
                                                                        </multiline>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>

                                                    </tr>

                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </layout>
                                <!-- END Two Columns -->
                            </repeater>
                            <!-- Footer -->

                            <!-- END Footer -->
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td align="center"><img src="{{asset('images/footer.png')}}" width="500" height="162" alt="" /></td>
        </tr>
    </table>
</body>

</html>