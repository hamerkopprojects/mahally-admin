@extends('layouts.master')

@section('content-title')
OTHER SETTINGS
@endsection
@section('content')
@include('user.flash-message')
<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
            <form id="name_reset_form" action="{{route('other.store')}}" method="post">
                @csrf
                                    <div class="row">
                                            <div class="col-md-6">
                                                <label class="control-label">Currency (EN) <span class="text-danger">*</span></label>
                                            @if(!empty($other->currency_en))
                                                <input type="text" class="form-control form-white" id="currency_en"
                                                    name="currency_en" value="{{ old('currency_en',$other->currency_en) }} " placeholder="Enter Currency">
                                                    @else
                                                    <input type="text" class="form-control form-white" id="currency_en"
                                                    name="currency_en" placeholder="Enter Currency" value="{{ old('currency_en') }}">
                                                    @endif
                                                    @error('currency_en')
                                                    <small class="error">
                                                        <strong>{{ $message }}</strong>
                                                    </small>

                                                @enderror
                                            </div>
                                            <div class="col-md-6">
                                                <label class="control-label">Currency (AR) <span class="text-danger">*</span></label>
                                                @if(!empty($other->currency_ar))
                                                <input type="text" class="form-control form-white text-right" id="currency_ar"
                                                    name="currency_ar" placeholder="أدخل العملة" value="{{ old('currency_en',$other->currency_ar) }}">
                                                    @else
                                                    <input type="text" placeholder="أدخل العملة" class="form-control form-white text-right" id="currency_ar"
                                                    name="currency_ar" value="{{ old('currency_ar') }}">
                                                    @endif
                                                    @error('currency_ar')
                                                    <small class="error">
                                                        <strong>{{ $message }}</strong>
                                                    </small>

                                                @enderror
                                            </div>
                                            <div class="col-md-6">
                                                <label class="control-label">EARNING ON HOTEL SIGN UP (SAR) <span class="text-danger">*</span></label>
                                                @if(!empty($other->earning_amount))
                                                <input type="text" class="form-control form-white" id="earning_amount"
                                                    name="earning_amount" placeholder="Enter Amount" value="{{ old('earning_amount',$other->earning_amount) }}">
                                                    @else
                                                    <input type="text" placeholder="Enter Amount" class="form-control form-white" id="earning_amount"
                                                    name="earning_amount" value="{{ old('earning_amount') }}">
                                                    @endif
                                                    @error('earning_amount')
                                                    <small class="error">
                                                        <strong>{{ $message }}</strong>
                                                    </small>

                                                @enderror
                                            </div>
                                </div>
                            <div class="col-lg-12">
                                <div class="row justify-content-end pb-5">
                                    <div class="col-md-12 filter-by-boat-jalbooth-butn01 text-md-left">
                                        <button type="submit" class="btn btn-primary buttonxl mr-md-2">
                                            Save
                                        </button>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

@push('scripts')
<script>
    if ($("#name_reset_form").length > 0) {
        $.validator.addMethod('minStrict', function (value, el, param) {
            return this.optional( el ) || value >= param && !(value % 1);
        });

        $("#name_reset_form").validate({
            rules: {
                currency_en: {
                    required: true,    
                },
                currency_ar: {
                    required: true,
                }, 
                earning_amount: {
                    required: true,
                    number: true,
                },  
            },
            messages: {
                currency_en: {
                    required: "Currency in EN is required",
                },
                currency_ar: {
                    required: "Currency in AR is required",
                }, 
                earning_amount: {
                    required: "Earning amount is required",
                    number: "Accept only numeric value",
                },       
            },
        })
    } 
</script>
@endpush