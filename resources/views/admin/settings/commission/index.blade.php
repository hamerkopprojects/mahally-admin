@extends('layouts.master')

@section('content-title')
COMMISSION CATEGORY
@endsection
@section('add-btn')
<button  class="btn btn-info create_btn" id="commission_add_btn">
    <i class="ti-plus"></i> Add New Commission Category
</button>
@endsection
@section('content')
{{-- @include('admin.cms.faqs.popup') --}}
<div class="row">
    <div class="col-lg-12">                  
            <div class="card">
            <div class="card-body">
                <div id="msgDiv"></div>
                <div class="table-responsive">
                    <table class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>S.No.</th>
                                <th>Commission Category Title</th>
                                <th>Value</th>
                                <th width="20%"></th>
                            </tr>
                        </thead>
                        <tbody>
                            @if (count($commision) > 0)
                            @php
                            $i = 1;
                            @endphp
                            @foreach ($commision as $row_data)
                            <tr>
                                <th>{{ $i++ }}</th>
                                <td>{{ $row_data->name ?? '' }}</td>
                                <td>{{ $row_data->value ?? '' }}</td>
                                <td class="text-center">
                                    <a class="btn btn-sm btn-success text-white edit_btn page_edit commission_edit"  title="Edit" data-id="{{ $row_data->id }}"><i class="fa fa-edit"></i></a>
                                    <a class="btn btn-sm btn-danger text-white commission_id_del" data-id="{{ $row_data->id }}" data-activate="{{ $row_data['status'] == 'active' ? 'Deactivate' : 'Activate' }}" title="Delete"><i class="fa fa-trash"></i></a>
                                </td>
                            </tr>
                            @endforeach
                            @else
                            <tr>
                                <td colspan="8" class="text-center">No records found!</td>
                            </tr>
                            @endif
                        </tbody>
                    </table>
                </div>
                <div class="text-center d-flex justify-content-center mt-3">
                    {{ $commision->links() }}
                </div>
            </div>
        </div>
    </div>
</div>
{{-- popup --}}
<div class="modal fade" id="commission-popup" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel">Add Commission Category</h4>
                <a href="{{route('commission.get')}}" class="btn close cancel-btn"> <span aria-hidden="true">&times;</span></a>
            </div>
            <div class="modal-body" data-no-padding="no-padding">
                <form id="add-commission" method="POST" action="#">
                    <input type="hidden" id="id_pg" name="id_pg">
                    @csrf
                    {{-- @method('PUT') --}}
                    <div class="row mb-3">
                        <div class="col-md-6">
                            <label>Commission Category Title <span class="text-danger">*</span></label>
                            <input type="text" placeholder="Enter commission category title" class="form-control form-white" id="commission_name"
                                                    name="commission_name" value="{{ old('commission_name') }}">
                            <label class="commission_name_error"></label>                        
                        </div>
                        <div class="col-md-3">
                            <label>Value <span class="text-danger">*</span></label>
                            <input type="text" placeholder="Enter value" class="form-control form-white" id="commission_value"
                                                    name="commission_value" value="{{ old('commission_value') }}">
                            <label class="commission_value_error"></label>                        
                        </div>
                        <div class="col-lg-12">
                            <div class="row 5">
                                <div class="col-md-12 text-md-left">
                                    <button type="submit"  class="btn btn-info waves-effect waves-light save_page">
                                        Save
                                    </button>
                                    {{-- <button type="button" class="btn btn-default waves-effect cancel-btn" data-dismiss="modal">
                                        Cancel
                                    </button> --}}
                                    <a href="{{route('commission.get')}}" class="btn btn-default reset_style">Cancel</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
@push('css')
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<style>
.cancel_style{
    margin-left: 20px;
}
#site-error{
float:right;
margin-right: 260px;
margin-top: 10px;

}
.area{
        height: 80px;
    }

</style>

@endpush
@push('scripts')
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="//cdn.ckeditor.com/4.14.0/basic/ckeditor.js"></script>


<script>
   

        $('#commission_add_btn').on('click',function(e){
            $('#add-commission').trigger("reset")
            $('#exampleModalLabel').html('Add Commission Category');
            $('#commission-popup').modal({
                show:true
            })
        })

        $('.commission_edit').on('click',function(e){
            e.preventDefault();
            $('#exampleModalLabel').html('Edit Commission Category');

            commission = $(this).data('id')
            var url = "commission/edit/";
        
            $.get(url  + commission, function (data) {
                $('#commission_name').val(data.commission.name);
                $('#commission_value').val(data.commission.value);
                $('#id_pg').val(data.commission.id)
                
                $('#commission-popup').modal({
                    show: true

                });
            }) 
        })

        $('.save_page').on('click',function(e){
            console.log("inside")
            $("#add-commission").validate({
                ignore: [],
                rules: {
                    commission_name  : {        
                        required: true,         
                    },
                    commission_value: {          
                        required: true,
                        number:true,
                    },
                },
                messages: {               
                    commission_name: {
                        required:"Commission category name required",
                        
                        },
                    commission_value: {
                        required: "Commission category value required",
                        number: "Accept only numeric value"
                        },
                 },
                 errorPlacement: function(error, element) {
                    if (element.attr("name") == "commission_name" ) {
                        $(".commission_name_error").html(error);
                    }
                    if (element.attr("name") == "commission_value" ) {
                        $(".commission_value_error").html(error);
                    }   
                 },
                 submitHandler: function(form) {
                    $('.loading_box').show();
                    $('.loading_box_overlay').show();  
                    let edit_val=$('#id_pg').val();
                    var test = new Array();
                    $('button:submit').attr('disabled', true);
                    if(edit_val){
                    $.ajax({
                        type:"POST",
                        url: "{{route('commission.update')}}",
                        data:{ 
                            "_token": "{{ csrf_token() }}",
                            name:$('#commission_name').val(),
                            value:$('#commission_value').val(),
                            id:edit_val
                        },
                        success: function(result){
                            console.log(result.msg)
                            $('#commission-popup').modal('hide')
                                Toast.fire({
                                icon: 'success',
                                title: 'Commission category updated successfully'
                                });
                            window.location.reload();
                            $('.loading_box').hide();
                            $('.loading_box_overlay').hide();
                            $('button:submit').attr('disabled', false);
                        }       
                    });
                }else{
                    $.ajax({
                        type:"POST",
                        url: "{{route('commission.store')}}",
                        data:{
                            "_token": "{{ csrf_token() }}",
                            name:$('#commission_name').val(),
                            value:$('#commission_value').val(),
                        },
    
                        success: function(result){
                            console.log(result.msg)
                            $('#commission-popup').modal('hide')
                            if(result.msg ==="success")
                            {
                                Toast.fire({
                                icon: 'success',
                                title: 'Commission category added successfully'
                                });
                                window.location.reload();
                            }else{
                                Toast.fire({
                                icon: 'error',
                                title: 'some errors'
                                });
                            }
                            $('.loading_box').hide();
                            $('.loading_box_overlay').hide();
                            $('button:submit').attr('disabled', false);
                        }
                    }) ;
                    }
                
                }
            });
            
           
        });

       
        $('.commission_id_del').on('click',function(){
            var id = $(this).data("id");
            $.confirm({
            title: false,
            content: 'Are you sure to delete this commission category? <br><br>You wont be able to revert this',
            buttons: {
                Yes: function() {
           
                    $.ajax({
                        url: "{{route('commission.delete')}}" ,
                        type: 'POST',
                        data:{
                            "_token": "{{ csrf_token() }}",
                            id:id
                        },
                        success: function(data) {
                            if (data.status == 1) {
                                window.setTimeout(function() {
                                    window.location.href = '{{route("commission.get")}}';
                                }, 1000);
                                Toast.fire({
                                    icon: 'success',
                                    title: data.message
                                });
                            } else {
                                Toast.fire({
                                    icon: 'error',
                                    title: data.message
                                });
                            }
                        }
                    });
                },
                No: function() {
                    console.log('cancelled');
                }
            }
        });
    });
    $('.cancel-btn').on('click',function(){
          $("#id_pg").val('');
    });
    </script>
@endpush