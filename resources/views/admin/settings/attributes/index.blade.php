@extends('layouts.master')

@section('content-title')
Attributes
@endsection
@section('add-btn')
<button  class="btn btn-info create_btn" id="attribute_add_btn">
     <i class="ti-plus"></i> Add New Attribute
</button>
@endsection
@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <div class="row tablenav top text-right">
                    <div class="col-md-6 ml-0">
                        <form action="{{route('attribute.get')}}" id="search-faq-form" method="get">
                            <input type="text" value ="{{$search_field ?? ''}}"class="form-control" id="search_field" name="search_field" placeholder="Search Attribute">
                            <input type="hidden" name="faq_select" id="faq_select">
                        </form>
                       
                    </div>
                    <div class="col-md-6 text-left">
                        <button type="button" onclick="event.preventDefault(); 
                        document.getElementById('search-faq-form').submit();" class="btn btn-info"><font style="vertical-align: inherit;">Search</font></button>
                        <a href="{{route('attribute.get')}}" class="btn btn-default cancel_style">Reset</a>
                        
                    </div>
                </div>
            </div>
        </div>                  
            <div class="card">
            <div class="card-body">
                <div id="msgDiv"></div>
                <div class="table-responsive">
                    <table class="table table-bordered table-striped">
                        <thead>
                            <tr>

                                <th>S.No.</th>
                                <th>Attribute Name (EN)</th>
                                <th>Attribute Name (AR)</th>
                                <th width="20%"></th>
                            </tr>
                        </thead>
                         <tbody>
                            @if (count($attribute) > 0)
                            @php
                            $i = 1;
                            @endphp
                            @foreach ($attribute as $row_data)
                            <tr>
                                <th>{{ $i++ }}</th>
                                <td>{{ $row_data->lang[0]->name ?? '' }}</td>
                                <td class="right-align">{{ $row_data->lang[1]->name ?? '' }}</td>
                                <td class="text-center">
                                    <button
                                        type="button"
                                        class="change-status btn btn-sm btn-toggle ml-0 {{ $row_data['status']}}"
                                        data-toggle="button"
                                        data-id="{{ $row_data->id }}"
                                        data-activate="{{ $row_data['status'] == 'active' ? 'Deactivate' : 'Activate' }}"
                                        aria-pressed="true"
                                        autocomplete="off"
                                        id="active_faq">
                                        <div class="handle" data-toggle="tooltip" data-placement="top" title="Activate / Deactivate"></div>
                                    </button>
                                    <a class="btn btn-sm btn-success text-white edit_btn" title="Edit Attributes" data-id="{{ $row_data->id }}" ><i class="fa fa-edit"></i></a>
                                    <a class="btn btn-sm btn-danger text-white attribute_id_del" data-id="{{ $row_data->id }}" data-activate="{{ $row_data['status'] == 'active' ? 'Deactivate' : 'Activate' }}" title="Delete"><i class="fa fa-trash"></i></a>
                                </td>
                            </tr>
                            @endforeach
                            @else
                            <tr>
                                <td colspan="8" class="text-center">No records found!</td>
                            </tr>
                            @endif
                        </tbody>
                    </table>
                </div>
                <div class="text-center d-flex justify-content-center mt-3">
                    {{ $attribute->links() }}
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Modal pooup -->
<div class="modal fade none-border" id="formModal">
    <div class="modal-dialog modal-lg">
        <form id="frm_create_attribute" action="javascript:;" method="POST">
            <div class="modal-content">

            </div>
        </form>
    </div>
</div>
<!-- END MODAL -->

@endsection
@push('css')
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<style>
    .reset_style {
        margin-left: 15px;
    }
    .search_wid{
        width: 234px;
    }
   
    .btn-fea{
        background-color: #87b23e;
    }
    .validation
    {
      color: red;
     
    }
</style>

@endpush
@push('scripts')
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="//cdn.ckeditor.com/4.14.0/basic/ckeditor.js"></script>

<script>
   
      $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }});

         $('.create_btn').on('click', function () {
            var cat_id = $(this).data("cat-id");
            $.ajax({
            type: "GET",
                    url: "{{route('create_attribute')}}",
                    data: {'cat_id': cat_id},
                    success: function (data) {
                    $('.modal-content').html(data);
                    $('#formModal').modal('show');
                    }
            });
        });
        $('.edit_btn').on('click', function () {
            var attribute_id = $(this).data("id");
            $.ajax({
            type: "GET",
                    url: "{{route('create_attribute')}}",
                    data: {'id': attribute_id},
                    success: function (data) {
                    $('.modal-content').html(data);
                    $('#formModal').modal('show');
                    }
            });
        });
         $.validator.messages.required = "Attribute value (EN) required";
        jQuery.validator.addClassRules("att_value_en", {
            required: true,     
        });
        
        jQuery.validator.addClassRules("att_value_ar", {
            required: true       
        });

        $("#frm_create_attribute").validate({
             
    normalizer: function (value) {
    return $.trim(value);
    },
            rules: {
                    name_en: {
                    required: true,
                    },
                    name_ar: {
                    required: true,
                    },
            },
            messages: {
                    name_en: {
                    required: 'Name (EN) is required.'
                    },
                    name_ar: {
                    required: 'Name (AR) is required.'
                    },
            },
            submitHandler: function (form) {
                $('.loading_box').show();
                $('.loading_box_overlay').show();   
                $('button:submit').attr('disabled', true);
            $.ajax({
            type: "POST",
                    url: "{{route('save_attribute')}}",
                    data: $('#frm_create_attribute').serialize(),
                    dataType: "json",
                    headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    success: function (data) {
                        if (data.status == 1) {
                            Toast.fire({
                            icon: 'success',
                                    title: data.message
                            });
                            window.setTimeout(function () {
                            window.location.href = '{{route("attribute.get")}}';
                            }, 1000);
                            $("#frm_create_attribute")[0].reset();
                        } else {
                            Toast.fire({
                            icon: 'error',
                                    title: data.message
                            });
                        }
                        $('.loading_box').hide();
                        $('.loading_box_overlay').hide();
                        $('button:submit').attr('disabled', false);
                    },
                    error: function (err) {
                        $('.loading_box').hide();
                        $('.loading_box_overlay').hide();
                        $('button:submit').attr('disabled', false);
                    }
            });
            return false;
            }
    });
       

        $('.change-status').on('click', function () {
    var cust_id = $(this).data("id");
    var act_value = $(this).data("activate");
    $.confirm({
    title: act_value + ' Attributes',
            content: 'Are you sure to ' + act_value + ' the attribute?',
            buttons: {
            Yes: function () {
            $.ajax({
            type: "POST",
                    url: "{{route('activate_attribute')}}",
                    data: {id: cust_id},
                    dataType: "json",
                    headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    success: function (data) {
                    if (data.status == 1) {
                    Toast.fire({
                    icon: 'success',
                            title: data.message
                    });
                    window.setTimeout(function () {
                    window.location.href = '{{route("attribute.get")}}';
                    }, 1000);
                    } else {
                    Toast.fire({
                    icon: 'error',
                            title: data.message
                    });
                    }
                    }
            });
            },
                    No: function () {
                    window.location.reload();
                    }
            }
    });
    });

            $('.attribute_id_del').on('click',function(){
           
            Swal.fire({  
                title: 'Are you sure to delete?',  
                text: "You won't be able to revert this!",  
                icon: 'warning',  
                showCancelButton: true,  
                confirmButtonColor: '#3085d6',  
                cancelButtonColor: '#d33',  
                confirmButtonText: 'Yes, delete it!'
            })
            .then((result) => {  
                if (result.value) {    
                    $.ajax({
                        url: "{{route('attribute.delete')}}" ,
                        type: 'POST',
                        data:{
                            id:$(this).data('id')
                        },
                        success: function(data) {
                            Toast.fire({
                                    icon: 'success',
                                    title: 'Deleted successfully'
                                    });
                                    window.location.reload();
                        }
                    });
                }
            })
        });

  
    </script>
@endpush