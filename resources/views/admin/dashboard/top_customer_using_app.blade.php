<div class="col-lg-6 ml-0">
    <div class="col-md-6">
        <h5>Top Customer Using App</h5>
    </div>
    <div class="card">
        <div class="card-body">
            <div id="msgDiv"></div>
            <div class="table-responsive">
                <table class="table table-bordered color-table">
                    <thead>
                        <tr>
                            <th>S. No.</th>
                            <th>Customer Name</th>
                            <th>No .Of Orders</th>
                            <th>Total Sale</th>
                            
                        </tr>
                    </thead>
                    <tbody>
                        @if(count($top_customer_using_app) > 0 )
                        @php
                        $i = 1;
                        @endphp
                        @foreach ($top_customer_using_app as $item)
                        <tr> 
                            <td>{{$i++}}</td>
                            <td>{{$item->customer[0]->name ?? 'N/A'}}</td>
                            <td>{{$item->total_order ?? ''}}</td>
                            <td>SAR {{$item->grand_total ?? ''}}</td>
                        </tr>
                        @endforeach
                            
                        @else
                        <tr>
                            <td colspan="8" class="text-center">No records found!</td>
                        </tr>

                        @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>