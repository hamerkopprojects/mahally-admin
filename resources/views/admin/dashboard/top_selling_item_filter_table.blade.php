<table class="table table-bordered color-table">
        <thead>
            <tr>
                <th>S. No.</th>
                <th>Item Name</th>
                <th>Restaurant Name</th>
                <th class="left-align">Total Sale</th>
                
            </tr>
        </thead>
        <tbody>
            @if(count($top_selling_item) > 0 )
            @php
            $i = 1;
            @endphp
            @foreach ($top_selling_item as $item)
            <tr> 
                <td>{{$i++}}</td>
                <td>{{$item->menu_items->lang[0]->name ?? ''?? ''}}</td>
                <td>{{$item->order->restaurant->lang[0]->name?? ''}}</td>
                <td>{{$item->item_count ?? ''}}</td>
            </tr>
            @endforeach
                
            @else
            <tr>
                <td colspan="8" class="text-center">No records found!</td>
            </tr>

            @endif
        </tbody>
    </table>