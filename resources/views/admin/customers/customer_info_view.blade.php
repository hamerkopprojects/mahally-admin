<form id="frm_create_product" action="javascript:;" method="POST">
    <div class="tab-pane active" id="pdt_info" role="tabpanel">
        <div class="modal-body">
            <div class="row">
                <div class="col-md-4">
                    <label class="control-label">Name</label>
                    <br />
                      {{ $customer['name'] ?? 'N/A' }} 
                </div>
                <div class="col-md-4">
                    <label class="control-label">Phone</label><br>
                     {{ $customer['phone_number'] ?? 'N/A' }} 
                </div>
                <div class="col-md-4">
                    <label class="control-label">Email</label><br>
                     {{ $customer['email'] ?? 'N/A' }} 
                </div>
                 <div class="col-md-4">
                    <label class="control-label">City</label><br>
                     {{ $customer['city'] ?? 'N/A'}} 
                </div>
            </div>
        </div>
        <div class="modal-footer">
           
        </div>
    </div>
</form>

<style>

label.control-label {
    font-size: 16px;
    color: #373757;
}
    </style>
