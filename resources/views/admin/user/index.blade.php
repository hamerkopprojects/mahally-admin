@extends('layouts.master')
@section('content-title')
USER
@endsection
@section('add-btn')
<button class="btn btn-primary" id="add_new_user">
    <i class="ti-plus"></i> Add New User
</button>
@endsection
@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <form id="posts-filter" method="get" action="{{ route('user-list.get') }}">
                    <div class="row tablenav top text-right">
                        <div class="col-md-5 ml-0">
                            <input class="form-control" type="text" name="search" value="{{$search}}" placeholder="Search by Name / Email / Phone/User ID">
                        </div>
                        <div class="col-md-4 ml-0">
                            <select class="form-control search_val" name="select_role" id="select_role">
                                <option value="">By Role</option>
                                @foreach($role as $rolelist)
                                <option value="{{$rolelist->id}}" {{ $rolelist->id ==  $role_id  ? 'selected' : '' }}>{{$rolelist->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-3 text-left">
                            <button type="submit" class="btn btn-info">
                                <font style="vertical-align: inherit;">Search</font>
                            </button>
                            <a href="{{ route('user-list.get') }}" class="btn btn-default reset_style">Reset</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="card">
            <div class="card-body">
                <div id="msgDiv"></div>
                <div class="table-responsive">
                    <table class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>S. No.</th>
                                <th>User ID</th>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Phone</th>
                                <th>Role</th>
                                <th width="25%"></th>
                            </tr>
                        </thead>
                        <tbody>
                            @if (count($user) > 0)
                            @php
                            $i = 1;
                            @endphp

                            @foreach ($user as $row_data)
                            <tr>
                                <th>{{ $i++ }}</th>
                                <td>{{ $row_data->user_id }}</td>
                                <td>{{ $row_data->name }}</td>
                                <td>{{ $row_data->email }}</td>
                                <td>{{ $row_data->phone }}</td>
                                <td>{{ $row_data->roles[0]->name ?? ''}}</td>
                                <td class="text-right">
                                    @if($row_data->role_id == 3)
                                        <a href="{{ route('user-list.link_resturant',$row_data->id) }}" class="btn btn-sm btn-danger text-white" data-id="{{ $row_data->id }}"><i class="fa fa-plus-circle"></i></a>
                                    @endif
                                    <a class="btn btn-sm btn-danger text-white password_send" title="Send welcome mail" data-id="{{ $row_data->id }}"><i class="fa fa-envelope"></i></a>
                                    <a class="btn btn-sm btn-success text-white edit_btn edit_user" title="Edit" data-id="{{ $row_data->id }}"><i class="fa fa-edit"></i></a>
                                    <a class="btn btn-sm btn-danger text-white" title="Delete Brand" onclick="deleteUser({{ $row_data->id }})"><i class="fa fa-trash"></i></a>
                                    <button type="button" class="change-status btn btn-sm btn-toggle ml-0 {{$row_data->status}}" data-toggle="button" data-id="{{ $row_data->id }}" data-status="{{ $row_data['status'] == 'active' ? 'Deactivate' : 'Activate' }}" aria-pressed="true" autocomplete="off">
                                        <div class="handle" data-toggle="tooltip" data-placement="top" title="Activate / Deactivate"></div>
                                    </button>
                                </td>
                            </tr>
                            @endforeach
                            @else
                            <tr>
                                <td colspan="8" class="text-center">No records found!</td>
                            </tr>
                            @endif
                        </tbody>
                    </table>
                </div>
                <div class="text-center d-flex justify-content-center mt-3">
                    {{ $user->links() }}
                </div>
            </div>
        </div>
    </div>
</div>
{{-- Pop Up --}}
<div class="modal fade" id="user_popup" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="name_change"><strong>
                        ADD USER
                    </strong></h4>
                <button type="button" class="close cancel-btn" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
            <form id="user_form" action="javascript:;" method="POST">
                <div class="modal-body">
                    <div class="msg_div"></div>
                    <div class="row">
                        <input type="hidden" name="user_unique" id='user_unique'>
                        <div class="col-md-6">
                            <label class="control-label">Name <span class="text-danger">*</span></label>
                            <input class="form-control form-white" placeholder="Enter name" type="text" name="user_name" id="user_name" />
                        </div>
                        <div class="col-md-6">
                            <label class="control-label">Email <span class="text-danger">*</span></label>
                            <input class="form-control form-white" placeholder="Enter email address" type="email" id="email" name="email" />
                        </div>
                        <div class="col-md-2">
                            <label class="control-label">Phone <span class="text-danger">*</span></label>
                            <input class="form-control form-white" type="text" name="country_code" value="+966" id="country_code" readonly/>
                        </div>
                        <div class="col-md-4">
                            <label class="control-label">&nbsp;</label>
                            <input class="form-control form-white" placeholder="Enter phone number" type="text" name="phone" id="phone" />
                        </div>
                        <div class="col-md-6">
                            <label class="control-label">Role <span class="text-danger">*</span></label>
                            <select class="form-control" name="role" id="role">
                                <option value=""> Select Role </option>
                                @foreach($role as $rolelist)
                                <option value="{{$rolelist->id}}">{{$rolelist->name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-info waves-effect waves-light save-categorys" id="save_data">
                        ADD
                    </button>
                    <button type="button" class="btn btn-default waves-effect cancel-btn" data-dismiss="modal">Cancel</button>
                </div>
            </form>
        </div>
    </div>
</div>
{{-- end popup --}}
@endsection
@push('css')
<style>
    .reset_style {
        margin-left: 15px;
    }
</style>
@endpush
@push('scripts')

<script>
    $('.cancel-btn').click(function() {
           var validator = $("#user_form").validate();
           validator.resetForm();
         });

    $('#add_new_user').on('click', function() {
        $('#user_form').trigger("reset")
        $('#name_change').html('Add User');
        $('#save_data').text('Add').button("refresh");
        $("#user_form")[0].reset();
        $('#user_popup').modal({
            show: true
        });
    })
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $('.save-categorys').on('click', function(e) {
        $("#user_form").validate({
            rules: {
                user_name: {
                    required: true,
                },
                email: {
                    required: true,
                    email: true

                },
                phone: {
                    required: true,
                    number: true,
                    minlength: 6, // will count space
                    maxlength: 12,

                },
                role: {
                    required: true,
                    // email:true

                },
            },
            messages: {
                user_name: {
                    required: "Name required",
                },
                email: {
                    required: "Email required",
                    email: "Enter valid email"
                },
                phone: {
                    required: "Phone number required",
                    number: "Please enter not more than 12 digits"
                },
                role: {
                    required: 'Role required'

                },
                //  field3: "Please specify the City and State and Country",
                //  field4: "Please specify the City and State and Country"
            },
            submitHandler: function(form) {
                user_unique = $("#user_unique").val();
                $('.loading_box').show();
                $('.loading_box_overlay').show();
                $('button:submit').attr('disabled', true);
                if (user_unique) {
                    $.ajax({
                        type: "POST",
                        url: "{{route('user-list.update')}}",
                        data: {
                            name: $('#user_name').val(),
                            email: $('#email').val(),
                            phone: $('#phone').val(),
                            role: $('#role').val(),
                            user_unique: user_unique
                        },

                        success: function(data) {
                            if (data.status == 1) {
                                Toast.fire({
                                    icon: 'success',
                                    title: data.message
                                });
                                window.setTimeout(function() {
                                    window.location.reload();
                                }, 1000);
                                $("#user_form")[0].reset();
                            } else {
                                // console.log(data.message);
                                Toast.fire({
                                    icon: 'error',
                                    title: data.message
                                });
                            }
                            $('.loading_box').hide();
                            $('.loading_box_overlay').hide();
                            $('button:submit').attr('disabled', false);
                        }
                    });


                } else {
                    $.ajax({
                        type: "POST",
                        url: "{{route('user-list.store')}}",
                        data: {
                            name: $('#user_name').val(),
                            email: $('#email').val(),
                            phone: $('#phone').val(),
                            role: $('#role').val(),
                        },

                        success: function(data) {
                            if (data.status == 1) {
                                Toast.fire({
                                    icon: 'success',
                                    title: data.message
                                });
                                window.setTimeout(function() {
                                    window.location.reload();
                                }, 1000);
                                $("#user_form")[0].reset();
                            } else {
                                // console.log(data.message);
                                Toast.fire({
                                    icon: 'error',
                                    title: data.message
                                });
                            }
                            $('.loading_box').hide();
                            $('.loading_box_overlay').hide();
                            $('button:submit').attr('disabled', false);
                        }
                    });

                }



            }
        })
    });

    $('.edit_user').on('click', function(e) {
        page = $(this).data('id')
        // console.log("id",page);
        // console.log('editInside');

        $('#name_change').html('Edit User');
        $('#save_data').text('Save').button("refresh");


        var url = "user-list/edit/";

        $.get(url + page, function(data) {
            console.log(data);
            $('#user_name').val(data.user.name),
                $('#email').val(data.user.email),
                $('#phone').val(data.user.phone),
                $('#phone').val(data.user.phone),
                $('#role').val(data.user.role_id),
            $('#user_unique').val(data.user.id)
            $('#user_popup').modal({
                show: true

            });
        });
    });

    $('.change-status').on('click', function(e) {

        var id = $(this).data('id');
        var act_value = $(this).data('status');
     
        $.confirm({
            title: act_value + ' User',
            content: 'Are you sure to ' + act_value + ' the user?',
            buttons: {
                Yes: function() {
        $.ajax({
            type: "POST",
            url: "{{route('user-list.status.update')}}",
            data: {
                id: id,
                status: act_value
            },

            success: function(data) {
                            if (data.status == 1) {
                                Toast.fire({
                                    icon: 'success',
                                    title: data.message
                                });
                                window.setTimeout(function() {
                                    window.location.href = '{{route("user-list.get")}}';
                                }, 1000);

                            } else {
                                Toast.fire({
                                    icon: 'error',
                                    title: data.message
                                });
                            }
                        }
                    });
                },
                No: function() {
                    window.location.reload();
                }
            }
        });
    });
   
    $('.password_send').on('click', function(e) {
        e.preventDefault();
        var id = $(this).data("id");
        $.confirm({
            title: 'Confirmation',
            content: 'Are you sure you want to send password?',
            buttons: {
                Yes: function() {
           
                    
                    $.ajax({
                        url: "{{route('user-list.setPassword')}}",
                        type: 'POST',
                        data: {
                            id: id
                        },
                        success: function(data) {
                           
                            if (data.status == 1) {
                                Toast.fire({
                                    icon: 'success',
                                    title: data.message
                                });
                                window.setTimeout(function() {
                                    window.location.href = '{{route("user-list.get")}}';
                                }, 1000);

                            } else {
                                Toast.fire({
                                    icon: 'error',
                                    title: data.message
                                });
                            }
                        }
                    });
                
                },
         
                No: function() {
                    window.location.reload();
                }
            }
        });
    });
    function deleteUser(id) {
        $.confirm({
            title: false,
            content: 'Are you sure to delete this user? <br><br>You wont be able to revert this',
            buttons: {
                Yes: function() {
                    $.ajax({
                        type: "POST",
                        url: "{{route('user-list.delete')}}",
                        data: {
                            id: id
                        },
                        dataType: "json",
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        success: function(data) {
                            if (data.status == 1) {
                                window.setTimeout(function() {
                                    window.location.href = '{{route("user-list.get")}}';
                                }, 1000);
                                Toast.fire({
                                    icon: 'success',
                                    title: data.message
                                });
                            } else {
                                Toast.fire({
                                    icon: 'error',
                                    title: data.message
                                });
                            }
                        }
                    });
                },
                No: function() {
                    console.log('cancelled');
                }
            }
        });
    }

    $('.cancel-btn').on('click',function(){
          $("#user_unique").val('');
          $("#user_form")[0].reset(); 
      });
      
</script>
@endpush