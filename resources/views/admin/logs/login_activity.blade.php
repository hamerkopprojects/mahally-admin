@extends('layouts.master')
@section('content-title')
LOGIN ACTIVITY
@endsection
@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <form id="posts-filter" method="get" action="{{ route('logs') }}">
                    <div class="row tablenav top text-right">
                        <div class="col-md-6 ml-0">
                            <div class='input-group date' id='datepicker'>
                                <input class="form-control" type="text" name="search" value="{{ $search }}" placeholder="Search by date" autocomplete="off">
                                <span class="input-group-addon">
                                    <span class="fa fa-calendar icon-style"></span>
                                </span>
                            </div>
                        </div>
                        <div class="col-md-3 text-left">
                            <button type="submit" class="btn btn-info"><font style="vertical-align: inherit;">Search</font></button>
                            <a href="{{ route('logs') }}" class="btn btn-default">Reset</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="card">
            <div class="card-body">
                <div id="msgDiv"></div>
                <div class="table-responsive">
                    <table class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>S. No.</th>
                                <th>Name</th>
                                <th>Email</th>
                                <th>IP Address</th>
                                <th>Last Login Time</th>
                                <th class="cls_last_child">Last Activity Time</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if (count($user) > 0)
                            @php
                            $i = 1;
                            @endphp
                            @foreach ($user as $row_data)
                            <tr>
                                <th>{{ $i++ }}</th>
                                <td>{{ $row_data->name }}</td>
                                <td>{{ $row_data->email }}</td>
                                <td>{{ $row_data->ip_address }}</td>
                                <td>{{ !empty($row_data->last_login_date) ? \Carbon\Carbon::parse($row_data->last_login_date)->format('d F Y g:i A') : ''}}</td>
                                <td class="cls_last_child">{{ !empty($row_data->last_activity_date) ? \Carbon\Carbon::parse($row_data->last_activity_date)->format('d F Y g:i A') : ''}}</td>
                            </tr>
                            @endforeach
                            @else
                            <tr>
                                <td colspan="9" class="text-center">No records found!</td>
                            </tr>
                            @endif
                        </tbody>
                    </table>
                </div>
                <div class="text-center d-flex justify-content-center mt-3">
                    {{ $user->links() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('css')
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
@endpush
@push('scripts')
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<script>
$('input[name="search"]').daterangepicker({
    "singleDatePicker": true,
    "autoUpdateInput": false,
    "autoApply": true,
    "maxDate": new Date(),
    locale: {
            format: 'DD-MM-YYYY'
        }
});
$('input[name="search"]').on('apply.daterangepicker', function (ev, picker) {
    $(this).val(picker.startDate.format('DD-MM-YYYY'));
});
</script>

@endpush

