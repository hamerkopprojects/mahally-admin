@extends('layouts.master')
@section('content-title')
ORDER ACTIVITY
@endsection
@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <form id="posts-filter" method="get" action={{route('order-activity')}}>
                    <div class="row tablenav top text-left">

                        <div class="col-md-3 ml-0">
                            <div class='input-group date' id='datepicker'>
                                <input class="form-control datetimepicker" type="text" name="date" value="{{$date ? \Carbon\Carbon::parse($date)->format('d-m-Y') : ''}}" placeholder="Search by date" autocomplete="off">
                                <span class="input-group-addon">
                                    <span class="fa fa-calendar icon-style"></span>
                                </span>
                            </div>
                        </div>
                        <div class="col-md-3 ml-0">
                            <select class="form-control search_val" name="user_id" id="user_id" autocomplete="off">
                                <option value="">By User</option>
                                @foreach ($users as $user)
                                <option value="{{$user->id}}" {{ $user->id == $user_id ? "selected" :""}}>{{ucfirst($user->name)}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-3 ml-0">
                            <select class="form-control search_val" name="customer_id" id="customer_id">
                                <option value="">By Customer</option>
                                @foreach ($customer as $cust)
                                <option value="{{$cust->id}}" {{ $cust->id == $customer_id ? "selected" :""}}>{{ucfirst($cust->name)}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-3 text-left">
                            <button type="submit" class="btn btn-info">
                                <font style="vertical-align: inherit;">Search</font>
                            </button>
                            <a href="{{route('order-activity')}}" class="btn btn-default">Reset</a>
                        </div>
                    </div>

                </form>
            </div>
        </div>
        <div class="card">
            <div class="card-body">
                <div id="msgDiv"></div>
                <div class="table-responsive">
                    <table class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>S.No.</th>
                                <th class="text-left">Order Activity</th>

                            </tr>
                        </thead>
                        <tbody>
                            @if (count($activity) > 0)
                            @php
                            $i = 1;
                            @endphp
                            @foreach ($activity as $list)
                            <tr>
                                <td>{{ $i++ }}</td>
                                <td class="text-left">{!! $list->log !!}</td>

                            </tr>
                            @endforeach
                            @else
                            <tr>
                                <td colspan="8" class="text-center">No records found!</td>
                            </tr>
                            @endif

                        </tbody>
                    </table>
                </div>
                <div class="text-center d-flex justify-content-center mt-3">
                    {{ $activity->appends(request()->input())->links() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('css')
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<style>
    .select2-container--default .select2-selection--single {
        height: 42px !important;border-radius: 0px !important;font-size: 1rem !important;
        padding: .4rem !important;
    }
    .select2-container--default .select2-selection--single .select2-selection__arrow b { top:20px !important}

</style>
@endpush
@push('scripts')
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<script>
$('.datetimepicker').daterangepicker({
    "singleDatePicker": true,
    "autoUpdateInput": false,
    "autoApply": true,
    locale: {
        format: 'DD-MM-YYYY'
    }
});
$('.datetimepicker').on('apply.daterangepicker', function (ev, picker) {
    $(this).val(picker.startDate.format('DD-MM-YYYY'));
});

$("#user_id").select2();
$("#customer_id").select2();
</script>
@endpush