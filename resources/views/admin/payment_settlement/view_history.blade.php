<div class="modal-header">
    <h4 class="modal-title"><strong>
            Payment Settlement
        </strong></h4>
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
</div>
<div class="modal-body">
    <div class="col-lg-12">
            <div class="table-responsive">
                <table class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>S. No.</th>
                            <th>Order ID</th>
                            <th>Customer</th>
                            <th>Sale Amount</th>
                            <th>Commission</th>
                            <th class="cls_last_child">Restaurant Revenue</th>
                        </tr>
                    </thead>
                    <tbody>
                    @if(count($order)> 0)
                    @php
                    $i = 1;
                    @endphp
                    @foreach($order as $row_data)
                    @php
                    $commission = $row_data->grand_total * $c_perecent / 100;
                    $final_amount = $row_data->grand_total - $commission;
                    @endphp
                    <tr>
                        <th>{{ $i++ }}</th>
                        <td>{{ $row_data->order_id }}</td>
                        <td>{{ $row_data->customer[0]->name }}</td>
                        <td>{{ $row_data->grand_total }}</td>
                        <td>{{$commission}}</td>
                        <td class="cls_last_child">{{$final_amount}}</td>
                    </tr>
                    @endforeach
                    @else
                    <tr><td colspan="8" class="text-center">No records found!</td></tr>
                    @endif
                    </tbody>
                </table>
            </div>
            <div class="text-center d-flex justify-content-center mt-3">
                {{ $order->appends(request()->input())->links() }}
            </div>
</div>
</div>