<div class="modal-header">
    <h4 class="modal-title"><strong>
            Payment Settlement
        </strong></h4>
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
</div>
<div class="modal-body">
    <div class="msg_div"></div>
    <div class="row">
        <div class="col-md-6">
            <label class="control-label">Settlement Date <span class="text-danger">*</span></label>
            <input class="form-control form-white" placeholder="Select Date" type="text" name="settlement_date" value=""/>
        </div>
        <div class="col-md-6">
            <label class="control-label">Settlement Amount <span class="text-danger">*</span></label>
            <input class="form-control form-white" placeholder="Enter settlement amount" type="text" name="settlement_amount" value="{{$settled_amount ?? 0}}"/>
        </div>
        <div class="col-md-6">
            <label class="control-label">Transaction Type <span class="text-danger">*</span></label>
            <select class="form-control" name="trans_type">
                <option value=""> Select transaction type </option>
                @foreach($transaction_type as $key => $val)
                <option value="{{$key }}" >{{ $val }}</option>
                @endforeach
            </select>
        </div>
    </div>
</div>
<div class="modal-footer">
    <input type="hidden" name="order_ids" value="{{$order_ids}}">
    <input type="hidden" name="restaurant_id" value="{{$restaurant_id}}">
    <button type="submit" class="btn btn-info waves-effect waves-light save-categorys">Done</button>
    <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Cancel</button>
</div>
<script>
    $('input[name="settlement_date"]').daterangepicker({
        "singleDatePicker": true,
        "autoUpdateInput": false,
        "autoApply": true,
         "drops": 'up',
        locale: {
            format: 'DD-MM-YYYY'
        }
    });
    $('input[name="settlement_date"]').on('apply.daterangepicker', function (ev, picker) {
        $(this).val(picker.startDate.format('DD-MM-YYYY'));
    });
</script>