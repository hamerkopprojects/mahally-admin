@extends('layouts.master')

@section('content-title')
HOW TO USE APP
@endsection
@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <div id="msgDiv"></div>
                <div class="table-responsive">
                    <table class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                {{-- <th>S. No.</th> --}}
                                <th>Title (EN)</th>
                                <th>Description (EN)</th>
                                <th class="right-align">Title (AR)</th>
                                <th class="right-align">Description (AR)</th>
                               
                                {{-- <th>Apps</th> --}}
                                <th width="10%"></th>
                            </tr>
                        </thead>
                        <tbody>
                            @if (count($pages) > 0)
                            @php
                            $i = 1;
                            @endphp
                            @foreach ($pages as $row_data)
                            <tr>
                                {{-- <th>{{ $i++ }}</th> --}}
                                <td>{!! \Illuminate\Support\Str::limit($row_data->lang[0]->title ?? '', 50, '...') !!}</td>
                                <td>
                                    {!! \Illuminate\Support\Str::limit($row_data->lang[0]->content ?? '', 50, '...') !!}</td>
                                    <td class="right-align">{!! \Illuminate\Support\Str::limit($row_data->lang[1]->title ?? '', 50, '...') !!}</td>
                                <td class="right-align">{!! \Illuminate\Support\Str::limit($row_data->lang[1]->content ?? '', 50, '...') !!}</td>
                                
                                {{-- <td>{{ $row_data->available_for }}</td> --}}
                                <td class="text-center" width="12%">
                                    <a class="btn btn-sm btn-success text-white edit_btn page_edit" title="Edit" data-id="{{ $row_data->id }}"><i class="fa fa-edit"></i></a>
                                    <button type="button" class="change-status btn btn-sm btn-toggle ml-0 {{$row_data->status}}" data-toggle="button" data-id="{{ $row_data->id }}" data-status="{{ $row_data['status'] === 'active' ? 'Deactivate' : 'Activate' }}" aria-pressed="true" autocomplete="off">
                                        <div class="handle" data-toggle="tooltip" data-placement="top" title="Activate / Deactivate"></div>
                                    </button>
                                </td>
                            </tr>
                            @endforeach
                            @else
                            <tr>
                                <td colspan="8" class="text-center">No records found!</td>
                            </tr>
                            @endif
                        </tbody>
                    </table>
                </div>
                <div class="text-center d-flex justify-content-center mt-3">
                    {{ $pages->links() }}
                </div>
            </div>
        </div>
    </div>
</div>
{{-- popup --}}
<div class="modal fade" id="page-popup" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
aria-hidden="true">
<div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title" id="exampleModalLabel">Edit How to Use App</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body" data-no-padding="no-padding">
            <form id="use_app" method="POST" action="#">
                <input type="hidden" id="id_pg" name="id_pg">
                @csrf
                {{-- @method('PUT') --}}
                <div class="row mb-3">
                    <div class="col-md-6">
                        <div class="mb-2">Screenshot (EN)</div>
                        <div class="cover-photo">
                            <div id="screen_en_upload" class="add ">+</div>
                            <div id="screenEn_loader" class="loader" style="display: none;">
                                <img src="{{ asset('assets/images/loader.gif') }}" alt="">
                            </div>
                            <div class="preview-image-container" id="screenEn_image_preview" style="display:none;">
                                <div class="scrn-link" style="position: relative;bottom:129px;">
                                    <button type="button" 
                                        class="scrn-img-close delete-files"
                                         data-type="screenEn">
                                        <i class="ti-close" 
                                            style="position: absolute; top: 5px; right: 5px;">
                                        </i>
                                    </button>
                                    {{-- <div id="img_en"> --}}
                                    <img class="scrn-img cr-copy img_en" style="max-width: 200px"  src="" alt="">
                                    {{-- </div> --}}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="control-fileupload" style="display: none;">
                        <label for="image_en" data-nocap="1">Screen Shot (EN)</label>
                        <input type="file" id="image_en" name="image_en"  data-imgw="200" data-imgh="200"/>
                    </div>
                    <div class="col-md-6">
                        <div class="mb-2">Screenshot (AR)</div>
                        <div class="cover-photo">
                            <div id="screen_ar_upload" class="add">+</div>
                            <div id="screenAr_loader" class="loader" style="display: none;">
                                <img src="{{ asset('assets/images/loader.gif') }}" alt="">
                            </div>
                            <div class="preview-image-container" id="screenAr_image_preview" style="display:none;">
                                <div class="scrn-link" style="position: relative;bottom: 129px;">
                                    <button type="button" class="scrn-img-close delete-files" data-type="screenAr">
                                        <i class="ti-close" style="position: absolute; top: 5px; right: 5px;">
                                        </i>
                                    </button>
                                    {{-- <div id="ar_img" > --}}
                                    <img class="scrn-img cr-copy"style="max-width: 200px"  src="" alt="">
                                    {{-- </div> --}}
                               </div>
                            </div>
                        </div>
                    </div>
                    <div class="control-fileupload" style="display: none;">
                        <label for="image_ar" data-nocap="1">Screen Shot (AR)</label>
                        <input type="file" id="image_ar" name="image_ar" />
                    </div>
                    <div class="col-md-6">
                        <label>Title (EN)</label>
                        <textarea class="form-control area" name="title_en" id="title_en" ></textarea>
                        <label class="title_en_error"></label>
                    </div>
                    <div class="col-md-6">
                        <label>Title (AR)</label>
                        <textarea class="form-control area" name="title_ar" id="title_ar"
                            style="text-align:right !important" ></textarea>
                        <label class="title_ar_error"></label>
                    </div>
                    <div class="col-md-6">
                        <label>Content (EN)</label>
                        <textarea class="form-control " id="summaryen" name="summaryen"></textarea>
                        <label class="summaryen_error"></label>
                    </div>
                    <div class="col-md-6">
                        <label>Content (AR)</label>
                        <textarea class="form-control " id="summaryar" name="summaryar"></textarea>
                        <label class="summaryar_error"></label>
                    </div>
                    {{-- <div class="col-md-12 form-group adbot01" id="type_of_app">
                          
                                               
                        @foreach ($app_type as $item)
                            <input type="checkbox" name="app_type" id="app_type" value="{{$item}}" >{{$item}}
                        @endforeach
                        
                    </div> --}}
                    <div class="col-lg-12">
                        <div class="row 5">
                            <div class="col-md-12 text-md-left">
                                <button type="submit" id="save_page" class="btn btn-info waves-effect waves-light">
                                    Save
                                </button>
                                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">
                                    Cancel
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
</div>
{{-- endpopup --}}

{{-- Crop --}}
<div class="modal none-border" id="image-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl " role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Crop Image</h5>
                <button type="button" class="close" data-dismiss-modal="modal2" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" id="crop-images">
                <div class="row">
                    <div class="col-md-6">
                        <img id="cropper" src="" alt="" style="max-width: 100%">
                    </div>
                    <div class="col-md-4">
                        <div class="preview"></div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button id="crop" type="button" class="btn btn-primary">Crop & Save</button>
                <button type="button" class="btn btn-secondary" data-dismiss-modal="modal2">Close</button>
            </div>
        </div>
    </div>
</div>
@endsection
@push('css')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.2/animate.min.css">
<link rel="stylesheet" href="{{ asset('assets/css/lib/cropper/cropper.min.css') }}">
<style>
.modal-dialog{
    overflow-y: initial !important
}
.modal-body{
    height: 620px;
    overflow-y: auto;
}
.area{
        height: 80px;
    }
.scrn-img {
        width: 200px;
        margin-right: 0px;
        height: 125px;
        transition-duration: 0.5s;
        box-shadow: 0 5px 15px rgba(0, 0, 0, 0.2);
        border-radius: 6px;
        object-fit: cover;
    }

    .scrn-img:hover {
        box-shadow: 0 10px 10px rgba(0, 0, 0, 0.15);
    }

    .scrn-img-close {
        border-radius: 1000px;
        border: none;
        /* background: #fff; */
        position: relative;
        transition-duration: 0.5s;
        float: right;
        right: 15px;
        cursor: pointer;
        box-shadow: 0 5px 15px rgba(0, 0, 0, 0.3);
        top: -5px;
        width: 20px;
        height: 20px;
        opacity: 0;
        
    }

    .scrn-img-close:hover {
        background: #1f91f3;
    }

    .scrn-img-close:hover i {
        color: #ffffff;
    }

    .scrn-img-close i {
        font-size: 10px;
        position: relative;
        right: 1px;
        transition-duration: 0.5s;
        top: -3px;
        opacity: 0.7;
    }

    .scrn-link {
        display: inline-block;
        transition-duration: 0.5s;
    }

    .scrn-link:hover .scrn-img-close {
        opacity: 1;
    }

    .scrn-link:hover {
        transform: scale(1.1);
    }
    .scrn-crd {
        padding: 20px;
        padding-left: 0;
    }
    .loader {
        position: absolute;
        top: 14px;
        left: 83px;
        z-index: 1050;
    }

    .cover-photo .preview-image-container {
        position: relative;
        top: 0px;
        width: 200px;
        height: 125px;
    }

    .preview-image-container button.scrn-img-close {
        top: 10px;
        right: -7px;
    }
    .preview {
        overflow: hidden;
        width: 160px;
        height: 160px;
        margin: 10px;
        border: 1px solid red;
    }
    #image-modal .modal-body{
        height: 300px;
    }
    .cover-photo {
        width: 200px;
        height: 125px;
        background-color: #a9a9a9;
        text-align: center;
        line-height: 109px;
        color: white;
        font-size: 40px;
        cursor: pointer;
    }
    input[type=checkbox],input[type=radio]{
        box-sizing: border-box;
        padding: 0;
        margin: 0 10px 0 5px;
    }
</style>
@endpush
@push('scripts')
<script src="{{ asset('assets/js/lib/cropper/cropper.min.js') }}"></script>
<script src="{{ asset('assets/js/lib/cropper/jquery-cropper.min.js') }}"></script>
<script src="{{ asset('assets/js/custom_crop.js') }}"></script>
<script src="//cdn.ckeditor.com/4.14.0/basic/ckeditor.js"></script>
<script>
CKEDITOR.replace('summaryen', {
    removePlugins: 'link',
    // fullPage:true
} );
    CKEDITOR.replace('summaryar', {
    removePlugins: 'link',
    contentsLangDirection: 'rtl'
    } );

</script>
<script>
   
      $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $('#screen_en_upload').click(function(e) {
        // console.log("image")
            $('#image_en').click();
        });
        $('#screen_ar_upload').click(function(e) {
            $('#image_ar').click();
        });
        
        $('#image_en').on('change', function () {
            
            // var imgw = $(this).data('imgw');
            // var imgh = $(this).data('imgh');
            let id =$('#id_pg').val();
            const file = $(this)[0].files[0];
            readUrl(file, 'screenEn', uploadFile, 200, 200, id);
            
            // uploadFile(file, 'screenEn');  
        });
        $('#image_ar').on('change', function () {
            const file = $(this)[0].files[0];
            let id =$('#id_pg').val();
            readUrl(file, 'screenAr', uploadFile, 200, 200, id);
            // console.log(file);
            // uploadFile(file, 'screenAr');  
        });

        function uploadFile(file,type){
            var formData = new FormData();
            formData.append('photo', file);
            formData.append('type', type);
            user_id= $('#id_pg').val();
            url="howto-use/upload/file/"
            $.ajax({
                url : url + user_id,
                type : 'POST',
                data : formData,
                processData: false,  // tell jQuery not to process the data
                contentType: false,  // tell jQuery not to set contentType
                beforeSend: function() {
                    $(`#${type}_loader`).show();
                    $('#image-modal').modal('hide');
                    // $(`#${type}-error`).hide();
                },
                success : function(data) {
                    console.log(type);
                    if (type === 'screenEn') {
                        $(`#${type}_image_preview img`).attr('src', data.image);
                    } 
                    else if(type === 'screenAr'){
                        $(`#${type}_image_preview img`).attr('src', data.image);
                    }
                    $('input[type="file"]').val('');
                    $(`#${type}_image_preview`).show();
                    $(`#${type}_loader`).hide();
                   
                },
                
            });
        }

    $('.page_edit').on('click', function(e) {
        e.preventDefault();
        page = $(this).data('id')
        var url = "howto-use/edit/";
        
       $.get(url  + page, function (data) {
           console.log(data);
           $('#title_en').val(data.page.lang[0].title);
            $('#title_ar').val(data.page.lang[1].title ?? '');
           
            $('#id_pg').val(data.page.id)
            $('#type_of_app').empty();
            $('#type_of_app').append(`
                <label>App Type</label>
                <br>
                <label><strong> ${data.page.available_for} Apps </strong></label>
                `)
            // $("input[name='app_type'][value='" + data.page.available_for + "']").prop('checked', true);
            CKEDITOR.instances['summaryen'].setData(data.page.lang[0].content);
            CKEDITOR.instances['summaryar'].setData(data.page.lang[1].content);
            // $('#ar_img').html('<img src="' +data.page.lang[1].screen_shot + '" style="max-width: 200px"  class="scrn-img cr-copy"/>');
            $('#screenEn_image_preview img').attr('src',data.imgEn);
            $('#screenAr_image_preview img').attr('src', data.imgAr );
            if(data.imgEn){
                $('#screenEn_image_preview').show();
            }
            if(data.imgAr)
            {
                $('#screenAr_image_preview').show();
            }
            
            
            
            // $(`#${type}_image_preview img`).data('url', '');
           $('#page-popup').modal({
            show: true

            });
       }) 
        
       
    })

    $('input[type="checkbox"]').on('change', function() {
        $('input[name="app_type"]').not(this).prop('checked', false);
    });

    CKEDITOR.instances.summaryen.on('change', function() { 	
        if(CKEDITOR.instances.summaryen.getData().length >  0) {
            $('label[for="summaryen"]').hide();
        }
    });
    CKEDITOR.instances.summaryar.on('change', function() { 	
        if(CKEDITOR.instances.summaryar.getData().length >  0) {
            $('label[for="summaryar"]').hide();
        }
    });
    $('#save_page').on('click',function(e){
    
        $("#use_app").validate({
                ignore: [],
                rules: {
                title_en  : {        
                    required: true,         
                },
                title_ar: {          
                    required: true,
                },
                summaryen :{
                    required: function(textarea) {
                     CKEDITOR.instances[textarea.id].updateElement();
                        var editorcontent = textarea.value.replace(/<[^>]*>/gi, '');
                     return editorcontent.length === 0;
                        //  required:  
                        //  function() {
                        //     var messageLength = CKEDITOR.instances['summaryen'].getData().replace(/<[^>]*>/gi, '').length;
                        //     return messageLength === 0;
                         }
                       
                    },
                summaryar :{
                        //  required: function() 
                        // {
                        //  CKEDITOR.instances.summaryar.updateElement();
                        required: function(textarea) {
                            CKEDITOR.instances[textarea.id].updateElement();
                            var editorcontent = textarea.value.replace(/<[^>]*>/gi, '');
                            return editorcontent.length === 0;
                        },
                    },
                    // app_type:{
                    //     required:true
                    // }
                
                },
                messages: {               
                title_en: {
                      required:"Title(EN) required",
                      
                      },
                title_ar: {
                      required: "Title (AR) required",
                      },
                summaryen: {
                      required: "Description (EN) required",
                      },
                summaryar: {
                      required: "Description(AR) required",
                      },
                // app_type:{
                //     required:"App type required"
                // }
                 },
                 errorPlacement: function(error, element) {
                    //  console.log('error',#site-error.error);
                   
                    if (element.attr("name") == "summaryar" ) {
                        $(".summaryar_error").html(error);
                    }
                    if (element.attr("name") == "summaryen" ) {
                        $(".summaryen_error").html(error);
                    }
                    if (element.attr("name") == "title_ar" ) {
                        $(".title_ar_error").html(error);
                    }
                    if (element.attr("name") == "title_en" ) {
                        $(".title_en_error").html(error);
                    }
                    
                 },
                 submitHandler: function(form) {
                    $('.loading_box').show();
                    $('.loading_box_overlay').show();
                    $('button:submit').attr('disabled', true);
                    $.ajax({
                        type:"POST",
                        url: "{{route('use.update')}}",
                        data:{ 
                            title_en:$('#title_en').val(),
                            title_ar:$('#title_ar').val(),
                            id:$('#id_pg').val(),
                            content_en: CKEDITOR.instances['summaryen'].getData(),
                            content_ar:CKEDITOR.instances['summaryar'].getData(),
                            app_type:$('#app_type:checked').val()
                        },
    
                        success: function(result){
                            $('#page-popup').modal('hide')
                           
                            // // $('#cancel-reason-table').DataTable().ajax.reload()
                            Toast.fire({
                                icon: 'success',
                                title: 'How to use updated successfully'
                            });
                            window.location.reload();
                            $('.loading_box').hide();
                            $('.loading_box_overlay').hide();
                        }
                    })

                 }
        });

                       
    })

    $('.delete-files').click(function(e) {
            let type = $(this).data('type');
            let id=$('#id_pg').val();
            Swal.fire({  
                title: 'Are you sure to delete?',  
                text: "You won't be able to revert this!",  
                icon: 'warning',  
                showCancelButton: true,  
                confirmButtonColor: '#3085d6',  
                cancelButtonColor: '#d33',  
                confirmButtonText: 'Yes, delete it!'
            })
            .then((result) => {  
                if (result.value) {    
                    $.ajax({
                        url: "{{route('use.image.delete')}}" ,
                        type: 'POST',
                        data:{
                            type:type,
                            id:id
                        },
                        success: function(data) {
                            if (type === 'screenEn') {
                                $(`#${type}_image_preview img`).data('url', '');
                            } else {
                                $(`#${type}_image_preview img`).attr('src', '');
                            }
                            $(`#${type}_image_preview`).hide();
                        }
                    });
                }
            })
        });
        
        $("button[data-dismiss-modal=modal2]").click(function () {
            $('#image-modal').modal('hide');
        });

        $('.change-status').on('click', function(e) {

            let id = $(this).data('id');
            let act_value = $(this).data('status');

            $.confirm({
                title: act_value + ' Details',
                content: 'Are you sure to ' + act_value + ' the details?',
                buttons: {
                    Yes: function() {
                $.ajax({
                    type: "POST",
                    url: "{{route('userapp.status-change')}}",
                    data: {
                        id: id,
                        status: act_value
                    },

                    success: function(data) {
                            if (data.status == 1) {
                                Toast.fire({
                                    icon: 'success',
                                    title: data.message
                                });
                                window.setTimeout(function() {
                                   window.location.reload();
                                }, 1000);

                            } else {
                                Toast.fire({
                                    icon: 'error',
                                    title: "! Error"
                                });
                            }
                        }
                    });
        },
        No: function() {
            window.location.reload();
        }
    }
});
});
</script>
    
@endpush