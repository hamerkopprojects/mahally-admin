@extends('layouts.master')

@section('content-title')
PAGES
@endsection
@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <div id="msgDiv"></div>
                <div class="table-responsive">
                    <table class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>S. No.</th>
                                <th>Title (EN)</th>
                                <th class="right-align">Title (AR)</th>
                                {{-- <th>App</th> --}}
                                <th width="25%"></th>
                            </tr>
                        </thead>
                        <tbody>
                            @if (count($pages) > 0)
                            @php
                            $i = 1;
                            @endphp
                            @foreach ($pages as $row_data)
                            <tr>
                                <th>{{ $i++ }}</th>
                                <td>{{ $row_data->lang[0]->title ?? '' }}</td>
                                <td class="right-align">{{ $row_data->lang[1]->title ?? '' }}</td>
                                {{-- <td>{{ $row_data->available_for }}</td> --}}
                                <td class="text-center">
                                    <a class="btn btn-sm btn-success text-white edit_btn page_edit" title="Edit"
                                        data-id="{{ $row_data->id }}"><i class="fa fa-edit"></i></a>
                                </td>
                            </tr>
                            @endforeach
                            @else
                            <tr>
                                <td colspan="8" class="text-center">No records found!</td>
                            </tr>
                            @endif
                        </tbody>
                    </table>
                </div>
                <div class="text-center d-flex justify-content-center mt-3">
                    {{ $pages->links() }}
                </div>
            </div>
        </div>
    </div>
</div>
{{-- popup --}}
<div class="modal fade" id="page-popup" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel">Edit Page</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" data-no-padding="no-padding">
                <form id="page-change">
                    <input type="hidden" id="id_pg" name="id_pg">
                    @csrf
                    {{-- @method('PUT') --}}
                    <div class="row mb-3">
                        <div class="col-md-6">
                            <label>Title (EN)</label>
                            <textarea class="form-control area" name="title_en" id="title_en" ></textarea>

                        </div>
                        <div class="col-md-6">
                            <label>Title (AR)</label>
                            <textarea class="form-control area" name="title_ar" id="title_ar"
                                style="text-align:right !important" ></textarea>
                        </div>
                        <div class="col-md-6">
                            <label>Content (EN)</label>
                            <textarea class="form-control" id="summaryen" name="summaryen"></textarea>
                        </div>
                        <div class="col-md-6">
                            <label>Content (AR)</label>
                            <textarea class="form-control" id="summaryar" name="summaryar"></textarea>
                        </div>
                        {{-- <div class="col-md-12 form-group adbot01">
                           
                            <br/>
                            @foreach ($app_type as $item)
                                <input type="checkbox" name="app_type" id="app_type" value="{{$item}}" >{{$item}}
                            @endforeach
                            <label class="site_error"></label>
                        </div> --}}
                        <div class="col-lg-12">
                            <div class="row 5">
                                <div class="col-md-12 text-md-left">
                                    <button type="submit" id="save_page" class="btn btn-info waves-effect waves-light">
                                        Save
                                    </button>
                                    <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">
                                        Cancel
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
{{-- endpopup --}}
@endsection
@push('css')
<style>
    .area{
        height: 80px;
    }
    
    input[type=checkbox],input[type=radio]{
        box-sizing: border-box;
        padding: 0;
        margin: 0 10px 0 5px;
    }
</style>
@endpush
@push('scripts')
<script src="//cdn.ckeditor.com/4.14.0/basic/ckeditor.js"></script>

<script>
    CKEDITOR.replace('summaryen', {
    removePlugins: 'link',
    // fullPage:true
} );
    CKEDITOR.replace('summaryar', {
    removePlugins: 'link',
    contentsLangDirection: 'rtl'
    } );

</script>
<script>
    
      $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $('.page_edit').on('click', function(e) {
        e.preventDefault();
        page = $(this).data('id')
        var url = "pages/edit/";
        
       $.get(url  + page, function (data) {
           $('#title_en').val(data.page.lang[0].title);
            $('#title_ar').val(data.page.lang[1].title ?? '');
            $('#id_pg').val(data.page.id)
            $("input[name='app_type'][value='" + data.page.available_for + "']").prop('checked', true);
            // $('#app_type').val(data.page.available_for);
            CKEDITOR.instances['summaryen'].setData(data.page.lang[0].content);
            CKEDITOR.instances['summaryar'].setData(data.page.lang[1].content);
           $('#page-popup').modal({
            show: true

            });
       }) 
        
       
    });

    $('input[type="checkbox"]').on('change', function() {
        $('input[name="app_type"]').not(this).prop('checked', false);
    });
    CKEDITOR.instances.summaryen.on('change', function() { 	
        if(CKEDITOR.instances.summaryen.getData().length >  0) {
        $('label[for="summaryen"]').hide();
        }
    });
CKEDITOR.instances.summaryar.on('change', function() { 	
    if(CKEDITOR.instances.summaryar.getData().length >  0) {
     $('label[for="summaryar"]').hide();
    }
});
    $('#save_page').on('click',function(e){
              
        $("#page-change").validate({
            ignore: [],
        rules: {
            title_en : {
                required: true,
                // minlength: 5
            },
            title_ar: {
                required: true
            },
            summaryen :{
                    required: function(textarea) {
                     CKEDITOR.instances[textarea.id].updateElement();
                        var editorcontent = textarea.value.replace(/<[^>]*>/gi, '');
                     return editorcontent.length === 0;
                        //  required:  
                        //  function() {
                        //     var messageLength = CKEDITOR.instances['summaryen'].getData().replace(/<[^>]*>/gi, '').length;
                        //     return messageLength === 0;
                         }
                       
                    },
                summaryar :{
                        //  required: function() 
                        // {
                        //  CKEDITOR.instances.summaryar.updateElement();
                        required: function(textarea) {
                            CKEDITOR.instances[textarea.id].updateElement();
                            var editorcontent = textarea.value.replace(/<[^>]*>/gi, '');
                            return editorcontent.length === 0;
                        },
                    },
                    app_type:{
                        required:true
                    }

        },
        messages: {
            title_en: "Title (EN) required",
            title_ar: "Title (AR) required",
            summaryen: {
                      required: "Content  (EN) required",
                      },
            summaryar: {
                      required: "Content (AR) required",
                      },
            app_type:{
                required:"Application type required"
            }   
           },
           submitHandler: function(form) {
            $('.loading_box').show();
            $('.loading_box_overlay').show();
            $('button:submit').attr('disabled', true);
             $.ajax({
             type:"POST",
             url: "{{route('pages.update')}}",
             data:{ 
                 title_en:$('#title_en').val(),
                 title_ar:$('#title_ar').val(),
                 id:$('#id_pg').val(),
                 content_en: CKEDITOR.instances['summaryen'].getData(),
                 content_ar:CKEDITOR.instances['summaryar'].getData(),
                 app_type:$('#app_type:checked').val()
        
             },

             success: function(result){
                 $('#page-popup').modal('hide')
                
                 // // $('#cancel-reason-table').DataTable().ajax.reload()
                 Toast.fire({
                     icon: 'success',
                     title: 'Page updated  successfully'
                 });
                 window.location.reload();
                $('.loading_box').hide();
                $('.loading_box_overlay').hide();
             }
         })
      }
});
    });
  
</script>

@endpush