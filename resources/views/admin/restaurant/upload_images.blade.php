 <div class="mb-2">Product Images</div>
                <div>
                    <div id="photos_loader" class="loader" style="display: none;">
                        <img src="{{ asset('assets/images/loader.gif') }}" alt="">
                    </div>
                    <div class="uploaded-images">
                        @foreach ($pdt_images as $photo)
                        <div class="scrn-crd scrn-link" id='pdt_image_val_{{ $photo->id }}'>
                            <button
                                type="button"
                                class="scrn-img-close delete-pdt_image" data-type="pdt_image" data-id="{{ $photo->id }}">
                                <i class="ti-close"></i>
                            </button>
                            <img class="scrn-img" src="{{ !empty($photo->path) ? url('uploads/'.$photo->path): '' }}" alt="">
                        </div>
                        @endforeach
                        <div class="cover-photo" id="photos-upload">
                            <div class="add">+</div>
                        </div>
                    </div>
                </div>

                <input type="file" id="pdt_image" name="pdt_image" style="display: none;" data-id="{{ $pdt_id }}" data-imgw="800" data-imgh="800"/>
                <div class="mt-2 small">
                    <p>Max file size: 1024KB<br />
                        Supported formats: jpeg,png<br />
                        File dimension: 800 x 800 pixels<br />
                    </p>

                    <span style="display: none;" class="error" id="photos-error"></span>
                </div>