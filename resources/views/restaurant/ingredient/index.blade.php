@extends('layouts.master')

@section('content-title')
{{ __('messages.ingredient.ingredient') }}
@endsection
@section('add-btn')
<button  class="btn btn-info create_btn" id="main_addons_add_btn" >
    <i class="ti-plus"></i> {{ __('messages.ingredient.add_ingredient_from_main_list') }}
</button>
<button  class="btn btn-info create_btn" id="addons_add_btn">
    <i class="ti-plus"></i> {{ __('messages.ingredient.add_ingredient') }}
</button>
@endsection
@section('content')
<div class="row">
    <div class="col-lg-12">
        {{-- <div class="card">
            <div class="card-body">
                <form id="posts-filter" method="get" action="{{ route('ingredients') }}">
                    <div class="row tablenav top text-right">
                        <div class="col-md-5 ml-0">
                            <input class="form-control" type="text" name="search" value="{{$search_field}}" placeholder="Search by Name / Email / Phone/User ID">
                            <input type="hidden" name="ingredient_select" id="ingredient_select">
                        </div>
                        <div class="col-md-3 text-left">
                            <button type="submit" class="btn btn-info">
                                <font style="vertical-align: inherit;">{{ __('messages.search') }}</font>
                            </button>
                            <a href="{{ route('ingredients') }}" class="btn btn-default reset_style">{{ __('messages.reset') }}</a>
                        </div>
                    </div>
                </form>
            </div>
        </div> --}}
                         
            <div class="card">
            <div class="card-body">
                <div id="msgDiv"></div>
                <div class="table-responsive">
                    <table class="table table-bordered table-striped">
                        <thead>
                            <tr>    
                                <th>{{ __('messages.ingredient.table_header.ingr_name_en') }}</th>
                                <th>{{ __('messages.ingredient.table_header.ingr_name_ar') }}</th>
                                <th>{{ __('messages.ingredient.table_header.from') }}</th>
                                <th width="20%"></th>
                            </tr>
                        </thead>
                        <tbody>
                            @if (count($ingredients) > 0)
                            @php
                            $i = 1;
                            @endphp
                            @foreach ($ingredients as $row_data)
                            <tr>   
                                <td>{{ $row_data->lang[0]->name ?? '' }}</td>
                                <td class="right-align">{{ $row_data->lang[1]->name ?? '' }}</td>
                                <td>{{$row_data->type ?? ''}}</td>
                                @if($row_data->type == 'By Restaurant')
                                <td class="text-center">
                                     <button type="button" class="change-status btn btn-sm btn-toggle ml-0 {{$row_data->status}}" data-toggle="button" data-id="{{ $row_data->id }}" data-status="{{ $row_data['status'] == 'active' ? 'Deactivate' : 'Activate' }}" aria-pressed="true" autocomplete="off">
                                        <div class="handle" data-toggle="tooltip" data-placement="top" title="Activate / Deactivate"></div>
                                    </button>
                                    <a class="btn btn-sm btn-success text-white edit_btn page_edit ingredient_edit"  title="Edit" data-id="{{ $row_data->id }}"><i class="fa fa-edit"></i></a> 
                                     <a class="btn btn-sm btn-danger text-white" title="Delete ingredient" onclick="deleteUser({{ $row_data->id }})"><i class="fa fa-trash"></i></a>
                                </td>
                                @else 
                                <td></td>
                                @endif
                            </tr>
                            @endforeach
                            @else
                            <tr>
                                <td colspan="8" class="text-center">No records found!</td>
                            </tr>
                            @endif
                        </tbody>
                    </table>
                </div>
                <div class="text-center d-flex justify-content-center mt-3">
                    {{ $ingredients->links() }}
                </div>
            </div>
        </div>
    </div>
</div>
{{-- popup --}}
<div class="modal fade" id="ingredient-popup" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
            <h4 class="modal-title" id="exampleModalLabel"></h4>
                 <a href="{{route('ingredients')}}" class="btn close cancel-btn"> <span aria-hidden="true">&times;</span></a>
            </div>
            <div class="modal-body" data-no-padding="no-padding">
                <form id="addons-form" method="POST" action="#">
                    <input type="hidden" id="id_pg" name="id_pg">
                    @csrf
                    <div class="row mb-3">
                        <div class="col-md-6">
                            <label>{{ __('messages.ingredient.form.ingr_name_en') }} <span class="text-danger">*</span></label>
                            <input type="text" placeholder="{{ __('messages.ingredient.placeholder.ingr_name_en') }}" class="form-control form-white" id="title_en" name="title_en" value="{{ old('title_en') }}">
                            <label class="title_en_error error-msg"></label>
                        </div> 
                         <div class="col-md-6">
                            <label>{{ __('messages.ingredient.form.ingr_name_ar') }} <span class="text-danger">*</span></label>
                             <input type="text" placeholder="{{ __('messages.ingredient.placeholder.ingr_name_ar') }}" class="form-control form-white text-right" id="title_ar" name="title_ar" value="{{ old('title_ar') }}">
                            <label class="title_ar_error error-msg"></label>
                        </div> 
                        <div class="col-md-6">
                            <label>{{ __('messages.ingredient.form.img') }}</label>
                            <div class ="imgup">
                                <div class="scrn-link">
                                    <button type="button" class="scrn-img-close delete-img" data-id="" onclick="removesrc_en()" data-type="app_image" style="display: none;">
                                        <i class="ti-close" style="position: absolute; top: 5px; right: 5px;"></i>
                                    </button>
                                    <img id="previewimage_en" class="cover-photo" onclick="$('#uploadFile_en').click();" src="{{ asset('assets/images/upload.png') }}" />
                                </div>
                                <input type="file" id="uploadFile_en" name="upload_image" style="visibility: hidden;" accept="image/*" value="" />
                                <input type="hidden" name="hidden_image_en" id="hidden_image_en">
                            </div>
                            <span class="error"></span>
                            <div class="catimg">
                                <p class="small">Max file size: 1MB</p><br>
                                <p class="small" style="margin-top:-30px">Supported formats: jpeg,png</p><br>
                                <p class="small" style="margin-top:-30px">File dimension: 1200 x 360 pixels</p>
                            </div>
                        </div>  
                        <div class="col-lg-12">
                            <div class="row 5">
                                <div class="col-md-12 text-md-left">
                                    <button type="submit"  class="btn btn-info waves-effect waves-light save_page">
                                        {{ __('messages.form.save') }}
                                    </button>
                                    
                                <a href="{{route('ingredients')}}" class="btn btn-default reset_style">{{ __('messages.form.cancel') }}</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

{{-- End Popup --}}
{{-- Main List Popup --}}
<div class="modal fade" id="addon-main-popup" tabindex="-1" role="dialog" aria-labelledby="mainList"
    aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
            <h4 class="modal-title" id="mainList"></h4>
                 <a href="{{route('ingredients')}}" class="btn close cancel-btn"> <span aria-hidden="true">&times;</span></a>
            </div>
            <div class="modal-body" data-no-padding="no-padding">
                <form id="ingredient-main-form" method="POST" action="#">
                    <input type="hidden" id="restaurant_id" name="restaurant_id">
                    @csrf
                    <div class="row mb-3">
                        @if($ingredientMainList)
                        @foreach($ingredientMainList as $item)
                            <div class="col-md-3">
                                @if(in_array($item->id,$ingredient_data))
                                    <input type="checkbox" name="ingredients[]" class="ingredients" value="{{$item->id}}" checked> {{ $item->name}}
                                @else 
                                    <input type="checkbox" name="ingredients[]" class="ingredients" value="{{$item->id}}"> {{ $item->name}}
                                @endif 
                            </div> 
                        @endforeach
                        @endif
                         
                        <div class="col-lg-12">
                            <div class="row 5">
                                <div class="col-md-12 text-md-left">
                                    <button type="submit"  class="btn btn-info waves-effect waves-light save-addon-main">
                                        Save
                                    </button>
                                    
                                <a href="{{route('ingredients')}}" class="btn btn-default reset_style">Cancel</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
@push('css')
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<style>
    .reset_style {
        margin-left: 15px;
    }
    .search_wid{
        width: 234px;
    }
   
    .btn-fea{
        background-color: #87b23e;
    }
    .validation
    {
      color: red;
     
    }
    
</style>

@endpush
@push('scripts')
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="//cdn.ckeditor.com/4.14.0/basic/ckeditor.js"></script>

<script>
   
      $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }});


        $('#addons_add_btn').on('click',function(e){
            $('#addons-form').trigger("reset")
            $('#exampleModalLabel').html('Add Ingredients');
            $("#addons-form")[0].reset();
            $('#ingredient-popup').modal({
                show:true
            })
        })

        $('#main_addons_add_btn').on('click',function(e){
            $('#ingredient-main-form').trigger("reset")
            $('#mainList').html('Add From Main List');
            $("#ingredient-main-form")[0].reset();
            $('#addon-main-popup').modal({
                show:true
            })
        })
         $('.save_page').on('click',function(e){
           
            $("#addons-form").validate({
                ignore: [],
                rules: {
                    title_en  : {        
                        required: true,         
                    },
                    title_ar  : {        
                        required: true,         
                    },  
                },
                messages: {               
                    title_en: {
                        required: @json(__('messages.ingredient.validation.title_en') ),
                        
                    },
                     title_ar: {
                        required: @json(__('messages.ingredient.validation.title_ar') ),
                        
                    },
                },
                errorPlacement: function(error, element) {
                    if (element.attr("name") == "title_en" ) {
                        $(".title_en_error").html(error);
                    }
                    if (element.attr("name") == "title_ar" ) {
                        $(".title_ar_error").html(error);
                    }
                    
                },
                 submitHandler: function(form) {
                    $('.loading_box').show();
                    $('.loading_box_overlay').show();
                    let edit_val=$('#id_pg').val();
                    $('button:submit').attr('disabled', true);
                    if(edit_val){
                    $.ajax({
                        type:"POST",
                        url: "{{route('ingredients.update')}}",
                        data: new FormData(form),
                        mimeType: "multipart/form-data",
                        dataType: 'JSON',
                        contentType: false,
                        processData: false,
                        success: function(data) {
                            if (data.status == 1) {
                                Toast.fire({
                                    icon: 'success',
                                    title: data.message
                                });
                                window.setTimeout(function() {
                                    window.location.reload();
                                }, 1000);
                                $("#addons-form")[0].reset();
                            } else {
                                $('.loading_box').hide();
                                $('.loading_box_overlay').hide();
                                Toast.fire({
                                    icon: 'error',
                                    title: data.message
                                });
                            }
                        }      
                    });
                }else{
                    $.ajax({
                        type:"POST",
                        url: "{{route('ingredients.store')}}",
                        data: new FormData(form),
                        mimeType: "multipart/form-data",
                        dataType: 'JSON',
                        contentType: false,
                        processData: false,
                        success: function(data) {
                            if (data.status == 1) {
                                Toast.fire({
                                    icon: 'success',
                                    title: data.message
                                });
                                window.setTimeout(function() {
                                    window.location.reload();
                                }, 1000);
                                $("#addons-form")[0].reset();
                            } else {
                                $('.loading_box').hide();
                                $('.loading_box_overlay').hide();
                                Toast.fire({
                                    icon: 'error',
                                    title: data.message
                                });
                            }
                        }
                    }) ;
                    }
                
                }
            });
            
           
        });

        $('.save-addon-main').on('click',function(e){  
           $("#ingredient-main-form").validate({
                ignore: [],
                rules: {
                  
                },
                messages: {               
                    
                },
                errorPlacement: function(error, element) {
                       
                },
                 submitHandler: function(form) {
                    $('.loading_box').show();
                    $('.loading_box_overlay').show();
                    $('button:submit').attr('disabled', true);
                  
                    $.ajax({
                        type: "POST",
                        url: "{{route('ingredients.storeMainList')}}",
                        data: $('#ingredient-main-form').serialize(),

                        success: function(data) {
                            if (data.status == 1) {
                                Toast.fire({
                                    icon: 'success',
                                    title: data.message
                                });
                                window.setTimeout(function() {
                                    window.location.reload();
                                }, 1000);
                                $("#ingredient-main-form")[0].reset();
                            } else {
                               
                                Toast.fire({
                                    icon: 'error',
                                    title: data.message
                                });
                            }
                            $('.loading_box').hide();
                            $('.loading_box_overlay').hide();
                            $('button:submit').attr('disabled', false);
                        }
                    });
                  
                
                }
            });                 
        });

         $('.ingredient_edit').on('click',function(e){
            e.preventDefault();
            $('#exampleModalLabel').html('Edit Ingredient');

            page = $(this).data('id')
            var url = "ingredients/edit/";
        
            $.get(url  + page, function (data) {
                console.log(data);
                $('#title_en').val(data.page.lang[0].name);
                $('#title_ar').val(data.page.lang[1].name);
                $('#id_pg').val(data.page.id)
                $('#previewimage_en').val(data.page.image_path)
                var uploadsUrl = "<?php echo asset('/uploads/restaurant/ingredients') ?>";
                var imgurl = uploadsUrl + '/' + data.page.image_path;
                if (data.page.image_path) {
                    $('#previewimage_en').attr("src", imgurl);
                    $('#hidden_image_en').val(data.page.image_path);
                    $(".delete-img").show();
                }
                $('#ingredient-popup').modal({
                    show: true

                });
            }) 
        })

       
        $('.change-status').on('click', function(e) {
            var id = $(this).data('id');
            var act_value = $(this).data('status');
        
            $.confirm({
                title: act_value + ' Ingredient',
                content: 'Are you sure to ' + act_value + ' the ingredient?',
                buttons: {
                    Yes: function() {
            $.ajax({
                type: "POST",
                url: "{{route('ingredients.status.update')}}",
                data: {
                    id: id,
                    status: act_value
                },

                success: function(data) {
                                if (data.status == 1) {
                                    Toast.fire({
                                        icon: 'success',
                                        title: data.message
                                    });
                                    window.setTimeout(function() {
                                        window.location.href = '{{route("ingredients")}}';
                                    }, 1000);

                                } else {
                                    Toast.fire({
                                        icon: 'error',
                                        title: data.message
                                    });
                                }
                            }
                        });
                    },
                    No: function() {
                        window.location.reload();
                    }
                }
            });
        });

        function deleteUser(id) {
            $.confirm({
                title: false,
                content: 'Are you sure to delete this ingredient? <br><br>You wont be able to revert this',
                buttons: {
                    Yes: function() {
                        $.ajax({
                            type: "POST",
                            url: "{{route('ingredients.delete')}}",
                            data: {
                                id: id
                            },
                            dataType: "json",
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            success: function(data) {
                                if (data.status == 1) {
                                    window.setTimeout(function() {
                                        window.location.href = '{{route("ingredients")}}';
                                    }, 1000);
                                    Toast.fire({
                                        icon: 'success',
                                        title: data.message
                                    });
                                } else {
                                    Toast.fire({
                                        icon: 'error',
                                        title: data.message
                                    });
                                }
                            }
                        });
                    },
                    No: function() {
                        console.log('cancelled');
                    }
                }
            });
        }

         function removesrc_en(){
        $('#previewimage_en').attr('src', '{{ asset('assets/images/upload.png') }}');
        $('#hidden_image_en').remove();
        $("#uploadFile_en").val("");
         $(".delete-img").hide();
    };

    document.getElementById("uploadFile_en").onchange = function(e) {
        var focusSet = false;
        var reader = new FileReader();
        var fileUpload = document.getElementById("uploadFile_en");
        if (fileUpload!= '') {
            // var regex = new RegExp("([a-zA-Z0-9\s_\\.\-:])+(.jpg|.png|.jpeg)$");
            var filePath = fileUpload.value; 
            var allowedExtensions =  /(\.jpg|\.jpeg|\.png|\.gif)$/i; 
            var file_size = $('#uploadFile_en')[0].files[0].size;
            
                if (!allowedExtensions.exec(filePath)) {
                    $("#uploadFile_en").parent().next(".validation").remove(); // remove it
                    $('#previewimage_en').attr('src', '{{ asset('assets/images/upload.png') }}');
                    $('#hidden_image_en').remove();
                    $("#uploadFile_en").val("");
                    $("#uploadFile_en").parent().after("<div class='validation' style='color:red;font-size: 12px;  font-weight: 400;'>The image must be of the format jpeg or png </div>");
                    $(".delete-img").hide();
                }
                else if(file_size > 1024000) {
                    $("#uploadFile_en").parent().next(".validation").remove(); // remove it
                    $('#previewimage_en').attr('src', '{{ asset('assets/images/upload.png') }}');
                    $('#hidden_image_en').remove();
                    $("#uploadFile_en").val("");
                    $("#uploadFile_en").parent().after("<div class='validation' style='color:red;font-size: 12px;  font-weight: 400;'>The image must be less than 1MB in size </div>");
                    $(".delete-img").hide();
                }
            else if(fileUpload!= ''){
                    $("#uploadFile_en").parent().next(".validation").remove(); // remove it

                    var fileUpload = document.getElementById("uploadFile_en");
                    var reader = new FileReader();

                    //Read the contents of Image File.
                    reader.readAsDataURL(fileUpload.files[0]);
                    reader.onload = function (e) {

                        //Initiate the JavaScript Image object.
                        var image = new Image();

                        //Set the Base64 string return from FileReader as source.
                        image.src = e.target.result;

                        //Validate the File Height and Width.
                        image.onload = function () {
                            var height = this.height;
                            var width = this.width;

                            $("#uploadFile_en").parent().next(".validation").remove(); // remove it
                            var reader = new FileReader();
                        
                            document.getElementById("previewimage_en").src = e.target.result;
                        
                            reader.readAsDataURL(fileUpload.files[0]);
                            $(".delete-img").show();
                        }           
                    }
                }
                else{  
                $("#uploadFile_en").parent().next(".validation").remove(); // remove it
                    var reader = new FileReader();
                    reader.onload = function(e) {
                    document.getElementById("previewimage_en").src = e.target.result;
                    };
                    reader.readAsDataURL(this.files[0]);
                }
                 $(".delete-img").show();
                
        }else{
            $("#uploadFile_en").parent().next(".validation").remove(); // remove it
        
        }
    };

    $('.cancel-btn').on('click',function(){
          $('#id_pg').val('');
         $('#previewimage_en').attr('src', '{{ asset('assets/images/upload.png') }}');
            $("#addons-form")[0].reset();        
    });

    // $("#search_field").autocomplete({
    //     source: function( request, response ) {
    //     $.ajax( {
    //       url: "{{route('addons.search')}}",
    //       method:'post',
    //       data: {
    //         search: request.term
    //       },
    //       success: function( data ) {
    //         response( data );
    //       }
    //     } );
    //   },
    //   minLength: 1,
    //   select: function( event, ui ) {
    //     $('#search_field').val(ui.item.label); // display the selected text
    //     $('#addons_select').val(ui.item.value); // save selected id to input
    //        return false;
    //   }
    // });

    </script>
@endpush