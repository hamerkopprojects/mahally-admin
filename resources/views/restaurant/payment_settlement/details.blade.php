
<div class="col-lg-6">
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-md-6 ml-0">
                    <h6>{{ __('messages.payment_settlement.rest_name') }}</h6>
                    <p>{{ucfirst($restaurant->lang[0]->name)}}</p>
                </div>
                <div class="col-md-6 ml-0">
                    <h6>{{ __('messages.payment_settlement.phone') }}</h6>
                    <p>{{$restaurant->contact[0]->phone}}</p>
                </div>
                <div class="col-md-6 ml-0">
                    <h6>{{ __('messages.payment_settlement.rest_code') }}</h6>
                    <p>{{$restaurant->unique_id}}</p>
                </div>
                <div class="col-md-6 ml-0">
                    <h6>{{ __('messages.payment_settlement.email') }}</h6>
                    <p>{{$restaurant->contact[0]->email}}</p>
                </div>
                <div class="col-md-6 ml-0">
                    <h6>{{ __('messages.payment_settlement.comm_percent') }}</h6>
                    <p>{{$c_perecent}} %</p>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="col-lg-6">
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-md-8 ml-0">
                    <label>{{ __('messages.payment_settlement.total_number_of_order') }}:</label>
                </div>
                <div class="col-md-4 ml-0">
                    <h6>{{$total_orders}}</h6>
                </div>
                <div class="col-md-8 ml-0">
                    <label>{{ __('messages.payment_settlement.tot_ord_online') }}:</label>
                </div>
                <div class="col-md-4 ml-0">
                    <h6>{{$total_order_online->online_count}}</h6>
                </div>
                <div class="col-md-8 ml-0">
                    <label>{{ __('messages.payment_settlement.tot_ord_counter') }}:</label>
                </div>
                <div class="col-md-4 ml-0">
                    <h6>{{$total_order_at_counter->online_count}}</h6>
                </div>
                <div class="col-md-8 ml-0">
                    <label>{{ __('messages.payment_settlement.total_sale') }}:</label>
                </div>
                <div class="col-md-4 ml-0">
                    <h6>SAR {{ $total_sale ?? 0}}</h6>
                </div>
                <div class="col-md-8 ml-0">
                    <label>{{ __('messages.payment_settlement.commision_of_mahally') }}:</label>
                </div>
                <div class="col-md-4 ml-0">
                    <h6>SAR {{$commmission ?? 0}}</h6>
                </div>
                <div class="col-md-8 ml-0">
                    <label>{{ __('messages.payment_settlement.settlement_amount') }}:</label>
                </div>
                <div class="col-md-4 ml-0">
                    <h6>SAR {{$total_revenue}}</h6>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="col-lg-12">
    <div class="card">
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>{{ __('messages.payment_settlement.sno') }}</th>
                            <th>{{ __('messages.payment_settlement.settlement_date') }}</th>
                            <th>{{ __('messages.payment_settlement.settlement_amt') }}</th>
                            <th class="cls_last_child">{{ __('messages.payment_settlement.settlement_type') }}</th>
                        </tr>
                    </thead>
                    <tbody>
                    @if(count($payment_settlement)> 0)
                    @php
                    $i = 1;
                    @endphp
                    @foreach($payment_settlement as $row_data)
                    <tr>
                        <th>{{ $i++ }}</th>
                        <td>{{date('j F Y', strtotime($row_data->settlement_date))}}</td>
                        <td>{{$row_data->settlement_amount}}</td>
                        <td class="cls_last_child">{{$transaction_type[$row_data->type] ?? ''}}</td>
                    </tr>
                    @endforeach
                    @else
                    <tr><td colspan="8" class="text-center">{{ __('messages.payment_settlement.no_records_found') }}</td></tr>
                    @endif
                    </tbody>
                </table>
            </div>
            <div class="text-center d-flex justify-content-center mt-3">
                {{ $payment_settlement->appends(request()->input())->links() }}
            </div>
        </div>
    </div>
</div>
