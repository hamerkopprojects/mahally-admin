<div class="modal-header">
    <h4 class="modal-title"><strong>
            {{( isset($row_data['id']) && $row_data['id'] != '' ) ? 'Edit' : 'Add' }}
            {{ __('messages.menu_items.tab.menu_item') }}
        </strong></h4>
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
</div>
<div class="col-sm-12">
    <ul class="nav nav-tabs" role="tablist" id="item_tabs">
        <li class="nav-item w-25 "> <a class="nav-link active" href="#basic_info" data-id="{{ isset($row_data['id']) ? $row_data['id']  : '' }}" role="tab"><span class="hidden-sm-up"><i class="ti-folder"></i></span> <span class="hidden-xs-down">BASIC INFO</span></a> </li>
        <li class="nav-item w-25  disabled"> <a class="nav-link" href="#photos" data-id="{{ isset($row_data['id']) ? $row_data['id']  : '' }}" role="tab"><span class="hidden-sm-up"><i class="ti-folder"></i></span> <span class="hidden-xs-down">PHOTOS</span></a> </li>
        <li class="nav-item w-25  disabled"> <a class="nav-link" href="#attributes" data-id="{{ isset($row_data['id']) ? $row_data['id']  : '' }}" role="tab"><span class="hidden-sm-up"><i class="ti-folder"></i></span> <span class="hidden-xs-down">ATTRIBUTES</span></a> </li>
        <li class="nav-item w-25  disabled"> <a class="nav-link" href="#related_items" data-id="{{ isset($row_data['id']) ? $row_data['id']  : '' }}" role="tab"><span class="hidden-sm-up"><i class="ti-folder"></i></span> <span class="hidden-xs-down">RELATED ITEMS</span></a> </li>
    </ul>
</div>
<div class="tab-content">
    @include('restaurant.menu_items.basic_info')
</div>


<style type="text/css">
    img {
        display: block;
        max-width: 100%;
    }
    .preview {
        overflow: hidden;
        width: 160px;
        height: 160px;
        margin: 10px;
        border: 1px solid red;
    }
    #formModal { overflow-y:auto; }
 
</style>

<script>
    $("button[data-dismiss=modal]").click(function () {
        location.reload();
    });
    $('.nav-tabs a').on('click', function (e) {
        var id = $(this).data("id");
        var x = $(e.target).text();
        var location = $(this).attr('href');
        if (id != '') {
            $.ajax({
                type: "GET",
                url: "{{route('item_tabs')}}",
                data: {'activeTab': x, menu_id: id},
                success: function (result) {
                $('#item_tabs a[href="'+location+'"]').tab('show');
                    $('.tab-content').html(result);
                }
            });
        }
    });
</script>