<div class="row">
    <div class="col-lg-12 ml-0">
        <div class="page-title">
            <h5>{{ __('messages.dashboard.recent_orders') }}</h5>
        </div>
        <div class="card">
            <div class="card-body">
                <div id="msgDiv"></div>
                <div class="table-responsive">
                    <table class="table table-bordered table-striped">
                        <thead>
                        
                            <tr>
                                <th>{{ __('messages.orders.form.sl_no') }}</th>
                                <th>{{ __('messages.orders.form.order_id') }}</th>
                                <th>{{ __('messages.orders.form.customer') }}</th>
                                <th>{{ __('messages.orders.form.order_date') }}</th>
                                <th>{{ __('messages.orders.form.amount') }}</th>
                                <th>{{ __('messages.orders.form.status') }}</th>
                                <th>{{ __('messages.orders.form.payment_type') }}</th>
                                <th width="10%"></th>
                            </tr>
                        </thead>
                        <tbody>
                          
                            @if(count($recent_order) > 0 )
                            @php
                            $i = 1;
                            @endphp
                            @foreach ($recent_order as $item)
                            <tr> 
                                <td>{{$i++}}</td>
                                <td>{{$item->order_id ?? ''}}</td>
                                <td>{{$item->customer[0]->name ?? ''}}</td>
                                <td>{{ \Carbon\Carbon::parse($item->created_at)->format('d-m-Y')}}</td>
                                <td>SAR {{$item->grand_total ?? ''}}</td>
                                <td>
                                    @if($item->order_status=='2')
                                    <button class="button" style="background-color: #4CAF50;">{{$order_status[$item->order_status]}}</button>
                                   
                                     @elseif($item->order_status=='3')   
                                    <button class="button" style="background-color: #ffd400;">{{$order_status[$item->order_status]}}</button>

                                    @else
                                    <button class="button" style="background-color: #ff6a00;">{{$order_status[$item->order_status]}}</button>
                                    @endif
                                </td>
                                 <td>{{$item->payment_mode}}</td>
                                <td class="text-center">
                                     <a class="btn btn-sm btn-success text-white view_btn" title="View Order Details" data-id="{{ $item->id }}"><i class="fa fa-eye"></i></a>
                                </td>
                            </tr>
                            @endforeach
                                
                            @else
                            <tr>
                                <td colspan="8" class="text-center">No records found!</td>
                            </tr>

                            @endif
                        </tbody>
                    </table>
                </div>
                <div class="text-center d-flex justify-content-center mt-3">
                </div>
            </div>
        </div>
    </div>
</div>
@push('css')
<style>
.button {
  border: none;
  color: white;
  padding: 2px 10px;
  text-align: center;
  text-decoration: none;
  display: inline-block;
  font-size: 16px;
  margin: 4px 2px;
  cursor: pointer;
  border-radius: 12px;
}
</style>
@endpush
@push('scripts')
<script>
    $('.view_btn').on('click', function() {
    var id = $(this).data("id");
    $.ajax({
    type: "GET",
            url: "order/details/" + id,
            success: function(data) {
            $('.modal-content').html(data);
            $('#formModal').modal('show');
            }
    });
    });
</script>
  @endpush