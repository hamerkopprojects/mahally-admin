<div class="row">
    <a  href="#" class="col-lg-4 col-md-6 col-sm-6">
        <div class="card card-stats">
            <div class="card-header card-header-warning card-header-icon">
                 <p class="align-p card-count" id="total_customers">{{$total_customers ?? ''}}</p>
                <h3 class="card-title1">{{ __('messages.dashboard.total_customers') }}</h3>
            </div>
            <div class="card-footer">
            </div>
        </div>
    </a>
    <a href="#" class="col-lg-4 col-md-6 col-sm-6">
        <div class="card card-stats">
            <div class="card-header card-header-success card-header-icon">
                <p class="align-p card-count" id="total_orders">{{$total_orders ?? ''}}</p>
                <h3 class="card-title1">{{ __('messages.dashboard.total_orders') }}</h3>
            </div>
            <div class="card-footer">
            </div>
        </div>
    </a>
    <a class="col-lg-4 col-md-6 col-sm-6">
        <div class="card card-stats">
            <div class="card-header card-header-danger card-header-icon">
                <p class="align-p card-count" id="total_sale">{{$sales_count ?? ''}}</p>
                <h3 class="card-title1">{{ __('messages.dashboard.total_sale') }}</h3>
            </div>
            <div class="card-footer">
            </div>
        </div>
    </a>
   
</div>