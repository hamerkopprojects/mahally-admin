@extends('layouts.master')

@section('content-title')
{{ __('messages.free_meals.free_meals') }}
@endsection
@section('add-btn')
<button  class="btn btn-info create_btn" id="offer_add_btn">
    <i class="ti-plus"></i> {{ __('messages.free_meals.add_free_meals') }}
</button>
@endsection
@section('content')
<div class="row">
    <div class="col-lg-12">  
            <div class="card">
            <div class="card-body">
                <div id="msgDiv"></div>
                <div class="table-responsive">
                    <table class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>{{ __('messages.free_meals.table_header.sl_no') }}</th>    
                                <th>{{ __('messages.free_meals.table_header.title_en') }}</th>
                                <th>{{ __('messages.free_meals.table_header.title_ar') }}</th>
                                <th>{{ __('messages.free_meals.table_header.item') }}</th>
                                <th>{{ __('messages.free_meals.table_header.no_order') }}</th>
                                <th>{{ __('messages.free_meals.table_header.expire_in') }}</th>
                                <th width="20%"></th>
                            </tr>
                        </thead>
                        <tbody>
                            @if (count($free_meals) > 0)
                            @php
                            $i = 1;
                            @endphp
                            @foreach ($free_meals as $row_data)
                            <tr>  
                                <td>{{ $i++}}</td> 
                                <td>{{ $row_data->lang[0]->name ?? '' }}</td>
                                <td>{{ $row_data->lang[1]->name ?? '' }}</td>
                                @php 
                                    $itemData = \App\Models\MenuItems::with('lang')->where('id', $row_data->menu_item_id)->first();
                                @endphp
                                <td>{{ $itemData->lang[0]->name ?? '' }}</td>
                                <td>{{$row_data->order ?? ''}}</td>
                                <td>{{$row_data->expire_in ?? ''}}</td>
                                <td class="text-center">
                                     <button type="button" class="change-status btn btn-sm btn-toggle ml-0 {{$row_data->status}}" data-toggle="button" data-id="{{ $row_data->id }}" data-status="{{ $row_data['status'] == 'active' ? 'Deactivate' : 'Activate' }}" aria-pressed="true" autocomplete="off">
                                        <div class="handle" data-toggle="tooltip" data-placement="top" title="Activate / Deactivate"></div>
                                    </button>
                                    <a class="btn btn-sm btn-success text-white edit_btn page_edit offer_edit"  title="Edit" data-id="{{ $row_data->id }}"><i class="fa fa-edit"></i></a> 
                                     <a class="btn btn-sm btn-danger text-white" title="Delete Addons" onclick="deleteUser({{ $row_data->id }})"><i class="fa fa-trash"></i></a>
                                </td>
                               
                            </tr>
                            @endforeach
                            @else
                            <tr>
                                <td colspan="8" class="text-center">No records found!</td>
                            </tr>
                            @endif
                        </tbody>
                    </table>
                </div>
                <div class="text-center d-flex justify-content-center mt-3">
                    {{ $free_meals->links() }}
                </div>
            </div>
        </div>
    </div>
</div>
{{-- popup --}}
<div class="modal fade" id="offer-popup" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
            <h4 class="modal-title" id="exampleModalLabel"></h4>
                 <a href="{{route('free_meal_loyalty')}}" class="btn close cancel-btn"> <span aria-hidden="true">&times;</span></a>
            </div>
            <div class="modal-body" data-no-padding="no-padding">
                <form id="free-meal-form" method="POST" action="#">
                    <input type="hidden" id="id_pg" name="id_pg">
                    @csrf
                    <div class="row mb-3">
                        <div class="col-md-6">
                            <label>{{ __('messages.free_meals.form.title_en') }} <span class="text-danger">*</span></label>
                            <input type="text" placeholder="{{ __('messages.free_meals.placeholder.title_en') }}" class="form-control form-white" id="title_en" name="title_en" value="{{ old('title_en') }}">
                            <label class="title_en_error error-msg"></label>
                        </div> 
                         <div class="col-md-6">
                            <label>{{ __('messages.free_meals.form.title_ar') }} <span class="text-danger">*</span></label>
                             <input type="text" placeholder="{{ __('messages.free_meals.placeholder.title_ar') }}" class="form-control form-white text-right" id="title_ar" name="title_ar" value="{{ old('title_ar') }}">
                            <label class="title_ar_error error-msg"></label>
                        </div>
                        <div class="col-md-6">
                            <label class="control-label">{{ __('messages.free_meals.form.sel_item') }} <span class="text-danger">*</span></label><br/>
                            <select class="form-control" name="menu_item_id" id="menu_item_id">
                                <option value="">Select item</option>
                                @foreach($menuItemList as $item)
                                <option  value="{{ $item->id }}" {{ (isset($row_data['menu_item_id']) &&  $item->id  == $row_data['menu_item_id']) ? 'selected' : '' }}>{{ $item->lang[0]->name }}</option>
                                @endforeach
                            </select>
                            <label class="menu_item_id_error"></label>
                        </div>
                        <div class="col-md-6"></div>
                        <div class="col-md-6">
                            <label class="control-label">{{ __('messages.free_meals.form.no_order') }} <span class="text-danger">*</span></label>
                            <input class="form-control form-white" placeholder="{{ __('messages.free_meals.placeholder.no_order') }}" type="text" id="order" name="order" />
                            <label class="order_error"></label>
                        </div>
                        <div class="col-md-6">
                            <label class="control-label">{{ __('messages.free_meals.form.expire_in') }} <span class="text-danger">*</span></label>
                            <div class='input-group date' id='datepicker'>
                                <input class="form-control" type="text" name="expire_in" id="expire_in" placeholder="{{ __('messages.free_meals.placeholder.expire_in') }}" autocomplete="off">
                                <span class="input-group-addon">
                                    <span class="fa fa-calendar icon-style"></span>
                                </span>
                            </div>
                            <label class="expire_in_error"></label>
                        </div>
                         <div class="col-md-6">
                            <label>{{ __('messages.free_meals.form.description_en') }} </label>
                            <textarea class="form-control area" id="description_en" name="description_en"placeholder="{{ __('messages.free_meals.placeholder.description_en') }}"></textarea>
                            <label class="description_en_error"></label>
                        </div> 
                        <div class="col-md-6">
                            <label>{{ __('messages.free_meals.form.description_ar') }}</label>
                            <textarea class="form-control area" id="description_ar" name="description_ar" placeholder="{{ __('messages.free_meals.placeholder.description_ar') }}" style="text-align:right !important"></textarea>
                            <label class="description_ar_error"></label>
                        </div> 
                        <div class="col-lg-12">
                            <div class="row 5">
                                <div class="col-md-12 text-md-left">
                                    <button type="submit"  class="btn btn-info waves-effect waves-light save_page">
                                        {{ __('messages.form.save') }}
                                    </button>
                                    
                                <a href="{{route('free_meal_loyalty')}}" class="btn btn-default reset_style">{{ __('messages.form.cancel') }}</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

{{-- End Popup --}}

@endsection
@push('css')
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<style>
    .reset_style {
        margin-left: 15px;
    }
    .search_wid{
        width: 234px;
    }
   
    .btn-fea{
        background-color: #87b23e;
    }
    .validation
    {
      color: red;
     
    }
</style>

@endpush
@push('scripts')
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<script>
    $('input[name="expire_in"]').daterangepicker({
    "singleDatePicker": true,
    // "autoUpdateInput": false,
    "autoApply": true,
    'changeMonth': true,
    'changeYear': true,
    locale: {
        format: 'DD-M-YYYY'
       
    }
});
</script>
<script>
   
      $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }});


        $('#offer_add_btn').on('click',function(e){
            $('#free-meal-form').trigger("reset")
            $('#exampleModalLabel').html('Add Free Meal Loyalty');
             $("#free-meal-form")[0].reset();
            $('#offer-popup').modal({
                show:true
            })
        })

        
         $('.save_page').on('click',function(e){
            $("#free-meal-form").validate({
                ignore: [],
                rules: {
                    title_en  : {        
                        required: true,         
                    },
                    title_ar  : {        
                        required: true,         
                    },
                    menu_item_id  : {        
                        required: true,         
                    },
                    expire_in  : {        
                        required: true,         
                    },
                    order  : {        
                        required: true,
                        number:true,         
                    },   
                },
                messages: {               
                    title_en: {
                        required: @json(__('messages.free_meals.validation.title_en') ),     
                    },
                    title_ar: {
                        required: @json(__('messages.free_meals.validation.title_ar') ),     
                    },
                    menu_item_id  : {        
                        required: @json(__('messages.free_meals.validation.menu_item_id') ),         
                    },
                    expire_in  : {        
                        required: @json(__('messages.free_meals.validation.expire_in') ),         
                    },
                    order  : {        
                        required: @json(__('messages.free_meals.validation.order') ),
                        number: @json(__('messages.free_meals.validation.order1') ),         
                    },
                },
                errorPlacement: function(error, element) {
                    if (element.attr("name") == "title_en" ) {
                        $(".title_en_error").html(error);
                    }
                    if (element.attr("name") == "title_ar" ) {
                        $(".title_ar_error").html(error);
                    }
                    if (element.attr("name") == "menu_item_id" ) {
                        $(".menu_item_id_error").html(error);
                    }
                    if (element.attr("name") == "expire_in" ) {
                        $(".expire_in_error").html(error);
                    }
                    if (element.attr("name") == "order" ) {
                        $(".order_error").html(error);
                    }
                    
                },
                 submitHandler: function(form) {
                    $('.loading_box').show();
                    $('.loading_box_overlay').show();
                    let edit_val=$('#id_pg').val();
                    $('button:submit').attr('disabled', true);
                    if(edit_val){
                    $.ajax({
                        type:"POST",
                        url: "{{route('free_meal_loyalty.update')}}",
                        data: $('#free-meal-form').serialize(),
                        dataType: "json",
                        success: function(data) {
                            if (data.status == 1) {
                                Toast.fire({
                                    icon: 'success',
                                    title: data.message
                                });
                                window.setTimeout(function() {
                                    window.location.reload();
                                }, 1000);
                                $("#free-meal-form")[0].reset();
                            } else {
                                $('.loading_box').hide();
                                $('.loading_box_overlay').hide();
                                Toast.fire({
                                    icon: 'error',
                                    title: data.message
                                });
                            }
                        }      
                    });
                }else{
                    $.ajax({
                        type:"POST",
                        url: "{{route('free_meal_loyalty.store')}}",
                        data: $('#free-meal-form').serialize(),
                        dataType: "json",
                        success: function(data) {
                            if (data.status == 1) {
                                Toast.fire({
                                    icon: 'success',
                                    title: data.message
                                });
                                window.setTimeout(function() {
                                    window.location.reload();
                                }, 1000);
                                $("#free-meal-form")[0].reset();
                            } else {
                                $('.loading_box').hide();
                                $('.loading_box_overlay').hide();
                                Toast.fire({
                                    icon: 'error',
                                    title: data.message
                                });
                            }
                        }
                    }) ;
                    }
                
                }
            });
            
           
        });
         $('.offer_edit').on('click',function(e){
            e.preventDefault();
            $('#exampleModalLabel').html('Edit Offer');

            page = $(this).data('id')
            var url = "free_meal_loyalty/edit/";
        
            $.get(url  + page, function (data) {
                console.log(data);
                $('#title_en').val(data.fee_meal.lang[0].name);
                $('#title_ar').val(data.fee_meal.lang[1].name);
                $('#description_en').val(data.fee_meal.lang[0].description);
                $('#description_ar').val(data.fee_meal.lang[1].description); 
                $('#menu_item_id').val(data.fee_meal.menu_item_id);
                $('#expire_in').val(formatDate(data.fee_meal.expire_in));
                $('#order').val(data.fee_meal.order);
                $('#id_pg').val(data.fee_meal.id)
                $('#offer-popup').modal({
                    show: true
                });
            }) 
        })

       
        $('.change-status').on('click', function(e) {
            var id = $(this).data('id');
            var act_value = $(this).data('status');
        
            $.confirm({
                title: act_value + ' Free Meal',
                content: 'Are you sure to ' + act_value + ' the free meal?',
                buttons: {
                    Yes: function() {
            $.ajax({
                type: "POST",
                url: "{{route('free_meal_loyalty.status.update')}}",
                data: {
                    id: id,
                    status: act_value
                },

                success: function(data) {
                                if (data.status == 1) {
                                    Toast.fire({
                                        icon: 'success',
                                        title: data.message
                                    });
                                    window.setTimeout(function() {
                                        window.location.href = '{{route("free_meal_loyalty")}}';
                                    }, 1000);

                                } else {
                                    Toast.fire({
                                        icon: 'error',
                                        title: data.message
                                    });
                                }
                            }
                        });
                    },
                    No: function() {
                        window.location.reload();
                    }
                }
            });
        });

        function deleteUser(id) {
            $.confirm({
                title: false,
                content: 'Are you sure to delete this free meal? <br><br>You wont be able to revert this',
                buttons: {
                    Yes: function() {
                        $.ajax({
                            type: "POST",
                            url: "{{route('free_meal_loyalty.delete')}}",
                            data: {
                                id: id
                            },
                            dataType: "json",
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            success: function(data) {
                                if (data.status == 1) {
                                    window.setTimeout(function() {
                                        window.location.href = '{{route("free_meal_loyalty")}}';
                                    }, 1000);
                                    Toast.fire({
                                        icon: 'success',
                                        title: data.message
                                    });
                                } else {
                                    Toast.fire({
                                        icon: 'error',
                                        title: data.message
                                    });
                                }
                            }
                        });
                    },
                    No: function() {
                        console.log('cancelled');
                    }
                }
            });
        }

        
    function formatDate(date) {
        var d = new Date(date),
            month = '' + (d.getMonth() + 1),
            day = '' + d.getDate(),
            year = d.getFullYear();

        if (month.length < 2) 
            month = '0' + month;
        if (day.length < 2) 
            day = '0' + day;

        return [day, month, year].join('-');
    }
    </script>
@endpush