@extends('layouts.master')

@section('content-title')
{{ __('messages.orders.order') }}
@endsection
@section('add-btn')

@endsection
@section('content')
<div class="row">
    <div class="col-sm-9">
        <x-order-tab activeTab="delivered_order" />
    </div> 
    <div class="col-sm-3">
        <a href="{{ route('order.excel-download.delivered') }}" class="btn btn-success" style="float:right">
            {{ __('messages.orders.dwld_reports') }}
        </a>
       
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <form id="posts-filter" method="get" action="{{ route('delivered_order') }}">
                    <div class="row tablenav top text-left">
                        <div class="col-md-4 ml-0">
                            <input class="form-control" type="text" name="search" value="{{$search ?? ''}}" placeholder="{{ __('messages.orders.placeholder.by_order_id') }}">
                        </div>
                        <div class="col-md-4 ml-0">
                            <select  class="select2 form-control" id="customer" name="customer" placeholder ="{{ __('messages.orders.placeholder.by_customer') }}">
                                <option value="">By Customer</option>
                                @foreach($customer as $customers)
                                    <option  value="{{ $customers->id }}" {{ $customers->id == $cust_id ? "selected" :""}}>{{$customers->name ? $customers->name : $customers->phone_number}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-4 ml-0">
                            
                        </div>
                    </div>
                    <div class="row tablenav top text-left">
                        
                        <div class="col-md-4 ml-0">
                            <div class='input-group date' id='datepicker'>
                                <input class="form-control" type="text" value= "{{ $date ?? ''}}"  name="order_date" id="order_date" placeholder="{{ __('messages.orders.placeholder.by_date') }}" autocomplete="off">
                                <span class="input-group-addon">
                                    <span class="fa fa-calendar icon-style"></span>
                                </span>
                            </div>
                            <label class="expire_in_error"></label>
                        </div>
                        
                        <div class="col-md-4 text-left">    
                            <button type="submit" class="btn btn-info">
                                <font style="vertical-align: inherit;">{{ __('messages.search') }}</font>
                            </button>
                            <a href="{{ route('delivered_order') }}" class="btn btn-default reset_style">{{ __('messages.reset') }}</a>
                        </div>
                    </div>
                    <div class="row tablenav top text-left">
                        
                    </div>
                </form>
            </div>
        </div>
                         
        <div class="card">
            <div class="card-body">
                <div id="msgDiv"></div>
                <div class="table-responsive">
                    <table class="table table-bordered table-striped">
                        <thead>
                        
                            <tr>
                                <th>{{ __('messages.orders.form.sl_no') }}</th>
                                <th>{{ __('messages.orders.form.order_id') }}</th>
                                <th>{{ __('messages.orders.form.customer') }}</th>
                                <th>{{ __('messages.orders.form.order_date') }}</th>
                                <th>{{ __('messages.orders.form.amount') }}</th>
                                <th>{{ __('messages.orders.form.payment_type') }}</th>
                                <th width="10%"></th>
                            </tr>
                        </thead>
                        <tbody>
                            @if(count($orders) > 0 )
                            @php
                            $i = 1;
                            @endphp
                            @foreach ($orders as $item)
                            <tr> 
                                <td>{{$i++}}</td>
                                <td>{{$item->order_id}}</td>
                                <td>{{$item->customer[0]->name ?? ''}}</td>
                                <td>{{ \Carbon\Carbon::parse($item->created_at)->format('d-m-Y')}}</td>
                                <td>SAR {{$item->grand_total ?? '0.00'}}</td>
                                {{-- <td>{{$order_status[$item->order_status]}}</td> --}}
                                 <td>{{$item->payment_mode}}</td>
                                <td class="text-center">
                                   <a class="btn btn-sm btn-success text-white view_btn" title="View Order Details" data-id="{{ $item->id }}"><i class="fa fa-eye"></i></a>
                                </td>
                            </tr>
                            @endforeach
                                
                            @else
                            <tr>
                                <td colspan="8" class="text-center">No records found!</td>
                            </tr>

                            @endif
                        </tbody>
                    </table>
                </div>
                <div class="text-center d-flex justify-content-center mt-3">
                    {{ $orders->links() }}
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Modal pooup -->
<div class="modal none-border" id="formModal" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-lg" style="max-height:85%;  margin-top: 50px; margin-bottom:50px;max-width: 100%">
        <div class="modal-content">

        </div>
    </div>
</div>
<!-- END MODAL -->
@endsection
@push('css')
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />

@endpush
@push('scripts')
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<script>
     $('input[name="order_date"]').daterangepicker({
    "singleDatePicker": true,
    "autoUpdateInput": false,
    "autoApply": true,
    locale: {
        format: 'DD-M-YYYY'
       
    }
});
$('input[name="order_date"]').on('apply.daterangepicker', function (ev, picker) {
    $(this).val(picker.startDate.format('DD-MM-YYYY'));
});
$('.view_btn').on('click', function() {
    var id = $(this).data("id");
    $.ajax({
    type: "GET",
            url: "order/delivered_order_details/" + id,
            success: function(data) {
            $('.modal-content').html(data);
            $('#formModal').modal('show');
            }
    });
    });
</script>   
@endpush