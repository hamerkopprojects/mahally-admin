<form id="frm_create_product" action="javascript:;" method="POST">
    <div class="tab-pane active" id="order_info" role="tabpanel">
        <div class="modal-body">
            @if(isset($reviews['rating']))
            <div class="row">
                <div class="col-md-8">
                </div>
                <div class="col-md-4 review_status">
                      <button type="" class="btn btn-info waves-effect waves-light review_status_btn" value="approve" data-id="{{$reviews['id']}}">APPROVE</button>
                      <button type="" class="btn btn-default  waves-effect waves-light review_status_btn" value="reject" data-id="{{$reviews['id']}}">REJECT</button>
                </div>
                <div class="col-md-2">
                   <button type="" class="btn btn-info rating_btn">{{$reviews['rating']}}</button>
                </div>
                <div class="col-md-10">
                    <p>
                         @for($i=0;$i<$reviews['rating'];$i++)
                                    <i class="fa fa-star" aria-hidden="true"></i>
                        @endfor
                    </p>
                    <p>
                        {{$reviews->customer[0]['name'] ?? ''}}
                    </p>
                    <p>
                        {{$reviews['comment']}}
                    </p>
                </div> 
            </div>
            <div class="row">
                @if(isset($review_segments))
                @foreach ($review_segments as $item)
                <div class="col-md-2">
                   {{$item->segment['lang']['0']['name']}}
                </div> 
                 <div class="col-md-2">
                  
                </div>
                <div class="col-md-8">
                    @for($i=0;$i<$item->rating;$i++)
                        <i class="fa fa-star" aria-hidden="true"></i>
                    @endfor
                </div>   
                @endforeach
                @endif
            </div>
        </div>
         @else 
         <div class="row">
            <div class="col-md-4"></div>
            <div class="col-md-4 text-center"> No Reviews</div>
            <div class="col-md-4"></div>
        </div>
        @endif
        <div class="modal-footer">
           
        </div>
    </div>
</form>

<style>

label.control-label {
    font-size: 16px;
    color: #373757;
}.review_status{
    text-align: right;
}.rating_btn{
   width: 100px;
    height: 50px;
}
    </style>
<script>
     $('.review_status_btn').on('click', function () {    
        var status = $(this).val();
        var review_id = $(this).data("id");
    
        $.ajax({
            type: "POST",
            url: "{{route('review_status_update')}}",
            data: { status: status,review_id:review_id},
            success: function (data) {
                if (data.status == 1) {
                    Toast.fire({
                        icon: 'success',
                        title: data.message
                    });
                } else {
                    Toast.fire({
                        icon: 'error',
                        title: data.message
                    });
                }
            }
        });
    });
</script>