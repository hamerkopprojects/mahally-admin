<div class="modal-header">
    <h4 class="modal-title"><strong>{{ __('messages.orders.order_info.order_details') }}</strong></h4>
    <a href="{{route('order_invoice',$order['id'])}}"class="btn btn-sm btn-danger waves-effect" title="print order details"  target="_blank" style="     margin-right: -500px;"><i class="fa fa-print"></i></a>
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
</div>
<div class="col-sm-12">
    <ul class="nav nav-tabs" role="tablist" id="pdt_tab">
        <li class="nav-item"> <a class="nav-link active" data-id="{{$order['id']}}" href="#order_info" role="tab" data-toggle="tab"><span class="hidden-sm-up"><i class="ti-shopping-cart"></i></span> <span class="hidden-xs-down">ORDER INFO</span></a> </li>
        <li class="nav-item disabled"> <a class="nav-link" href="#reviews" data-id="{{$order['id']}}" role="tab" data-toggle="tab"><span class="hidden-sm-up"><i class="ti-folder"></i></span> <span class="hidden-xs-down">REVIEWS</span></a> </li>
    </ul>
</div>
<div class="tab-content">
    @include('restaurant.orders.views.basic_details')
</div>


<style>

label.control-label {
    font-size: 16px;
    color: #373757;
}
.row ,.modal-header,.nav-tabs{
    margin-left: 50.5px;
    margin-right: 30.5px;
}
 .btn-danger {
    background: #612467;
    border-color: #612467;
    -webkit-transition: all 0.3s ease;
    -ms-transition: all 0.3s ease;
    transition: all 0.3s ease;
    font-size: 30px;
}   
</style>

<script>
     $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }});
    $('.nav-tabs a').on('click', function (e) {
        e.preventDefault()
        $(this).tab('show')
    });
    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e)
    {    var id = $(this).data("id");
        var x = $(e.target).text();
        $.ajax({
            type: "GET",
            url: "{{route('orderview_tabs')}}",
            data: {'activeTab': x,
            id:id
                   },
            success: function (result) {
                $('.tab-content').html(result);
            }
        });
    });
 
 </script>