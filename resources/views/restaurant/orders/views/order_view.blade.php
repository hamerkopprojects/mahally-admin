<div class="modal-header">
    <h4 class="modal-title"><strong>{{ __('messages.orders.order_info.order_details') }}</strong></h4>
    <button type="" class="btn btn-default  waves-effect waves-light status_btn" value="{{$order->order_status + 1}}" data-id="{{$order['id']}}">{{ $change_order_status[$order->order_status + 1] }}</button>
    <a href="{{route('order_invoice',$order['id'])}}"class="btn btn-sm btn-danger waves-effect" title="print order details"  target="_blank"><i class="fa fa-print"></i></a>
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
     
</div>

  <form id="frm_create_product" action="javascript:;" method="POST">
    <div class="tab-pane active" id="pdt_info" role="tabpanel">
        <div class="modal-body">
            <div class="row">
                <div class="col-md-4">
                    <label class="control-label">{{ __('messages.orders.order_info.order_info') }}</label>
                    <br />
                </div>
                <div class="col-md-8">
                </div>
                <div class="col-md-4">
                    <label class="control-label">{{ __('messages.orders.order_info.order_id') }}</label><br>
                      {{ $order['order_id'] }}
                </div>
                <div class="col-md-4">
                    <label class="control-label">{{ __('messages.orders.order_info.order_date') }} </label><br>
                    {{-- {{ $product[0]['lang'][1]['name'] }} --}}
                     {{ $order['created_at'] }}
                </div>
                <div class="col-md-4">
                    <label class="control-label">{{ __('messages.orders.order_info.payment_type') }} </label><br>
                   {{ $order['payment_mode'] }}
                </div>
                <div class="col-md-4">
                    <label class="control-label">{{ __('messages.orders.order_info.customer') }} </label><br>
                     {{ $order->customer[0]->name }}
                  
                </div>
                <div class="col-md-4">
                    <label class="control-label">{{ __('messages.orders.order_info.status') }}</label><br/>
                     {{ $order_status[$order->order_status] }}
                        
                </div>
                <div class="col-md-4"></div>
            </div>
            <div class="tbl-bd" style="border-style: groove;margin: 50px">
                <div class="row">   
                    <div class="col-md-4">
                        <label class="control-label">{{ __('messages.orders.order_info.item') }} </label><br> 
                    </div>
                    <div class="col-md-4">
                        <label class="control-label">{{ __('messages.orders.order_info.quantity') }}</label><br>   
                    </div>
                    <div class="col-md-4">
                        <label class="control-label">{{ __('messages.orders.order_info.price') }}</label><br>   
                    </div>
                    @php 
                        $total = 0;
                        $discount = 0;
                    @endphp
                    @foreach ($order->order_item as $item)
                        @if($item->menu_items)
                        <div class="col-md-4">
                            {{$item->menu_items->lang[0]->name}}
                        </div>
                        <div class="col-md-4">
                            {{$item->item_count}}
                        </div>
                        <div class="col-md-4">
                            {{$item->menu_items->price}}
                            @if($item->menu_items->price)
                            @php 
                                    $total =  $total+ ($item->item_count * $item->menu_items->price);
                            @endphp
                            @endif
                            @if($item->menu_items->discount_price)
                            @php 
                                    $discount =  $discount+ ($item->item_count *$item->menu_items->discount_price);
                            @endphp
                            @endif
                        </div>
                        @endif
                        @if($order->order_addons)
                        @foreach ($order->order_addons as $add_ons)
                        <div class="col-md-4">
                            <span style="padding-left: 25px;">{{$add_ons->lang[0]->name ?? ''}}</span>
                        </div>
                        <div class="col-md-4">
                            {{$add_ons->pivot->item_count ?? ''}}
                        </div>
                        <div class="col-md-4">
                            {{$add_ons->price}}
                            @if($add_ons->price)
                            @php 
                                    $total =  $total+ ($add_ons->price * $add_ons->pivot->item_count);
                            @endphp
                            @endif
                        </div>
                        @endforeach
                        @endif
                    @endforeach
                </div><br><br>
                <div class="row">
                    <div class="col-md-4"></div>
                    <div class="col-md-4">{{ __('messages.orders.order_info.subtotal') }}</div>
                    <div class="col-md-4">SAR {{$total }}</div>
                    <div class="col-md-4"></div>
                    <div class="col-md-4">{{ __('messages.orders.order_info.discount') }}</div>
                    <div class="col-md-4">SAR {{$discount }}</div>
                </div> 
                <div class="row">
                    <div class="col-md-3"></div>
                    <div class="col-md-7">
                        <hr>
                    </div>
                    <div class="col-md-2"></div>
                    <div class="col-md-4"></div>
                    <div class="col-md-4">{{ __('messages.orders.order_info.total_amt') }}</div>
                    <div class="col-md-4">SAR {{$total-$discount }}</div>
                    
                </div>  
            </div>  
        </div>
        <div class="modal-footer">
           
        </div>
    </div>
</form>

<style>

label.control-label {
    font-size: 16px;
    color: #373757;
}
.row ,.modal-header{
    margin-left: 50.5px;
    margin-right: 30.5px;
}.status_btn{
  margin-left: 300px; 
  background: #612467;
  border-color: #612467; 
}
.btn-danger {
    background: #612467;
    border-color: #612467;
    -webkit-transition: all 0.3s ease;
    -ms-transition: all 0.3s ease;
    transition: all 0.3s ease;
    font-size: 30px;
}
</style>
<script>
     $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }});

    $('.status_btn').on('click', function(e) {

        var status = $(this).val();
        var order_id = $(this).data("id");
     
        $.confirm({
            title: 'Order Status',
            content: 'Are you sure to change status?',
            buttons: {
                Yes: function() {
        $.ajax({
            type: "POST",
            url: "{{route('order_status_update')}}",
            data: {
                id: order_id,
                status: status
            },

            success: function(data) {
                            if (data.status == 1) {
                                Toast.fire({
                                    icon: 'success',
                                    title: data.message
                                });
                                window.setTimeout(function() {
                                    window.location.href = '{{route("active_orders")}}';
                                }, 1000);

                            } else {
                                Toast.fire({
                                    icon: 'error',
                                    title: data.message
                                });
                            }
                        }
                    });
                },
                No: function() {
                    window.location.reload();
                }
            }
        });
    });
</script>
