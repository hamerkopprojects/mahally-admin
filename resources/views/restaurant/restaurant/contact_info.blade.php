<form id="frm_create_contact_info" action="javascript:;" method="POST">
    <div class="tab-pane active" id="pdt_info" role="tabpanel">
        <div class="modal-body">
            
            @if(!empty($contact_details) && count($contact_details) >= 1)
                <div class="row">
                    <div class="col-md-3"><label class="control-label">{{ __('messages.edit_details.form.contact_person') }}<span class="text-danger">*</span></label></div>
                    <div class="col-md-3"> <label class="control-label">{{ __('messages.edit_details.form.designation') }} <span class="text-danger">*</span></label></div>
                    <div class="col-md-3"> <label class="control-label">{{ __('messages.edit_details.form.phone') }} <span class="text-danger">*</span></label></div>
                    <div class="col-md-3"> <label class="control-label">{{ __('messages.edit_details.form.email') }} <span class="text-danger">*</span></label></div>
                    @php
                      $i = 0;
                    @endphp
                    <div class="cls_outer_div1 mb-3">
                    @foreach($contact_details as $row_data)
                        @php
                        $i++;
                        @endphp
                         
                            <div class="row ml-0 cls_content_var_div{{$i}}">
                                <div class="col-md-11">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <input type="hidden" name="contact_id[]" value="{{ $row_data->id }}">
                                            <input class="form-control form-white" placeholder="Enter name" type="text" name="contact_person[]" value="{{ $row_data->contact_person ?? '' }}" id="cnt_psn_{{$i}}"/>
                                        </div>
                                        <div class="col-md-3">
                                            <input class="form-control form-white" placeholder="Enter designation" type="text" name="designation[]" value="{{ $row_data->designation ?? '' }}" id="designation_{{$i}}"/>
                                        </div>
                                        <div class="col-md-3">
                                            <input class="form-control form-white" placeholder="Enter phone number" type="text" name="phone[]" value="{{ $row_data->phone ?? '' }}" id="phone_{{$i}}"/>
                                        </div>
                                        <div class="col-md-3">
                                            <input class="form-control form-white" placeholder="Enter email address" type="text" name="email[]" value="{{ $row_data->email ?? '' }}" id="email_{{$i}}"/>
                                        </div>
                                    </div>
                                </div>
                                @if($i == 1)
                                <div class="col-md-1 text-left">
                                    <a class="btn btn-sm btn-success text-white add_price_btn_simple" title="Add New" data-id="{{$i}}" data-outer-id="{{$i}}" data-var_id="{{$row_data->id}}" id="add_contact"><i class="fa fa-plus"></i></a>
                                </div>
                                @else
                                    <div class="col-md-1 text-left">
                                        <button class="btn btn-sm btn-danger text-white btn_remove" title="Remove Attributes"><i class="fa fa-minus"></i></button>
                                    </div>
                                @endif
                            </div>
                        
                    @endforeach
                   
                </div>  
            @else
                <div class="cls_outer_div1 mb-3">
                    <div class="row ml-0 cls_content_div1">
                        <div class="row">
                            <div class="col-md-11">
                                <div class="row">
                                    <div class="col-md-3">
                                        <label class="control-label">{{ __('messages.edit_details.form.contact_person') }} <span class="text-danger">*</span></label>
                                        <input class="form-control form-white" placeholder="Enter name" name="contact_person[]" value="" id="cnt_psn_1"/>
                                    </div>
                                    <div class="col-md-3">
                                        <label class="control-label">{{ __('messages.edit_details.form.designation') }} <span class="text-danger">*</span></label>
                                        <input class="form-control form-white" placeholder="Enter designation" type="text" name="designation[]" value="" id="designation_1"/>
                                    </div>
                                    <div class="col-md-3">
                                        <label class="control-label">{{ __('messages.edit_details.form.phone') }} <span class="text-danger">*</span></label>
                                        <input class="form-control form-white" placeholder="Enter phone number" type="text" name="phone[]" value="" id="phone_1"/>
                                    </div>
                                    <div class="col-md-3">
                                        <label class="control-label">{{ __('messages.edit_details.form.email') }} <span class="text-danger">*</span></label>
                                        <input class="form-control form-white" placeholder="Enter email address" type="text" name="email[]" value="" id="email_1"/>
                                    </div>
                                </div>
                            </div>  
                            <div class="col-md-1 text-left">
                                <label class="control-label"></label><br/>
                                <a class="btn btn-sm btn-success text-white mt-3 add_price_btn_simple" data-id="1" title="Add New" id="add_price_simple"><i class="fa fa-plus"></i></a>
                            </div>     
                        </div>       
                    </div>
                </div>
            @endif

        </div>
        <div class="modal-footer">
            <input type="hidden" id="rest_id" name="rest_id" value="{{ $rest_id }}">
            <input type="hidden" id="submit_action" value="" />
            @if(!empty($rest_id))
            <button type="submit" class="btn btn-info waves-effect waves-light save-btn">
                {{ __('messages.form.save') }}
            </button>
            @endif
            <button type="submit" class="btn btn-info waves-effect waves-light save-and-continue">
                {{ __('messages.form.save_and_continue') }}
            </button>
            <a class="btn btn-default waves-effect tab_back" data-id='{{ $rest_id }}' href="javascript:void(0);">{{ __('messages.form.back') }}</a>
        </div>
    </div>
</form>
<script>
    $(".save-and-continue").on('click', function () {
        $("#submit_action").val('continue');
    });
    $(".save-btn").on('click', function () {
        $("#submit_action").val('save');
    });

    $(".add_price_btn_simple").click(function () {
        var cnt = $(this).attr("data-id");
        var div_cnt = cnt + 1;
        $(".cls_outer_div1").append('<div class="row ml-1 cls_content_div' + div_cnt + '"><div class="row"><div class="col-md-11"> <div class="row"><div class="col-md-3">' +
                '<input class="form-control form-white" placeholder="Enter name" type="text" name="contact_person[]" value="" id="cnt_psn_' + div_cnt + '"/></div>' +
                '<div class="col-md-3"><input class="form-control form-white" placeholder="Enter designation" type="text" name="designation[]" value="" id="designation_' + div_cnt + '"/></div>' +
                '<div class="col-md-3"><input class="form-control form-white" placeholder="Enter phone number" type="text" name="phone[]" value="" id="phone_' + div_cnt + '"/></div>' +
                '<div class="col-md-3"><input class="form-control form-white" placeholder="Enter email address" type="text" name="email[]" value="" id="email_' + div_cnt + '"/></div></div></div>' +
                '<div class="col-md-1 text-left pl-2 pb-1"><button class="btn btn-sm btn-danger text-white btn_remove" title="Remove Attributes">' +
                '<i class="fa fa-minus"></i></button></div></div></div>');
        cnt++;
        $('#add_price_simple').attr('data-id', cnt);
    });
    $('body').on('click', '.btn_remove', function () {
        $(this).parent().parent().remove();
//        cnt--;
    });




    $('.tab_back').on('click', function () {
          $("#frm_create_contact_info")[0].reset();
        var rest_id = $(this).data("id");
        $.ajax({
            type: "GET",
            url: "{{route('rest_details_tabs')}}",
            data: {'activeTab': 'BASIC INFO', rest_id: rest_id},
            success: function (result) {
                $('#rest_tab a[href="#basic_info"]').tab('show');
                $('.tab-content').html(result);
            }
        });
    });
    $("#frm_create_contact_info").validate({
        normalizer: function (value) {
            return $.trim(value);
        },
        rules: {
            "contact_person[]": {
                required: true,
            },
            "designation[]": {
                required: true,
            },
            "phone[]": {
                required: true,
                number:true,
                minlength: 6, 
                maxlength: 12
            },
            "email[]": {
                required: true,
                email: true
            },
        },
        messages: {
            "contact_person[]": {
                required: 'Contact person is required',
            },
            "designation[]": {
                required: 'Designation is required',
            },
            "phone[]": {
                required: 'Phone number is required',
                number:'Invalid phone number'
            },
            "email[]": {
                required: "Email is required",
                email: "Invalid mail format",
            },
        },
        submitHandler: function (form) {
            $('.loading_box').show();
            $('.loading_box_overlay').show();
            $.ajax({
                type: "POST",
                url: "{{route('save_contact_info_details')}}",
                data: $('#frm_create_contact_info').serialize(),
                dataType: "json",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function (data) {
                    if (data.status == 1) {
                        if ($("#submit_action").val() == 'continue') {
                            Toast.fire({
                                icon: 'success',
                                title: data.message
                            });
                            $('#rest_tab a[href="#dmt_photo"]').tab('show');
                            $('.tab-content').html(data.result);
                        } else {
                            Toast.fire({
                                icon: 'success',
                                title: data.message
                            });
                        }
                    } else {
                        
                        Toast.fire({
                            icon: 'error',
                            title: data.message
                        });
                    }
                    $('.loading_box').hide();
                    $('.loading_box_overlay').hide();
                    $('button:submit').attr('disabled', false);
                },
                error: function (err) {
                    $('.loading_box').hide();
                    $('.loading_box_overlay').hide();
                    $('button:submit').attr('disabled', false);
                }
            });
            return false;
        }
    });

</script>