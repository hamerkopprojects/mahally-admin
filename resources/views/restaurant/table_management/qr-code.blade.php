<html>
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
   </head>
   <body>
       <table width="60%" border="0" cellspacing="0" cellpadding="1" align="center">
        <tr>
            <td style="border: 1px solid rgb(98, 19, 117);padding-bottom: 10px">

                <table class="center" bgcolor="#ffffff" cellpadding="0"  cellspacing="0" style="margin-right:auto;margin-left:auto;margin-top:auto;">
                    <tr>
                    <td>
                      <table  class="center" cellpadding="1"  cellspacing="0" style="margin-right:auto;margin-left:auto;margin-top:auto;">
          
                          <tbody>
                        <tr>
                            <td style="font-family:'Muli', Arial,sans-serif; font-size:16px; line-height:10px; text-align:center; padding-bottom:10px; padding-top:20px">
                                Powered by
                            </td>
                        </tr>
                         <tr>
                            <td style="text-align:center; padding-bottom:20px;">
                               <img src="{{ asset('assets/images/qrcode/qr_logo.png') }}" width="150">
                           
                            </td>
                          </tr>
                         <tr>
                            <td bgcolor="#F0F0F0" style="text-align:center; padding-bottom:20px; padding-top:20px;">
                              <span style="font-size:16px; font-weight: bold;"> {{$rest_data->lang[0]->name}}</span><br>
                              <span style="font-size:16px;text-decoration: underline">{{$rest_data->location}}</span>
                            </td>
                           
                          </tr>
                          <tr>
                            <td>
                            <h5 style="text-align:center;">Open Mahally app and scan the code to order</h5>
                           
                            </td>
                           
                          </tr>
                         <tr>
                            <td style="text-align:center;">
                               <img src="data:image/png;base64, {!! base64_encode(QrCode::format('png')->size(200)->generate( $tablecode)) !!}" style="padding: 5px;
                              width: 350px;">
                            </td>
                         </tr>
                         <tr>
                            <td bgcolor="#F0F0F0" style="text-align:center; padding-bottom:20px; padding-top:20px;">
                           {{$rest_data->unique_id}} - {{$data->table_number}}
                            </td>
                           
                          </tr>
                          <tr><td style="padding-bottom:20px;"></td></tr>
                      </tbody>
                   </table>
                </td>
                </tr>
                </table>

            </td>   
        </tr>    
    </table>
       <table width="60%" border="0" cellspacing="0" cellpadding="1" align="center" style="margin-top: 20px;">
        <tr>
        <td style=" background:rgb(102, 24, 102);">
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                 <tr>
                    <td style="color:#fff; font-family:'Muli', Arial,sans-serif; font-size:14px; line-height:20px; text-align:center; padding-bottom:10px;">
                       <multiline>To download app, Visit <a href=""><span class="link" style="color:#a5a74e; text-decoration:none;">www.mahallyapp.com</span></a></multiline>
                    </td>
                 </tr>
                 <tr>
                     <td style="line-height:20px; text-align:center; padding-bottom:10px;">
                        <img src="{{ asset('assets/images/qrcode/footer_icon.png') }}" width="150" height="20">
                     </td>
                 </tr>
              </table>
           </td>
        </tr>
     </table>  
   
   </body>
</html>